<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

/* Admin Auth Route*/
Route::get('admin/', 'LoginController@index');
Route::get('admin/login', 'LoginController@index')->name('admin.login'); 
Route::post('admin/login', 'LoginController@authCheck')->name('admin.login');
Route::get('admin/logout', 'LoginController@logout')->name('admin.logout');


Route::group(['middleware' => 'auth:Admin',  'prefix' => 'admin'], function(){

	Route::get('/dashboard','Admin\DashboardController@index')->name('admin.dashboard');

	Route::resource('employee','Admin\EmployeeController');
	Route::resource('admin-influencer','Admin\InfluencerController');
  Route::get('approve-reject/{id}/{flag}', 'Admin\InfluencerController@approve_reject')->name('admin.approve_reject');
  Route::post('/admin-influencer-list','Admin\InfluencerController@influencer_list')->name('admin.influencer_list');


	Route::resource('admin-shop','Admin\ShopController');
  Route::post('/admin-shop-list','Admin\ShopController@shop_list')->name('admin.shop_list');

  Route::resource('admin-category','Admin\CategoryController');  

  Route::resource('admin-users','Admin\UserController');
  Route::get('/paying-users','Admin\UserController@index')->name('admin.paying_users');
  Route::get('/paid-users','Admin\UserController@paid_users')->name('admin.paid_users');

  Route::get('/assign-shop-into-category','Admin\ShopController@assign_shop_into_category')->name('admin.assign_shop_into_category');

  Route::get('/setting','Admin\DashboardController@setting')->name('admin.setting');
  Route::post('/setting-update','Admin\DashboardController@setting_update')->name('admin.setting_update');

  Route::resource('admin-notification','Admin\NotificationController');

  Route::resource('admin-country','Admin\CountryController');

  Route::get('/import-influencer','Admin\InfluencerController@import_influencer')->name('admin.import_influencer');
  Route::post('/import-influencer','Admin\InfluencerController@import_influencer_store')->name('admin.import_influencer_store');
});


/* User Auth Route*/
Route::get('/', 'LoginController@index');
Route::get('user/login', 'LoginController@index')->name('user.login'); 
Route::post('user/login', 'LoginController@authCheck')->name('user.login');
Route::get('user/logout', 'LoginController@logout')->name('user.logout'); 

Route::get('user/signup', 'SignupController@index')->name('user.signup');
Route::post('user/signup', 'SignupController@signup')->name('user.signup');
Route::post('user/payment-request', 'SignupController@stripe_payment_request')->name('user.stripe_payment_request');

Route::get('user/is-email-already-exists-user', 'SignupController@is_email_already_exists_user')->name('user.is_email_already_exists_user');

Route::group(['middleware' => 'auth:User',  'prefix' => 'user'], function(){

    Route::get('/dashboard','User\DashboardController@index')->name('user.dashboard');
    Route::get('/profile','User\ProfileController@index')->name('user.profile');
    Route::post('/update-email','User\ProfileController@update_email')->name('user.update_email');
    Route::post('/update-password','User\ProfileController@update_password')->name('user.update_password');
    Route::get('/check-old-password','User\ProfileController@check_old_password')->name('user.check_old_password');

    Route::get('/cancel-subscription','User\ProfileController@cancel_subscription')->name('user.cancel_subscription');

    Route::resource('shop','User\ShopController');
    
    Route::resource('trend','User\TrendController');

    Route::resource('influencer','User\InfluencerController');
    Route::get('user/influencer-favourite', 'User\InfluencerController@influencer_favourite')->name('user.influencer_favourite'); 

    Route::resource('live-now','User\LivePromotionController');

    Route::resource('influencer-list','User\InfluencerListController');
    Route::get('assign-influencer/delete', 'User\InfluencerListController@delete')->name('user.influencer_list.delete');
    Route::post('assign-influencer/update', 'User\InfluencerListController@update')->name('user.influencer_list.update');
    Route::get('favourite-influencer-list', 'User\InfluencerListController@favourite_influencer_list')->name('user.favourite_influencer_list'); 

    Route::get('user/assign-influencer', 'User\InfluencerListController@assign_influencer')->name('user.assign_influencer'); 

    Route::resource('shop-list','User\ShopListController');
    Route::get('assign-shop/delete', 'User\ShopListController@delete')->name('user.shop_list.delete');
    Route::post('assign-shop/update', 'User\ShopListController@update')->name('user.shop_list.update');

    Route::get('favourite-shop-list', 'User\ShopListController@favourite_shop_list')->name('user.favourite_shop_list');

    Route::get('user/assign-shop', 'User\ShopListController@assign_shop')->name('user.assign_shop'); 

    Route::resource('media-list','User\MediaListController');
    Route::get('assign-media/delete', 'User\MediaListController@delete')->name('user.media_list.delete');
    Route::post('assign-media/update', 'User\MediaListController@update')->name('user.media_list.update');

    Route::get('user/assign-media', 'User\MediaListController@assign_media')->name('user.assign_media'); 

    Route::resource('notification','User\NotificationController');
     
});

Route::get('/instagram-profile','InstagramAPITestController@index');
Route::get('/profile-stories','InstagramAPITestController@profile_stories');
Route::get('/set-influencer-rank','InstagramAPITestController@set_influencer_rank');
Route::get('/shop-video-concat','InstagramAPITestController@shop_video_concat');
Route::get('/sendinblue-contact-status-update','InstagramAPITestController@sendinblue_contact_status_update');


Route::get('/inst-demo-profile','InstagramAPIDemoController@index');
Route::get('/profile-demo-stories','InstagramAPIDemoController@profile_stories');

Route::get('/clear', function() {

   Artisan::call('cache:clear');
   Artisan::call('config:clear');
   Artisan::call('config:cache');
   Artisan::call('view:clear');
   Artisan::call('clear-compiled');

   return "Cleared!";

});
<?php

//declare(strict_types=1);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);


use Instagram\Api;
//use Instagram\Auth\Checkpoint\ImapClient;
use Instagram\Exception\InstagramException;

use Psr\Cache\CacheException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

require realpath(dirname(__FILE__)) . '/../vendor/autoload.php';
$credentials = include_once realpath(dirname(__FILE__)) . '/credentials.php';

// DB connection
$db_config = include_once realpath(dirname(__FILE__)) . '/db_config.php';

$cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');

try {
    $api = new Api($cachePool);
    //$imapClient = new ImapClient($credentials->getImapServer(), $credentials->getImapLogin(), $credentials->getImapPassword());

    $api->login($credentials->getLogin(), $credentials->getPassword(), $imapClient);


    $influencers = mysqli_query($conn, "SELECT * FROM influencer WHERE followers IS NULL AND deleted_at IS NULL");

        while ($influencer = mysqli_fetch_assoc($influencers)) {
        {
            $profile = $api->getProfile($influencer['instagram_id']);
                
                sleep(1);
              // echo "<pre>";
               //print_r($profile);exit;

                /* Start - Engagement  Rate Calculation */
                $followers = empty($profile->getFollowers()) ? 1 : $profile->getFollowers();
                $postEngRate  = 0;
                $cnt = 0;
                $engRate = 0;
                $totalLikes = 0;
                $totalComments = 0;
                $totalVideos = 0;
                $totalImages = 0;
                $minLikes = 0;
                $maxLikes = 0;
                $last3MonthsTotalPosts = 0;
                $last_post_date = !empty($profile->getMedias()) ? $profile->getMedias()[0]->getDate()->format('Y-m-d H:i:s') : NULL;

                if(!empty($profile->getMedias())){

                        foreach ($profile->getMedias() as $key => $post) {
                            $last3MonthsTotalPosts += (strtotime($post->getDate()->format('Y-m-d')) > strtotime('-3 months')) ? 1 : 0;

                            $minLikes = ($minLikes > $post->getLikes()) ? $post->getLikes() : $minLikes;
                            $maxLikes = ($maxLikes > $post->getLikes()) ? $maxLikes : $post->getLikes() ;

                            $totalLikes += $post->getLikes();
                            $totalComments += $post->getComments();
                            $totalVideos += $post->getTypeName() == 'GraphVideo' ? 1 : 0;
                            //$totalImages += $post->getTypeName() == 'GraphImage' ? 1 : 0;

                            $postEngRate  += (($post->getLikes() + $post->getComments()) / $followers) * 100;
                            $cnt++;
                        }
                    
                    $engRate =  $postEngRate  / $cnt;
                }
                
                $cnt = ($cnt == 0) ? 1 : $cnt;

                $uploads = $profile->getMediaCount();
                $avg_likes= round($totalLikes / $cnt);
                $avg_comments= round($totalComments / $cnt);
                $videos = round(($totalVideos / $cnt) * 100);
                $photos = 100 - $videos;
                $likes_consistency = $minLikes;
                $comments_consistency = $maxLikes;
                $frequency = !empty($last3MonthsTotalPosts) ? round(($last3MonthsTotalPosts / 12)) : 0;
            
                // For save profile pic
                $img = ('../../profile_pic/').$influencer['instagram_id'].'.jpg';
                file_put_contents($img, file_get_contents($profile->getProfilePicture()));
                file_put_contents(('../../profile_pic/thumb/').$influencer['instagram_id'].'.jpg', file_get_contents($profile->getProfilePicture()));

                $is_verified = $profile->isVerified() ? 1 : 0;
                mysqli_query($conn, "UPDATE influencer SET profile_pic='".$influencer['instagram_id'].".jpg', followers='".$profile->getFollowers()."', engagement_rate='".$engRate."', uploads='".$uploads."', avg_likes='".$avg_likes."', avg_comments='".$avg_comments."', videos='".$videos."', photos='".$photos."', last_post_date='".$last_post_date."', likes_consistency='".$likes_consistency."', comments_consistency='".$comments_consistency."', frequency='".$frequency."', is_verified='".$is_verified."' WHERE instagram_id='".$influencer['instagram_id']."'");
        }   

    }

} catch (InstagramException $e) {
    print_r($e->getMessage());
} catch (CacheException $e) {
    print_r($e->getMessage());
}

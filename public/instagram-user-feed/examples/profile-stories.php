<?php 

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);


//declare(strict_types=1);

use Instagram\Api;
//use Instagram\Auth\Checkpoint\ImapClient;
use Instagram\Exception\InstagramException;

use Psr\Cache\CacheException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

require realpath(dirname(__FILE__)) . '/../vendor/autoload.php';
$credentials = include_once realpath(dirname(__FILE__)) . '/credentials.php';

$cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');


// DB connection
$db_config = include_once realpath(dirname(__FILE__)) . '/db_config.php';

// Perform query
try {
    $api = new Api($cachePool);
    //$imapClient = new ImapClient($credentials->getImapServer(), $credentials->getImapLogin(), $credentials->getImapPassword());

    $api->login($credentials->getLogin(), $credentials->getPassword(), $imapClient);


    $shop_media_story = array();
    $shop_videos = mysqli_query($conn, "SELECT * FROM shop_media");

    while ($shop_video = mysqli_fetch_assoc($shop_videos)) {
        $shop_media_story[] = $shop_video['story_id'];
    } 

    $influencers = mysqli_query($conn, "SELECT * FROM influencer");

        while ($influencer = mysqli_fetch_assoc($influencers)) {

            // we need instagram user id
            $profile = $api->getProfile($influencer['instagram_id']);
            
            sleep(1);
            $feedStories = $api->getStories($profile->getId());


            if (count($feedStories->getStories())) {
                /** @var \Instagram\Model\StoryMedia $story */
                foreach ($feedStories->getStories() as $story) {


                   if(!empty($story->getCtaUrl())){ 

                        /* For insert data in shop table */
                        $cta_url = $story->getCtaUrl();

                        if (strpos($cta_url, 'instagram.com') === false && strpos($cta_url, 'youtube.com') === false) {

                            $story_id = $story->getId();
                           
                            if(!in_array($story_id, $shop_media_story)){

                                $cta_url_domain = preg_replace("/^www\./", "", strtolower(parse_url($cta_url, PHP_URL_HOST)));


                                $shop = mysqli_query($conn, "SELECT * FROM shop WHERE influencer_id=".$influencer['influencer_id']." AND DATE(created_at) = '".date('Y-m-d')."' AND (REPLACE(LOWER(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(website, 
                                    '?', 1), # split on url params to remove weirdest stuff first 
                                    '://', -1), # remove protocal http:// https:// ftp:// ...
                                    '/', 1), # split on path 
                                    ':', 2), # split on user:pass
                                    '@', 1), # split on user:port@
                                    ':', 1), # split on port
                                    'www.', -1), # remove www.
                                    '.', 4), # keep TLD + domain name
                                    '/', 1) ), 'www.','')) = '".$cta_url_domain."' ");
                                $shop = mysqli_fetch_assoc($shop);

                                if(empty($shop['shop_id'])){

                                    $website_screenshot = store_site_screenshot($cta_url);

                                    $shop = mysqli_query($conn, "INSERT INTO shop(website, website_screenshot, influencer_id, taken_at, created_at) VALUES ('$cta_url','$website_screenshot','".$influencer['influencer_id']."','".$story->getTakenAtDate()->format('Y-m-d h:i:s')."','".date('Y-m-d h:i:s')."')");

                                    $shop_id = mysqli_insert_id($conn);
                                } else {
                                    $shop_id = $shop['shop_id'];
                                }

                                if(!empty($story->getVideoResources())){

                                    $video_url = $story->getVideoResources()[0]->src;
                                    if (!is_dir(storage_path('app/public/shop_media/'.$shop_id.'/'))) {
                                      mkdir(storage_path('app/public/shop_media/'.$shop_id.'/'), 0777, true);
                                    }
                                    $video = storage_path('app/public/shop_media/'.$shop_id.'/').$story_id.'.mp4';
                                    file_put_contents($video, file_get_contents($video_url));

                                    $shop = mysqli_query($conn, "INSERT INTO shop_media(shop_id, story_id, video) VALUES ('$shop_id','$story_id','".$story_id.".mp4')");

                                }
                            }
                        }

                   }

                }
            } else {
                echo 'No stories' . "\n";
            }

        }

    } catch (InstagramException $e) {
        print_r($e->getMessage());
    } catch (CacheException $e) {
        print_r($e->getMessage());
    }



function store_site_screenshot($url){

        $query_string = http_build_query(array(
            'url' => $url,
            'userkey' => 'IAAIEYKBJAXYPV6IQBUHHOATCG',
            'width' => 1280,
            'full_size' => 1,
            'delay_time' => 5000,
            'timeout' => 90000,
            /*'response_type' => 'json',*/
        ));
        $image = file_get_contents("https://api.site-shot.com/?$query_string");
       
        $img_name = md5($url).'.png';
        $file_name = public_path('/site_screenshot/') . $img_name;

        file_put_contents($file_name, $image);

        return $img_name;
}

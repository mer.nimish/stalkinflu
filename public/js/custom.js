var owl = $('.owl-carousel');
owl.owlCarousel({
    loop:true, // loop set
    nav:true,  // next-prev button show
    margin:6, // content item space set
    // autoplay:true, //content item autoplay 
    autoplayTimeout:2000, //content item autoplay time set
    // autoplayHoverPause:true,  //content item autoplay Hover Pause
    smartSpeed: 1500,   //next-prev speed
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },            
        960:{
            items:2
        },
        1200:{
            items:4 // Row in content item set
        }
    }
});

 // font awesome icons add
 $( ".owl-prev").html('<i class="fa fa-angle-left sidebtn"></i>');
 $( ".owl-next").html('<i class="fa fa-angle-right sidebtn"></i>');

// Images add
 // $( ".owl-prev").html('<img src="images/comment_2.png" style="width: 50px;">');
 // $( ".owl-next").html('<img src="images/comment_2.png" style="width: 50px;">');

// owl.on('mousewheel', '.owl-stage', function (e) {
//     if (e.deltaY>0) {
//         owl.trigger('next.owl');
//     } else {
//         owl.trigger('prev.owl');
//     }
//     e.preventDefault();
// });
// $('.play').on('click',function(){
//     owl.trigger('play.owl.autoplay',[1000])
// })
// $('.stop').on('click',function(){
//     owl.trigger('stop.owl.autoplay')
// })


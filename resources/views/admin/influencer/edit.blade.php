@extends('layout.main')
@section('content')

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="create-emp promotedshops">

            <div class="topbar">
                <h3>Edit Influencer</h3>
            </div>
            @if ($errors->any())
                <div class="error-contains">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="alert alert-danger text-danger"><i class="fa fa-close"></i> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="">
                <div class="panel-body">
                    <div class="form-wrap">
                        
                         {!! Form::open(array('route' => ['admin-influencer.update', $influencer->influencer_id],'method'=>'PATCH', 'class'=>'form-horizontal', 'id'=>'edit-influencer', 'enctype'=>'multipart/form-data')) !!}

                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="instagram_id">Instagram ID:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="instagram_id" name="instagram_id" value="{{$influencer->instagram_id}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="instagram_id">Country:</label>
                                <div class="col-sm-9">
                                    <select name="country_id" id="country_id" class="form-control">
                                        @foreach($countries as $country)
                                            @if($country->country_id == $influencer->country_id)
                                                <option value="{{$country->country_id}}" selected="">{{$country->country_name}}</option>
                                            @else
                                                 <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mb-0">
                            <hr /> 
                                <div class="text-center">
                                  <button type="submit" class="btn btn-success"><span class="btn-text">Submit</span></button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        
    </div>
</div>
</div>
                    
@endsection

@section('pagescript')
    <script type="text/javascript" src="{{ asset('js/jquery.validate.js')}}"></script>
    <script type="text/javascript">

    $(document).ready(function () {
        $('#edit-influencer').validate({ 
            rules: {
                instagram_id: {
                    required: true
                },
                country: {
                    required: true
                },
            },
            messages: {
                instagram_id:{
                    required: 'Please enter instagram id',
                },
                country:{
                    required: 'Please enter country',
                },
            },
            errorElement : 'div',
            /*errorPlacement: function(error, element){
                $(element).addClass('error');
            },*/
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
        });

        jQuery.validator.addMethod("emailfull", function(value, element) {
             return this.optional(element) || /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(value);
        }, "Please enter valid email address");

    });
    </script>
@stop

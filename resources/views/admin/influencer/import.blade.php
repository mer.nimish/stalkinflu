@extends('layout.main')
@section('content')

@section('pagestylesheet')
    
@stop


<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="create-emp promotedshops">

            <div class="topbar">
                <h3>Import Influencer</h3>
                <div class="pull-right"><a href="{{ asset('inf_excel/demo.csv')}}">Import CSV Format</a> | <a href="{{ asset('inf_excel/demo.xlsx')}}">Import Excel Format</a></div>

            </div>
            @if ($errors->any())
                <div class="error-contains">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="alert alert-danger text-danger"><i class="fa fa-close"></i> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="error-contains">
            @if ($message = Session::get('success'))
                <div class="alert alert-success text-success">
                    <i class="fa fa-check"></i> {{ $message }}
                </div>
            @endif
            </div>
            <div class="">
                <div class="panel-body">
                    <div class="form-wrap">
                        
                        {!! Form::open(array('route' => 'admin.import_influencer_store','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'import-influencer', 'enctype'=>'multipart/form-data')) !!}
                           
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="instagram_id">Select Excel/CSV File:</label>
                                <div class="col-sm-9">
                                    <input type="file" id="influencer_file" name="influencer_file">
                                </div>
                            </div>
                            <div class="form-group mb-0">
                            <hr /> 
                                <div class="text-center">
                                  <button type="submit" class="btn btn-success"><span class="btn-text">Submit</span></button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        
    </div>
</div>
</div>
                    
@endsection

@section('pagescript')

    <script type="text/javascript" src="{{ asset('js/jquery.validate.js')}}"></script>
    <script type="text/javascript">

    $(document).ready(function () {
        $('#import-influencer').validate({ 
            rules: {
                influencer_file: {
                    required: true
                },
            },
            messages: {
                influencer_file:{
                    required: 'Please select file.',
                },
            },
            errorElement : 'div',
            /*errorPlacement: function(error, element){
                $(element).addClass('error');
            },*/
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
        });
    });
    </script>
@stop

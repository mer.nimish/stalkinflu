@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <!--alerts CSS -->
    <link href="{{ asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <style type="text/css">
        #influencertable_filter{
            float: right;
        }
        #influencertable_filter input{
                margin-left: 10px;
        }
        .shopslistings table td:last-child a{
            width: unset;
            padding: 1px 5px;
            border-radius: 3px;
        }
    </style>
@stop

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="promotedshops">
            <div class="topbar">
                <h3>Influencers</h3>
                 <div class="pull-right">
                    <a href="{{route('admin-influencer.create')}}" class="btn btn-blue">Add Influencer</a>

                    <a href="{{route('admin.import_influencer')}}" class="btn btn-blue">Import Influencer</a>
                </div>
            </div>

            <div class="error-contains">
            @if ($message = Session::get('success'))
                <div class="alert alert-success text-success">
                    <i class="fa fa-check"></i> {{ $message }}
                </div>
            @endif
            </div>
           
            <div class="shopslistings">
                <div class="shopslistingsinner">
                    <table id="influencertable" class="table display nowrap" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center">Instagram ID</th>
                            <th class="text-center">Country</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center no-sort">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php /*@if(!empty($influencers))
                            @foreach($influencers as $influencer)
                            <tr>
                                <td class="text-center"><a href="https://www.instagram.com/{{$influencer->instagram_id}}" target="_blank">{{$influencer->instagram_id}}</a></td>
                                <td class="text-center">{{$influencer->country_name}}</td>
                                <td class="text-center">{{date("M d Y h:i A", strtotime($influencer->created_at))}}</td>
                                <td class="text-center">
                                    <a href="{{route('admin-influencer.edit', $influencer->influencer_id)}}"  class="btn btn-primary btn-xs action"><i class="fa fa-pencil"></i></a>
                                    
                                     <form action="{{route('admin-influencer.destroy', $influencer->influencer_id)}}" method='POST' id='laravel_datatable-{{$influencer->influencer_id}}' style="    display: -webkit-inline-box;">
                                        <input name='_method' type='hidden' value='DELETE'>
                                        {{csrf_field()}}
                                        <button type='button' title='DELETE' class='btn btn-danger btn-xs action' onclick='delete_influencer("{{$influencer->influencer_id}}");'>
                                            <i class='fa fa-trash-o text-danger'></i>
                                        </button>
                                    </form>
                                    @if($influencer->is_approved == 1)
                                        <a href="{{route('admin.approve_reject', [$influencer->influencer_id, $influencer->is_approved])}}"  class="btn btn-success btn-xs action"><i class="fa fa-check"></i></a>
                                    @else
                                        <a href="{{route('admin.approve_reject', [$influencer->influencer_id, $influencer->is_approved])}}"  class="btn btn-danger btn-xs action"><i class="fa fa-close"></i></a>
                                    @endif
                                    
                                </td>
                                <td style="display: none;"></td>
                            </tr>
                            @endforeach
                        @endif */ ?>
                    </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>

                    
@endsection

@section('pagescript')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
     <!-- Sweet-Alert  -->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    <script>
        $(document).ready(function() {
           /* $('#shoptable').DataTable( {
                searching: true,
                ordering:  true,
                "language": {
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'><i/>",
                        "next": "<i class='fa fa-angle-right'><i/>",

                    }
                },
            } );*/

            $('#influencertable').DataTable({
                "scrollX": true,
                "processing": true,
                "serverSide": true,
                "searching": true,
                "ajax": {
                             "url"      : "{{route('admin.influencer_list')}}",
                             "dataType" : "JSON",
                             "type"     : "POST",
                             "data"     : { _token: $('meta[name="csrf-token"]').attr('content') }
                        },
                "columns": [
                      { "data": "instagram_id", "class" : "text-center" },
                      { "data": "country_name", "class" : "text-center"},
                      { "data": "created_at", "class" : "text-center"},
                      { "data": "influencer_id",  "orderable": false,"class" : "text-center"},
                ],
                "aaSorting": [],
                "language": {
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'><i/>",
                        "next": "<i class='fa fa-angle-right'><i/>",

                    }
                },
            });
        } );

        function delete_influencer(influencer_id){

            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this data!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#8BC34A",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                /*iconColor: '#000',*/
            }, function(){   
                swal("Deleted!", "Your data has been deleted.", "success"); 
                $("#laravel_datatable-"+influencer_id).submit();
                return true;
            });

            return false;
        }
    </script>
@stop
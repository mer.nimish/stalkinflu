@extends('layout.main')
@section('content')

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="create-emp promotedshops">

            <div class="topbar">
                <h3>Setting</h3>
            </div>
            @if ($errors->any())
                <div class="error-contains">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="alert alert-danger text-danger"><i class="fa fa-close"></i> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="">
                <div class="panel-body">
                    <div class="form-wrap">
                        
                         {!! Form::open(array('route' => ['admin.setting_update', $setting->setting_id],'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'edit-setting')) !!}

                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="name">Bug or an idea link:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="bug_or_an_idea_link" name="bug_or_an_idea_link" value="{{$setting->bug_or_an_idea_link}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="name">Commission text:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="commission_text" name="commission_text" value="{{$setting->commission_text}}">
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="name">Dashboard Header:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="dashboard_header" name="dashboard_header" value="{{$setting->dashboard_header}}">
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="name">Dashboard Text:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="dashboard_text" name="dashboard_text" value="{{$setting->dashboard_text}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="name">Dashboard Total Members:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="dashboard_total_members" name="dashboard_total_members" value="{{$setting->dashboard_total_members}}">
                                </div>
                            </div>
                        
                            <div class="form-group mb-0">
                            <hr /> 
                                <div class="text-center">
                                  <button type="submit" class="btn btn-success"><span class="btn-text">Submit</span></button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        
    </div>
</div>
</div>
                    
@endsection

@section('pagescript')
    <script type="text/javascript" src="{{ asset('js/jquery.validate.js')}}"></script>
    <script type="text/javascript">

    $(document).ready(function () {
        $('#edit-setting').validate({ 
            rules: {
                bug_or_an_idea_link:{
                    required: true,
                },
                commission_text:{
                    required: true
                },
                dashboard_text:{
                    required: true
                },
                dashboard_header:{
                    required: true
                },
                dashboard_total_members:{
                    required: true
                },
            },
            messages: {
                bug_or_an_idea_link:{
                    required: 'Please enter bug or an idea link',
                },
                commission_text:{
                    required: 'Please enter commission text',
                },
                dashboard_text:{
                    required: 'Please enter dashboard text',
                },
                dashboard_header:{
                    required: 'Please enter dashboard header',
                },
                dashboard_total_members:{
                    required: 'Please enter dashboard total members',
                },
            },
            errorElement : 'div',
            /*errorPlacement: function(error, element){
                $(element).addClass('error');
            },*/
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
        });

    });
    </script>
@stop

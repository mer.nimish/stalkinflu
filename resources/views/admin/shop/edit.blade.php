@extends('layout.main')
@section('content')
@section('pagestylesheet')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.css')}}">
    <style type="text/css">
        button.btn.dropdown-toggle.btn-default{
            background-color: #fff;
            border-radius: unset;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            color: #555;
            font-weight: 100;
        }
    </style>    
@stop
<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="create-emp promotedshops">

            <div class="topbar">
                <h3>Edit Shop</h3>
            </div>
            @if ($errors->any())
                <div class="error-contains">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="alert alert-danger text-danger"><i class="fa fa-close"></i> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="">
                <div class="panel-body">
                    <div class="form-wrap">
                        
                         {!! Form::open(array('route' => ['admin-shop.update', $shop->shop_id],'method'=>'PATCH', 'class'=>'form-horizontal', 'id'=>'edit-shop', 'enctype'=>'multipart/form-data')) !!}

                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="instagram_id">Influencer ID:</label>
                                <div class="col-sm-9">
                                    <select name="influencer_id" id="influencer_id" class="form-control">
                                        @foreach($users as $user)
                                            @if($user->influencer_id == $shop->influencer_id)
                                                <option value="{{$user->influencer_id}}" selected="">{{$user->instagram_id}}</option>
                                            @else
                                                 <option value="{{$user->influencer_id}}">{{$user->instagram_id}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="name">Website:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="website" name="website"  value="{{$shop->website}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="instagram_id">Category:</label>
                                <div class="col-sm-9">
                                    <select name="category_id[]" id="category_id" class="form-control selectpicker" multiple="">
                                        @foreach($categories as $category)
                                            @if(in_array($category->category_id, $shop_category_ids))
                                                <option selected="" value="{{$category->category_id}}">{{$category->category_name}}</option>
                                            @else
                                                <option value="{{$category->category_id}}">{{$category->category_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="instagram_id">Website Screenshot:</label>
                                <div class="col-sm-9">
                                    <input type="file" name="screenshot" id="screenshot">
                                    <br />
                                    <div class="shop-details-website-frame">
                                    <iframe id="website-frame" style="border: 1.5px solid #508ff4;" src="{{Config::get('constants.site_screenshot_url').$shop->website_screenshot}}" frameborder="0" allowfullscreen></iframe>
                                   
                                </div>
                                </div>
                            </div>
                            
                            <div class="form-group mb-0">
                            <hr /> 
                                <div class="text-center">
                                  <button type="submit" class="btn btn-success"><span class="btn-text">Submit</span></button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        
    </div>
</div>
</div>
                    
@endsection

@section('pagescript')
    <script type="text/javascript" src="{{ asset('js/jquery.validate.js')}}"></script>
    <script src="{{ asset('js/bootstrap-select.js')}}"></script>
    <script type="text/javascript">

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#website-frame').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#screenshot").change(function(){
            readURL(this);
        });

    $(document).ready(function () {

        $("#website-frame").contents().find("img").attr("style","width:100%")

        $('.selectpicker').selectpicker({
            tickIcon: 'fa fa-check',
        });
        
        $('#edit-shop').validate({ 
            rules: {
                influencer_id:{
                    required: true,
                },
                website: {
                    required: true,
                    website: true
                },
            },
            messages: {
                influencer_id:{
                    required: 'Please enter influencer id',
                },
                website:{
                    required: 'Please enter website',
                },
            },
            errorElement : 'div',
            /*errorPlacement: function(error, element){
                $(element).addClass('error');
            },*/
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
        });

        jQuery.validator.addMethod("emailfull", function(value, element) {
             return this.optional(element) || /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(value);
        }, "Please enter valid email address");
        jQuery.validator.addMethod("website", function(value, element) {
             return this.optional(element) || /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
        }, "Please enter valid website");

    });
    </script>
@stop

@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <!--alerts CSS -->
    <link href="{{ asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.css')}}">
    <style type="text/css">
        #shopTable_filter{
            float: right;
        }
        #shopTable_filter input{
                margin-left: 10px;
        }
        .shopslistings table td:last-child a{
            width: unset;
            padding: 1px 5px;
            border-radius: 3px;
        }
    </style>
@stop

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="promotedshops">
            <div class="topbar">
                <h3>Shops</h3>
                 <div class="pull-right">
                    <a href="{{route('admin-shop.create')}}" class="btn btn-blue">Add Shop</a>
                    <a href="Javascript:void(0);" onclick="select_category_list()" class="btn btn-blue">Assign Category</a>
                </div>
            </div>

            <div class="error-contains">
            @if ($message = Session::get('success'))
                <div class="alert alert-success text-success">
                    <i class="fa fa-check"></i> {{ $message }}
                </div>
            @endif
            </div>
           
            <div class="shopslistings">
                <div class="shopslistingsinner">
                    <table id="shopTable" class="table display nowrap" width="100%">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="checked-all"></th>
                            <th>Website</th>
                            <th class="text-center">Country</th>
                            <th class="text-center">Influencer</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center">Categories</th>
                            <th class="text-center no-sort">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php /*
                        @if(!empty($shops))
                            @foreach($shops as $shop)
                            <tr>
                                <td><input type="checkbox" name="shop_ids[]" class="checked-shop-ids" value="{{$shop->shop_id}}" /></td>
                                <td class="text-center"><a href="{{$shop->website}}" target="_blank">{{ Str::limit($shop->website, 25) }}</a></td>
                                <td class="text-center">{{$shop->country_name}}</td>
                                <td class="text-center">{{$shop->name}}</td>
                                <td class="text-center">{{date("M d Y h:i A", strtotime($shop->created_at))}}</td>
                                
                                <td>@if(!empty($shop->categories))
                                      @php
                                      $categories2 = explode("<br />", $shop->categories);
                                      @endphp

                                      <span class="category-text">{{ $categories2[0] }}</span>
                                      @if(count($categories2) > 1)
                                          <span class="category-cnt" data-toggle="tooltip" data-placement="top" data-html="true" title="{{$shop->categories}}">+{{ $shop->category_cnt }}</span>
                                      @endif
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a href="{{route('admin-shop.edit', $shop->shop_id)}}"  class="btn btn-primary btn-xs action" target="_blank"><i class="fa fa-pencil"></i></a>
                                    
                                     <form action="{{route('admin-shop.destroy', $shop->shop_id)}}" method='POST' id='laravel_datatable-{{$shop->shop_id}}' style="    display: -webkit-inline-box;">
                                        <input name='_method' type='hidden' value='DELETE'>
                                        {{csrf_field()}}
                                        <button type='button' title='DELETE' class='btn btn-danger btn-xs action' onclick='delete_shop("{{$shop->shop_id}}");'>
                                            <i class='fa fa-trash-o text-danger'></i>
                                        </button>
                                    </form>
                                </td>
                                <td style="display: none;"></td>
                            </tr>
                            @endforeach
                        @endif */ ?>
                    </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>

<div id="assign-shop-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">     
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Assign shop into category</h4>
          </div>
          <div class="modal-body">
            
            <input type="hidden" id="assign_influencer_id">
            <div class="form-group row">
                <label class="control-label col-sm-2">Category Name:</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" id="assign_category_ids" multiple=""> 
                        <option value="" disabled="">Select Category List</option>
                        @foreach($categories as $category)
                         <option value="{{$category->category_id}}">{{$category->category_name}}</option>
                        @endforeach
                    </select>
                    <div class="assign-shop-msg text-success"></div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-blue assign-shop-btn">Submit</button>
          </div>
    </div>
  </div>
</div>                    
@endsection

@section('pagescript')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
     <!-- Sweet-Alert  -->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap-select.js')}}"></script>
    <script>
        $(document).ready(function() {

            $('#shopTable').DataTable({
                "scrollX": true,
                "processing": true,
                "serverSide": true,
                "searching": true,
                "ajax": {
                             "url"      : "{{route('admin.shop_list')}}",
                             "dataType" : "JSON",
                             "type"     : "POST",
                             "data"     : { _token: $('meta[name="csrf-token"]').attr('content') }
                        },
                "columns": [
                      { "data": "shop_id", "orderable": false, "class" : "text-center" },
                      { "data": "website", "class" : "text-left"},
                      { "data": "country_name", "class" : "text-center"},
                      { "data": "instagram_id", "class" : "text-center"},
                      { "data": "created_at", "class" : "text-center"},
                      { "data": "categories", "orderable": false, "class" : "text-center"},
                      { "data": "shop_id2", "orderable": false, "class" : "text-center"},
                ],
                "aaSorting": [],
                "language": {
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'><i/>",
                        "next": "<i class='fa fa-angle-right'><i/>",

                    }
                },
            });


            $('.selectpicker').selectpicker({
                tickIcon: 'fa fa-check'
            });

            /*$('#shoptable').DataTable( {
                searching: true,
                ordering:  true,
                "language": {
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'><i/>",
                        "next": "<i class='fa fa-angle-right'><i/>",

                    }
                },
            } );*/
        } );
        $(document).on("click", "#checked-all", function(){
            if(this.checked)
                $("input[type=checkbox]").prop('checked', true);
            else
                $("input[type=checkbox]").prop('checked', false);
        });
        $(document).on('click', '.assign-shop-btn', function(e) { 

            if($("#assign_category_ids").val() != null){
                var shop_ids = $('input.checked-shop-ids:checked').map( function () { 
                    return $(this).val();
                }).get();
               // var shop_ids = $("#assign_shop_ids").val();
                var category_ids = $("#assign_category_ids").val();
                $.ajax({
                    url : "{{route('admin.assign_shop_into_category')}}",
                    data : { shop_ids:shop_ids, category_ids:category_ids},
                }).done(function (data) { 
                    $('.assign-shop-msg').html('Shop assigned successfully.');
                    $('input.checked-shop-id').prop('checked', false);
                    location.reload();
                });
            } else {
                alert("Please select at least one category.");
            }
        });

        function select_category_list(){
            if($('input.checked-shop-ids:checked').length > 0){
                $("#assign-shop-modal").modal("show");
            } else {
                alert("Please select at least one shop.");
            }
        }
        function delete_shop(shop_id){

            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this data!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#8BC34A",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                /*iconColor: '#000',*/
            }, function(){   
                swal("Deleted!", "Your data has been deleted.", "success"); 
                $("#laravel_datatable-"+shop_id).submit();
                return true;
            });

            return false;
        }
    </script>
@stop
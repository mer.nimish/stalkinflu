@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <!--alerts CSS -->
    <link href="{{ asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css">
     <style type="text/css">
        #shoptable_filter{
            float: right;
        }
        #shoptable_filter input{
                margin-left: 10px;
        }
    </style>
@stop

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="promotedshops">
            <div class="topbar">
                <h3>Categories</h3>
                 <div class="pull-right">
                    <a href="{{route('admin-category.create')}}" class="btn btn-blue">Add Category</a>
                </div>
            </div>

            <div class="error-contains">
            @if ($message = Session::get('success'))
                <div class="alert alert-success text-success">
                    <i class="fa fa-check"></i> {{ $message }}
                </div>
            @endif
            </div>
           
            <div class="shopslistings">
                <div class="shopslistingsinner">
                    <table id="shoptable" class="table display nowrap">
                    <thead>
                        <tr>
                            <th>Category</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center no-sort">Action</th>
                            <th style="display: none;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($categories))
                            @foreach($categories as $category)
                            <tr>
                                <td class="text-center">{{$category->category_name}}</td>
                                <td class="text-center">{{date("M d Y h:i A", strtotime($category->created_at))}}</td>
                                <td class="text-center">
                                    <a href="{{route('admin-category.edit', $category->category_id)}}"  class="btn btn-primary btn-xs action"><i class="fa fa-pencil"></i></a>
                                    
                                     <form action="{{route('admin-category.destroy', $category->category_id)}}" method='POST' id='laravel_datatable-{{$category->category_id}}' style="    display: -webkit-inline-box;">
                                        <input name='_method' type='hidden' value='DELETE'>
                                        {{csrf_field()}}
                                        <button type='button' title='DELETE' class='btn btn-danger btn-xs action' onclick='delete_category("{{$category->category_id}}");'>
                                            <i class='fa fa-trash-o text-danger'></i>
                                        </button>
                                    </form>
                                </td>
                                <td style="display: none;"></td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>

                    
@endsection

@section('pagescript')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
     <!-- Sweet-Alert  -->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#shoptable').DataTable( {
                searching: true,
                ordering:  true,
                "language": {
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'><i/>",
                        "next": "<i class='fa fa-angle-right'><i/>",

                    }
                },
            } );
        } );

        function delete_category(category_id){

            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this data!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#8BC34A",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                /*iconColor: '#000',*/
            }, function(){   
                swal("Deleted!", "Your data has been deleted.", "success"); 
                $("#laravel_datatable-"+category_id).submit();
                return true;
            });

            return false;
        }
    </script>
@stop
@extends('layout.main')
@section('content')

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="create-emp promotedshops">

            <div class="topbar">
                <h3>Edit Category</h3>
            </div>

             <div class="error-contains">
            @if ($message = Session::get('success'))
                <div class="alert alert-success text-success">
                    <i class="fa fa-check"></i> {{ $message }}
                </div>
            @endif
            </div>
            
            @if ($errors->any())
                <div class="error-contains">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="alert alert-danger text-danger"><i class="fa fa-close"></i> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="">
                <div class="panel-body">
                    <div class="form-wrap">
                        
                         {!! Form::open(array('route' => ['admin-category.update', $category->category_id],'method'=>'PATCH', 'class'=>'form-horizontal', 'id'=>'edit-category', 'enctype'=>'multipart/form-data')) !!}

                            
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="name">Category Name:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="category_name" name="category_name"  value="{{$category->category_name}}">
                                </div>
                            </div>
                            
                            <div class="form-group mb-0">
                            <hr /> 
                                <div class="text-center">
                                  <button type="submit" class="btn btn-success"><span class="btn-text">Submit</span></button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        
    </div>
</div>
</div>
                    
@endsection

@section('pagescript')
    <script type="text/javascript" src="{{ asset('js/jquery.validate.js')}}"></script>
    <script type="text/javascript">

    $(document).ready(function () {
        $('#edit-category').validate({ 
            rules: {
                category_name:{
                    required: true,
                },
            },
            messages: {
                category_name:{
                    required: 'Please enter category name',
                },
            },
            errorElement : 'div',
            /*errorPlacement: function(error, element){
                $(element).addClass('error');
            },*/
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
        });
    });
    </script>
@stop

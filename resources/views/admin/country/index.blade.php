@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <!--alerts CSS -->
    <link href="{{ asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css">
     <style type="text/css">
        #shoptable_filter{
            float: right;
        }
        #shoptable_filter input{
                margin-left: 10px;
        }
    </style>
@stop

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="promotedshops">
            <div class="topbar">
                <h3>Countries</h3>
                 <div class="pull-right">
                    <a href="{{route('admin-country.create')}}" class="btn btn-blue">Add Country</a>
                </div>
            </div>

            <div class="error-contains">
            @if ($message = Session::get('success'))
                <div class="alert alert-success text-success">
                    <i class="fa fa-check"></i> {{ $message }}
                </div>
            @endif
            </div>
           
            <div class="shopslistings">
                <div class="shopslistingsinner">
                    <table id="shoptable" class="table display nowrap">
                    <thead>
                        <tr>
                            <th class="text-center">Code</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Flag</th>
                            <th class="text-center no-sort">Action</th>
                            <th style="display: none;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($countries))
                            @foreach($countries as $country)
                            <tr>
                                <td class="text-center">{{$country->country_code}}</td>
                                <td class="text-center">{{$country->country_name}}</td>
                                <td class="text-center"><img src="{{asset('images/flags/'.Str::lower($country->country_code).'.svg')}}" width="20" /></td>
                                <td class="text-center">
                                    <a href="{{route('admin-country.edit', $country->country_id)}}"  class="btn btn-primary btn-xs action"><i class="fa fa-pencil"></i></a>
                                    
                                     <form action="{{route('admin-country.destroy', $country->country_id)}}" method='POST' id='laravel_datatable-{{$country->country_id}}' style="    display: -webkit-inline-box;">
                                        <input name='_method' type='hidden' value='DELETE'>
                                        {{csrf_field()}}
                                        <button type='button' title='DELETE' class='btn btn-danger btn-xs action' onclick='delete_country("{{$country->country_id}}");'>
                                            <i class='fa fa-trash-o text-danger'></i>
                                        </button>
                                    </form>
                                </td>
                                <td style="display: none;"></td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>

                    
@endsection

@section('pagescript')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
     <!-- Sweet-Alert  -->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#shoptable').DataTable( {
                searching: true,
                ordering:  true,
                "language": {
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'><i/>",
                        "next": "<i class='fa fa-angle-right'><i/>",

                    }
                },
            } );
        } );

        function delete_country(country_id){

            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this data!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#8BC34A",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                /*iconColor: '#000',*/
            }, function(){   
                swal("Deleted!", "Your data has been deleted.", "success"); 
                $("#laravel_datatable-"+country_id).submit();
                return true;
            });

            return false;
        }
    </script>
@stop
@extends('layout.main')
@section('content')

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="create-emp promotedshops">

            <div class="topbar">
                <h3>Edit Country</h3>
            </div>

             <div class="error-contains">
            @if ($message = Session::get('success'))
                <div class="alert alert-success text-success">
                    <i class="fa fa-check"></i> {{ $message }}
                </div>
            @endif
            </div>
            
            @if ($errors->any())
                <div class="error-contains">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="alert alert-danger text-danger"><i class="fa fa-close"></i> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="">
                <div class="panel-body">
                    <div class="form-wrap">
                        
                         {!! Form::open(array('route' => ['admin-country.update', $country->country_id],'method'=>'PATCH', 'class'=>'form-horizontal', 'id'=>'edit-country', 'enctype'=>'multipart/form-data')) !!}

                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="name">Country Code:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="country_code" name="country_code" maxlength="2" value="{{$country->country_code}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="name">Country Name:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="country_name" name="country_name" value="{{$country->country_name}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="instagram_id">Flag Image:</label>
                                <div class="col-sm-4 mt-10">
                                    <input type="file" name="flag_image" id="flag_image">
                                    
                                </div>
                                <div class="col-sm-5 mt-10">
                                    <img src="{{asset('images/flags/'.Str::lower($country->country_code).'.svg')}}" id="flag-image-show" width="100">
                                </div>
                            </div>
                            
                            <div class="form-group mb-0">
                            <hr /> 
                                <div class="text-center">
                                  <button type="submit" class="btn btn-success"><span class="btn-text">Submit</span></button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        
    </div>
</div>
</div>
                    
@endsection

@section('pagescript')
    <script type="text/javascript" src="{{ asset('js/jquery.validate.js')}}"></script>
    <script type="text/javascript">

     function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#flag-image-show').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#flag_image").change(function(){
        readURL(this);
    });

    $(document).ready(function () {
        $('#edit-country').validate({ 
            rules: {
                country_name:{
                    required: true,
                },
            },
            messages: {
                country_name:{
                    required: 'Please enter country name',
                },
            },
            errorElement : 'div',
            /*errorPlacement: function(error, element){
                $(element).addClass('error');
            },*/
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
        });
    });
    </script>
@stop

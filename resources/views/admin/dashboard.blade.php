@extends('layout.main')
@section('content')
<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
        <div class="middlepart">
            <div class="statisctic">
                <h3>Welcome!</h3>
                 <div class="statiscticitem">
                    <div class="icon">
                        <img src="{{ asset('images/statistic1.png')}}">
                    </div>
                    <div class="info">
                        <h4>{{$employees}}</h4>
                        <span>Total Employees</span>
                    </div>
                </div>
                <div class="statiscticitem">
                    <div class="icon">
                        <img src="{{ asset('images/statistic2.png')}}">
                    </div>
                    <div class="info">
                        <h4>{{$shops}}</h4>
                        <span>Total shops</span>
                    </div>
                </div>
                <div class="statiscticitem">
                    <div class="icon">
                        <img src="{{ asset('images/statistic3.png')}}">
                    </div>
                    <div class="info">
                        <h4>{{$influencers}}</h4>
                        <span>Total influencers </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('pagescript')

@stop
@endsection
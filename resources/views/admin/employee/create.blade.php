@extends('layout.main')

@section('pagestylesheet')
    
@stop

@section('content')

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="create-emp promotedshops">

            <div class="topbar">
                <h3>Create Employee</h3>
            </div>
            @if ($errors->any())
                <div class="error-contains">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="alert alert-danger text-danger"><i class="fa fa-close"></i> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="">
                <div class="panel-body">
                    <div class="form-wrap">
                        
                        {!! Form::open(array('route' => 'employee.store','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'create-employee')) !!}
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="name">Name:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="email">E-mail:</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="password">Password:</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="ConfirmPassword">Confirm Password:</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" id="confirm_password" name="confirm_password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="emp_access">Access:</label>
                                <div class="col-sm-9">
                                   <select class="form-control" name="emp_access">
                                       <option value="WEBSITE_INFLUENCER">WEBSITE & INFLUENCER</option>
                                       <option value="WEBSITE">WEBSITE</option>
                                       <option value="INFLUENCER">INFLUENCER</option>
                                   </select>
                                </div>
                            </div>
                            
                            <div class="form-group mb-0">
                            <hr /> 
                                <div class="text-center">
                                  <button type="submit" class="btn btn-success"><span class="btn-text">Submit</span></button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        
    </div>
</div>
</div>
                    
@endsection

@section('pagescript')

    <script type="text/javascript" src="{{ asset('js/jquery.validate.js')}}"></script>
    <script type="text/javascript">

    $(document).ready(function () {
        $('#create-employee').validate({ 
            rules: {
                name:{
                    required: true,
                },
                email:{
                    required: true,
                    emailfull: true
                },
                password: {
                    required: true
                },
                confirm_password: {
                    required: true,
                    equalTo : "#password"
                },
                emp_access: {
                    required: true
                },
            },
            messages: {
                name:{
                    required: 'Please enter name',
                },
                email:{
                    required: 'Please enter email',
                },
                password:{
                    required: 'Please enter password',
                },
                confirm_password:{
                    required: 'Please enter confirm password',
                },
                emp_access:{
                    required: 'Please enter access',
                },
            },
            errorElement : 'div',
            /*errorPlacement: function(error, element){
                $(element).addClass('error');
            },*/
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
        });

        jQuery.validator.addMethod("emailfull", function(value, element) {
             return this.optional(element) || /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(value);
        }, "Please enter valid email address");

    });
    </script>
@stop

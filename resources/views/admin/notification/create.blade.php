@extends('layout.main')

@section('pagestylesheet')
     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@stop

@section('content')

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="create-emp promotedshops">

            <div class="topbar">
                <h3>Create Notification</h3>
            </div>
            @if ($errors->any())
                <div class="error-contains">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="alert alert-danger text-danger"><i class="fa fa-close"></i> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
             <div class="error-contains">
            @if ($message = Session::get('success'))
                <div class="alert alert-success text-success">
                    <i class="fa fa-check"></i> {{ $message }}
                </div>
            @endif
            </div>
            <div class="">
                <div class="panel-body">
                    <div class="form-wrap">
                        
                        {!! Form::open(array('route' => 'admin-notification.store','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'create-notification')) !!}
                            
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="name">Notification:</label>
                                <div class="col-sm-9">
                                    <textarea name="notification" style="width: 100%;"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="name">Date:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control datepicker" id="date" name="date">
                                </div>
                            </div>
                            
                            <div class="form-group mb-0">
                            <hr /> 
                                <div class="text-center">
                                  <button type="submit" class="btn btn-success"><span class="btn-text">Submit</span></button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        
    </div>
</div>
</div>
                    
@endsection

@section('pagescript')

    <script type="text/javascript" src="{{ asset('js/jquery.validate.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.js')}}"></script>
    <script type="text/javascript">

    $(document).ready(function () {

        $(".datepicker").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
        });

        $('#create-category').validate({ 
            rules: {
                notification:{
                    required: true,
                },
            },
            messages: {
                notification:{
                    required: 'Please enter notification',
                }
            },
            errorElement : 'div',
            /*errorPlacement: function(error, element){
                $(element).addClass('error');
            },*/
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
        });
    });
    </script>
@stop

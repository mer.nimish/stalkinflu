@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <!--alerts CSS -->
    <link href="{{ asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css">
     <style type="text/css">
        #shoptable_filter{
            float: right;
        }
        #shoptable_filter input{
                margin-left: 10px;
        }
    </style>
@stop

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="promotedshops">
            <div class="topbar">
                <h3>Paid Users</h3>
                 
            </div>

            <div class="error-contains">
            @if ($message = Session::get('success'))
                <div class="alert alert-success text-success">
                    <i class="fa fa-check"></i> {{ $message }}
                </div>
            @endif
            </div>
           
            <div class="shopslistings">
                <div class="shopslistingsinner">
                    <table id="shoptable" class="table display nowrap">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Created At</th>
                            <th class="text-center">Status</th>
                            <th style="display: none;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($users))
                            @foreach($users as $user)
                            <tr>
                                <td class="text-center">{{$user->first_name.' '.$user->last_name}}</td>
                                <td class="text-center">{{$user->email}}</td>
                                <td class="text-center">{{date("M d Y h:i A", strtotime($user->created_at))}}</td>
                                <td class="text-center">{{$user->status}}</td>
                                <td style="display: none;"></td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>

                    
@endsection

@section('pagescript')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
     <!-- Sweet-Alert  -->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#shoptable').DataTable( {
                searching: true,
                ordering:  true,
                "language": {
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'><i/>",
                        "next": "<i class='fa fa-angle-right'><i/>",

                    }
                },
            } );
        } );

        function delete_employee(admin_id){

            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this data!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#8BC34A",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                /*iconColor: '#000',*/
            }, function(){   
                swal("Deleted!", "Your data has been deleted.", "success"); 
                $("#laravel_datatable-"+admin_id).submit();
                return true;
            });

            return false;
        }
    </script>
@stop
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
       
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        
        @php
        $currentRoute = Request::route()->getName();
        @endphp
        @if($currentRoute)
            <title>{{Config::get('constants.meta_tags.'.$currentRoute.'.title')}}</title>
            <meta name="title" content="{{Config::get('constants.meta_tags.'.$currentRoute.'.title')}}">
            <meta name="keywords" content="{{Config::get('constants.meta_tags.'.$currentRoute.'.keywords')}}">
            <meta name="description" content="{{Config::get('constants.meta_tags.'.$currentRoute.'.description')}}">
            <meta name="author" content="{{Config::get('constants.meta_tags.'.$currentRoute.'.author')}}">
        @else
            <title>{{Config::get('constants.meta_tags.title')}}</title>
            <meta name="title" content="{{Config::get('constants.meta_tags.title')}}">
            <meta name="keywords" content="{{Config::get('constants.meta_tags.keywords')}}">
            <meta name="description" content="{{Config::get('constants.meta_tags.description')}}">
            <meta name="author" content="{{Config::get('constants.meta_tags.author')}}">
        @endif

        <!-- Favicon -->
	    <link rel="shortcut icon" href="{{ asset('images/favicon.png')}}">

        <!--Main Stylesheet-->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css')}}">
         <link rel="stylesheet" type="text/css"
    href="https://mkkeck.github.io/jquery-ui-iconfont/styles/jquery-ui.icon-font.min.css" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css')}}">
        
        @yield('pagestylesheet')


        <script type="text/javascript">
        (function() {
        window.sib = { equeue: [], client_key: "3fbctqlhzj9nz91j7bltt1of" };
        // OPTIONAL: email to identify request/
        // window.sib.email_id = 'example@domain.com';
        // OPTIONAL: to hide the chat on your script uncomment this line/
        // window.sib.display_chat = 0;
        // window.sib.display_logo = 0;
        // OPTIONAL: to overwrite the default welcome message uncomment this line/
        // window.sib.custom_welcome_message = 'Hello, how can we help you?';
        // OPTIONAL: to overwrite the default offline message uncomment this line/
        // window.sib.custom_offline_message = 'We are currently offline. In order to answer you, please indicate your email in your messages.';
        window.sendinblue = {}; for (var j = ['track', 'identify', 'trackLink', 'page'], i = 0; i < j.length; i++) { (function(k) { window.sendinblue[k] = function(){ var arg = Array.prototype.slice.call(arguments); (window.sib[k] || function() { var t = {}; t[k] = arg; window.sib.equeue.push(t);})(arg[0], arg[1], arg[2]);};})(j[i]);}var n = document.createElement("script"),i = document.getElementsByTagName("script")[0]; n.type = "text/javascript", n.id = "sendinblue-js", n.async = !0, n.src = "https://sibautomation.com/sa.js?key=" + window.sib.client_key, i.parentNode.insertBefore(n, i), window.sendinblue.page();
        })();
        </script>

    </head>
    <body>

    <div class="header">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="logo">
                    @if((Auth::guard('User')->check()))
                        <a href="{{ route('user.dashboard')}}"><img src="{{ asset('images/logo.png')}}" alt=""></a>
                    @elseif((Auth::guard('Admin')->check()))
                        <a href="{{ route('admin.dashboard')}}"><img src="{{ asset('images/logo.png')}}" alt=""></a>
                    @endif
                </div>
            </div>
            @if (Auth::guard('User')->check())
            <div class="col-md-8 col-sm-8 col-xs-8">
                <div class="userinfoicons text-right">
                    <a href="{{route('influencer.create')}}" class="add-an-influencer"> ADD AN INFLUENCER</a>
                    <a href="{{$bug_or_an_idea_link}}" class="a-bug-or-an-idea"> A BUG OR AN IDEA ?</a>
                    <a href="{{route('notification.index')}}">
                        @if($is_unread_notification == 0)
                            <span></span>
                        @endif
                        <i class="fa fa-bell-o"></i></a>
                    <a href="{{route('user.profile')}}"><i class="fa fa-user-o"></i></a>
                </div>
            </div> 
            @endif
        </div>
    </div>
  
    <div class="mainsection">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-3 col-sm-3 col-xs-12 no-padding">
                    <div class="leftside">
                        <div class="userinfo">
                            @if (Auth::guard('Admin')->check())
                                <img src="{{asset('images/profile.png')}}">
                                <h5>{{Auth::guard('Admin')->user()->name}}</h5>
                            @else
                                @if(!empty(Auth::guard('User')->user()->profile_pic))
                                    <img src="{{asset(Config::get('constants.profile_image_thumb_url'). Auth::guard('User')->user()->profile_pic)}}">
                                @else
                                    <img src="{{asset('images/profile.png')}}">
                                @endif
                                <h5>{{Auth::guard('User')->user()->first_name.' '.Auth::guard('User')->user()->last_name}}</h5>
                            @endif
                            
                        </div>
                        <div class="dashboardmenu">
                          @if (Auth::guard('Admin')->check())  
                            <ul>
                                <li class="{{ (request()->is('admin/dashboard')) ? 'active' : '' }}"><a href="{{route('admin.dashboard')}}"><i class="fa fa-th-large"></i> Dashboard</a></li>
                                @if(Auth::guard('Admin')->user()->is_admin)
                                    <li class="{{ (request()->is('admin/employee*')) ? 'active' : '' }}"><a href="{{route('employee.index')}}"><i class="fa fa-user"></i> Employees</a></li>
                                @endif
                                @if(Auth::guard('Admin')->user()->emp_access != 'WEBSITE')
                                    <li class="{{ (request()->is('admin/admin-influencer*')) ? 'active' : '' }}"><a href="{{route('admin-influencer.index')}}"><i class="fa fa-user"></i> Influencers</a></li>
                                @endif
                                @if(Auth::guard('Admin')->user()->emp_access != 'INFLUENCER')
                                    <li class="{{ (request()->is('admin/admin-shop*')) ? 'active' : '' }}"><a href="{{route('admin-shop.index')}}"><i class="fa fa-shopping-basket"></i> Shops</a></li>
                                @endif
                                <li class="{{ (request()->is('admin/category*')) ? 'active' : '' }}"><a href="{{route('admin-category.index')}}"><i class="fa fa-th-large"></i> Category</a></li>

                                <li class="{{ (request()->is('admin/paying-users*')) ? 'active' : '' }}"><a href="{{route('admin.paying_users')}}"><i class="fa fa-user"></i> Paying Users</a></li>

                                <li class="{{ (request()->is('admin/paid-users*')) ? 'active' : '' }}"><a href="{{route('admin.paid_users')}}"><i class="fa fa-user"></i> Paid Users</a></li>

                                <li class="{{ (request()->is('admin/setting')) ? 'active' : '' }}"><a href="{{route('admin.setting')}}"><i class="fa fa-cog"></i> Setting</a></li>

                                <li class="{{ (request()->is('admin/notification')) ? 'active' : '' }}"><a href="{{route('admin-notification.index')}}"><i class="fa fa-cog"></i> Notifications</a></li>

                                 <li class="{{ (request()->is('admin/country')) ? 'active' : '' }}"><a href="{{route('admin-country.index')}}"><i class="fa fa-cog"></i> Countries</a></li>
                              
                                <li class="logout"><a href="{{ route('admin.logout')}}"><i class="fa fa-sign-out"></i> Logout</a></li>
                            </ul>
                        @else
                             <ul>
                                  <li class="{{ (request()->is('user/dashboard')) ? 'active' : '' }}"><a href="{{route('user.dashboard')}}">
                                    @if((request()->is('user/dashboard')))
                                        <img src="{{ asset('images/blue/dashboard.svg')}}" />
                                    @else
                                        <img src="{{ asset('images/grey/dashboard.svg')}}" />
                                    @endif
                                  Dashboard</a></li>

                                  <li class="{{ (request()->is('user/trend*')) ? 'active' : '' }}"><a href="{{route('trend.index')}}">
                                    @if(request()->is('user/trend*'))
                                        <img src="{{ asset('images/blue/trends.svg')}}" />
                                    @else
                                        <img src="{{ asset('images/grey/trends.svg')}}" />
                                    @endif
                                  Trend</a></li>

                                  <li class="{{ (request()->is('user/shop')) ? 'active' : '' }}"><a href="{{route('shop.index')}}">
                                   @if((request()->is('user/shop')))
                                        <img src="{{ asset('images/blue/shops.svg')}}" />
                                    @else
                                        <img src="{{ asset('images/grey/shops.svg')}}" />
                                    @endif 
                                  Shops</a></li>

                                  <li class="{{ (request()->is('user/influencer')) ? 'active' : '' }}"><a href="{{route('influencer.index')}}">
                                    @if((request()->is('user/influencer')))
                                        <img src="{{ asset('images/blue/influencer.svg')}}" />
                                    @else
                                        <img src="{{ asset('images/grey/influencer.svg')}}" />
                                    @endif 
                                Influencers</a></li>

                                  <li class="{{ (request()->is('user/live-now*')) ? 'active' : '' }}"><a href="{{route('live-now.index')}}">
                                    @if((request()->is('user/live-now*')))
                                    <img src="{{ asset('images/blue/live_now.svg')}}" />
                                    @else
                                    <img src="{{ asset('images/grey/live_now.svg')}}" />
                                    @endif 
                                   Live now</a></li>

                                  <li class="{{ (request()->is('user/influencer-list*')) ? 'active' : '' }}"><a href="{{route('influencer-list.index')}}">
                                    @if((request()->is('user/influencer-list*')))
                                        <img src="{{ asset('images/blue/list.svg')}}" />
                                    @else
                                        <img src="{{ asset('images/grey/list.svg')}}" />
                                    @endif 
                                  List of Influencers</a></li>
                                  <li class="{{ (request()->is('user/shop-list')) ? 'active' : '' }}"><a href="{{route('shop-list.index')}}">
                                    @if((request()->is('user/shop-list')))
                                        <img src="{{ asset('images/blue/list.svg')}}" />
                                    @else
                                        <img src="{{ asset('images/grey/list.svg')}}" />
                                    @endif 
                                  List of Shops</a></li>

                                  <li class="{{ (request()->is('user/media-list')) ? 'active' : '' }}"><a href="{{route('media-list.index')}}">
                                    @if((request()->is('user/media-list')))
                                        <img src="{{ asset('images/blue/list.svg')}}" />
                                    @else
                                        <img src="{{ asset('images/grey/list.svg')}}" />
                                    @endif 
                                  List of Media</a></li>

                                  <!-- <li class="add-an-influencer"><a href="{{route('influencer.create')}}"> ADD AN INFLUENCER</a></li>
                                  <li class="a-bug-or-an-idea"><a href="#"> A BUG OR AN IDEA ?</a></li> -->
                                 
                                  <li class="need-help">
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#need-help-modal">
                                         <img src="{{ asset('images/grey/help.svg')}}" />
                                      Need Help ?</a>
                                  </li>
                                  <li class="profilesetting {{ (request()->is('user/profile')) ? 'active' : '' }}"><a href="{{route('user.profile')}}"> 
                                    @if((request()->is('user/profile')))
                                        <img src="{{ asset('images/blue/profile.svg')}}" />
                                    @else
                                         <img src="{{ asset('images/grey/profile.svg')}}" />
                                    @endif 
                                    Profile Setting</a></li>
                                  <li class="logout"><a href="{{ route('user.logout')}}"><img src="{{ asset('images/grey/logout.svg')}}" /> Logout</a></li>
                              </ul>
                        @endif
                        </div>
                    </div>
                </div>	
    
                  @yield('content')

            </div>
        </div>

        <div id="need-help-modal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">     
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><h2>Need help</h2></h4>
                  </div>
                  <div class="modal-body">
                    <p>
                        If you need help from our technical or commercial support, we will give you 2 possibilities to have a quick answer. First you can try to click the little blue button (chat button) at the bottom right corner. If for any reasons it's not working or if you prefer to use mail instead, you can write us at : <a href="mailto:info@stalkinflu.com">info@stalkinflu.com</a>
                    </p>
                    <br />
                    <p>
                        Also, if you need to contact us by voice call, then we can schedule a call at your convenience. Please contact us from the 2 methods above, and we will try to schedule the call. Thanks you for using our app.
                    </p>
                  </div>
                  <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-blue assign-shop-btn">Submit</button>
                  </div> -->
            </div>
          </div>
        </div>
    
		 <!--jQuery JS-->
        <script src="{{ asset('js/jquery.min.js')}}"></script>
        <!--Bootstrap JS-->
        <script src="{{ asset('js/popper.min.js')}}"></script>
        
        
	    @yield('pagescript')

        <script src="{{ asset('js/bootstrap.min.js')}}"></script> 
 
         <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>

         <script type="text/javascript">
            $(function() {
                $('body').tooltip({selector: '[data-toggle="tooltip"]'});
            });

             $('body').append('<div class="pre-loding-image"><lottie-player src="https://assets3.lottiefiles.com/packages/lf20_XvpeBY/data.json" mode="bounce" background="rgba(255, 255, 255, 0.85)" speed="1" style="width: 600px; height: 600px;" loop autoplay></lottie-player></div>');
                $(window).on('load', function(){ 
                  setTimeout(removeLoader, 1000); 
                });
                function removeLoader(){
                    $( ".pre-loding-image" ).fadeOut(500, function() {
                      $( ".pre-loding-image" ).remove(); 
                  });  
                }
         </script>
    </body>
</html>
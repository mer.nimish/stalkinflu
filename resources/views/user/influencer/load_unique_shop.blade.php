@if(count($shops) > 0)
    @foreach($shops as $shop)
    <tr class="shop-row-link">
        <td class="text-center except add-list-checkbox">
           <div class="juiceboxes">
            <input type="checkbox" id="checked-unique-shop-{{$shop->shop_id}}" class="checked-shop-id" value="{{$shop->shop_id}}"> 
             <label for="checked-unique-shop-{{$shop->shop_id}}"></label>
          </div>
        </td>
        <td class="text-left website" data-shop-details-url="{{route('shop.show', $shop->shop_id)}}">
           <!--  <a href="#"><img class="addlist" src="{{ asset('images/addlist.png')}}"></a> -->
            <span title="{{$shop->website}}" data-toggle="tooltip" data-placement="top">{{ Str::limit($shop->website, 25) }}</span>
        </td>
        <td class="text-center" data-shop-details-url="{{route('shop.show', $shop->shop_id)}}">{{date("m/d/y", strtotime($shop->first_added_date)) }}</td>
        <td class="text-center" data-shop-details-url="{{route('shop.show', $shop->shop_id)}}">{{Helpers::time_elapsed_string($shop->last_seen_date) }}</td>
        <td class="text-center" data-shop-details-url="{{route('shop.show', $shop->shop_id)}}">
            @if($shop->activity == 'high')
                <span class="activity-color-red">{{$shop->promo_this_week}}</span>
            @elseif($shop->activity == 'medium')
                <span class="activity-color-green">{{$shop->promo_this_week}}</span>
            @else
               <span class="activity-color-yellow">{{$shop->promo_this_week}}</span>
            @endif
        </td>
         <!-- <td class="text-left instagram-profile-pic">
            @php
                $influencer_data = explode("--", $shop->influencer_data);
            @endphp
            @if(!empty($influencer_data[1]))
                <img src="{{asset(Config::get('constants.profile_image_thumb_url').$influencer_data[1])}}">
            @else
                <img src="{{asset('images/profile.png')}}">
            @endif
            <a href="https://www.instagram.com/{{$influencer_data[0]}}" target="_blank">{{$influencer_data[0]}}</a>
            @if($influencer_data[2])
                <img src="{{ asset('images/verified-badge.png')}}" class="verified-badge" />
            @endif
                 </td> -->
        <td class="text-center" data-shop-details-url="{{route('shop.show', $shop->shop_id)}}">{{ $shop->promo_this_week_by_this_inf }}</td>
        <td class="text-center" data-shop-details-url="{{route('shop.show', $shop->shop_id)}}">{{$shop->nb_of_promotions}}</td>
        <td class="text-center" data-shop-details-url="{{route('shop.show', $shop->shop_id)}}">{{$shop->total_promotions_by_this_inf}}</td>
        
        <td class="text-center" data-shop-details-url="{{route('shop.show', $shop->shop_id)}}"><img class="country-flag" src="{{asset('images/flags/'.Str::lower($shop->country_code).'.svg')}}" data-toggle="tooltip" data-placement="top" data-html="true" title="{{$shop->country_name}}"/></td>
        <td class="text-center" data-shop-details-url="{{route('shop.show', $shop->shop_id)}}">
           @if($shop->category_cnt != 0)
            @if(!empty($shop->categories))
              @php
              $categories = explode("<br />", $shop->categories);
              @endphp

              <span class="category-text">{{ $categories[0] }}</span>
              @if(count($categories) > 1)
                  @php
                    unset($categories[0]);
                  @endphp
                  <span class="category-cnt" data-toggle="tooltip" data-placement="top" data-html="true" title="{{implode('<br />', $categories)}}">+{{ $shop->category_cnt }}</span>
              @endif
            @endif
          @else
              <span class="category-text">Coming soon</span>
          @endif 
        </td>
        <!-- <td class="text-center">
            <a href="{{route('shop.show', $shop->shop_id)}}"  class="btn btn-primary btn-xs">show more</a>
        </td> -->
    </tr>
    @endforeach

    <tr>
      <td><a href="Javascript:void(0);" class="selected-deselected" id="select-all-unique-shop"><img src="{{ asset('images/-deselected.png')}}"></a></td>
        <td colspan="9" class="pagination-section">
            @if($shops->total() == $total_rec_cnt)
                <p class="pagination-info">{{ $total_rec_cnt }} shops
                </p>
            @else
                <p class="pagination-info">{{ $shops->total() }} out of {{$total_rec_cnt}} shops
                </p>
            @endif
            
            {!! $shops->links() !!} 
        </td>
        <td style="display: none;"></td>
    </tr>
@endif
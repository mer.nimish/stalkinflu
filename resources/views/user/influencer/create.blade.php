@extends('layout.main')

@section('pagestylesheet')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.css')}}">

@stop

@section('content')

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="create-emp promotedshops">

            <div class="topbar">
                <h3>Create Influencer</h3>
            </div>
            @if ($errors->any())
                <div class="error-contains">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="alert alert-danger text-danger"><i class="fa fa-close"></i> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="">
                <div class="panel-body">
                    <div class="col-md-3"></div>
                    <div class="form-wrap col-md-6">
                        
                        {!! Form::open(array('route' => 'influencer.store','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'create-influencer')) !!}
                           <!--  <div class="form-group">
                               <label class="control-label mb-10 col-sm-3" for="first_name">First Name:</label>
                               <div class="col-sm-9">
                                   <input type="text" class="form-control" id="first_name" name="first_name">
                               </div>
                           </div>
                           <div class="form-group">
                               <label class="control-label mb-10 col-sm-3" for="last_name">Last Name:</label>
                               <div class="col-sm-9">
                                   <input type="text" class="form-control" id="last_name" name="last_name">
                               </div>
                           </div> -->
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="instagram_id">Instagram ID:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="instagram_id" name="instagram_id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-3" for="instagram_id">Country:</label>
                                <div class="col-sm-9">
                                    <select name="country_id" id="country_id" class="form-control selectpicker">
                                        @foreach($countries as $country)
                                            <option value="{{$country->country_id}}" data-content="<img width='20' src='{{asset('images/flags/'.Str::lower($country->country_code).'.svg')}}' /> {{ $country->country_name}}">{{$country->country_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            
                            <div class="form-group mb-0">
                            <hr /> 
                                <div class="text-center">
                                  <button type="submit" class="btn btn-blue"><span class="btn-text">Submit</span></button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        
    </div>
</div>
</div>
                    
@endsection

@section('pagescript')

    <script type="text/javascript" src="{{ asset('js/jquery.validate.js')}}"></script>

    <script src="{{ asset('js/bootstrap-select.js')}}"></script>
    <script type="text/javascript">

    $(document).ready(function () {

        $('.selectpicker').selectpicker({
            tickIcon: 'fa fa-check'
        });

        $('#create-influencer').validate({ 
            rules: {
                instagram_id: {
                    required: true
                },
                country: {
                    required: true
                },
            },
            messages: {
                instagram_id:{
                    required: 'Please enter instagram id',
                },
                country:{
                    required: 'Please enter country',
                },
            },
            errorElement : 'div',
            /*errorPlacement: function(error, element){
                $(element).addClass('error');
            },*/
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
        });

    });
    </script>
@stop

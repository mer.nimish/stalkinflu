@if(count($shops) > 0)
    @foreach($shops as $shop)
        
    <div class="col-md-3 col-sm-4 col-xs-12">
        <div class="item">
            <div class="mediaheader">
               <a href="{{route('shop.show', $shop->shop_id)}}" target="_blank"> <span title="{{$shop->website}}" data-toggle="tooltip" data-placement="top">{{ Str::limit($shop->website, 18) }}</span> </a>
                <a href="#" onclick="select_shop_list('{{$shop->shop_id}}');"><img class="addlist" src="{{ asset('images/addlist.png')}}"></a>
            </div>
            <div class="image">
                @if(File::exists('public/storage/shop_media/main/'.$shop->shop_id.'.mp4'))
                    <video controls>
                      <source src="{{ asset('storage/shop_media/main/'.$shop->shop_id.'.mp4')}}" type="video/mp4">
                      Your browser does not support the video tag.
                    </video>
                @else
                    <img src="{{asset('images/no-video.png') }}" width="240" height="368" />
                @endif
                
            </div>
            <div class="mediacontent">
                <div class="countryinfo">
                    <img class="countryicon" src="{{asset('images/flags/'.Str::lower($shop->country_code).'.svg')}}" data-toggle="tooltip" data-placement="top" data-html="true" title="{{$shop->country_name}}"/>
                    <span class="story">{{$shop->nb_of_story}} stories</span>
                    <!-- <a href="#"><img class="addlist" src="{{ asset('images/addlist.png')}}"></a> -->
                    <span class="today">{{Helpers::time_elapsed_string($shop->created_at)}}</span>
                </div>
                <!-- <div class="info">
                    <span>info</span>
                    <span>{{$shop->nb_of_story}} stories</span>
                    <span>Today</span>
                </div> -->
            </div>
        </div>
    </div>

    @endforeach

    <div class="pagination-section">
        @if($shops->total() == $total_rec_cnt)
            <p class="pagination-info">{{ $total_rec_cnt }} promotions
            </p>
        @else
            <p class="pagination-info">{{ $shops->total() }} out of {{$total_rec_cnt}} promotions
            </p>
        @endif

        {!! $shops->links() !!} 
    </div>

@else
    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
        Not Media Found.
    </div>
@endif


@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <!--alerts CSS -->
    <link href="{{ asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.css')}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
@stop
    
<div class="col-md-9 col-sm-9 col-xs-12 no-padding influencersection">
    <div class="middlepart">
        <div class="promotedshops">
            <div class="topbar">
                @if(isset($name_of_the_list))
                    <h3>{{$name_of_the_list}} <span class="influencer-list-favourite" data-influencer-list-id="{{$influencer_list_id}}" data-toggle="tooltip" data-placement="top" data-html="true" title="If you want to pin a favorite influencer list then show this influencer list in your dashboard."><i class="fa <?=($is_favourite == 1) ? 'fa-heart' : 'fa-heart-o';?> "></i></span></h3>
                @else
                    <h3>Our Influencer's database</h3>
                @endif
                 <div class="sortby">
                    <label>SORT BY:</label>
                     <select name="sort_by" id="sort_by" class="selectpicker" data-iconBase="fa">
                        <option data-sorting-type="asc" data-column-name="engagement_rate" data-icon="fa fa-arrow-up sort-icon">By engagement rate</option>
                        <option data-sorting-type="desc" data-column-name="engagement_rate" data-icon="fa fa-arrow-down sort-icon">By engagement rate</option>
                        <option data-sorting-type="asc" data-column-name="country_name" data-icon="fa fa-arrow-up sort-icon">By country</option>
                        <option data-sorting-type="desc" data-column-name="country_name" data-icon="fa fa-arrow-down sort-icon">By country</option>
                        <option data-sorting-type="asc" data-column-name="followers" data-icon="fa fa-arrow-up sort-icon">By followers</option>
                        <option data-sorting-type="desc" data-column-name="followers" data-icon="fa fa-arrow-down sort-icon">By followers</option>
                       <!--  <option data-sorting-type="asc" data-column-name="lastest_promo" data-icon="fa fa-arrow-up sort-icon">By latest promo date</option>
                       <option data-sorting-type="desc" data-column-name="lastest_promo" data-icon="fa fa-arrow-down sort-icon">By latest promo date</option> -->
                        <option data-sorting-type="asc" data-column-name="promo_this_week" data-icon="fa fa-arrow-up sort-icon">By Promotions this week</option>
                        <option data-sorting-type="desc" data-column-name="promo_this_week" data-icon="fa fa-arrow-down sort-icon">By Promotions this week</option>
                        <option data-sorting-type="asc" data-column-name="categories" data-icon="fa fa-arrow-up sort-icon">By categories/niches</option>
                        <option data-sorting-type="desc" data-column-name="categories" data-icon="fa fa-arrow-down sort-icon">By categories/niches</option>
                        <option data-sorting-type="asc" data-column-name="avg_promo_shops" data-icon="fa fa-arrow-up sort-icon">By average promo/shop</option>
                        <option data-sorting-type="desc" data-column-name="avg_promo_shops" data-icon="fa fa-arrow-down sort-icon">By average promo/shop</option>
                    </select>
                </div>
            </div>

             <div class="searchwebsite">
                <label>Search an Influencer :</label>
                <input type="text" name="search_instagram_id" id="search_instagram_id" placeholder="ENTER AN INSTAGRAM ID" onfocus="this.placeholder = ''" onblur="this.placeholder = 'ENTER AN INSTAGRAM ID'">
                
            </div>
            <div class="error-contains">
            @if ($message = Session::get('success'))
                <div class="alert alert-success text-success">
                    <i class="fa fa-check"></i> {{ $message }}
                </div>
            @endif
            </div>
            <div class="showonly">
                <label>Show only: </label>
                <div class="showonlylist">
                     <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'COUNTRY')">WORLD</a>
                    <!--  <select class="selectpicker" multiple id="filter_country_ids">
                       @foreach($countries as $country)
                         <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.png')}}' /> {{ $country->country_name}}"></option>
                       @endforeach
                                    </select> -->
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'PROMOTION_THIS_WEEK')">PROMOTION THIS WEEK</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'ENGAGEMENT_RATE')">ENGAGEMENT RATE</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'FOLLOWERS')">FOLLOWERS</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'LATEST_PROMO_DATE')">LATEST PROMO DATE</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'NICHES')">NICHES</a>
                    <input type="hidden" id="is_verified" value="0">
                    <a class="verified-filter"><img src="{{ asset('images/verified.png')}}"></a>
                </div>
            </div>
            <div class="filter-tab-content">
                <div id="COUNTRY" class="tabcontent">
                    <div class="col-md-2">
                        <select class="selectpicker" multiple id="filter_country_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.svg')}}' /> {{ $country->country_name}}"></option>
                        @endforeach
                        </select>
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="country_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div> 

                <div id="PROMOTION_THIS_WEEK" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="promo_this_week_from" id="promo_this_week_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="promo_this_week_to" id="promo_this_week_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="promo_this_week_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="ENGAGEMENT_RATE" class="tabcontent">
                     <div class="col-md-3">
                        <input type="text" name="engagement_rate_from" id="engagement_rate_from" placeholder="FROM" class="only-number-allowed" maxlength='2'> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="engagement_rate_to" id="engagement_rate_to" placeholder="TO" class="only-number-allowed" maxlength='3'>
                    </div>
                     <!-- <div class="col-md-3">
                        <button id="engagement_rate_filter" class="filter-btn">FILTER</button>
                                         </div> -->
                </div>
                <div id="FOLLOWERS" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="followers_from" id="followers_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="followers_to" id="followers_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="followers_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="LATEST_PROMO_DATE" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="latest_promo_date_from" id="latest_promo_date_from" placeholder="FROM"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="latest_promo_date_to" id="latest_promo_date_to" placeholder="TO">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="latest_promo_date_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="NICHES" class="tabcontent">
                    <div class="col-md-4">
                        <select class="selectpicker" multiple id="filter_category_ids">
                        @foreach($categories as $category)
                          <option value="{{ $category->category_id}}"> {{ $category->category_name}}</option>
                        @endforeach
                        </select> 
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="category_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
            </div>
            <div class="activated-filter showonly">
                <label>Activated filters: </label>
                <span class="country-filter-contains"></span>
                <span class="promo-this-week-filter-contains"></span>
                <span class="engagement-rate-filter-contains"></span>
                <span class="followers-filter-contains"></span>
                <span class="latest-promo-date-filter-contains"></span>
                <span class="category-filter-contains"></span>
                <span class="verified-filter-contains"></span>
            </div>
    </div>
    <div class="promotedshops-listing">

                <div class="shopslistings">
                    <div class="shopslistingsinner">
                        <table id="shoptable" class="table display nowrap">
                        <thead>
                            <tr>
                                <th width="2%">
                                    <a href="Javascript:void(0);" onclick="select_influencer_list()"><img class="assign-list-btn" src="{{ asset('images/icons8-add_new.svg')}}" /></a>
                                </th>
                               <!--  <th class="sorting text-center" data-sorting_type="asc" data-column_name="country_name">Country <span id="country_name_icon"></span></th> -->
                                <!-- <th class="text-center">Rank</th> -->
                                <th class="text-center">Instagram ID</th>
                                <th class="text-center">Country</th>
                                <th class="text-center">Followers</th>
                                <!-- <th class="text-center">Latest promo</th> -->
                                <th class="text-center">Average promo/shop</th>
                                <th class="text-center">Eng. rate</th>
                                <!-- <th class="text-center">Lastest website promoted</th> -->
                                <th class="text-center">Promo this week</th>
                               <!--  <th class="text-center">Most promoted Shop this week</th> -->
                                 <th class="text-center no-sort">CATEGORIES/NICHES</th>
                            </tr>
                        </thead>
                            @if (count($influencers) > 0)
                            <tbody class="influencers_tr">
                                 @include('user.influencer.load_influencer')
                             </tbody>   
                            @endif
                        </table>
  
                    </div>
                    </div>
        </div>
    </div>
</div>
<div id="assign-influencer-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">     
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Assign influencer into list</h4>
          </div>
          <div class="modal-body">
            
            <input type="hidden" id="assign_influencer_id">
            <div class="form-group row">
                <label class="control-label col-sm-2">List Name:</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" id="assign_influencer_list_id" multiple=""> 
                        <option value="" disabled="">Select Influencer List</option>
                        @foreach($influencer_list as $list)
                         <option value="{{$list->influencer_list_id}}">{{$list->list_name}}</option>
                        @endforeach
                    </select>
                    <div class="assign-influencer-msg text-success"></div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-blue assign-influencer-btn">Submit</button>
          </div>
    </div>
  </div>
</div>
                    
@endsection

@section('pagescript')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
     <!-- Sweet-Alert  -->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap-select.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.js')}}"></script>

    <script type="text/javascript">
        function openTab(evt, tabName) {
            const div = evt.currentTarget;
              if(div.classList.contains('active')){
                 evt.currentTarget.className = "tablinks";
                 document.getElementById(tabName).style.display = "none";
              } else {
                  var i, tabcontent, tablinks;
                  tabcontent = document.getElementsByClassName("tabcontent");
                  for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                  }
                  tablinks = document.getElementsByClassName("tablinks");
                  for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                  }
                  document.getElementById(tabName).style.display = "block";
                  evt.currentTarget.className += " active";
              }
        }
    </script>
    <script type="text/javascript">

    function select_influencer_list(){
        //$("#assign_influencer_id").val(influencer_id);
        if($('input.checked-influencer-id:checked').length > 0){
            $("#assign-influencer-modal").modal("show");
        } else {
            alert("Please select at least one influencer.");
        }
    }

    $(function() {

         $(document).on("click", ".influencer-list-favourite", function(){
                $(".influencer-list-favourite i").toggleClass('fa-heart fa-heart-o');
                var influencer_list_id = $(this).data("influencer-list-id");
                $.ajax({
                    url : "{{route('user.favourite_influencer_list')}}",
                    data : { influencer_list_id: influencer_list_id},
                    dataType: 'json',
                }).done(function (data) { 
                    //$('.shops_tr').html(data.body);
                });
          });

        $(document).on("click", "#select-all-inf", function() { 
            var $img = $(this).find('img') ,
                src = $img.attr('src') ,
                onCheck = /\-deselected\.png$/ ,
                offCheck = /\-selected\.png$/ ,
                normal = /\.png$/
            ;

            if(src.match(onCheck)) {
                $img.attr('src', src.replace(onCheck, '-selected.png'));
                $(".checked-influencer-id").prop('checked', true);
            } else if (src.match(offCheck)) {
                $img.attr('src', src.replace(offCheck, '-deselected.png'));
                $(".checked-influencer-id").prop('checked', false);
            } else {
                $img.attr('src', src.replace(normal, '-deselected.png'));
                 $(".checked-influencer-id").prop('checked', false);
            }
        });

        $(document).on('click', '.assign-influencer-btn', function(e) { 

            if($("#assign_influencer_list_id").val() != null){
                var influencer_ids = $('input.checked-influencer-id:checked').map( function () { 
                    return $(this).val();
                }).get();
               // var shop_ids = $("#assign_shop_ids").val();
                var influencer_list_ids = $("#assign_influencer_list_id").val();
                $.ajax({
                    url : "{{route('user.assign_influencer')}}",
                    data : { influencer_list_ids:influencer_list_ids, influencer_ids:influencer_ids},
                }).done(function (data) { 
                    $('.assign-influencer-msg').html('Influencer assigned successfully.');
                    $('input.checked-influencer-id').prop('checked', false);
                });
            } else {
                alert("Please select at least one influencer list.");
            }
        });

        $(document).on('click', '.influencer-row-link td:not(.except)', function(){ 
            //window.location.href = $(this).data("influencer-details-url");
            window.open($(this).data("influencer-details-url"), '_blank');
        });

        $('.selectpicker').selectpicker({
            tickIcon: 'fa fa-check'
        });

        var url = $(this).attr('href');

        $('#filter_category_ids').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'SELECT CATEGORY'
        });
        $("button[data-id=filter_category_ids]").html("SELECT CATEGORY");
        $('#filter_category_ids').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).text()+' '+'<a href="Javascript:void(0);" class="remove-category" data-category-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .category-filter-contains").html(results);
            $("button[data-id=filter_category_ids]").html("SELECT CATEGORY");

            var url = $(this).attr('href');
            getInfluencer(url);
        });
        
        $(document).on('click', '.category-filter-contains .item .remove-category', function(e) { 
            var wanted_id = $(this).data("category-id"); 
            var wanted_option = $('#filter_category_ids option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#filter_category_ids').selectpicker('refresh');
            $(this).parent().remove(); 

            getInfluencer(url); 
        });

        $('#filter_country_ids').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'SELECT COUNTRY'
        });
        $("button[data-id=filter_country_ids]").html("SELECT COUNTRY");
        $('#filter_country_ids').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).data('content')+' '+'<a href="Javascript:void(0);" class="remove-country" data-country-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .country-filter-contains").html(results);
            $("button[data-id=filter_country_ids]").html("SELECT COUNTRY");

            var url = $(this).attr('href');
            getInfluencer(url);
        });
        
        $(document).on('click', '.country-filter-contains .item .remove-country', function(e) { 
            var wanted_id = $(this).data("country-id"); 
            var wanted_option = $('#filter_country_ids option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#filter_country_ids').selectpicker('refresh');
            $(this).parent().remove(); 

            getInfluencer(url); 
        });
        $(document).on('click', '.promo-this-week-filter-contains .item .remove-promo-this-week', function(e) { 
            $(this).parent().remove(); 
            $("#promo_this_week_from").val('');
            $("#promo_this_week_to").val('');
            getInfluencer(url); 
        });
        $(document).on('click', '.engagement-rate-filter-contains .item .remove-engagement-rate', function(e) { 
            $(this).parent().remove();
            $("#engagement_rate_from").val('');
            $("#engagement_rate_to").val('');
            getInfluencer(url);  
        });
        $(document).on('click', '.followers-filter-contains .item .remove-followers', function(e) { 
            $(this).parent().remove();
            $("#followers_from").val('');
            $("#followers_to").val('');
            getInfluencer(url);  
        });
        $(document).on('click', '.latest-promo-date-filter-contains .item .remove-latest-promo-date', function(e) { 
            $(this).parent().remove();
            $("#latest_promo_date_from").val('');
            $("#latest_promo_date_to").val('');
            getInfluencer(url);  
        });
 
        $("#latest_promo_date_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');

                //$('#latest_promo_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#latest_promo_date_to').datepicker('option', 'minDate', minDate);
                latest_promo_date_change();
            }
        });
        $("#latest_promo_date_to").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () {
                latest_promo_date_change();
            }
        });

        $(document).on('click', '.pagination a', function(e) { 
           
            e.preventDefault();
            var url = $(this).attr('href');
            getInfluencer(url);
            window.history.pushState("", "", url);
        });

        $(document).on('change', '#sort_by', function(e) {
           // e.preventDefault(); 
            var url = $(this).attr('href');
            getInfluencer(url) 
        });

        /*$(document).on('click', '#country_filter', function(e) { 
            if($("#filter_country_ids").val() != null){
                var url = $(this).attr('href');
                getInfluencer(url);
            }
        });*/

        $('.only-number-allowed').bind('keyup paste', function(){
                this.value = this.value.replace(/[^0-9]/g, '');
        });

        //$(document).on('click', '#engagement_rate_filter', function(e) {
        $(document).on( "keyup", "#engagement_rate_from, #engagement_rate_to", function( e ) {
            if($("#engagement_rate_from").val() != '' && $("#engagement_rate_to").val() != ''){
                $(".activated-filter .engagement-rate-filter-contains").html('<span class="item">ENG. RATE '+$("#engagement_rate_from").val()+' TO '+$("#engagement_rate_to").val()+' '+'<a href="Javascript:void(0);" class="remove-engagement-rate"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else if($("#engagement_rate_from").val() != ''){
                $(".activated-filter .engagement-rate-filter-contains").html('<span class="item">ENG. RATE > '+$("#engagement_rate_from").val()+' '+'<a href="Javascript:void(0);" class="remove-engagement-rate"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else if($("#engagement_rate_to").val() != ''){
                $(".activated-filter .engagement-rate-filter-contains").html('<span class="item">ENG. RATE < '+$("#engagement_rate_to").val()+' '+'<a href="Javascript:void(0);" class="remove-engagement-rate"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else {
                $(".activated-filter .engagement-rate-filter-contains").html('');
            }
        });

       // $(document).on('click', '#followers_filter', function(e) {
        $(document).on( "keyup", "#followers_from, #followers_to", function( e ) {
            if($("#followers_from").val() != '' && $("#followers_to").val() != ''){
                $(".activated-filter .followers-filter-contains").html('<span class="item">FOLLOWERS '+$("#followers_from").val()+' TO '+$("#followers_to").val()+' '+'<a href="Javascript:void(0);" class="remove-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else if($("#followers_from").val() != ''){
                $(".activated-filter .followers-filter-contains").html('<span class="item">FOLLOWERS > '+$("#followers_from").val()+' <a href="Javascript:void(0);" class="remove-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else if($("#followers_to").val() != ''){
                $(".activated-filter .followers-filter-contains").html('<span class="item">FOLLOWERS < '+$("#followers_to").val()+' <a href="Javascript:void(0);" class="remove-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else {
                $(".activated-filter .followers-filter-contains").html('');
            }
        });

        //$(document).on('click', '#promo_this_week_filter', function(e) {
        $(document).on( "keyup", "#promo_this_week_from, #promo_this_week_to", function( e ) {
            if($("#promo_this_week_from").val() != '' && $("#promo_this_week_to").val() != ''){
                $(".activated-filter .promo-this-week-filter-contains").html('<span class="item">PROMO THIS WEEK '+$("#promo_this_week_from").val()+' TO '+$("#promo_this_week_to").val()+' '+'<a href="Javascript:void(0);" class="remove-promo-this-week"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else if($("#promo_this_week_from").val() != ''){
                $(".activated-filter .promo-this-week-filter-contains").html('<span class="item">PROMO THIS WEEK > '+$("#promo_this_week_from").val()+' '+'<a href="Javascript:void(0);" class="remove-promo-this-week"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else if($("#promo_this_week_to").val() != ''){
                $(".activated-filter .promo-this-week-filter-contains").html('<span class="item">PROMO THIS WEEK < '+$("#promo_this_week_to").val()+' '+'<a href="Javascript:void(0);" class="remove-promo-this-week"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else {
                $(".activated-filter .promo-this-week-filter-contains").html('');
            }
        });

        //$(document).on('click', '#latest_promo_date_filter', function(e) {
        //$(document).on( "change keyup", "#latest_promo_date_from, #latest_promo_date_to", function( e ) {  
         function latest_promo_date_change(){ 
            if($("#latest_promo_date_from").val() != '' && $("#latest_promo_date_to").val() != ''){
                $(".activated-filter .latest-promo-date-filter-contains").html('<span class="item">PROMO THIS WEEK '+$("#latest_promo_date_from").val()+' TO '+$("#latest_promo_date_to").val()+' '+'<a href="Javascript:void(0);" class="remove-latest-promo-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else if($("#latest_promo_date_from").val() != ''){
                $(".activated-filter .latest-promo-date-filter-contains").html('<span class="item">PROMO THIS WEEK > '+$("#latest_promo_date_from").val()+' '+'<a href="Javascript:void(0);" class="remove-latest-promo-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else if($("#latest_promo_date_to").val() != ''){
                $(".activated-filter .latest-promo-date-filter-contains").html('<span class="item">PROMO THIS WEEK < '+$("#latest_promo_date_to").val()+' '+'<a href="Javascript:void(0);" class="remove-latest-promo-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else {
                $(".activated-filter .latest-promo-date-filter-contains").html('');
            } 
        }

        $(document).on('click', '.verified-filter', function(e) {
            $(".activated-filter .verified-filter-contains").html('<span class="item"><img src="{{ asset('images/verified.png')}}" width="20"/> '+'<a href="Javascript:void(0);" class="remove-verified"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            $("#is_verified").val(1);
            var url = $(this).attr('href');
            //var action = 'is_verified';
            getInfluencer(url);
        });
        $(document).on('click', '.verified-filter-contains .item .remove-verified', function(e) { 
            $(this).parent().remove();
            $("#is_verified").val(0);
            getInfluencer(url);  
        });

        /*$(document).on('click', '.favourite', function(e) {
            var url = $(this).attr('href');
            var action = 'is_favourite';
            getInfluencer(url, action);
        });*/

        /*$('#search_instagram_id').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                var url = $(this).attr('href');
                getInfluencer(url);
            }
        });*/
        $(document).on( "keyup", "#search_instagram_id", function( e ) {
            e.stopImmediatePropagation();
            var url = $(this).attr('href');
            getInfluencer(url);
        });

        /*$(document).on('click', '.sorting', function(){
              var sort_by = $(this).data('column_name');
              var order_type = $(this).data('sorting_type');
              var sort_type = '';
              if(order_type == 'asc')
              {
                   $(this).data('sorting_type', 'desc');
                   sort_type = 'desc';
                   $('#'+sort_by+'_icon').html('<span class="fa fa-arrow-down"></span>');
              }
              if(order_type == 'desc')
              {
              $(this).data('sorting_type', 'asc');
                   sort_type = 'asc';
                   $('#'+sort_by+'_icon').html('<span class="fa fa-arrow-up"></span>');
              }

              var url = $(this).attr('href');
              getInfluencer(url, sort_by, sort_type) 
         });*/

        function getInfluencer(url, action="") 
        { 
            var sort_by = $("#sort_by").find(':selected').attr('data-column-name');
            var sort_type =  $("#sort_by").find(':selected').attr('data-sorting-type');
            var search_instagram_id = $("#search_instagram_id").val();
            var filter_country_ids = $("#filter_country_ids").val();
            var eng_rate_from = $("#engagement_rate_from").val();
            var eng_rate_to = $("#engagement_rate_to").val();
            var followers_from = $("#followers_from").val();
            var followers_to = $("#followers_to").val();
            var promo_this_week_from = $("#promo_this_week_from").val();
            var promo_this_week_to = $("#promo_this_week_to").val();
            var latest_promo_date_from = $("#latest_promo_date_from").val(); 
            var latest_promo_date_to = $("#latest_promo_date_to").val(); 
            var filter_category_ids = $("#filter_category_ids").val();
            var is_verified = $("#is_verified").val();
             var flag = '{{Request::get("flag")}}';
            /*var is_favourite = 0;
            if(action != '' && action == 'is_favourite'){
                is_favourite = 1;
            } */ 
           
            $.ajax({
                url : url,
                data : { search_instagram_id:search_instagram_id, sort_type:sort_type, sort_by:sort_by, filter_country_ids:filter_country_ids, eng_rate_from:eng_rate_from, eng_rate_to:eng_rate_to, followers_from:followers_from, followers_to:followers_to, promo_this_week_from: promo_this_week_from, promo_this_week_to:promo_this_week_to, latest_promo_date_from:latest_promo_date_from, latest_promo_date_to:latest_promo_date_to, filter_category_ids:filter_category_ids, is_verified:is_verified, flag:flag},
                dataType: 'json',
            }).done(function (data) { 
                $('.influencers_tr').html(data.body);
            });
        }
    });
    </script>
    <script>
        $(document).ready(function() {
            /*$('#shoptable').DataTable( {
                searching: false,
                ordering:  true,
                "language": {
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'><i/>",
                        "next": "<i class='fa fa-angle-right'><i/>",

                    }
                },
            } );*/
        } );
    </script>
@stop
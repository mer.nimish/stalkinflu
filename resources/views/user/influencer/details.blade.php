@extends('layout.main')
@section('content')

@section('pagestylesheet')
    <link href="{{ asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.css')}}">
 
     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
@stop

<div class="col-md-9 col-sm-12 col-xs-12 col-lprp-0 influencer-details-page"> 

<div class="col-md-8 col-sm-8 col-xs-12 no-padding col-lprp-0">
    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
        <div class="middlepart">
            <div class="shop-details-back">
                <a href="{{route('influencer.index')}}">BACK</a>
            </div>
            <div class="shop-details-head text-cetner">
                <a href="https://instagram.com/{{$influencer->instagram_id}}" target="_blank">
                @if(!empty($influencer->profile_pic))
                    <img src="{{asset(Config::get('constants.profile_image_thumb_url').$influencer->profile_pic)}}">
                @else
                    <img src="{{asset('images/profile.png')}}">
                @endif
            {{ $influencer->instagram_id }}</a>

            <select id="add_to_influencer_list" class="selectpicker">
                <option><img src="{{ asset('images/addlist.png')}}" width="17" /></option>
                @foreach($influencer_list as $list)
                 <option value="{{$list->influencer_list_id}}">{{$list->list_name}}</option>
                @endforeach
            </select>    
           
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
    <div class="middlepart">
            <div class="shopdetails-webinfo">
                <h3 class="sectitle">Content:</h3>
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Uploads</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{Helpers::short_number_format($influencer->uploads)}}</p>
                    </div>
                </div>
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Photo vs. video</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{$influencer->photos}}% vs. {{$influencer->videos}}%</p>
                    </div>
                </div>
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Latest Post</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{Helpers::time_elapsed_string($influencer->last_post_date)}}</p>
                    </div>
                </div>
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Frequency</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{number_format($influencer->frequency,1)}}/week</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
        <div class="middlepart inf-profile-pic-section">
            @if(!empty($influencer->profile_pic))
                <img src="{{asset(Config::get('constants.profile_image_url').$influencer->profile_pic)}}">
            @else
                <img src="{{asset('images/profile.png')}}">
            @endif
           <!-- <div class="shopdetails-webinfo community-eng">
                <h3 class="sectitle">Community engagement:</h3>
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Likes Consistency</p>
                    </div>
                </div>
                <div class="item">
                    <div class="webinfopromos">
                        <p>{{Helpers::short_number_format($influencer->likes_consistency)}}</p>
                    </div>
                </div>
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Comments Consistency</p>
                    </div>
                </div>
                <div class="item">
                    <div class="webinfopromos">
                        <p>{{Helpers::short_number_format($influencer->comments_consistency)}}</p>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
        <div class="statisctic">
                <h3>Statistics 
                @if($influencer->followers >= 0 && $influencer->followers <= 100000)
                    <span>micro influencerr</span>
                @elseif($influencer->followers >= 100000 && $influencer->followers <= 1000000)
                    <span>macro influencer</span>
                @else
                    <span>mega influencer</span>
                @endif
                </h3>
                <div class="statiscticitem followers">
                    <div class="icon">
                        <img src="{{asset('images/influencers.png')}}">
                    </div>
                    <div class="info">
                        <h4>{{Helpers::short_number_format($influencer->followers)}}</h4>
                        <span>Followers</span>
                    </div>
                </div>
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{$influencer->engagement_rate}}%</h4>
                        <span>Eng. rate</span>
                    </div>
                </div>
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{Helpers::short_number_format($influencer->avg_comments)}}</h4>
                        <span>Average coms.</span>
                    </div>
                </div>
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{Helpers::short_number_format($influencer->avg_likes)}}</h4>
                        <span>Average likes</span>
                    </div>
                </div>
            </div>
        </div>
</div>

<div class="col-md-4 col-sm-4 col-xs-12 no-padding">
    <div class="middlepart">
      <div class="shopdetails-webinfo top-promo-of-the-week">
            <h3 class="sectitle">Top promo of the week ({{$top_promo_of_week_cnt}}):</h3>
            @foreach($top_promo_of_week as $top_promo)
            <div class="item">
                <div class="site">
                    <a href="{{$top_promo->website}}" target="_blank">{{Str::limit($top_promo->website, 18)}}</a>
                </div>
                <div class="promotions">
                    <p>{{$top_promo->promotions}} promotions</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
<div class="stories-and-promotion">
        <h3>Stories and promotions informations for <span>{{$influencer->instagram_id}}</span>:</h3>
        <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
            <div class="statiscticitem">
                <div class="info">
                    <h4>{{number_format($frequency_of_promotions_week,1)}}/week</h4>
                    <span>Frequency of promotions (week)</span>
                </div>
            </div>
            <div class="statiscticitem">
                <div class="info">
                    <h4>{{number_format($frequency_of_promotions_mon,1)}}/month</h4>
                    <span>Frequency of promotions (month)</span>
                </div>
            </div>
            <div class="statiscticitem">
                <div class="info">
                    <a href="{{route('shop.show',$most_promoted_shop_ever_shop_id)}}" target="_blank" title="{{$most_promoted_shop_ever}}" data-toggle="tooltip" data-placement="top"><h4>{{Str::limit($most_promoted_shop_ever, 18)}}</h4></a>
                    <span>Most promoted shop ever</span>
                </div>
            </div>
            <div class="statiscticitem">
                <div class="info">
                    <a href="{{route('shop.show',$latest_promoted_shop_shop_id)}}" target="_blank" title="{{$latest_promoted_shop}}" data-toggle="tooltip" data-placement="top"><h4>{{Str::limit($latest_promoted_shop, 18)}}</h4></a>
                    <span>Latest promoted shop</span>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
            <div class="statiscticitem">
                <div class="info">
                    <h4>{{$total_promotion_in_database}}</h4>
                    <span>Total promotion in our database</span>
                </div>
            </div>
            <div class="statiscticitem">
                <div class="info">
                    <h4>{{$date_of_latest_promotion}}</h4>
                    <span>Date of latest promotion</span>
                </div>
            </div>
            <div class="statiscticitem">
                <div class="info">
                    <a href="{{route('shop.show',$most_promoted_shop_week_shop_id)}}" target="_blank" title="{{$most_promoted_shop_week}}" data-toggle="tooltip" data-placement="top"><h4>{{Str::limit($most_promoted_shop_week, 18)}}</h4></a>
                    <span>Most promoted shop this week</span>
                </div>
            </div>
            <div class="statiscticitem">
                <div class="info">
                    <a href="{{route('shop.show',$most_promoted_shop_month_shop_id)}}" target="_blank" title="{{$most_promoted_shop_month}}" data-toggle="tooltip" data-placement="top"><h4>{{Str::limit($most_promoted_shop_month, 18)}}</h4></a>
                    <span>Most promoted shop this month</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
    <div class="rentability-analyser">
        <h3><span>RENTABILITY</span> ANALYSER : </h3>
        <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
            <div class="col-md-4">
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{round($multiple_promotions_indicator, 1)}}%</h4>
                        <span>MULTIPLE PROMO INDICATOR</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{number_format($average_promo_shop,1)}}/shop</h4>
                        <span>AVERAGE PROMO PER SHOP</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{number_format($average_promo_day,1)}}/day</h4>
                        <span>FREQUENCY OF PROMO PER DAY</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 no-padding shops-with-media-sections">
    <div class="middlepart">
        <div class="promotedshops">
            
            <div class="topbar">
                <h3>All promotions of <span>{{$influencer->instagram_id}} WITH MEDIAS:</span></h3>
                <div class="sortby">
                    <label>SORT BY:</label>
                    <select name="sort_by_with_media" id="sort_by_with_media" class="selectpicker">
                        <option data-sorting-type="asc" data-column-name="first_added_date" data-icon="fa fa-arrow-up sort-icon">By first added</option>
                        <option data-sorting-type="desc" data-column-name="first_added_date" data-icon="fa fa-arrow-down sort-icon">By first added</option>
                         <option data-sorting-type="asc" data-column-name="country_name" data-icon="fa fa-arrow-up sort-icon">By market</option>
                        <option data-sorting-type="desc" data-column-name="country_name" data-icon="fa fa-arrow-down sort-icon">By market</option>
                        <option data-sorting-type="asc" data-column-name="nb_of_story" data-icon="fa fa-arrow-up sort-icon">By NB of Story</option>
                        <option data-sorting-type="desc" data-column-name="nb_of_story" data-icon="fa fa-arrow-down sort-icon">By NB of Story</option>
                         <option data-sorting-type="asc" data-column-name="created_at" data-icon="fa fa-arrow-up sort-icon">By promotion date</option>
                        <option data-sorting-type="desc" data-column-name="created_at" data-icon="fa fa-arrow-down sort-icon">By promotion date</option>
                       <!--  <option data-sorting-type="asc" data-column-name="last_seen_date" data-icon="fa fa-arrow-up sort-icon">By last seen</option>
                        <option data-sorting-type="desc" data-column-name="last_seen_date" data-icon="fa fa-arrow-down sort-icon">By last seen</option> -->
                    </select> 
                   
                </div>
            </div>
            <div class="searchwebsite">
                <label>Search a influencer:</label>
                <input type="text" name="search_influencer" id="search_influencer" placeholder="ENTER AN INSTAGRAM ID" autocomplete="false" onfocus="this.placeholder = ''" onblur="this.placeholder = 'ENTER AN INSTAGRAM ID'">
            </div>

            <div class="showonly">
                <label>Show only: </label>
                <div class="showonlylist">
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'MARKET')">WORLD</a>
                    <!-- <select class="selectpicker" multiple id="with_media_filter_country_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.png')}}' /> {{ $country->country_name}}"></option>
                        @endforeach
                    </select> -->

                     <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'FIRST_ADDED_LAST_WEEK')">FIRST ADDED</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'LAST_SEEN_TODAY')">LAST SEEN</a>

                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'PROMOTION_DATE_RANGE')">PROMOTION DATE</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'NB_OF_STORY')">NB OF STORY</a>
                </div>
            </div>
        
             <div class="filter-tab-content">
                <div id="MARKET" class="tabcontent">
                    <div class="col-md-2">
                        <select class="selectpicker" multiple id="with_media_filter_country_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.svg')}}' /> {{ $country->country_name}}"></option>
                        @endforeach
                        </select>
                    </div>
                   <!--  <div class="col-md-3">
                       <button id="country_filter_with_media" class="filter-btn">FILTER</button>
                   </div> -->
                </div>

                <div id="PROMOTION_DATE_RANGE" class="tabcontent">
                     <div class="col-md-3">
                        <input type="text" name="promotion_date_from" id="promotion_date_from" placeholder="FROM" class="only-number-allowed datepicker"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="promotion_date_to" id="promotion_date_to" placeholder="TO" class="only-number-allowed datepicker">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="promotion_date_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="NB_OF_STORY" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="nb_of_story_from" id="nb_of_story_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="nb_of_story_to" id="nb_of_story_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="nb_of_story_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                 <div id="FIRST_ADDED_LAST_WEEK" class="tabcontent">
                        <div class="col-md-3">
                            <input type="text" name="with_media_first_added_from" id="with_media_first_added_from" placeholder="FROM"> 
                        </div>
                        <div class="col-md-3">
                            <input type="text" name="with_media_first_added_to" id="with_media_first_added_to" placeholder="TO">
                        </div>
                    </div>
                    <div id="LAST_SEEN_TODAY" class="tabcontent">
                         <div class="col-md-3">
                            <input type="text" name="with_media_last_seen_from" id="with_media_last_seen_from" placeholder="FROM"> 
                        </div>
                        <div class="col-md-3">
                            <input type="text" name="with_media_last_seen_to" id="with_media_last_seen_to" placeholder="TO">
                        </div>
                    </div>
            </div>
            <div class="activated-filter showonly">
                <label>Activated filters: </label>
                <span class="with-media-country-filter-contains"></span>
                <span class="with-media-first-added-filter-contains"></span>
                <span class="with-media-last-seen-filter-contains"></span>
                <span class="with-media-promotion-date-range-filter-contains"></span>
                <span class="with-media-nb-of-story-filter-contains"></span>
            </div>
        </div>
        <div class="promotedshops-listing">
            <div class="shopslistings">

                <div class="error-contains">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success text-success">
                        <i class="fa fa-check"></i> {{ $message }}
                    </div>
                @endif
                </div>

                <div class="promotions-of-this-shop promotions-of-medias">
                    <div class="promotionsofmediaslistings">
                        <div class="col-md-12 col-sm-12 col-xs-12 promotions-with-story">
                        </div>
                  </div>
                </div>
              </div>
            
        </div>
    </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="promotedshops">
            
            <div class="topbar">
                <h3>All promotions of <span>{{$influencer->instagram_id}} WITHOUT MEDIAS</span>:</h3>
                <div class="sortby">
                    <label>SORT BY:</label>
                    <select name="sort_by" id="sort_by" class="selectpicker">
                        <option data-sorting-type="asc" data-column-name="first_added_date" data-icon="fa fa-arrow-up sort-icon">By first added</option>
                        <option data-sorting-type="desc" data-column-name="first_added_date" data-icon="fa fa-arrow-down sort-icon">By first added</option>
                        <option data-sorting-type="asc" data-column-name="country_name" data-icon="fa fa-arrow-up sort-icon">By market</option>
                        <option data-sorting-type="desc" data-column-name="country_name" data-icon="fa fa-arrow-down sort-icon">By market</option>
                        <option data-sorting-type="asc" data-column-name="last_seen_date" data-icon="fa fa-arrow-up sort-icon">By last seen</option>
                        <option data-sorting-type="desc" data-column-name="last_seen_date" data-icon="fa fa-arrow-down sort-icon">By last seen</option>
                        <!-- <option data-sorting-type="asc" data-column-name="activity" data-icon="fa fa-arrow-up sort-icon">By activity</option>
                        <option data-sorting-type="desc" data-column-name="activity" data-icon="fa fa-arrow-down sort-icon">By activity</option> -->
                        <option data-sorting-type="asc" data-column-name="nb_of_story" data-icon="fa fa-arrow-up sort-icon">By nb of story</option>
                        <option data-sorting-type="desc" data-column-name="nb_of_story" data-icon="fa fa-arrow-down sort-icon">By nb of story</option>
                        <option data-sorting-type="asc" data-column-name="created_at" data-icon="fa fa-arrow-up sort-icon">By promotion date</option>
                        <option data-sorting-type="desc" data-column-name="created_at" data-icon="fa fa-arrow-down sort-icon">By promotion date</option>
                        <option data-sorting-type="asc" data-column-name="categories" data-icon="fa fa-arrow-up sort-icon">By categories/niches</option>
                        <option data-sorting-type="desc" data-column-name="categories" data-icon="fa fa-arrow-down sort-icon">By categories/niches</option>
                        <option data-sorting-type="asc" data-column-name="promo_this_week" data-icon="fa fa-arrow-up sort-icon">By promo this week</option>
                        <option data-sorting-type="desc" data-column-name="promo_this_week" data-icon="fa fa-arrow-down sort-icon">By promo this week</option>
                    </select>
                </div>
            </div>
            <div class="searchwebsite">
                <label>Search a website:</label>
                <input type="text" name="search_website" id="search_website" placeholder="ENTER A WEBSITE" autocomplete="false" onfocus="this.placeholder = ''" onblur="this.placeholder = 'ENTER A WEBSITE'">
            </div>

            <div class="showonly">
                <label>Show only: </label>
                <div class="showonlylist">
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'WOTM_MARKET')">WORLD</a>
                    <!-- <select class="selectpicker" multiple id="filter_country_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.png')}}' /> {{ $country->country_name}}"></option>
                        @endforeach
                    </select> -->

                    <!-- <a href="Javascript:void(0);" class="tablinks first-added-last-week">FIRST ADDED</a>
                    <input type="hidden" id="first_added_last_week" value="0">
                    <a href="Javascript:void(0);" class="tablinks last-seen-today">LAST SEEN</a>
                    <input type="hidden" id="last_seen_today" value="0"> -->
                    <!-- <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'ALL_ACTIVITY')">ALL ACTIVITY</a> -->
                   <!--  <select class="selectpicker" multiple id="filter_activity">
                       <option value="high">High</option>
                       <option value="medium">Medium</option>
                       <option value="low">Low</option>
                   </select> -->
                    <!-- <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'NB_OF_PROMOTION')">NB OF PROMOTION</a>
                     <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'PROMOTION_BY_THIS_INF')">PROMOTION BY THIS INF.</a> -->
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'WOTM_FIRST_ADDED_LAST_WEEK')">FIRST ADDED</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'WOTM_LAST_SEEN_TODAY')">LAST SEEN</a>
                     <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'WOTM_NB_OF_STORY')">NB OF STORY</a>
                     <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'WOTM_PROMOTION_DATE_RANGE')">PROMOTION DATE</a>
                     
                </div>
            </div>
        
             <div class="filter-tab-content">
                <div id="WOTM_MARKET" class="tabcontent">
                    <div class="col-md-4">
                        <select class="selectpicker" multiple id="filter_country_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.svg')}}' /> {{ $country->country_name}}"></option>
                        @endforeach
                        </select>
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="language_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div> 

                <!-- <div id="FIRST_ADDED_LAST_WEEK" class="tabcontent">
                </div>
                <div id="LAST_SEEN_TODAY" class="tabcontent">
                </div>
                <div id="ALL_ACTIVITY" class="tabcontent">
                     <div class="col-md-4">
                        <select class="selectpicker" multiple id="filter_activity">
                            <option value="high">High</option>
                            <option value="medium">Medium</option>
                            <option value="low">Low</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button id="activity_filter" class="filter-btn">FILTER</button>
                    </div>
                </div> -->
                 <div id="WOTM_PROMOTION_DATE_RANGE" class="tabcontent">
                     <div class="col-md-3">
                        <input type="text" name="without_media_promotion_date_from" id="without_media_promotion_date_from" placeholder="FROM" class="only-number-allowed datepicker"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="without_media_promotion_date_to" id="without_media_promotion_date_to" placeholder="TO" class="only-number-allowed datepicker">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="without_media_promotion_date_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="WOTM_NB_OF_STORY" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="without_media_nb_of_story_from" id="without_media_nb_of_story_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="without_media_nb_of_story_to" id="without_media_nb_of_story_to" placeholder="TO" class="only-number-allowed">
                    </div>
                   <!--  <div class="col-md-3">
                       <button id="without_media_nb_of_story_filter" class="filter-btn">FILTER</button>
                   </div> -->
                </div>
                <div id="WOTM_FIRST_ADDED_LAST_WEEK" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="without_media_first_added_from" id="without_media_first_added_from" placeholder="FROM"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="without_media_first_added_to" id="without_media_first_added_to" placeholder="TO">
                    </div>
                </div>
                <div id="WOTM_LAST_SEEN_TODAY" class="tabcontent">
                     <div class="col-md-3">
                        <input type="text" name="without_media_last_seen_from" id="without_media_last_seen_from" placeholder="FROM"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="without_media_last_seen_to" id="without_media_last_seen_to" placeholder="TO">
                    </div>
                </div>
                <!-- <div id="NB_OF_PROMOTION" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="nb_of_promotions_from" id="nb_of_promotions_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="nb_of_promotions_to" id="nb_of_promotions_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <div class="col-md-3">
                        <button id="nb_of_promotions_filter" class="filter-btn">FILTER</button>
                    </div>
                </div>
                <div id="PROMOTION_BY_THIS_INF" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="total_promo_of_shop_by_this_influencer_from" id="total_promo_of_shop_by_this_influencer_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="total_promo_of_shop_by_this_influencer_to" id="total_promo_of_shop_by_this_influencer_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <div class="col-md-3">
                        <button id="total_promo_of_shop_by_this_influencer_filter" class="filter-btn">FILTER</button>
                    </div>
                </div> -->
            </div>
            <div class="activated-filter showonly">
                <label>Activated filters: </label>
                <span class="without-media-country-filter-contains"></span>
                <span class="without-media-first-added-filter-contains"></span>
                <span class="without-media-last-seen-filter-contains"></span>
                <span class="without-media-all-activity-filter-contains"></span>
                <span class="without-media-nb-of-story-filter-contains"></span>
                <span class="without-media-promotion-date-filter-contains"></span>
            </div>
        </div>
        <div class="promotedshops-listing">
            <div class="shopslistings">

                <div class="error-contains">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success text-success">
                        <i class="fa fa-check"></i> {{ $message }}
                    </div>
                @endif
                </div>

                <div class="shopslistingsinner">
                    <table id="" class="table display nowrap">
                    <thead>
                        <tr>
                             <th width="2%">
                                <a href="Javascript:void(0);" onclick="select_shop_list_2()"><img class="assign-list-btn" src="{{ asset('images/icons8-add_new.svg')}}" /></a>
                             </th>
                            <th width="17%">URL</th>
                            <th class="text-left">Added</th>
                            <th class="text-left">Last seen</th>
                            <th class="text-center">Promo this week</th>
                            <th class="text-center">Nb of stories</th>
                            <!-- <th class="text-left" width="10%">Total promotion 
                            of the shop</th>
                            <th class="text-left" width="10%">Total promo of the 
                            shop by this influencer</th> 
                            <th class="text-left" width="10%">Nb of media of the promotion</th>-->
                            
                            <th class="text-left">Market</th>
                            <th class="text-center no-sort">CATEGORIES/NICHES</th>
                            <th class="text-left">Promo date</th>
                        </tr>
                    </thead>
                        <tbody class="shops_tr">
                        </tbody>  
                    </table>
                </div>
              </div>
            
        </div>
    </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 no-padding unique-shop-promoted-section">
    <div class="middlepart">
        <div class="promotedshops">
            
            <div class="topbar">
                <h3>Unique shop promoted by <span>{{$influencer->instagram_id}}</span>:</h3>
                <div class="sortby">
                    <label>SORT BY:</label>
                    <select name="usp_sort_by" id="usp_sort_by" class="selectpicker">
                        <option data-sorting-type="asc" data-column-name="first_added_date" data-icon="fa fa-arrow-up sort-icon">By first added</option>
                        <option data-sorting-type="desc" data-column-name="first_added_date" data-icon="fa fa-arrow-down sort-icon">By first added</option>
                        <option data-sorting-type="asc" data-column-name="language_sort_name" data-icon="fa fa-arrow-up sort-icon">By market</option>
                        <option data-sorting-type="desc" data-column-name="language_sort_name" data-icon="fa fa-arrow-down sort-icon">By market</option>
                        <option data-sorting-type="asc" data-column-name="last_seen_date" data-icon="fa fa-arrow-up sort-icon">By last seen</option>
                        <option data-sorting-type="desc" data-column-name="last_seen_date" data-icon="fa fa-arrow-down sort-icon">By last seen</option>
                       <!--  <option data-sorting-type="asc" data-column-name="activity" data-icon="fa fa-arrow-up sort-icon">By activity</option>
                       <option data-sorting-type="desc" data-column-name="activity" data-icon="fa fa-arrow-down sort-icon">By activity</option> -->
                        <option data-sorting-type="asc" data-column-name="nb_of_promotions" data-icon="fa fa-arrow-up sort-icon">By total promo</option>
                        <option data-sorting-type="desc" data-column-name="nb_of_promotions" data-icon="fa fa-arrow-down sort-icon">By total promo</option>
                        <option data-sorting-type="asc" data-column-name="categories" data-icon="fa fa-arrow-up sort-icon">By categories/niches</option>
                        <option data-sorting-type="desc" data-column-name="categories" data-icon="fa fa-arrow-down sort-icon">By categories/niches</option>
                        <option data-sorting-type="asc" data-column-name="total_promotions_by_this_inf" data-icon="fa fa-arrow-up sort-icon">By total promo by this influ</option>
                        <option data-sorting-type="desc" data-column-name="total_promotions_by_this_inf" data-icon="fa fa-arrow-down sort-icon">By total promo by this influ</option>
                        <option data-sorting-type="asc" data-column-name="promo_this_week_by_this_inf" data-icon="fa fa-arrow-up sort-icon">By promo this week by this inf</option>
                        <option data-sorting-type="desc" data-column-name="promo_this_week_by_this_inf" data-icon="fa fa-arrow-down sort-icon">By promo this week by this inf</option>
                    </select>
                </div>
            </div>
            <div class="searchwebsite">
                <label>Search a website:</label>
                <input type="text" name="usp_search_website" id="usp_search_website" placeholder="ENTER A WEBSITE" autocomplete="false" onfocus="this.placeholder = ''" onblur="this.placeholder = 'ENTER A WEBSITE'">
            </div>

            <div class="showonly">
                <label>Show only: </label>
                <div class="showonlylist">
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'USP_MARKET')">WORLD</a>
                    <!-- <select class="selectpicker" multiple id="usp_filter_country_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.png')}}' /> {{ $country->country_name}}"></option>
                        @endforeach
                    </select> -->

                    <!-- <input type="hidden" id="ups_first_added_last_week" value="0">
                    <a href="javascript:void(0);" class="tablinks usp-first-added">FIRST ADDED</a>
                    
                    <input type="hidden" id="ups_last_seen_today" value="0">
                    <a href="javascript:void(0);" class="tablinks usp-last-seen">LAST SEEN</a> -->
                    <!-- <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'USP_ACTIVITY_OF_THE_WEEK')">ACTIVITY OF THE WEEK</a> -->
                    <!-- <select class="selectpicker" multiple id="usp_filter_activity">
                        <option value="high">High</option>
                        <option value="medium">Medium</option>
                        <option value="low">Low</option>
                    </select> -->
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'USP_FIRST_ADDED_LAST_WEEK')">FIRST ADDED</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'USP_LAST_SEEN_TODAY')">LAST SEEN</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'USP_NB_OF_PROMOTION')">NB OF PROMOTION</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'USP_NB_OF_PROMO_BY_THIS_INF')">NB OF PROMO BY THIS INF.</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'USP_PROMO_THIS_WEEK_BY_THIS_INF')">PROMO THIS WEEK BY THIS INF.</a>
                </div>
            </div>
        
             <div class="filter-tab-content">
                 <div id="USP_MARKET" class="tabcontent">
                    <div class="col-md-2">
                        <select class="selectpicker" multiple id="usp_filter_country_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.svg')}}' /> {{ $country->country_name}}"></option>
                        @endforeach
                        </select>
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="usp_country_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div> 

                <!-- <div id="USP_FIRST_ADDED" class="tabcontent">
                </div>
                <div id="USP_LAST_SEEN" class="tabcontent">
                </div> -->
                <!-- <div id="USP_ACTIVITY_OF_THE_WEEK" class="tabcontent">
                     <div class="col-md-4">
                        <select class="selectpicker" multiple id="usp_filter_activity">
                            <option value="high">High</option>
                            <option value="medium">Medium</option>
                            <option value="low">Low</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button id="usp_activity_filter" class="filter-btn">FILTER</button>
                    </div>
                </div> -->
                <div id="USP_NB_OF_PROMOTION" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="usp_nb_of_promotions_from" id="usp_nb_of_promotions_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="usp_nb_of_promotions_to" id="usp_nb_of_promotions_to" placeholder="TO" class="only-number-allowed">
                    </div>
                   <!--  <div class="col-md-3">
                       <button id="usp_nb_of_promotions_filter" class="filter-btn">FILTER</button>
                   </div> -->
                </div>
                <div id="USP_NB_OF_PROMO_BY_THIS_INF" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="usp_nb_of_promo_by_inf_from" id="usp_nb_of_promo_by_inf_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="usp_nb_of_promo_by_inf_to" id="usp_nb_of_promo_by_inf_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="usp_nb_of_promo_by_inf_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="USP_PROMO_THIS_WEEK_BY_THIS_INF" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="usp_promo_this_week_by_inf_from" id="usp_promo_this_week_by_inf_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="usp_promo_this_week_by_inf_to" id="usp_promo_this_week_by_inf_to" placeholder="TO" class="only-number-allowed">
                    </div>
                   <!--  <div class="col-md-3">
                       <button id="usp_promo_this_week_by_inf_filter" class="filter-btn">FILTER</button>
                   </div> -->
                </div>
                 <div id="USP_FIRST_ADDED_LAST_WEEK" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="usp_first_added_from" id="usp_first_added_from" placeholder="FROM"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="usp_first_added_to" id="usp_first_added_to" placeholder="TO">
                    </div>
                </div>
                <div id="USP_LAST_SEEN_TODAY" class="tabcontent">
                     <div class="col-md-3">
                        <input type="text" name="usp_last_seen_from" id="usp_last_seen_from" placeholder="FROM"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="usp_last_seen_to" id="usp_last_seen_to" placeholder="TO">
                    </div>
                </div>
            </div>
            <div class="activated-filter showonly">
                <label>Activated filters: </label>
                <span class="unique-shop-country-filter-contains"></span>
                <span class="unique-shop-first-added-filter-contains"></span>
                <span class="unique-shop-last-seen-filter-contains"></span>
                <span class="unique-shop-all-activity-filter-contains"></span>
                <span class="unique-shop-nb-of-promotion-filter-contains"></span>
                <span class="unique-shop-nb-of-promotion-by-this-inf-filter-contains"></span>
                <span class="unique-shop-promo-this-week-by-this-inf-filter-contains"></span>
            </div>
        </div>
        <div class="promotedshops-listing">
            <div class="shopslistings">

                <div class="error-contains">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success text-success">
                        <i class="fa fa-check"></i> {{ $message }}
                    </div>
                @endif
                </div>

                <div class="shopslistingsinner">
                    <table id="" class="table display nowrap">
                    <thead>
                        <tr>
                            <th width="2%">
                                <a href="Javascript:void(0);" onclick="select_shop_list_2()"><img class="assign-list-btn" src="{{ asset('images/icons8-add_new.svg')}}" /></a>
                            </th>
                            <th width="17%">URL</th>
                            <th class="text-left">Added</th>
                            <th class="text-left">Last seen</th>
                            <th class="text-left">Promo this week</th>
                            <!-- <th class="text-left" width="15%">Latest promotion</th> -->
                            <th class="text-left">Promo this week by this influ</th>
                            <th class="text-left">Total promo</th>
                            <th class="text-left">Total promo by this influ</th>
                            <th class="text-left">Market</th>
                            <th class="text-center no-sort">CATEGORIES/NICHES</th>
                        </tr>
                    </thead>
                        <tbody class="unique-shop-list">
                        </tbody>  
                    </table>
                </div>
              </div>
            
        </div>
    </div>
</div>
      
</div>  


<div id="assign-shop-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">     
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Assign shop into list</h4>
          </div>
          <div class="modal-body">
            
            <input type="hidden" id="assign_shop_id">
            <div class="form-group row">
                <label class="control-label col-sm-2">List Name:</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" id="assign_shop_list_id" multiple=""> 
                        <option value="" disabled="">Select Shop List</option>
                        @foreach($shop_list as $list)
                         <option value="{{$list->shop_list_id}}">{{$list->list_name}}</option>
                        @endforeach
                    </select>
                    <div class="assign-shop-msg text-success"></div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-blue assign-shop-btn">Submit</button>
          </div>
    </div>
  </div>
</div>   

<div id="assign-shop-modal-2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">     
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Assign shop into list</h4>
          </div>
          <div class="modal-body">
            
            <div class="form-group row">
                <label class="control-label col-sm-2">List Name:</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" id="assign_shop_list_id_2" multiple=""> 
                        <option value="" disabled="">Select Shop List</option>
                        @foreach($shop_list as $list)
                         <option value="{{$list->shop_list_id}}">{{$list->list_name}}</option>
                        @endforeach
                    </select>
                    <div class="assign-shop-msg2 text-success"></div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-blue assign-shop-btn-2">Submit</button>
          </div>
    </div>
  </div>
</div>                 
@endsection

@section('pagescript')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
     <!-- Sweet-Alert  -->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap-select.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.js')}}"></script>
    <script src="{{ asset('js/highcharts.js')}}"></script>
    <script type="text/javascript">
        Highcharts.chart('activity-chart', {
          chart: {
            type: 'line',
            height : 300
          },
         /* title: {
            text: 'Monthly Average Temperature'
          },
          subtitle: {
            text: 'Source: WorldClimate.com'
          },*/
          xAxis: {
            categories: ['June 12', 'June 13', 'June 14', 'June 15', 'June 16', 'June 17', 'June 18', 'June 19', 'June 20', 'June 21', 'June 22', 'June 23'],
            gridLineColor:'#000000'
          },
          yAxis: {
            title: {
              text: 'Promotions'
            },
            gridLineColor:'#000000'
          },
          plotOptions: {
            line: {
              dataLabels: {
                enabled: true
              },
              enableMouseTracking: false
            }
          },
          series: [{
            name: 'Promotions',
            data: [0, 15, 11, 0, 13, 25, 11, 22, 30, 25, 20, 40]
          }]
        });
    </script>

    <script type="text/javascript">
        function openTab(evt, tabName) {
            const div = evt.currentTarget;
            if(div.classList.contains('active')){
                 evt.currentTarget.className = "tablinks";
                 document.getElementById(tabName).style.display = "none";
            } else {
                  var i, tabcontent, tablinks;
                  tabcontent = document.getElementsByClassName("tabcontent");
                  for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                  }
                  tablinks = document.getElementsByClassName("tablinks");
                  for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                  }
                  document.getElementById(tabName).style.display = "block";
                  evt.currentTarget.className += " active";
            }
        }
    </script>

    <script type="text/javascript">

    function select_shop_list(shop_id){
        $("#assign_shop_id").val(shop_id);
        $("#assign-shop-modal").modal("show");
    } 

    function select_shop_list_2(){
        $("#assign-shop-modal-2").modal("show");
    }     

    $(function() {

        $(document).on("click", "#select-all-shop", function() { 
            var $img = $(this).find('img') ,
                src = $img.attr('src') ,
                onCheck = /\-deselected\.png$/ ,
                offCheck = /\-selected\.png$/ ,
                normal = /\.png$/
            ;

            if(src.match(onCheck)) {
                $img.attr('src', src.replace(onCheck, '-selected.png'));
                $(".shops_tr .checked-shop-id").prop('checked', true);
            } else if (src.match(offCheck)) {
                $img.attr('src', src.replace(offCheck, '-deselected.png'));
                $(".shops_tr .checked-shop-id").prop('checked', false);
            } else {
                $img.attr('src', src.replace(normal, '-deselected.png'));
                 $(".shops_tr .checked-shop-id").prop('checked', false);
            }
        });
        $(document).on("click", "#select-all-unique-shop", function() {
            var $img = $(this).find('img') ,
                src = $img.attr('src') ,
                onCheck = /\-deselected\.png$/ ,
                offCheck = /\-selected\.png$/ ,
                normal = /\.png$/
            ;

            if(src.match(onCheck)) {
                $img.attr('src', src.replace(onCheck, '-selected.png'));
                $(".unique-shop-list .checked-shop-id").prop('checked', true);
            } else if (src.match(offCheck)) {
                $img.attr('src', src.replace(offCheck, '-deselected.png'));
                $(".unique-shop-list .checked-shop-id").prop('checked', false);
            } else {
                $img.attr('src', src.replace(normal, '-deselected.png'));
                 $(".unique-shop-list .checked-shop-id").prop('checked', false);
            }
        });

        $('#add_to_influencer_list').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : '<img src="{{ asset('images/addlist.png')}}" width="17" />'
        });
        $('#add_to_influencer_list').on('change', function(e){
            if($("#add_to_influencer_list").val() != null && $("#add_to_influencer_list").val() != ''){ 
                var influencer_ids = [];
                influencer_ids.push("{{$influencer->influencer_id}}");
                 var influencer_list_ids = [];
                influencer_list_ids.push($("#add_to_influencer_list").val());
                $.ajax({
                    url : "{{route('user.assign_influencer')}}",
                    data : { influencer_list_ids:influencer_list_ids, influencer_ids:influencer_ids},
                }).done(function (data) {
                    $('.assign-influencer-msg').html('Influencer assigned successfully.');
                });
            } else {
                alert("Please select at least one influencer list.");
            }
        });

        $(document).on('click', '.shop-row-link td:not(.except)', function(){ 
            //window.location.href = $(this).data("shop-details-url");
            window.open($(this).data("shop-details-url"), '_blank');
        });

        $(document).on('click', '.assign-shop-btn', function(e) {
            if($("#assign_shop_list_id").val() != null){ 
                var shop_ids = [];
                shop_ids.push($("#assign_shop_id").val());
                var shop_list_ids = $("#assign_shop_list_id").val();
                $.ajax({
                    url : "{{route('user.assign_shop')}}",
                    data : { shop_list_ids:shop_list_ids, shop_ids:shop_ids},
                }).done(function (data) {
                    $('.assign-shop-msg').html('Shop assigned successfully.');
                });
            } else {
                alert("Please select at least one shop list.");
            }
        });

        $(document).on('click', '.assign-shop-btn-2', function(e) { 

            if($("#assign_shop_list_id_2").val() != ''){
                var shop_ids = $('input.checked-shop-id:checked').map( function () { 
                    return $(this).val();
                }).get();

               // var shop_ids = $("#assign_shop_ids").val();
                var shop_list_ids = $("#assign_shop_list_id_2").val();
                $.ajax({
                    url : "{{route('user.assign_shop')}}",
                    data : { shop_list_ids:shop_list_ids, shop_ids:shop_ids},
                }).done(function (data) { 
                    $('.assign-shop-msg2').html('Shop assigned successfully.');
                    $('input.checked-shop-id').prop('checked', false);
                });
            } else {
                alert("Please select at least one shop list.");
            }
        });

        //$('.datepicker').datepicker();
        $("#promotion_date_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');

                //$('#promotion_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#promotion_date_to').datepicker('option', 'minDate', minDate);

                promotion_date_change();
            }
        });
         $("#promotion_date_to").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                promotion_date_change();
            }
        });
        $("#without_media_promotion_date_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');

                //$('#without_media_promotion_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#without_media_promotion_date_to').datepicker('option', 'minDate', minDate);
                without_media_promotion_date_change();
            }
        });
        $("#without_media_promotion_date_to").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () {
                without_media_promotion_date_change();
            }
        });

        $("#usp_first_added_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');

                //$('#without_media_promotion_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#usp_first_added_to').datepicker('option', 'minDate', minDate);
                usp_first_added_change();
            }
        });
        $("#usp_first_added_to").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () {
                usp_first_added_change();
            }
        });

        $("#usp_last_seen_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');

                //$('#without_media_promotion_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#usp_last_seen_to').datepicker('option', 'minDate', minDate);
                usp_last_seen_change();
            }
        });
        $("#usp_last_seen_to").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () {
                usp_last_seen_change();
            }
        });



        $("#without_media_first_added_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');

                //$('#without_media_promotion_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#without_media_first_added_to').datepicker('option', 'minDate', minDate);
                without_media_first_added_change();
            }
        });
        $("#without_media_first_added_to").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () {
                without_media_first_added_change();
            }
        });

        $("#with_media_first_added_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');

                //$('#without_media_promotion_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#with_media_first_added_to').datepicker('option', 'minDate', minDate);
                with_media_first_added_change();
            }
        });
        $("#with_media_first_added_to").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () {
                with_media_first_added_change();
            }
        });

        $("#with_media_last_seen_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');

                //$('#without_media_promotion_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#with_media_last_seen_to').datepicker('option', 'minDate', minDate);
                with_media_last_seen_change();
            }
        });
        $("#with_media_last_seen_to").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () {
                with_media_last_seen_change();
            }
        });


        $("#without_media_last_seen_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');

                //$('#without_media_promotion_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#without_media_last_seen_to').datepicker('option', 'minDate', minDate);
                without_media_last_seen_change();
            }
        });
        $("#without_media_last_seen_to").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () {
                without_media_last_seen_change();
            }
        });


        $('.selectpicker').selectpicker({
            tickIcon: 'fa fa-check'
        });


        /*$(document).on("click", ".inf-favourite", function(){
            $(".inf-favourite i").toggleClass('fa-heart fa-heart-o');

            $.ajax({
                url : "{{route('user.influencer_favourite')}}",
                data : { influencer_id: '{{$influencer->influencer_id}}'},
                dataType: 'json',
            }).done(function (data) { 
                $('.shops_tr').html(data.body);
            });
        });*/

        var url = $(this).attr('href');
        getShop(url);
        getShopWithMedia(url);
        getUniqueShop(url);
         
        $(document).on('click', '.shops_tr .pagination a', function(e) { 
           
            e.preventDefault();
            var url = $(this).attr('href');
            getShop(url);
            window.history.pushState("", "", url);
        });

        $(document).on('change', '#sort_by', function(e) {
           // e.preventDefault();
            var url = $(this).attr('href');
            getShop(url);
        });

        /*$(document).on('click', '#language_filter', function(e) { 
            if($("#filter_country_ids").val() != null){
                var url = $(this).attr('href');
                getShop(url);
            }
        });*/

        var url = $(this).attr('href');

        $('#filter_country_ids').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'SELECT COUNTRY'
        });
        $("button[data-id=filter_country_ids]").html("SELECT COUNTRY");
        $('#filter_country_ids').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).data('content')+' '+'<a href="Javascript:void(0);" class="remove-without-media-country" data-country-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .without-media-country-filter-contains").html(results);
            $("button[data-id=filter_country_ids]").html("SELECT COUNTRY");

            var url = $(this).attr('href');
            getShop(url);
        });
        
        $(document).on('click', '.without-media-country-filter-contains .item .remove-without-media-country', function(e) { 
            var wanted_id = $(this).data("country-id"); 
            var wanted_option = $('#filter_country_ids option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#filter_country_ids').selectpicker('refresh');
            $(this).parent().remove(); 

            getShop(url); 
        });

        /*$(document).on('click', '#activity_filter', function(e) { 
            if($("#filter_activity").val() != null){ 
                var url = $(this).attr('href');
                getShop(url);
            }
        });*/

        /*$('#filter_activity').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'ALL ACTIVITY'
        });
        $("button[data-id=filter_activity]").html("ALL ACTIVITY");
        $('#filter_activity').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).text()+' '+'<a href="Javascript:void(0);" class="remove-without-media-all-activity" data-activity-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .without-media-all-activity-filter-contains").html(results);
            $("button[data-id=filter_activity]").html("ALL ACTIVITY");

            getShop(url); 
        });
        
        $(document).on('click', '.without-media-all-activity-filter-contains .item .remove-without-media-all-activity', function(e) { 
            var wanted_id = $(this).data("activity-id"); 
            var wanted_option = $('#filter_activity option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#filter_activity').selectpicker('refresh');
            $(this).parent().remove(); 

            getShop(url); 
        });*/
        $(document).on('click', '.without-media-first-added-filter-contains .item .remove-without-media-first-added', function(e) { 
            $(this).parent().remove(); 
            $("#without_media_first_added_from").val('');
            $("#without_media_first_added_to").val('');
            getShop(url); 
        });

        $(document).on('click', '.without-media-last-seen-filter-contains .item .remove-without-media-last-seen', function(e) { 
            $(this).parent().remove(); 
            $("#without_media_last_seen_from").val('');
            $("#without_media_last_seen_to").val('');
            getShop(url); 
        });

        function without_media_first_added_change(){
            if($("#without_media_first_added_from").val() != '' && $("#without_media_first_added_to").val() != ''){
                $(".activated-filter .without-media-first-added-filter-contains").html('<span class="item">FIRST ADDED '+$("#without_media_first_added_from").val()+' TO '+$("#without_media_first_added_to").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url); 
            } else if($("#without_media_first_added_from").val() != ''){
                  $(".activated-filter .without-media-first-added-filter-contains").html('<span class="item">FIRST ADDED > '+$("#without_media_first_added_from").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else if($("#without_media_first_added_to").val() != ''){
                  $(".activated-filter .without-media-first-added-filter-contains").html('<span class="item">FIRST ADDED < '+$("#without_media_first_added_to").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else {
                $(".activated-filter .without-media-first-added-filter-contains").html('');
            }
        }

        function without_media_last_seen_change(){
            if($("#last_seen_from").val() != '' && $("#without_media_last_seen_to").val() != ''){
                $(".activated-filter .without-media-last-seen-filter-contains").html('<span class="item">LAST SEEN '+$("#without_media_last_seen_from").val()+' TO '+$("#without_media_last_seen_to").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url); 
            } else if($("#without_media_last_seen_from").val() != ''){
                  $(".activated-filter .without-media-last-seen-filter-contains").html('<span class="item">LAST SEEN > '+$("#without_media_last_seen_from").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else if($("#without_media_last_seen_to").val() != ''){
                  $(".activated-filter .without-media-last-seen-filter-contains").html('<span class="item">LAST SEEN < '+$("#without_media_last_seen_to").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else {
                $(".activated-filter .without-media-last-seen-filter-contains").html('');
            }
        }
        /*$(document).on('click', '.first-added-last-week', function(e) { 
            $(".activated-filter .without-media-first-added-filter-contains").html('<span class="item">ADDED '+"{{Carbon\Carbon::parse('next sunday')->format('m/d/y')}}"+' '+'<a href="Javascript:void(0);" class="remove-without-media-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            $("#first_added_last_week").val(1);
            getShop(url);
        });

        $(document).on('click', '.last-seen-today', function(e) { 
            $(".activated-filter .without-media-last-seen-filter-contains").html('<span class="item">LAST SEEN '+"{{Carbon\Carbon::parse('today')->format('m/d/y')}}"+' '+'<a href="Javascript:void(0);" class="remove-without-media-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            $("#last_seen_today").val(1);
            getShop(url);
        });*/
        $(document).on('click', '.without-media-last-seen-filter-contains .item .remove-without-media-last-seen', function(e) { 
            $(this).parent().remove();
            $("#last_seen_today").val(0);
            getShop(url);  
        });

        //$(document).on('click', '#nb_of_promotions_filter', function(e) {
        $(document).on( "keyup", "#nb_of_promotions_from, #nb_of_promotions_to", function( e ) { 
            if($("#nb_of_promotions_from").val() != '' && $("#nb_of_promotions_to").val() != ''){
                
                var url = $(this).attr('href');
                getShop(url) 
            } else if($("#nb_of_promotions_from").val() != ''){
                
                var url = $(this).attr('href');
                getShop(url) 
            } else if($("#nb_of_promotions_to").val() != ''){
                
                var url = $(this).attr('href');
                getShop(url) 
            } 
        });

        //$(document).on('click', '#without_media_nb_of_story_filter', function(e) {
        $(document).on( "keyup", "#without_media_nb_of_story_from, #without_media_nb_of_story_to", function( e ) {  
            if($("#without_media_nb_of_story_from").val() != '' && $("#without_media_nb_of_story_to").val() != ''){
                $(".activated-filter .without-media-nb-of-story-filter-contains").html('<span class="item">NB OF STORY '+$("#without_media_nb_of_story_from").val()+' TO '+$("#without_media_nb_of_story_to").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url);
            } else if($("#without_media_nb_of_story_from").val() != ''){
                $(".activated-filter .without-media-nb-of-story-filter-contains").html('<span class="item">NB OF STORY > '+$("#without_media_nb_of_story_from").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url);
            } else if($("#without_media_nb_of_story_to").val() != ''){
                $(".activated-filter .without-media-nb-of-story-filter-contains").html('<span class="item">NB OF STORY < '+$("#without_media_nb_of_story_to").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url);
            } else {
                $(".activated-filter .without-media-nb-of-story-filter-contains").html('');
            }
        });
        $(document).on('click', '.without-media-nb-of-story-filter-contains .item .remove-without-media-nb-of-story', function(e) { 
            $(this).parent().remove(); 
            $("#without_media_nb_of_story_from").val('');
            $("#without_media_nb_of_story_to").val('');
            getShop(url); 
        });

        $(document).on('click', '#total_promo_of_shop_by_this_influencer_filter', function(e) {
            if($("#total_promo_of_shop_by_this_influencer_from").val() != '' && $("#total_promo_of_shop_by_this_influencer_to").val() != ''){
                var url = $(this).attr('href');
                getShop(url) 
            } 
        });
        //$(document).on('click', '#without_media_promotion_date_filter', function(e) {
        function without_media_promotion_date_change(){
            if($("#without_media_promotion_date_from").val() != null && $("#without_media_promotion_date_to").val() != null && $("#without_media_promotion_date_from").val() != '' && $("#without_media_promotion_date_to").val() != ''){
                $(".activated-filter .without-media-promotion-date-filter-contains").html('<span class="item">PROMOTION DATE '+$("#without_media_promotion_date_from").val()+' TO '+$("#without_media_promotion_date_to").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-promotion-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url);
            } else if($("#without_media_promotion_date_from").val() != null && $("#without_media_promotion_date_from").val() != ''){
                $(".activated-filter .without-media-promotion-date-filter-contains").html('<span class="item">PROMOTION DATE > '+$("#without_media_promotion_date_from").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-promotion-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url);
            } else if($("#without_media_promotion_date_to").val() != null && $("#without_media_promotion_date_to").val() != ''){
                $(".activated-filter .without-media-promotion-date-filter-contains").html('<span class="item">PROMOTION DATE < '+$("#without_media_promotion_date_to").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-promotion-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url);
            } else {
                $(".activated-filter .without-media-promotion-date-filter-contains").html('');
            }
        }

        function without_media_first_added_change(){
            if($("#without_media_first_added_from").val() != '' && $("#without_media_first_added_to").val() != ''){
                $(".activated-filter .without-media-first-added-filter-contains").html('<span class="item">FIRST ADDED '+$("#without_media_first_added_from").val()+' TO '+$("#without_media_first_added_to").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url); 
            } else if($("#without_media_first_added_from").val() != ''){
                  $(".activated-filter .without-media-first-added-filter-contains").html('<span class="item">FIRST ADDED > '+$("#without_media_first_added_from").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else if($("#without_media_first_added_to").val() != ''){
                  $(".activated-filter .without-media-first-added-filter-contains").html('<span class="item">FIRST ADDED < '+$("#without_media_first_added_to").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else {
                $(".activated-filter .without-media-first-added-filter-contains").html('');
            }
        }

        function without_media_last_seen_change(){
            if($("#last_seen_from").val() != '' && $("#without_media_last_seen_to").val() != ''){
                $(".activated-filter .without-media-last-seen-filter-contains").html('<span class="item">LAST SEEN '+$("#without_media_last_seen_from").val()+' TO '+$("#without_media_last_seen_to").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url); 
            } else if($("#without_media_last_seen_from").val() != ''){
                  $(".activated-filter .without-media-last-seen-filter-contains").html('<span class="item">LAST SEEN > '+$("#without_media_last_seen_from").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else if($("#without_media_last_seen_to").val() != ''){
                  $(".activated-filter .without-media-last-seen-filter-contains").html('<span class="item">LAST SEEN < '+$("#without_media_last_seen_to").val()+' '+'<a href="Javascript:void(0);" class="remove-without-media-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else {
                $(".activated-filter .without-media-last-seen-filter-contains").html('');
            }
        }
        $(document).on('click', '.without-media-promotion-date-filter-contains .item .remove-without-media-promotion-date', function(e) { 
            $(this).parent().remove(); 
            $("#without_media_promotion_date_from").val('');
            $("#without_media_promotion_date_to").val('');
            getShop(url); 
        });

        /*$('#search_website').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                var url = $(this).attr('href');
                getShop(url);
            }
        });*/
        $(document).on( "keyup", "#search_website", function( e ) {
            e.stopImmediatePropagation();
            var url = $(this).attr('href');
            getShop(url);
        });

        function getShop(url, action='') 
        { 
            //var sort_by = $("#sort_by").val();

            var sort_by = $("#sort_by").find(':selected').attr('data-column-name');
            var sort_type =  $("#sort_by").find(':selected').attr('data-sorting-type');
            var search_website = $("#search_website").val();
            var filter_country_ids = $("#filter_country_ids").val();
            var filter_activity = $("#filter_activity").val();
            var nb_of_promotions_from = $("#nb_of_promotions_from").val();
            var nb_of_promotions_to = $("#nb_of_promotions_to").val();
            var total_promo_of_shop_by_this_inf_from = $("#total_promo_of_shop_by_this_influencer_from").val();
            var total_promo_of_shop_by_this_inf_to = $("#total_promo_of_shop_by_this_influencer_to").val();
            var nb_of_story_from = $("#without_media_nb_of_story_from").val();
            var nb_of_story_to = $("#without_media_nb_of_story_to").val();
            var promotion_date_from = $("#without_media_promotion_date_from").val();
            var promotion_date_to = $("#without_media_promotion_date_to").val();

            var first_added_from = $("#without_media_first_added_from").val();
            var first_added_to = $("#without_media_first_added_to").val();
            var last_seen_from = $("#without_media_last_seen_from").val();
            var last_seen_to = $("#without_media_last_seen_to").val();
            
            $.ajax({
                url : url,
                data : { with_media: 0, search_website:search_website, sort_type:sort_type, sort_by:sort_by, filter_country_ids:filter_country_ids, first_added_from:first_added_from,first_added_to:first_added_to, last_seen_from:last_seen_from, last_seen_to:last_seen_to, filter_activity:filter_activity, nb_of_promotions_from:nb_of_promotions_from, nb_of_promotions_to:nb_of_promotions_to,  total_promo_of_shop_by_this_inf_from:total_promo_of_shop_by_this_inf_from, total_promo_of_shop_by_this_inf_to:total_promo_of_shop_by_this_inf_to, nb_of_story_from:nb_of_story_from, nb_of_story_to:nb_of_story_to, promotion_date_from: promotion_date_from, promotion_date_to:promotion_date_to},
                dataType: 'json',
            }).done(function (data) { 
                $('.shops_tr').html(data.body);
            });
        }


        $(document).on('click', '.promotions-with-story .pagination a', function(e) { 
            e.preventDefault();
            var url = $(this).attr('href');
            getShopWithMedia(url);
            window.history.pushState("", "", url);
        });

        $(document).on('change', '#sort_by_with_media', function(e) {
           // e.preventDefault();
            var url = $(this).attr('href');
            getShopWithMedia(url);
        });

        /*$(document).on('click', '#country_filter_with_media', function(e) { 
            if($("#with_media_filter_country_ids").val() != null){
                var url = $(this).attr('href');
                getShopWithMedia(url);
            }
        });*/
        var url = $(this).attr('href');
 
        $('#with_media_filter_country_ids').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'SELECT COUNTRY'
        });
        $("button[data-id=with_media_filter_country_ids]").html("SELECT COUNTRY");
        $('#with_media_filter_country_ids').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).data('content')+' '+'<a href="Javascript:void(0);" class="remove-country-with-media" data-country-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .with-media-country-filter-contains").html(results);
            $("button[data-id=with_media_filter_country_ids]").html("SELECT COUNTRY");

            var url = $(this).attr('href');
            getShopWithMedia(url);
        });
        
        $(document).on('click', '.with-media-country-filter-contains .item .remove-country-with-media', function(e) { 
            var wanted_id = $(this).data("country-id"); 
            var wanted_option = $('#with_media_filter_country_ids option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#with_media_filter_country_ids').selectpicker('refresh');
            $(this).parent().remove(); 

            getShopWithMedia(url); 
        });

        //$(document).on('click', '#nb_of_story_filter', function(e) { 
        $(document).on( "keyup", "#nb_of_story_from, #nb_of_story_to", function( e ) {  
            if($("#nb_of_story_from").val() != '' && $("#nb_of_story_to").val() != ''){
                $(".activated-filter .with-media-nb-of-story-filter-contains").html('<span class="item">NB OF STORY '+$("#nb_of_story_from").val()+' TO '+$("#nb_of_story_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShopWithMedia(url);
            } else if($("#nb_of_story_from").val() != ''){
                $(".activated-filter .with-media-nb-of-story-filter-contains").html('<span class="item">NB OF STORY > '+$("#nb_of_story_from").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShopWithMedia(url);
            } else if($("#nb_of_story_to").val() != ''){
                $(".activated-filter .with-media-nb-of-story-filter-contains").html('<span class="item">NB OF STORY < '+$("#nb_of_story_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShopWithMedia(url);
            } else {
                $(".activated-filter .with-media-nb-of-story-filter-contains").html('');
            }
        });
        $(document).on('click', '.with-media-nb-of-story-filter-contains .item .remove-with-media-nb-of-story', function(e) { 
            $(this).parent().remove(); 
            $("#nb_of_story_from").val('');
            $("#nb_of_story_to").val('');
            getShopWithMedia(url); 
        });

        //$(document).on('click', '#promotion_date_filter', function(e) {
        function promotion_date_change(){  
            if($("#promotion_date_from").val() != null && $("#promotion_date_to").val() != null && $("#promotion_date_from").val() != '' && $("#promotion_date_to").val() != ''){
                $(".activated-filter .with-media-promotion-date-range-filter-contains").html('<span class="item">PROMOTION DATE '+$("#promotion_date_from").val()+' TO '+$("#promotion_date_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-promotion-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShopWithMedia(url);
            } else if($("#promotion_date_from").val() != null && $("#promotion_date_from").val() != ''){
                $(".activated-filter .with-media-promotion-date-range-filter-contains").html('<span class="item">PROMOTION DATE > '+$("#promotion_date_from").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-promotion-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShopWithMedia(url);
            } else if($("#promotion_date_to").val() != null && $("#promotion_date_to").val() != ''){
                $(".activated-filter .with-media-promotion-date-range-filter-contains").html('<span class="item">PROMOTION DATE < '+$("#promotion_date_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-promotion-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShopWithMedia(url);
            } else {
                $(".activated-filter .with-media-promotion-date-range-filter-contains").html('');
            }
        }
        $(document).on('click', '.with-media-promotion-date-range-filter-contains .item .remove-with-media-promotion-date', function(e) { 
            $(this).parent().remove(); 
            $("#promotion_date_from").val('');
            $("#promotion_date_to").val('');
            getShopWithMedia(url); 
        });

        function with_media_first_added_change(){
            if($("#with_media_first_added_from").val() != '' && $("#with_media_first_added_to").val() != ''){
                $(".activated-filter .with-media-first-added-filter-contains").html('<span class="item">FIRST ADDED '+$("#with_media_first_added_from").val()+' TO '+$("#with_media_first_added_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShopWithMedia(url); 
            } else if($("#with_media_first_added_from").val() != ''){
                  $(".activated-filter .with-media-first-added-filter-contains").html('<span class="item">FIRST ADDED > '+$("#with_media_first_added_from").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShopWithMedia(url)    
            } else if($("#with_media_first_added_to").val() != ''){
                  $(".activated-filter .with-media-first-added-filter-contains").html('<span class="item">FIRST ADDED < '+$("#with_media_first_added_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShopWithMedia(url)    
            } else {
                $(".activated-filter .with-media-first-added-filter-contains").html('');
            }
        }

        $(document).on('click', '.with-media-first-added-filter-contains .item .remove-with-media-first-added', function(e) { 
            $(this).parent().remove(); 
            $("#with_media_first_added_from").val('');
            $("#with_media_first_added_to").val('');
            getShopWithMedia(url); 
        });

        $(document).on('click', '.with-media-last-seen-filter-contains .item .remove-with-media-last-seen', function(e) { 
            $(this).parent().remove(); 
            $("#with_media_last_seen_from").val('');
            $("#with_media_last_seen_to").val('');
            getShopWithMedia(url); 
        });

        function with_media_last_seen_change(){
            if($("#last_seen_from").val() != '' && $("#with_media_last_seen_to").val() != ''){
                $(".activated-filter .with-media-last-seen-filter-contains").html('<span class="item">LAST SEEN '+$("#with_media_last_seen_from").val()+' TO '+$("#with_media_last_seen_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShopWithMedia(url); 
            } else if($("#with_media_last_seen_from").val() != ''){
                  $(".activated-filter .with-media-last-seen-filter-contains").html('<span class="item">LAST SEEN > '+$("#with_media_last_seen_from").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShopWithMedia(url)    
            } else if($("#with_media_last_seen_to").val() != ''){
                  $(".activated-filter .with-media-last-seen-filter-contains").html('<span class="item">LAST SEEN < '+$("#with_media_last_seen_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShopWithMedia(url)    
            } else {
                $(".activated-filter .with-media-last-seen-filter-contains").html('');
            }
        }

        /*$('#search_influencer').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                var url = $(this).attr('href');
                getShopWithMedia(url);
            }
        });*/
        $(document).on( "keyup", "#search_influencer", function( e ) {
            e.stopImmediatePropagation();
            var url = $(this).attr('href');
            getShopWithMedia(url);
        });

        function getShopWithMedia(url, action='') 
        { 
            var sort_by = $("#sort_by_with_media").find(':selected').attr('data-column-name');
            var sort_type =  $("#sort_by_with_media").find(':selected').attr('data-sorting-type');
            var search_influencer = $("#search_influencer").val();
            var filter_country_ids = $("#with_media_filter_country_ids").val();
            var nb_of_story_from = $("#nb_of_story_from").val();
            var nb_of_story_to = $("#nb_of_story_to").val();
            var promotion_date_from = $("#promotion_date_from").val();
            var promotion_date_to = $("#promotion_date_to").val();

            var first_added_from = $("#with_media_first_added_from").val();
            var first_added_to = $("#with_media_first_added_to").val();
            var last_seen_from = $("#with_media_last_seen_from").val();
            var last_seen_to = $("#with_media_last_seen_to").val();

            $.ajax({
                url : url,
                data : { with_media: 1, search_influencer:search_influencer, sort_type:sort_type, sort_by:sort_by, filter_country_ids:filter_country_ids, nb_of_story_from:nb_of_story_from, nb_of_story_to:nb_of_story_to, promotion_date_from:promotion_date_from, promotion_date_to:promotion_date_to, first_added_from:first_added_from, first_added_to:first_added_to,last_seen_from:last_seen_from,last_seen_to:last_seen_to},
                dataType: 'json',
            }).done(function (data) { 
                console.log(data.body);
                $('.promotions-with-story').html(data.body);
            });
        }

        
        // unique shop
       /* $('#usp_search_website').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                var url = $(this).attr('href');
                getUniqueShop(url);
            }
        });*/
        $(document).on( "keyup", "#usp_search_website", function( e ) {
            e.stopImmediatePropagation();
            var url = $(this).attr('href');
            getUniqueShop(url);
        });

        $(document).on('change', '#usp_sort_by', function(e) {
           // e.preventDefault();
            var url = $(this).attr('href');
            getUniqueShop(url);
        });

         $(document).on('click', '.unique-shop-first-added-filter-contains .item .remove-unique-shop-first-added', function(e) { 
            $(this).parent().remove(); 
            $("#usp_first_added_from").val('');
            $("#usp_first_added_to").val('');
            getUniqueShop(url); 
        });
        /*$(document).on('click', '.usp-first-added', function(e) { 
            $(".activated-filter .unique-shop-first-added-filter-contains").html('<span class="item">ADDED '+"{{Carbon\Carbon::parse('next sunday')->format('m/d/y')}}"+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            $("#ups_first_added_last_week").val(1);
            getUniqueShop(url);
        });

        $(document).on('click', '.usp-last-seen', function(e) { 
            $(".activated-filter .unique-shop-last-seen-filter-contains").html('<span class="item">LAST SEEN '+"{{Carbon\Carbon::parse('today')->format('m/d/y')}}"+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            $("#ups_last_seen_today").val(1);
            getUniqueShop(url);
        });*/
        $(document).on('click', '.unique-shop-last-seen-filter-contains .item .remove-unique-shop-last-seen', function(e) { 
            $(this).parent().remove();
            $("#usp_last_seen_from").val('');
            $("#usp_last_seen_to").val('');
            getUniqueShop(url);  
        });

        function usp_first_added_change(){
            if($("#usp_first_added_from").val() != '' && $("#usp_first_added_to").val() != ''){
                $(".activated-filter .unique-shop-first-added-filter-contains").html('<span class="item">FIRST ADDED '+$("#usp_first_added_from").val()+' TO '+$("#usp_first_added_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueShop(url); 
            } else if($("#usp_first_added_from").val() != ''){
                  $(".activated-filter .unique-shop-first-added-filter-contains").html('<span class="item">FIRST ADDED > '+$("#usp_first_added_from").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueShop(url)    
            } else if($("#usp_first_added_to").val() != ''){
                  $(".activated-filter .unique-shop-first-added-filter-contains").html('<span class="item">FIRST ADDED < '+$("#usp_first_added_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueShop(url)    
            } else {
                $(".activated-filter .unique-shop-first-added-filter-contains").html('');
            }
        }

        function usp_last_seen_change(){
            if($("#usp_last_seen_from").val() != '' && $("#usp_last_seen_to").val() != ''){
                $(".activated-filter .unique-shop-last-seen-filter-contains").html('<span class="item">LAST SEEN '+$("#usp_last_seen_from").val()+' TO '+$("#usp_last_seen_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueShop(url); 
            } else if($("#usp_last_seen_from").val() != ''){
                  $(".activated-filter .unique-shop-last-seen-filter-contains").html('<span class="item">LAST SEEN > '+$("#usp_last_seen_from").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueShop(url)    
            } else if($("#usp_last_seen_to").val() != ''){
                  $(".activated-filter .unique-shop-last-seen-filter-contains").html('<span class="item">LAST SEEN < '+$("#usp_last_seen_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueShop(url)    
            } else {
                $(".activated-filter .unique-shop-last-seen-filter-contains").html('');
            }
        }

        //$(document).on('click', '#usp_nb_of_promotions_filter', function(e) {
        $(document).on( "keyup", "#usp_nb_of_promotions_from, #usp_nb_of_promotions_to", function( e ) {  
            if($("#usp_nb_of_promotions_from").val() != '' && $("#usp_nb_of_promotions_to").val() != ''){
                $(".activated-filter .unique-shop-nb-of-promotion-filter-contains").html('<span class="item">NB OF PROMOTION '+$("#usp_nb_of_promotions_from").val()+' TO '+$("#usp_nb_of_promotions_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-nb-of-promotion"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                getUniqueShop(url); 
            } else if($("#usp_nb_of_promotions_from").val() != ''){
                $(".activated-filter .unique-shop-nb-of-promotion-filter-contains").html('<span class="item">NB OF PROMOTION > '+$("#usp_nb_of_promotions_from").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-nb-of-promotion"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                getUniqueShop(url); 
            } else if($("#usp_nb_of_promotions_to").val() != ''){
                $(".activated-filter .unique-shop-nb-of-promotion-filter-contains").html('<span class="item">NB OF PROMOTION < '+$("#usp_nb_of_promotions_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-nb-of-promotion"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                getUniqueShop(url); 
            } else {
                $(".activated-filter .unique-shop-nb-of-promotion-filter-contains").html('');
            }  
        });
        $(document).on('click', '.unique-shop-nb-of-promotion-filter-contains .item .remove-unique-shop-nb-of-promotion', function(e) { 
            $(this).parent().remove();
            $("#usp_nb_of_promotions_from").val('');
            $("#usp_nb_of_promotions_to").val('');
            getUniqueShop(url);  
        });

        //$(document).on('click', '#usp_nb_of_promo_by_inf_filter', function(e) {
        $(document).on( "keyup", "#usp_nb_of_promo_by_inf_from, #usp_nb_of_promo_by_inf_to", function( e ) {  
            if($("#usp_nb_of_promo_by_inf_from").val() != '' && $("#usp_nb_of_promo_by_inf_to").val() != ''){
                 $(".activated-filter .unique-shop-nb-of-promotion-by-this-inf-filter-contains").html('<span class="item">NB OF PROMO BY THIS INF. '+$("#usp_nb_of_promo_by_inf_from").val()+' TO '+$("#usp_nb_of_promo_by_inf_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-nb-of-promotion-by-this-inf"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                getUniqueShop(url);
            } else if($("#usp_nb_of_promo_by_inf_from").val()){
                 $(".activated-filter .unique-shop-nb-of-promotion-by-this-inf-filter-contains").html('<span class="item">NB OF PROMO BY THIS INF. > '+$("#usp_nb_of_promo_by_inf_from").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-nb-of-promotion-by-this-inf"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                getUniqueShop(url);
            } else if($("#usp_nb_of_promo_by_inf_to").val()){
                 $(".activated-filter .unique-shop-nb-of-promotion-by-this-inf-filter-contains").html('<span class="item">NB OF PROMO BY THIS INF. < '+$("#usp_nb_of_promo_by_inf_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-nb-of-promotion-by-this-inf"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                getUniqueShop(url);
            } else {
                $(".activated-filter .unique-shop-nb-of-promotion-by-this-inf-filter-contains").html('');
            }
        });
        $(document).on('click', '.unique-shop-nb-of-promotion-by-this-inf-filter-contains .item .remove-unique-shop-nb-of-promotion-by-this-inf', function(e) { 
            $(this).parent().remove();
            $("#usp_nb_of_promo_by_inf_from").val('');
            $("#usp_nb_of_promo_by_inf_to").val('');
            getUniqueShop(url);  
        });

        //$(document).on('click', '#usp_promo_this_week_by_inf_filter', function(e) {
         $(document).on( "keyup", "#usp_promo_this_week_by_inf_from, #usp_promo_this_week_by_inf_to", function( e ) { 
            if($("#usp_promo_this_week_by_inf_from").val() != '' && $("#usp_promo_this_week_by_inf_to").val() != ''){
                $(".activated-filter .unique-shop-promo-this-week-by-this-inf-filter-contains").html('<span class="item">PROMO THIS WEEK BY THIS INF. '+$("#usp_promo_this_week_by_inf_from").val()+' TO '+$("#usp_promo_this_week_by_inf_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-promo-this-week-by-this-inf"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                getUniqueShop(url); 
            } else if($("#usp_promo_this_week_by_inf_from").val() != ''){
                $(".activated-filter .unique-shop-promo-this-week-by-this-inf-filter-contains").html('<span class="item">PROMO THIS WEEK BY THIS INF. > '+$("#usp_promo_this_week_by_inf_from").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-promo-this-week-by-this-inf"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                getUniqueShop(url); 
            } else if($("#usp_promo_this_week_by_inf_to").val() != ''){
                $(".activated-filter .unique-shop-promo-this-week-by-this-inf-filter-contains").html('<span class="item">PROMO THIS WEEK BY THIS INF. < '+$("#usp_promo_this_week_by_inf_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-promo-this-week-by-this-inf"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                getUniqueShop(url); 
            } 
        });
        $(document).on('click', '.unique-shop-promo-this-week-by-this-inf-filter-contains .item .remove-unique-shop-promo-this-week-by-this-inf', function(e) { 
            $(this).parent().remove();
            $("#usp_promo_this_week_by_inf_from").val('');
            $("#usp_promo_this_week_by_inf_to").val('');
            getUniqueShop(url);  
        });

        /*$(document).on('click', '#usp_activity_filter', function(e) { 
            if($("#usp_filter_activity").val() != null){ 
                var url = $(this).attr('href');
                getUniqueShop(url);
            }
        });*/
        /*$('#usp_filter_activity').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'ACTIVITY OF THE WEEK'
        });
        $("button[data-id=usp_filter_activity]").html("ACTIVITY OF THE WEEK");
        $('#usp_filter_activity').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).text()+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-all-activity" data-activity-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .unique-shop-all-activity-filter-contains").html(results);
            $("button[data-id=usp_filter_activity]").html("ACTIVITY OF THE WEEK");
            getUniqueShop(url) 
        });
        
        $(document).on('click', '.unique-shop-all-activity-filter-contains .item .remove-unique-shop-all-activity', function(e) { 
            var wanted_id = $(this).data("activity-id"); 
            var wanted_option = $('#usp_filter_activity option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#usp_filter_activity').selectpicker('refresh');
            $(this).parent().remove(); 

            getUniqueShop(url) 
        });*/

        /*$(document).on('click', '#usp_country_filter', function(e) { 
            if($("#usp_filter_country_ids").val() != null){
                var url = $(this).attr('href');
                getUniqueShop(url);
            }
        });*/
        var url = $(this).attr('href');
 
        $('#usp_filter_country_ids').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'SELECT COUNTRY'
        });
        $("button[data-id=usp_filter_country_ids]").html("SELECT COUNTRY");
        $('#usp_filter_country_ids').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).data('content')+' '+'<a href="Javascript:void(0);" class="remove-unique-shop-country" data-country-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .unique-shop-country-filter-contains").html(results);
            $("button[data-id=usp_filter_country_ids]").html("SELECT COUNTRY");

            var url = $(this).attr('href');
            getUniqueShop(url);
        });
        
        $(document).on('click', '.unique-shop-country-filter-contains .item .remove-unique-shop-country', function(e) { 
            var wanted_id = $(this).data("country-id"); 
            var wanted_option = $('#usp_filter_country_ids option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#usp_filter_country_ids').selectpicker('refresh');
            $(this).parent().remove(); 

            getUniqueShop(url); 
        });

        $(document).on('click', '.unique-shop-list .pagination a', function(e) { 
           
            e.preventDefault();
            var url = $(this).attr('href');
            getUniqueShop(url);
            window.history.pushState("", "", url);
        });

        function getUniqueShop(url, action='') 
        {  
            var sort_by = $("#usp_sort_by").find(':selected').attr('data-column-name');
            var sort_type =  $("#usp_sort_by").find(':selected').attr('data-sorting-type');
            var search_website = $("#usp_search_website").val();
            var filter_country_ids = $("#usp_filter_country_ids").val();
            var filter_activity = $("#usp_filter_activity").val();
            var nb_of_promotions_from = $("#usp_nb_of_promotions_from").val();
            var nb_of_promotions_to = $("#usp_nb_of_promotions_to").val();
            var nb_of_promo_by_inf_from = $("#usp_nb_of_promo_by_inf_from").val();
            var nb_of_promo_by_inf_to = $("#usp_nb_of_promo_by_inf_to").val();
            var promo_this_week_by_inf_from = $("#usp_promo_this_week_by_inf_from").val();
            var promo_this_week_by_inf_to = $("#usp_promo_this_week_by_inf_to").val();
            /*var first_added_last_week = $("#ups_first_added_last_week").val();
            var last_seen_today = $("#ups_last_seen_today").val();*/
            var first_added_from = $("#usp_first_added_from").val();
            var first_added_to = $("#usp_first_added_to").val();
            var last_seen_from = $("#usp_last_seen_from").val();
            var last_seen_to = $("#usp_last_seen_to").val();

            $.ajax({
                url : url,
                data : { with_unique_shop: 1, search_website:search_website, sort_type:sort_type, sort_by:sort_by, filter_country_ids:filter_country_ids, filter_activity:filter_activity, nb_of_promotions_from:nb_of_promotions_from, nb_of_promotions_to:nb_of_promotions_to, nb_of_promo_by_inf_from:nb_of_promo_by_inf_from, nb_of_promo_by_inf_to:nb_of_promo_by_inf_to,promo_this_week_by_inf_from:promo_this_week_by_inf_from, promo_this_week_by_inf_to:promo_this_week_by_inf_to, first_added_from:first_added_from,first_added_to:first_added_to, last_seen_from:last_seen_from,last_seen_to:last_seen_to},
                dataType: 'json',
            }).done(function (data) { 
                console.log(data.body);
                $('.unique-shop-list').html(data.body);
            });
        }
    });
    </script>
    <script>
        function delete_shop(shop_id){

            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this data!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#8BC34A",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                /*iconColor: '#000',*/
            }, function(){   
                swal("Deleted!", "Your data has been deleted.", "success"); 
                $("#laravel_datatable-"+shop_id).submit();
                return true;
            });

            return false;
        }
    </script>
@stop
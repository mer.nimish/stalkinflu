<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
        
        @php
        $currentRoute = Request::route()->getName();
        @endphp
        @if($currentRoute)
            <title>{{Config::get('constants.meta_tags.'.$currentRoute.'.title')}}</title>
            <meta name="title" content="{{Config::get('constants.meta_tags.'.$currentRoute.'.title')}}">
            <meta name="keywords" content="{{Config::get('constants.meta_tags.'.$currentRoute.'.keywords')}}">
            <meta name="description" content="{{Config::get('constants.meta_tags.'.$currentRoute.'.description')}}">
            <meta name="author" content="{{Config::get('constants.meta_tags.'.$currentRoute.'.author')}}">
        @endif

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.png')}}">
        
        <!--Main Stylesheet-->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css')}}">
    </head>
    <body>

        <div class="mainsection">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-4 col-sm-4 col-xs-12">  
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="middlepart">

                        <div class="login-section">
                            <h1 class="text-center">Login</h1>
                            <div id="worker" class="tabcontent">
                                
                                @if(Session::has('error'))
                                    <div class="text-danger text-center">{{ Session::get('error') }}</div>
                                @elseif(Session::has('success'))
                                    <div class="text-success text-center">{{ Session::get('success') }}</div>
                                @endif
                                <form action="{{ route('user.login') }}" method="POST" id="login-form">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="type" value="User" id="type">
                                    
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label class="confirm-password">Password</label>
                                        <input type="password" class="form-control" name="password">
                                    </div>

                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-blue">LOGIN</button>
                                    </div>

                                    <div class="form-group text-center">
                                        <a href="{{route('user.signup')}}">Don't have an account yet?</a>
                                    </div>
                                </form>
                              
                            </div>
                                    
                        </div>

                    </div>
                </div>
            </div>
        </div>

    <!--jQuery JS-->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <!--Bootstrap JS-->
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('js/jquery.validate.js')}}"></script>
    <script type="text/javascript">

    $(document).ready(function () {
        $('#login-form').validate({ 
            rules: {
                email:{
                    required: true,
                    emailfull: true
                },
                password: {
                    required: true
                },
            },
            messages: {
                email:{
                    required: 'Please enter email',
                },
                password:{
                    required: 'Please enter password',
                },
            },
            errorElement : 'div',
            /*errorPlacement: function(error, element){
                $(element).addClass('error');
            },*/
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
        });

        jQuery.validator.addMethod("emailfull", function(value, element) {
             return this.optional(element) || /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(value);
        }, "Please enter valid email address");

    });
    </script>
    </body>
</html>

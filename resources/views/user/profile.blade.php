@extends('layout.main')
@section('content')

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="profileinfo">
            <h3 class="sectitle">Profile</h3>

            @if ($message = Session::get('success'))
                <div class="alert text-success">
                    <i class="fa fa-check"></i> {{ $message }}
                </div>
            @endif

            @if ($errors->any())
                <div class="error-contains">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="alert alert-danger text-danger"><i class="fa fa-close"></i> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="userprofinfo">
                <div class="username">
                    @if(!empty(Auth::guard('User')->user()->profile_pic))
                        <img src="{{asset(Config::get('constants.profile_image_thumb_url'). Auth::guard('User')->user()->profile_pic)}}" width="38">
                    @else
                        <img src="{{asset('images/profile.png')}}"  width="38">
                    @endif
                    <h5>{{Auth::guard('User')->user()->first_name.' '.Auth::guard('User')->user()->last_name}}</h5>
                </div>
                <div class="useremail">
                    <label>{{Auth::guard('User')->user()->email}}</label>
                </div>
            </div>
            <div class="userprofinfo" style="margin-top: 10px;">
                <div class="username" style="width: 100%">
                    <h5>Share Referral URL : <a href="{{route('user.signup').'?via='.Auth::guard('User')->user()->rewardful_token}}">{{route('user.signup').'?via='.Auth::guard('User')->user()->rewardful_token}}</a></h5>
                    <p style="margin-left: 10px;">({{$commission_text}})</p>
                </div>
            </div>
            @if(!empty($rewardful_app_login))
                <div class="userprofinfo" style="margin-top: 10px;">
                    <div class="username" style="width: 100%;">
                        <h5>Rewardful Dashboard : 
                        <a href="{{$rewardful_app_login}}" target="_blank">Your personal affiliates manager</a>
                        </h5>
                    </div>
                </div>
            @endif
            
            <div class="updateemailpass">
                <div class="updateemail" >
                    <a href="#" data-toggle="modal" data-target="#update-email-modal">UPDATE E-MAIL</a>
                </div>
                <div class="updatepass" >
                    <a href="#" data-toggle="modal" data-target="#update-password-modal">UPDATE PASSWORD</a>
                </div>
            </div>
            <div class="updateplan">
                <!-- <div class="updateplanlabel">
                    <a href="#">UPGRADE PLAN</a>
                </div> -->
                <div class="cancel-subscription">
                    <a href="{{route('user.cancel_subscription')}}" onclick="return confirm('Are you sure?')">CANCEL SUBSCRIPTION</a>
                </div> 
                <div class="updateplaninfo">
                    <h5>Current plan is : <span>GOLD</span> (79€ / month)</h5>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="update-email-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">     
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Update Email</h4>
          </div>
          <form method="post" action="{{route('user.update_email')}}" id="update-email-form">
            @csrf
          <div class="modal-body">
            <div class="form-group row">
                <label class="control-label col-sm-2">Email:</label>
                <div class="col-sm-10">
                    <input type="text" name="email" id="email" class="form-control" value="{{Auth::guard('User')->user()->email}}">
                    <div class="assign-influencer-msg text-success"></div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-blue assign-influencer-btn">Submit</button>
          </div>
      </form>
    </div>
  </div>
</div> 

<div id="update-password-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">     
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Update Password</h4>
          </div>
          <form method="post" action="{{route('user.update_password')}}" id="update-password-form">
            @csrf
          <div class="modal-body">
            <div class="form-group row">
                <label class="control-label col-sm-4">Old Password:</label>
                <div class="col-sm-8">
                    <input type="password" name="old_password" id="old_password" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-4">New Password:</label>
                <div class="col-sm-8">
                    <input type="password" name="new_password" id="new_password" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-4">Confirm Password:</label>
                <div class="col-sm-8">
                    <input type="password" name="confirm_password" id="confirm_password" class="form-control">
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-blue assign-influencer-btn">Submit</button>
          </div>
      </form>
    </div>
  </div>
</div>   
    
@section('pagescript')
<script type="text/javascript" src="{{ asset('js/jquery.validate.js')}}"></script>
<script type="text/javascript">
     $(document).ready(function () {

        $('#update-email-form').validate({ 
            rules: {
                email: {
                    required: true,
                    emailfull: true
                },
            },
            messages: {
                email:{
                    required: 'Please enter email.',
                },
            },
            errorElement : 'div',
            /*errorPlacement: function(error, element){
                $(element).addClass('error');
            },*/
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
        });
        jQuery.validator.addMethod("emailfull", function(value, element) {
             return this.optional(element) || /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(value);
        }, "Please enter valid email address");
    });
</script>
<script type="text/javascript">
       
        $(document).ready(function () {

            $('#update-password-form').validate({ 
                rules: {
                    new_password: {
                        required: true
                    },
                    confirm_password: {
                        required: true,
                        equalTo : "#new_password",
                    },
                    old_password: {
                        required: true,
                        remote: "{{route('user.check_old_password')}}"
                    },
                },
                messages: {
                    new_password:{
                        required: 'Please enter new password.',
                    },
                    confirm_password:{
                        required: 'Please enter confirm password.',
                    },
                    old_password:{
                        required: 'Please enter old password.',
                        remote: 'Your old password doesn\'t match.',
                    },
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
            });
        });
    </script>
@stop
@endsection
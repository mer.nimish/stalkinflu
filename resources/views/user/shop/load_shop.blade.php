@if(count($shops) > 0)
    @foreach($shops as $shop)
    <tr class="shop-row-link">
        <td class="text-center except add-list-checkbox">
           <div class="juiceboxes">
            <input type="checkbox" id="checked-shop-{{$shop->shop_id}}" class="checked-shop-id" value="{{$shop->shop_id}}"> 
             <label for="checked-shop-{{$shop->shop_id}}"></label>
          </div>

        </td>
        <td class="text-left website-col" data-shop-details-url="{{route('shop.show', $shop->shop_id)}}">
            <!-- <a href="#" onclick="select_shop_list('{{$shop->shop_id}}')"><img class="addlist" src="{{ asset('images/addlist.png')}}"></a> -->
            <span title="{{$shop->website}}" data-toggle="tooltip" data-placement="top">{{ Str::limit($shop->website, 25) }}</span>
        </td>
   

        <td data-shop-details-url="{{route('shop.show', $shop->shop_id)}}" class="text-center">{{ date("m/d/y", strtotime($shop->first_added_date)) }}</td>
        <td data-shop-details-url="{{route('shop.show', $shop->shop_id)}}" class="text-center">{{Helpers::time_elapsed_string($shop->last_seen_date) }}</td>
        <td data-shop-details-url="{{route('shop.show', $shop->shop_id)}}" class="text-center">
            @if($shop->activity == 'high')
                <span class="activity-color-red">{{$shop->promo_this_week}}</span>
            @elseif($shop->activity == 'medium')
                <span class="activity-color-green">{{$shop->promo_this_week}}</span>
            @else
               <span class="activity-color-yellow">{{$shop->promo_this_week}}</span>
            @endif
        </td>
        <td data-shop-details-url="{{route('shop.show', $shop->shop_id)}}" class="text-center">{{$shop->nb_of_promotions}}</td>
        
        <td data-shop-details-url="{{route('shop.show', $shop->shop_id)}}" class="text-center">
          @if(!empty($shop->country_codes))
              @php
              $countries = explode(",", $shop->country_codes);
              $countries2 = explode(",", $shop->country_names);
              @endphp

              @foreach($countries as $key=>$country_code)
                   <img class="country-flag" src="{{asset('images/flags/'.Str::lower($country_code).'.svg')}}" data-toggle="tooltip" data-placement="top" data-html="true" title="{{$countries2[$key]}}"/>&nbsp;
              @endforeach
          @endif
          <!-- <img class="country-flag" src="{{asset('images/flags/'.Str::lower($shop->country_code).'.svg')}}" data-toggle="tooltip" data-placement="top" data-html="true" title="{{$shop->country_name}}"/> -->

        </td>
        <td data-shop-details-url="{{route('shop.show', $shop->shop_id)}}" class="text-center">{{$shop->total_influencers}}</td>
        <td data-shop-details-url="{{route('shop.show', $shop->shop_id)}}" class="text-center">
        @if($shop->category_cnt != 0)
           @if(!empty($shop->categories))
              @php
              $categories = explode("<br />", $shop->categories);
              @endphp

              <span class="category-text">{{ $categories[0] }}</span>
              @if(count($categories) > 1)
                  @php
                    unset($categories[0]);
                  @endphp
                  <span class="category-cnt" data-toggle="tooltip" data-placement="top" data-html="true" title="{{implode('<br />', $categories)}}">+{{ ($shop->category_cnt-1) }}</span>
              @endif
            @endif
        @else
          <span class="category-text">Coming soon</span>
        @endif
        </td>
        <!-- <td class="text-center">
            <a href="{{route('shop.show', $shop->shop_id)}}"  class="btn btn-primary btn-xs">show more</a>
        </td> -->
    </tr>
    @endforeach
<tr>
   <td><a href="Javascript:void(0);" class="selected-deselected" id="select-all-shop"><img src="{{ asset('images/-deselected.png')}}"></a></td>
    <td colspan="9" class="pagination-section">
        @if($shops->total() == $total_rec_cnt)
            <p class="pagination-info">{{ $total_rec_cnt }} shops
            </p>
        @else
            <p class="pagination-info">{{ $shops->total() }} out of {{$total_rec_cnt}} shops
            </p>
        @endif

        {!! $shops->links() !!} 
    </td>
    <td style="display: none;"></td>
</tr>
@endif
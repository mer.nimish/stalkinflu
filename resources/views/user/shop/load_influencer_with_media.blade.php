@if(count($influencers) > 0)
    @foreach($influencers as $influencer)
        
    <div class="col-md-3 col-sm-4 col-xs-12 {{$influencer->shop_id}}">
        <div class="item">
            <div class="image">
                @if(File::exists('public/storage/shop_media/main/'.$influencer->shop_id.'.mp4'))
                    <video controls>
                      <source src="{{ asset('storage/shop_media/main/'.$influencer->shop_id.'.mp4')}}" type="video/mp4">
                      Your browser does not support the video tag.
                    </video>
                @else
                    <img src="{{asset('images/no-video.png') }}" width="240" height="368" />
                @endif
                
            </div>
            <div class="mediacontent">
                <div class="time-on-live">
                    <span>{{Helpers::time_elapsed_string($influencer->created_at)}}</span>
                </div>
                <div class="userinfo">
                    @if(!empty($influencer->profile_pic))
                        <img class="usericon" src="{{asset(Config::get('constants.profile_image_thumb_url').$influencer->profile_pic)}}">
                    @else
                        <img class="usericon" src="{{asset('images/profile.png')}}">
                    @endif
                    <span>
                        <a href="{{route('influencer.show', $influencer->influencer_id)}}" target="_blank">{{$influencer->instagram_id}}</a>
                        @if($influencer->is_verified)
                            <img class="profile-verified-icon" src="{{ asset('images/verified.png')}}"/>
                        @endif
                    </span>
                    <a href="#" onclick="select_influencer_list('{{$influencer->influencer_id}}');"><img class="addlist" src="{{ asset('images/addlist.png')}}"></a>
                </div>
                <div class="countryinfo">
                    <img class="countryicon" src="{{asset('images/flags/'.Str::lower($influencer->country_code).'.svg')}}" data-toggle="tooltip" data-placement="top" data-html="true" title="{{$influencer->country_name}}"/>
                    <span class="followers">{{Helpers::short_number_format($influencer->followers)}} followers</span>
                    
                    <span class="stories">{{$influencer->stories}} stories</span>
                </div>
                <div class="info">
                    <span class="eng-rate">{{$influencer->engagement_rate}}% eng.</span>

                    @if($influencer->category_cnt != 0)
               
                      @if(!empty($influencer->categories))
                        @php
                        $categories = explode("<br />", $influencer->categories);
                        @endphp

                        <span class="category-text">{{ $categories[0] }}</span>
                        @if(count($categories) > 1)
                            @php
                                unset($categories[0]);
                            @endphp
                            <span class="category-cnt" data-toggle="tooltip" data-placement="top" data-html="true" title="{{implode('<br />', $categories)}}">+{{ ($influencer->category_cnt-1) }}</span>
                        @endif
                      @endif
                    @else
                        <span class="category-text">Coming soon</span>
                    @endif 
                   
                </div>
            </div>
        </div>
    </div>

    @endforeach

    <div class="pagination-section">
        @if($influencers->total() == $total_rec_cnt)
            <p class="pagination-info">{{ $total_rec_cnt }} promotions
            </p>
        @else
            <p class="pagination-info">{{ $influencers->total() }} out of {{$total_rec_cnt}} promotions
            </p>
        @endif

        {!! $influencers->links() !!} 
    </div>

@else
    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
        Not Media Found.
    </div>
@endif


@if(count($influencers) > 0)
    @foreach($influencers as $influencer)
    <tr class="influencer-row-link">
        <td class="text-center except add-list-checkbox">
           <div class="juiceboxes">
            <input type="checkbox" id="checked-unique-influencer-{{$influencer->influencer_id}}" class="checked-influencer-id" value="{{$influencer->influencer_id}}"> 
             <label for="checked-unique-influencer-{{$influencer->influencer_id}}"></label>
          </div>
        </td>
        <td class="text-center influencer-rank" data-influencer-details-url="{{route('influencer.show', $influencer->influencer_id)}}">
             <!-- <a href="#"><img class="addlist" src="{{ asset('images/addlist.png')}}"></a> -->
            {{$influencer->rank}}.
        </td>
        <td class="text-left instagram-profile-pic" data-influencer-details-url="{{route('influencer.show', $influencer->influencer_id)}}">
            @if(!empty($influencer->profile_pic))
                <img src="{{asset(Config::get('constants.profile_image_thumb_url').$influencer->profile_pic)}}">
            @else
                <img src="{{asset('images/profile.png')}}">
            @endif
            {{$influencer->instagram_id}}
            @if($influencer->is_verified)
                <img src="{{ asset('images/verified.png')}}" class="verified-badge" />
            @endif
        </td>
        <td class="text-center" data-influencer-details-url="{{route('influencer.show', $influencer->influencer_id)}}"><img class="country-flag" data-toggle="tooltip" data-placement="top" data-html="true" src="{{asset('images/flags/'.Str::lower($influencer->country_code).'.svg')}}" title="{{$influencer->country_name}}"/></td>
        <td class="text-center" data-influencer-details-url="{{route('influencer.show', $influencer->influencer_id)}}"><span class="followers">{{Helpers::short_number_format($influencer->followers)}}</span></td>
        <td class="text-center" data-influencer-details-url="{{route('influencer.show', $influencer->influencer_id)}}">{{$influencer->engagement_rate}}%</td>
         
         <td class="text-center" data-influencer-details-url="{{route('influencer.show', $influencer->influencer_id)}}">
            {{$influencer->nb_of_promotion_of_the_shop}}
        </td>
        <td class="text-center" data-influencer-details-url="{{route('influencer.show', $influencer->influencer_id)}}">
            @if($influencer->category_cnt != 0)
                @if(!empty($influencer->categories))
                  @php
                  $categories = explode("<br />", $influencer->categories);
                  @endphp

                  <span class="category-text">{{ $categories[0] }}</span>
                  @if(count($categories) > 1)
                      @php
                          unset($categories[0]);
                      @endphp
                      <span class="category-cnt" data-toggle="tooltip" data-placement="top" data-html="true" title="{{implode('<br />', $categories)}}">+{{ ($influencer->category_cnt-1) }}</span>
                  @endif
                @endif
              @else
                  <span class="category-text">Coming soon</span>
              @endif 
        </td>
        <td class="text-center" data-influencer-details-url="{{route('influencer.show', $influencer->influencer_id)}}">{{ !empty($influencer->lastest_promo) ?  Helpers::time_elapsed_string($influencer->lastest_promo) : '' }}</td>
        <!-- <td class="text-center">
            <a href="{{route('influencer.show', $influencer->influencer_id)}}"  class="btn btn-primary btn-xs">show more</a>
        </td> -->
    </tr>
    @endforeach

<tr>
    <td><a href="Javascript:void(0);" class="selected-deselected" id="select-all-unique-inf"><img src="{{ asset('images/-deselected.png')}}"></a></td>
    <td colspan="8" class="pagination-section">

        @if($influencers->total() == $total_rec_cnt)
            <p class="pagination-info">{{ $total_rec_cnt }} influencers
            </p>
        @else
            <p class="pagination-info">{{ $influencers->total() }} out of {{$total_rec_cnt}} influencers
            </p>
        @endif
        
        {!! $influencers->links() !!} 
    </td>
    <td style="display: none;"></td>
</tr>
@endif
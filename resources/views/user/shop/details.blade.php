@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <!--alerts CSS -->
    <link href="{{ asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.css')}}">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@stop

<div class="col-md-9 col-sm-12 col-xs-12 col-lprp-0"> 

<div class="col-md-9 col-sm-6 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="shop-details-back">
            <a href="{{route('shop.index')}}">BACK</a>
        </div>
        <div class="shop-details-head text-cetner">
            <a href="{{$shop->website}}" target="_blank"><span title="{{$shop->website}}" data-toggle="tooltip" data-placement="top">{{ Str::limit($shop->website, 55) }}</span></a>
            <select id="add_to_shop_list">
                <option><img src="{{ asset('images/addlist.png')}}" width="17" /></option>
                @foreach($shop_list as $list)
                 <option value="{{$list->shop_list_id}}">{{$list->list_name}}</option>
                @endforeach
            </select>
        </div>

        <div class="shop-details-activity-section">
            <div class="section-inner">
                <div class="item">
                    <div class="webinfoinfo">
                        <p>ALL ACTIVITY</p>
                    </div>
                    <div class="webinfopromos">
                        <p>-</p>
                    </div>
                </div>
                <div class="item">
                    <div class="webinfoinfo">
                        <p>ACTIVITY THIS WEEK</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{$shop->activity}}</p>
                    </div>
                </div>
                <div class="item">
                    <div class="webinfoinfo">
                        <p>FIRST MARKET</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{$country_name}}</p>
                    </div>
                </div>    
            </div>
        </div>
        
        <!-- <div class="shop-details-activity-lan">
            <div class="col-md-4">
                <div class="item">
                    <div class="webinfoinfo">
                        <p>All Activity</p>
                    </div>
                    <div class="webinfopromos">
                        <p>-</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Activity This week</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{$shop->activity}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Country</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{$country_name}}</p>
                    </div>
                </div>
            </div>
        </div>  -->

        <div class="shop-details-website-frame">
            <iframe id="website-frame" src="{{Config::get('constants.site_screenshot_url').$shop->website_screenshot}}" frameborder="0" allowfullscreen></iframe>
           
        </div>
    </div>
</div> 
<div class="col-md-3 col-sm-3 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="shop-details-web-info shopdetails-webinfo">
            <h3 class="sectitle">Website information:</h3>
            <div class="item">
                <div class="webinfoinfo">
                    <div>Total</div>
                    <div class="sub-title">PROMO</div>
                </div>
                <div class="webinfopromos">
                    <p><span>{{$total_promotions}}</span></p>
                </div>
            </div>
            <div class="item">
                <div class="webinfoinfo">
                    <div>Total</div>
                    <div class="sub-title">INFLU</div>
                </div>
                <div class="webinfopromos">
                    <p><span>{{$total_influencers}}</span></p>
                </div>
            </div>
            
            <div class="item">
                <div class="webinfoinfo">
                    <div>First</div>
                    <div class="sub-title">ADDED</div>
                </div>
                <div class="webinfopromos">
                    <p><span>{{date("m/d/Y", strtotime($first_added))}}</span></p>
                </div>
            </div>
            <div class="item">
                <div class="webinfoinfo">
                    <div>last</div>
                    <div class="sub-title">Seen</div>
                </div>
                <div class="webinfopromos">
                    <p><span>{{Helpers::time_elapsed_string($last_seen)}}</span></p>
                </div>
            </div>
            
            <div class="item">
                <div class="webinfoinfo">
                     <div>Promo</div>
                    <div class="sub-title">This week</div>
                </div>
                <div class="webinfopromos">
                    <p><span>{{$promo_of_week}}</span></p>
                </div>
            </div>
            <div class="item">
                <div class="webinfoinfo">
                    <div>Latest</div>
                    <div class="sub-title">Promo</div>
                </div>
                <div class="webinfopromos">
                    <p><span>{{$latest_promotion}}</span></p>
                </div>
            </div>
            <div class="item">
               <!--  <div class="webinfoinfo" style="width: 35px;padding: 10px;">
                    <p style="transform: rotate(-91deg);margin-top: 63px;">CATEGORIES</p>
                </div> -->
                <div class="webinfoinfo">
                    <div>CATE-</div>
                    <div class="sub-title">GORIES</div>
                </div>
                <div class="webinfopromos">
                    <p>
                    
                    @if(!empty($category_names->category_name))
                    @php
                    $categories = explode(",", $category_names->category_name);
                    @endphp
                        @foreach($categories as $category)
                            <span class="category">{{ $category }}</span><br />
                        @endforeach
                    @endif
                   </p>     
                </div>
            </div>
        </div>
    </div>
</div>


<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
    <div class="more-info-of-site">
        <h3>More information of <span>{{ Str::limit($shop->website, 30) }}</span>: </h3>
        <div class="col-md-12 col-sm-12 col-xs-12 no-padding col-lprp-0">
            <div class="col-md-3">
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{$total_stories}}</h4>
                        <span>Total Stories (every promo)</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{$average_story_nb}}/promo</h4>
                        <span>Average Stories number</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{Helpers::short_number_format($total_unique_impressions)}}</h4>
                        <span>Total unique impressions</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{Helpers::short_number_format($average_impressions)}}/influencer</h4>
                        <span>Average impressions</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 no-padding col-lprp-0">
            <div class="col-md-3">
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{$most_used_influence}}</h4>
                        <span>Most used influencer</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{$nb_of_unique_markets}}</h4>
                        <span>Number of unique markets</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{$average_promo_by_day}}/day</h4>
                        <span>Average promo (day)</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="statiscticitem">
                    <div class="info">
                        <h4>{{$average_promo_by_week}}/week</h4>
                        <span>Average promo (week)</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="total-story-section-1">
            <div class="col-md-3">
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Total Stories</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{$total_stories}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Average Stories nb</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{$average_story_nb}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Total unique impressions</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{Helpers::short_number_format($total_followers)}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Average impressions</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{Helpers::short_number_format($average_impressions)}}/</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="total-story-section-1">
            <div class="col-md-3">
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Most used influencer</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{$most_used_influence}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Number of unique markets</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{$nb_of_unique_markets}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Just add a static Text for now</p>
                    </div>
                    <div class="webinfopromos">
                        <p>-</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="item">
                    <div class="webinfoinfo">
                        <p>Average promo by day</p>
                    </div>
                    <div class="webinfopromos">
                        <p>{{$average_promo_by_day}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --> 
<div class="col-md-12 col-sm-12 col-xs-12 no-padding explored-market-sec">
    <div class="middlepart">
        <div class="explored-market-section">
            <div class="col-md-2 col-lprp-0">
                <div class="title">
                    <div class="">
                        <h2><span>EXPLORED</span></h2>
                    </div>
                    <div class="">
                        <h2>MARKETS:</h2>
                    </div>
                </div>
            </div>
            <div class="col-md-10 col-lprp-0">
                @foreach($explored_market as $market)
                <div class="col-md-3">
                    <div class="item">
                        <div class="country-flag">
                           <img class="country-flag" src="{{asset('images/flags/'.Str::lower($market->country_code).'.svg')}}" title="{{$market->country_name}}" data-toggle="tooltip" data-placement="top" data-html="true" />
                        </div>
                        <div class="webinfopromos">
                            <p>{{$market->promo}} {{ ($market->promo > 1 ? "promos" : "promo")}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12 no-padding shops-with-media-sections all-promotions-media-sections">
    <div class="middlepart">
        <div class="promotedshops">
            
            <div class="topbar">
                <h3>All promotions of this shop <span>WITH MEDIAS:</span></h3>
                <div class="sortby">
                    <label>SORT BY:</label>
                    <select name="sort_by_with_media" id="sort_by_with_media" class="selectpicker">
                        <option data-sorting-type="asc" data-column-name="country_name" data-icon="fa fa-arrow-up sort-icon">By market</option>
                        <option data-sorting-type="desc" data-column-name="country_name" data-icon="fa fa-arrow-down sort-icon">By market</option>
                        <option data-sorting-type="asc" data-column-name="followers" data-icon="fa fa-arrow-up sort-icon">By followers</option>
                        <option data-sorting-type="desc" data-column-name="followers" data-icon="fa fa-arrow-down sort-icon">By followers</option>
                        <option data-sorting-type="asc" data-column-name="engagement_rate" data-icon="fa fa-arrow-up sort-icon">By engagement rate</option>
                        <option data-sorting-type="desc" data-column-name="engagement_rate" data-icon="fa fa-arrow-down sort-icon">By engagement rate</option>
                        <option data-sorting-type="asc" data-column-name="stories" data-icon="fa fa-arrow-up sort-icon">By NB of Story</option>
                        <option data-sorting-type="desc" data-column-name="stories" data-icon="fa fa-arrow-down sort-icon">By NB of Story</option>
                        <option data-sorting-type="asc" data-column-name="categories" data-icon="fa fa-arrow-up sort-icon">By categories/niches</option>
                        <option data-sorting-type="desc" data-column-name="categories" data-icon="fa fa-arrow-down sort-icon">By categories/niches</option>
                        <option data-sorting-type="asc" data-column-name="shop.created_at" data-icon="fa fa-arrow-up sort-icon">By promotion date</option>
                        <option data-sorting-type="desc" data-column-name="shop.created_at" data-icon="fa fa-arrow-down sort-icon">By promotion date</option>

                    </select>
                </div>
            </div>
            <div class="searchwebsite">
                <label>Search a influencer:</label>
                <input type="text" name="with_media_search_influencer" id="with_media_search_influencer" placeholder="ENTER AN INSTAGRAM ID" autocomplete="false" onfocus="this.placeholder = ''" onblur="this.placeholder = 'ENTER AN INSTAGRAM ID'">
            </div>

            <div class="showonly">
                <label>Show only: </label>
                <div class="showonlylist">
                   <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'WM_MARKET')">WORLD</a>
                    <!-- <select class="selectpicker" multiple id="with_media_filter_country_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.png')}}' /> {{ $country->country_name}}"></option>
                        @endforeach
                    </select> -->

                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'WM_FOLLOWERS')">NB OF FOLLOWERS</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'WM_PROMOTION_DATE_RANGE')">PROMOTION DATE RANGE</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'WM_NB_OF_STORY')">NB OF STORY</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'WM_ENGAGEMENT_RATE')">ENGAGEMENT RATE</a>
                   
                   <input type="hidden" id="with_media_is_verified" value="0">
                    <a class="with-media-verified-filter"><img src="{{ asset('images/verified.png')}}"></a>
                </div>
            </div>
        
             <div class="filter-tab-content">
                <div id="WM_MARKET" class="tabcontent">
                   <div class="col-md-2">
                       <select class="selectpicker" multiple id="with_media_filter_country_ids">
                       @foreach($countries as $country)
                         <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.svg')}}' /> {{ $country->country_name}}"></option>
                       @endforeach
                       </select>
                   </div>
                   <!-- <div class="col-md-3">
                       <button id="with_media_country_filter" class="filter-btn">FILTER</button>
                   </div> -->
               </div> 
                <div id="WM_FOLLOWERS" class="tabcontent">
                     <div class="col-md-3">
                        <input type="text" name="with_media_follower_from" id="with_media_follower_from" placeholder="FROM" class="only-number-allowed datepicker"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="with_media_follower_to" id="with_media_follower_to" placeholder="TO" class="only-number-allowed datepicker">
                    </div>
                   <!--  <div class="col-md-3">
                       <button id="with_media_follower_filter" class="filter-btn">FILTER</button>
                   </div> -->
                </div>
                <div id="WM_PROMOTION_DATE_RANGE" class="tabcontent">
                     <div class="col-md-3">
                        <input type="text" name="with_media_promotion_date_from" id="with_media_promotion_date_from" placeholder="FROM" class="only-number-allowed datepicker"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="with_media_promotion_date_to" id="with_media_promotion_date_to" placeholder="TO" class="only-number-allowed datepicker">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="with_media_promotion_date_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="WM_NB_OF_STORY" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="nb_of_story_from" id="with_media_nb_of_story_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="nb_of_story_to" id="with_media_nb_of_story_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="with_media_nb_of_story_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="WM_ENGAGEMENT_RATE" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="with_media_engagement_from" id="with_media_engagement_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="with_media_engagement_to" id="with_media_engagement_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="with_media_engagement_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
            </div>
            <div class="activated-filter showonly">
                <label>Activated filters: </label>
                <span class="with-media-country-filter-contains"></span>
                <span class="with-media-nb-of-followers-filter-contains"></span>
                <span class="with-media-promotions-date-filter-contains"></span>
                <span class="with-media-nb-of-story-filter-contains"></span>
                <span class="with-media-eng-rate-filter-contains"></span>
                <span class="with-media-verified-filter-contains"></span>
            </div>
    </div>
    <div class="promotedshops-listing">
            <div class="shopslistings">

                <div class="error-contains">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success text-success">
                        <i class="fa fa-check"></i> {{ $message }}
                    </div>
                @endif
                </div>

                <div class="promotions-of-medias">
                    <div class="promotionsofmediaslistings">
                        <div class="col-md-12 col-sm-12 col-xs-12 promotions-with-story">
                        </div>
                  </div>
                </div>
              </div>
            
        </div>
    </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 no-padding influencersection">
    <div class="middlepart">
        <div class="promotions-of-this-shop">
            <div class="topbar">
                <h3>All promotions of this shop <span>WITHOUT MEDIAS</span>:</h3>
                 <div class="sortby">
                    <label>SORT BY:</label>
                     <select name="sort_by" id="sort_by" class="selectpicker">
                        <option data-sorting-type="asc" data-column-name="engagement_rate" data-icon="fa fa-arrow-up sort-icon">By engagement rate</option>
                        <option data-sorting-type="desc" data-column-name="engagement_rate" data-icon="fa fa-arrow-down sort-icon">By engagement rate</option>
                        <option data-sorting-type="asc" data-column-name="country_name" data-icon="fa fa-arrow-up sort-icon">By country</option>
                        <option data-sorting-type="desc" data-column-name="country_name" data-icon="fa fa-arrow-down sort-icon">By country</option>
                        <option data-sorting-type="asc" data-column-name="followers" data-icon="fa fa-arrow-up sort-icon">By followers</option>
                        <option data-sorting-type="desc" data-column-name="followers" data-icon="fa fa-arrow-down sort-icon">By followers</option>
                        <option data-sorting-type="asc" data-column-name="shop.created_at" data-icon="fa fa-arrow-up sort-icon">By promotion date</option>
                        <option data-sorting-type="desc" data-column-name="shop.created_at" data-icon="fa fa-arrow-down sort-icon">By promotion date</option>
                        <option data-sorting-type="asc" data-column-name="categories" data-icon="fa fa-arrow-up sort-icon">By categories/niches</option>
                        <option data-sorting-type="desc" data-column-name="categories" data-icon="fa fa-arrow-down sort-icon">By categories/niches</option>
                    </select>
                </div>
            </div>

             <div class="searchwebsite">
                <label>Search an Influencer :</label>
                <input type="text" name="search_instagram_id" id="search_instagram_id" placeholder="ENTER AN INSTAGRAM ID" onfocus="this.placeholder = ''" onblur="this.placeholder = 'ENTER AN INSTAGRAM ID'">
            </div>
            <div class="showonly">
            <label>Show only: </label>
            <div class="showonlylist">
                <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'COUNTRY')">WORLD</a> 
               <!-- <select class="selectpicker" multiple id="filter_country_ids">
                    @foreach($countries as $country)
                      <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.png')}}' /> {{ $country->country_name}}"></option>
                    @endforeach
                </select> -->

                 <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'FOLLOWERS')">NB OF FOLLOWERS</a>
                <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'PROMOTION_DATE_RANGE')">PROMOTION DATE RANGE</a>
                <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'NB_OF_STORY')">NB OF STORY</a>
                <!-- <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'PROMOTION_THIS_WEEK')">PROMOTION THIS WEEK</a> -->
                <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'ENGAGEMENT_RATE')">ENGAGEMENT RATE</a>
               
               <input type="hidden" id="is_verified" value="0">
                <a class="verified-filter"><img src="{{ asset('images/verified.png')}}"></a>
                
            </div>
        </div>
            <div class="filter-tab-content">
                <div id="COUNTRY" class="tabcontent">
                    <div class="col-md-2">
                        <select class="selectpicker" multiple id="filter_country_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.svg')}}' /> {{ $country->country_name}}">{{ $country->country_name}}</option>
                        @endforeach
                        </select>
                    </div>
                   <!--  <div class="col-md-3">
                       <button id="country_filter" class="filter-btn">FILTER</button>
                   </div> -->
                </div>

                <div id="PROMOTION_THIS_WEEK" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="promo_this_week_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="promo_this_week_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="promo_this_week_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="ENGAGEMENT_RATE" class="tabcontent">
                     <div class="col-md-3">
                        <input type="text" name="engagement_rate_from" id="engagement_rate_from" placeholder="FROM" class="only-number-allowed" maxlength='2'> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="engagement_rate_to" id="engagement_rate_to" placeholder="TO" class="only-number-allowed" maxlength='3'>
                    </div>
                    <!--  <div class="col-md-3">
                       <button id="engagement_rate_filter" class="filter-btn">FILTER</button>
                                        </div> -->
                </div>
                <div id="FOLLOWERS" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="followers_from" id="followers_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="followers_to" id="followers_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="followers_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="PROMOTION_DATE_RANGE" class="tabcontent">
                     <div class="col-md-3">
                        <input type="text" name="promotion_date_from" id="promotion_date_from" placeholder="FROM" class="only-number-allowed datepicker"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="promotion_date_to" id="promotion_date_to" placeholder="TO" class="only-number-allowed datepicker">
                    </div>
                   <!--  <div class="col-md-3">
                       <button id="promotion_date_filter" class="filter-btn">FILTER</button>
                   </div> -->
                </div>
                <div id="NB_OF_STORY" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="nb_of_story_from" id="nb_of_story_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="nb_of_story_to" id="nb_of_story_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="nb_of_story_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
            </div>
            <div class="activated-filter showonly">
                <label>Activated filters: </label>
                <span class="country-filter-contains"></span>
                <span class="nb-of-followers-filter-contains"></span>
                <span class="promotions-date-filter-contains"></span>
                <span class="nb-of-story-filter-contains"></span>
                <span class="eng-rate-filter-contains"></span>
                <span class="verified-filter-contains"></span>
            </div>
    </div>
    <div class="promotedshops-listing">      
                <div class="shopslistings">
                    <div class="shopslistingsinner">
                        <table id="shoptable" class="table display nowrap">
                        <thead>
                            <tr>
                                <th width="2%">
                                     <a href="Javascript:void(0);" onclick="select_influencer_list_2()"><img class="assign-list-btn" src="{{ asset('images/icons8-add_new.svg')}}" /></a>
                                 </th>
                               <th class="text-center">Instagram ID</th>
                               <th class="text-center">Country</th>
                                <th class="text-center">Followers</th>
                                <th class="text-center">Eng. rate</th>
                                <th class="text-center">Nb of Story</th>
                                <!-- <th class="text-center">Latest promo</th> -->
                                <th class="text-center no-sort">CATEGORIES/NICHES</th>
                                <th class="text-center">Promo Date</th>
                            </tr>
                        </thead>
                           <tbody class="influencers_tr">
                                 
                             </tbody>  
                        </table>
                    </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 no-padding ranking-of-unique-inf">
    <div class="middlepart">
        <div class="promotions-of-this-shop">
            <div class="topbar">
                <h3>Ranking of unique influencers who promoted this shop :</h3>
                 <div class="sortby">
                    <label>SORT BY:</label>
                     <select name="rou_sort_by" id="rou_sort_by" class="selectpicker">
                        <option data-sorting-type="desc" data-column-name="nb_of_promotion_of_the_shop" data-icon="fa fa-arrow-down sort-icon">By promo for this shop</option>
                        <option data-sorting-type="asc" data-column-name="nb_of_promotion_of_the_shop" data-icon="fa fa-arrow-up sort-icon">By promo for this shop</option>
                        <option data-sorting-type="asc" data-column-name="country_name" data-icon="fa fa-arrow-up sort-icon">By market</option>
                        <option data-sorting-type="desc" data-column-name="country_name" data-icon="fa fa-arrow-down sort-icon">By market</option>
                        <option data-sorting-type="asc" data-column-name="followers" data-icon="fa fa-arrow-up sort-icon">By followers</option>
                        <option data-sorting-type="desc" data-column-name="followers" data-icon="fa fa-arrow-down sort-icon">By followers</option>
                        <option data-sorting-type="asc" data-column-name="lastest_promo" data-icon="fa fa-arrow-up sort-icon">By latest promo date</option>
                        <option data-sorting-type="desc" data-column-name="lastest_promo" data-icon="fa fa-arrow-down sort-icon">By latest promo date</option>
                        <option data-sorting-type="asc" data-column-name="engagement_rate" data-icon="fa fa-arrow-up sort-icon">By engagement rate</option>
                        <option data-sorting-type="desc" data-column-name="engagement_rate" data-icon="fa fa-arrow-down sort-icon">By engagement rate</option>
                        <option data-sorting-type="asc" data-column-name="categories" data-icon="fa fa-arrow-up sort-icon">By categories/niches</option>
                        <option data-sorting-type="desc" data-column-name="categories" data-icon="fa fa-arrow-down sort-icon">By categories/niches</option>
                    </select>
                </div>
            </div>

             <div class="searchwebsite">
                <label>Search an Influencer :</label>
                <input type="text" name="rou_search_instagram_id" id="rou_search_instagram_id" placeholder="ENTER AN INSTAGRAM ID" onfocus="this.placeholder = ''" onblur="this.placeholder = 'ENTER AN INSTAGRAM ID'">
            </div>
            <div class="showonly">
            <label>Show only: </label>
            <div class="showonlylist">
                <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'ROU_MARKET')">WORLD</a>
                 <!-- <select class="selectpicker" multiple id="rou_filter_market_ids">
                    @foreach($countries as $country)
                      <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.png')}}' /> {{ $country->country_name}}"></option>
                    @endforeach
                                 </select> -->


                <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'ROU_FOLLOWERS')">NB OF FOLLOWERS</a>
                <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'ROU_NB_OF_PROMO_OF_THE_SHOP')">NB OF PROMO OF THE SHOP</a>
                <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'ROU_LATEST_PROMO_DATE')">LATEST PROMO DATE</a>
                <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'ROU_ENGAGEMENT_RATE')">ENGAGEMENT RATE</a>
            </div>
        </div>
            <div class="filter-tab-content">
                <div id="ROU_MARKET" class="tabcontent">
                    <div class="col-md-2">
                        <select class="selectpicker" multiple id="rou_filter_market_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.svg')}}' /> {{ $country->country_name}}">{{ $country->country_name}}</option>
                        @endforeach
                        </select>
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="rou_market_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div> 
                <div id="ROU_FOLLOWERS" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="rou_followers_from" id="rou_followers_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="rou_followers_to" id="rou_followers_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="rou_followers_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="ROU_NB_OF_PROMO_OF_THE_SHOP" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="rou_nb_of_promo_of_shop_from" id="rou_nb_of_promo_of_shop_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="rou_nb_of_promo_of_shop_to" id="rou_nb_of_promo_of_shop_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="rou_nb_of_promo_of_shop_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="ROU_LATEST_PROMO_DATE" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="rou_latest_promo_date_from" id="rou_latest_promo_date_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="rou_latest_promo_date_to" id="rou_latest_promo_date_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="rou_latest_promo_date_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="ROU_ENGAGEMENT_RATE" class="tabcontent">
                     <div class="col-md-3">
                        <input type="text" name="rou_engagement_rate_from" id="rou_engagement_rate_from" placeholder="FROM" class="only-number-allowed" maxlength='2'> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="rou_engagement_rate_to" id="rou_engagement_rate_to" placeholder="TO" class="only-number-allowed" maxlength='3'>
                    </div>
                    <!--  <div class="col-md-3">
                       <button id="rou_engagement_rate_filter" class="filter-btn">FILTER</button>
                                        </div> -->
                </div>
               
            </div>
             <div class="activated-filter showonly">
                <label>Activated filters: </label>
                <span class="unique-inf-country-filter-contains"></span>
                <span class="unique-nb-of-followers-filter-contains"></span>
                <span class="unique-nb-of-promo-of-the-shop-filter-contains"></span>
                <span class="unique-latest-promo-date-filter-contains"></span>
                <span class="unique-eng-rate-filter-contains"></span>
            </div>
        </div>
        <div class="promotedshops-listing">
                <div class="shopslistings">
                    <div class="shopslistingsinner">
                        <table id="shoptable" class="table display nowrap">
                        <thead>
                            <tr>
                                <th width="2%">
                                    <a href="Javascript:void(0);" onclick="select_influencer_list_2()"><img class="assign-list-btn" src="{{ asset('images/icons8-add_new.svg')}}" /></a>
                                </th>
                                <th class="text-center" width="5%"></th>
                                <th class="text-center">Instagram ID</th>
                                <th class="text-center">Country</th> 
                                <th class="text-center">Followers</th>
                                <th class="text-center">Engagement rate</th>
                               
                                <th class="text-center">promo for this shop</th>
                                 <th class="text-center no-sort">CATEGORIES/NICHES</th>
                                <th class="text-center">Latest promo date</th>
                                
                            </tr>
                        </thead>
                           <tbody class="ranking-of-unique-inf-tab-body">
                                 
                             </tbody>  
                        </table>
                    </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="shop-details-all-activity-chart">
            <div class="topbar">
                <h3>All Activity</h3>
            </div>

            <div id="activity-chart"></div>
        </div>
    </div>
</div>
      
</div>    

<div id="assign-influencer-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">     
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Assign influencer into list</h4>
          </div>
          <div class="modal-body">
            
            <input type="hidden" id="assign_influencer_id">
            <div class="form-group row">
                <label class="control-label col-sm-2">List Name:</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" id="assign_influencer_list_id" multiple=""> 
                        <option value="" disabled="">Select Influencer List</option>
                        @foreach($influencer_list as $list)
                         <option value="{{$list->influencer_list_id}}">{{$list->list_name}}</option>
                        @endforeach
                    </select>
                    <div class="assign-influencer-msg text-success"></div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-blue assign-influencer-btn">Submit</button>
          </div>
    </div>
  </div>
</div>   

<div id="assign-influencer-modal-2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">     
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Assign influencer into list</h4>
          </div>
          <div class="modal-body">
            
            <div class="form-group row">
                <label class="control-label col-sm-2">List Name:</label>
                <div class="col-sm-10">
                    <select class="form-control selectpicker" id="assign_influencer_list_id_2" multiple=""> 
                        <option value="" disabled="">Select Influencer List</option>
                        @foreach($influencer_list as $list)
                         <option value="{{$list->influencer_list_id}}">{{$list->list_name}}</option>
                        @endforeach
                    </select>
                    <div class="assign-influencer-msg text-success"></div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-blue assign-influencer-btn-2">Submit</button>
          </div>
    </div>
  </div>
</div>    

@endsection

@section('pagescript')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
     <!-- Sweet-Alert  -->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap-select.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.js')}}"></script>
    <script src="{{ asset('js/highcharts.js')}}"></script>
    <script type="text/javascript">

        Highcharts.chart('activity-chart', {
          chart: {
            type: 'line',
            height : 300
          },
         /* title: {
            text: 'Monthly Average Temperature'
          },
          subtitle: {
            text: 'Source: WorldClimate.com'
          },*/
          xAxis: {
            categories: ['<?=implode("','", array_keys($promotions_data))?>'],
            gridLineColor:'#000000'
          },
          yAxis: {
            title: {
              text: 'Promotions'
            },
            gridLineColor:'#000000'
          },
          plotOptions: {
            line: {
              dataLabels: {
                enabled: true
              },
              enableMouseTracking: true
            }
          },
          series: [{
                    name: 'Promotions',
                    color: '#508FF4',
                    data: [<?=implode(",", $promotions_data)?>]
              },{
                    name: 'Average',
                    dataLabels:{
                        enabled : false
                    },
                    marker: {
                        enabled: false
                    },
                    data: [<?=implode(",", $avg_promotions_data)?>]
                }
            ]   
        });
    </script>

    <script type="text/javascript">
        function openTab(evt, tabName) {
            const div = evt.currentTarget;
            if(div.classList.contains('active')){
                evt.currentTarget.className = "tablinks";
                document.getElementById(tabName).style.display = "none";
            } else {
              var i, tabcontent, tablinks;
              tabcontent = document.getElementsByClassName("tabcontent");
              for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
              }
              tablinks = document.getElementsByClassName("tablinks");
              for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
              }
              document.getElementById(tabName).style.display = "block";
              evt.currentTarget.className += " active";
            }
        }
    </script>

    <script type="text/javascript">

    function select_influencer_list(influencer_id){
        $("#assign_influencer_id").val(influencer_id);
        $("#assign-influencer-modal").modal("show");
    } 

    function select_influencer_list_2(){
        $("#assign-influencer-modal-2").modal("show");
    }     

    $(function() {

        $("#website-frame").contents().find("img").attr("style","width:100%")

        $(document).on("click", "#select-all-inf", function() { 
            var $img = $(this).find('img') ,
                src = $img.attr('src') ,
                onCheck = /\-deselected\.png$/ ,
                offCheck = /\-selected\.png$/ ,
                normal = /\.png$/
            ;

            if(src.match(onCheck)) {
                $img.attr('src', src.replace(onCheck, '-selected.png'));
                $(".influencers_tr .checked-influencer-id").prop('checked', true);
            } else if (src.match(offCheck)) {
                $img.attr('src', src.replace(offCheck, '-deselected.png'));
                $(".influencers_tr .checked-influencer-id").prop('checked', false);
            } else {
                $img.attr('src', src.replace(normal, '-deselected.png'));
                 $(".influencers_tr .checked-influencer-id").prop('checked', false);
            }
        });
        $(document).on("click", "#select-all-unique-inf", function() {
            var $img = $(this).find('img') ,
                src = $img.attr('src') ,
                onCheck = /\-deselected\.png$/ ,
                offCheck = /\-selected\.png$/ ,
                normal = /\.png$/
            ;

            if(src.match(onCheck)) {
                $img.attr('src', src.replace(onCheck, '-selected.png'));
                $(".ranking-of-unique-inf-tab-body .checked-influencer-id").prop('checked', true);
            } else if (src.match(offCheck)) {
                $img.attr('src', src.replace(offCheck, '-deselected.png'));
                $(".ranking-of-unique-inf-tab-body .checked-influencer-id").prop('checked', false);
            } else {
                $img.attr('src', src.replace(normal, '-deselected.png'));
                 $(".ranking-of-unique-inf-tab-body .checked-influencer-id").prop('checked', false);
            }
        });

        $(document).on('click', '.influencer-row-link td:not(.except)', function(){ 
           // window.location.href = $(this).data("influencer-details-url");
            window.open($(this).data("influencer-details-url"), '_blank');
        });

        $('#add_to_shop_list').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : '<img src="{{ asset('images/addlist.png')}}" width="17" />'
        });
        $('#add_to_shop_list').on('change', function(e){
            if($("#add_to_shop_list").val() != null && $("#add_to_shop_list").val() != ''){ 
                var shop_ids = [];
                shop_ids.push("{{$shop->shop_id}}");
                 var shop_list_ids = [];
                shop_list_ids.push($("#add_to_shop_list").val());
                $.ajax({
                    url : "{{route('user.assign_shop')}}",
                    data : { shop_list_ids:shop_list_ids, shop_ids:shop_ids},
                }).done(function (data) {
                    //$('.assign-shop-msg').html('Shop assigned successfully.');
                });
            } else {
                alert("Please select at least one influencer list.");
            }
        });

        $(document).on('click', '.assign-influencer-btn', function(e) {
            if($("#assign_influencer_list_id").val() != null){ 
                var influencer_ids = [];
                influencer_ids.push($("#assign_influencer_id").val());
                var influencer_list_ids = $("#assign_influencer_list_id").val();
                $.ajax({
                    url : "{{route('user.assign_influencer')}}",
                    data : { influencer_list_ids:influencer_list_ids, influencer_ids:influencer_ids},
                }).done(function (data) {
                    $('.assign-influencer-msg').html('Influencer assigned successfully.');
                });
            } else {
                alert("Please select at least one influencer list.");
            }
        });

        $(document).on('click', '.assign-influencer-btn-2', function(e) { 

            if($("#assign_influencer_list_id_2").val() != ''){
                var influencer_ids = $('input.checked-influencer-id:checked').map( function () { 
                    return $(this).val();
                }).get();
               // var shop_ids = $("#assign_shop_ids").val();
                var influencer_list_ids = $("#assign_influencer_list_id_2").val();
                $.ajax({
                    url : "{{route('user.assign_influencer')}}",
                    data : { influencer_list_ids:influencer_list_ids, influencer_ids:influencer_ids},
                }).done(function (data) { 
                    $('.assign-influencer-msg').html('Influencer assigned successfully.');
                    $('input.checked-influencer-id').prop('checked', false);
                });
            } else {
                alert("Please select at least one influencer list.");
            }
        });

        $("#with_media_promotion_date_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');
                //$('#with_media_promotion_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#with_media_promotion_date_to').datepicker('option', 'minDate', minDate);

                with_media_promotion_date_change();
            }
        });
        $("#with_media_promotion_date_to").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                with_media_promotion_date_change();
            }
        });

        $("#rou_latest_promo_date_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');

                //$('#rou_latest_promo_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#rou_latest_promo_date_to').datepicker('option', 'minDate', minDate);

                rou_latest_promo_date_change();
            }
        });
        $("#rou_latest_promo_date_to").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () {
                rou_latest_promo_date_change();
            }
        });

        $("#promotion_date_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');
               // $('#promotion_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#promotion_date_to').datepicker('option', 'minDate', minDate);

                promotion_date_change();
            }
        });
        $("#promotion_date_to").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                promotion_date_change();
            }
        });

        
        $('.selectpicker').selectpicker({
            tickIcon: 'fa fa-check'
        });

        var url = $(this).attr('href');
        getInfluencer(url);
        getInfluencerWithMedia(url);
        getUniqueInfluencer(url);
        
        $(document).on('click', '.pagination a', function(e) { 
           
            e.preventDefault();
            var url = $(this).attr('href');
            getInfluencer(url);
            window.history.pushState("", "", url);
        });

        $(document).on('change', '#sort_by', function(e) {
           // e.preventDefault();
            var url = $(this).attr('href');
            getInfluencer(url);
        });

        /*$(document).on('click', '#country_filter', function(e) { 
            if($("#filter_country_ids").val() != null){
                var url = $(this).attr('href');
                getInfluencer(url);
            }
        });*/
        var url = $(this).attr('href');

        $('#filter_country_ids').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'SELECT COUNTRY'
        });
        $("button[data-id=filter_country_ids]").html("SELECT COUNTRY");
        $('#filter_country_ids').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).data('content')+' '+'<a href="Javascript:void(0);" class="remove-country" data-country-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .country-filter-contains").html(results);
            $("button[data-id=filter_country_ids]").html("SELECT COUNTRY");

            var url = $(this).attr('href');
            getInfluencer(url);
        });
        
        $(document).on('click', '.country-filter-contains .item .remove-country', function(e) { 
            var wanted_id = $(this).data("country-id"); 
            var wanted_option = $('#filter_country_ids option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#filter_country_ids').selectpicker('refresh');
            $(this).parent().remove(); 

            getInfluencer(url); 
        });

        $('.only-number-allowed').bind('keyup paste', function(){
                this.value = this.value.replace(/[^0-9]/g, '');
        });

       // $(document).on('click', '#engagement_rate_filter', function(e) {
        $(document).on( "keyup", "#engagement_rate_from, #engagement_rate_to", function( e ) { 
            if($("#engagement_rate_from").val() != '' && $("#engagement_rate_to").val() != ''){
                $(".activated-filter .eng-rate-filter-contains").html('<span class="item">ENG. RATE '+$("#engagement_rate_from").val()+' TO '+$("#engagement_rate_to").val()+' '+'<a href="Javascript:void(0);" class="remove-eng-rate"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else if($("#engagement_rate_from").val() != ''){
                $(".activated-filter .eng-rate-filter-contains").html('<span class="item">ENG. RATE > '+$("#engagement_rate_from").val()+' '+'<a href="Javascript:void(0);" class="remove-eng-rate"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else if($("#engagement_rate_to").val() != ''){
                $(".activated-filter .eng-rate-filter-contains").html('<span class="item">ENG. RATE < '+$("#engagement_rate_to").val()+' '+'<a href="Javascript:void(0);" class="remove-eng-rate"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url) 
            } else {
                $(".activated-filter .eng-rate-filter-contains").html('');
            }
        });
        $(document).on('click', '.eng-rate-filter-contains .item .remove-eng-rate', function(e) { 
            $(this).parent().remove();
            $("#engagement_rate_from").val('');
            $("#engagement_rate_to").val('');
            getInfluencer(url);  
        });

        //$(document).on('click', '#followers_filter', function(e) {
        $(document).on( "keyup", "#followers_from, #followers_to", function( e ) { 
            if($("#followers_from").val() != '' && $("#followers_to").val() != ''){
                $(".activated-filter .nb-of-followers-filter-contains").html('<span class="item">FOLLOWERS '+$("#followers_from").val()+' TO '+$("#followers_to").val()+' '+'<a href="Javascript:void(0);" class="remove-nb-of-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                getInfluencer(url) 
            } else if($("#followers_from").val() != ''){
                $(".activated-filter .nb-of-followers-filter-contains").html('<span class="item">FOLLOWERS > '+$("#followers_from").val()+' '+'<a href="Javascript:void(0);" class="remove-nb-of-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                getInfluencer(url) 
            } else if($("#followers_to").val() != ''){
                $(".activated-filter .nb-of-followers-filter-contains").html('<span class="item">FOLLOWERS < '+$("#followers_to").val()+' '+'<a href="Javascript:void(0);" class="remove-nb-of-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                getInfluencer(url) 
            } else {
                $(".activated-filter .nb-of-followers-filter-contains").html('');
            }
        });
        $(document).on('click', '.nb-of-followers-filter-contains .item .remove-nb-of-followers', function(e) { 
            $(this).parent().remove();
            $("#followers_from").val('');
            $("#followers_to").val('');
            getInfluencer(url);  
        });

        $(document).on('click', '#promo_this_week_filter', function(e) {
            if($("#promo_this_week_from").val() != '' && $("#promo_this_week_to").val() != ''){
                var url = $(this).attr('href');
                getInfluencer(url) 
            } 
        });

        //$(document).on('click', '#nb_of_story_filter', function(e) { 
        $(document).on( "keyup", "#nb_of_story_from, #nb_of_story_to", function( e ) { 
            if($("#nb_of_story_from").val() != null && $("#nb_of_story_to").val() != null && $("#nb_of_story_from").val() != '' && $("#nb_of_story_to").val() != ''){
                $(".activated-filter .nb-of-story-filter-contains").html('<span class="item">NB OF STORY '+$("#nb_of_story_from").val()+' TO '+$("#nb_of_story_to").val()+' '+'<a href="Javascript:void(0);" class="remove-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url);
            } else if($("#nb_of_story_from").val() != ''){
                $(".activated-filter .nb-of-story-filter-contains").html('<span class="item">NB OF STORY > '+$("#nb_of_story_from").val()+' '+'<a href="Javascript:void(0);" class="remove-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url);
            } else if($("#nb_of_story_to").val() != ''){
                $(".activated-filter .nb-of-story-filter-contains").html('<span class="item">NB OF STORY < '+$("#nb_of_story_to").val()+' '+'<a href="Javascript:void(0);" class="remove-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url);
            } else {
                $(".activated-filter .nb-of-story-filter-contains").html('');
            }
        });
        $(document).on('click', '.nb-of-story-filter-contains .item .remove-nb-of-story', function(e) { 
            $(this).parent().remove();
            $("#nb_of_story_from").val('');
            $("#nb_of_story_to").val('');
            getInfluencer(url);  
        });

        //$(document).on('click', '#promotion_date_filter', function(e) { 
        function promotion_date_change(){
            if($("#promotion_date_from").val() != null && $("#promotion_date_to").val() != null && $("#promotion_date_from").val() != '' && $("#promotion_date_to").val() != ''){
                 $(".activated-filter .promotions-date-filter-contains").html('<span class="item">PROMOTION DATE '+$("#promotion_date_from").val()+' TO '+$("#promotion_date_to").val()+' '+'<a href="Javascript:void(0);" class="remove-promotions-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url);
            } else if($("#promotion_date_from").val() != null && $("#promotion_date_from").val() != ''){
                 $(".activated-filter .promotions-date-filter-contains").html('<span class="item">PROMOTION DATE > '+$("#promotion_date_from").val()+' '+'<a href="Javascript:void(0);" class="remove-promotions-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url);
            } else if($("#promotion_date_to").val() != null && $("#promotion_date_to").val() != ''){
                 $(".activated-filter .promotions-date-filter-contains").html('<span class="item">PROMOTION DATE < '+$("#promotion_date_to").val()+' '+'<a href="Javascript:void(0);" class="remove-promotions-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencer(url);
            } else {
                $(".activated-filter .promotions-date-filter-contains").html('');
            }
        }
        $(document).on('click', '.promotions-date-filter-contains .item .remove-promotions-date', function(e) { 
            $(this).parent().remove();
            $("#promotion_date_from").val('');
            $("#promotion_date_to").val('');
            getInfluencer(url);  
        });

       /* $('#search_instagram_id').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                var url = $(this).attr('href');
                getInfluencer(url);
            }
        });*/
        $(document).on( "keyup", "#search_instagram_id", function( e ) {
            e.stopImmediatePropagation();
            var url = $(this).attr('href');
            getInfluencer(url);
        });
 
        $(document).on('click', '.verified-filter', function(e) {
            $(".activated-filter .verified-filter-contains").html('<span class="item"><img src="{{ asset('images/verified.png')}}" width="20"/> '+'<a href="Javascript:void(0);" class="remove-verified"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            $("#is_verified").val(1);
            var url = $(this).attr('href');
            //var action = 'is_verified';
            getInfluencer(url);
        });
        $(document).on('click', '.verified-filter-contains .item .remove-verified', function(e) { 
            $(this).parent().remove();
            $("#is_verified").val(0);
            getInfluencer(url);  
        });

        function getInfluencer(url, action='') 
        {
                //var sort_by = $("#sort_by").val();
                var sort_by = $("#sort_by").find(':selected').attr('data-column-name');
                var sort_type =  $("#sort_by").find(':selected').attr('data-sorting-type');
                var search_instagram_id = $("#search_instagram_id").val();
                var filter_country_ids = $("#filter_country_ids").val();
                var eng_rate_from = $("#engagement_rate_from").val();
                var eng_rate_to = $("#engagement_rate_to").val();
                var followers_from = $("#followers_from").val();
                var followers_to = $("#followers_to").val();
                var promo_this_week_from = $("#promo_this_week_from").val();
                var promo_this_week_to = $("#promo_this_week_to").val();
                var nb_of_story_from = $("#nb_of_story_from").val();
                var nb_of_story_to = $("#nb_of_story_to").val();
                var promotion_date_from = $("#promotion_date_from").val();
                var promotion_date_to = $("#promotion_date_to").val();
                var is_verified = $("#is_verified").val();

                $.ajax({
                    url : url,
                    data : {with_media: 0, search_instagram_id:search_instagram_id, sort_type:sort_type, sort_by:sort_by, filter_country_ids:filter_country_ids, eng_rate_from:eng_rate_from, eng_rate_to:eng_rate_to, followers_from:followers_from, followers_to:followers_to, promo_this_week_from: promo_this_week_from, promo_this_week_to:promo_this_week_to, is_verified:is_verified,nb_of_story_from:nb_of_story_from,nb_of_story_to:nb_of_story_to, promotion_date_from:promotion_date_from, promotion_date_to:promotion_date_to},
                    dataType: 'json',
                }).done(function (data) { 
                    $('.influencers_tr').html(data.body);
                });
        }

        $(document).on('click', '.promotions-with-story .pagination a', function(e) { 
            e.preventDefault();
            var url = $(this).attr('href');
            getInfluencerWithMedia(url);
            window.history.pushState("", "", url);
        });

        $(document).on('change', '#sort_by_with_media', function(e) {
           // e.preventDefault();
            var url = $(this).attr('href');
            getInfluencerWithMedia(url);
        });

        /*$(document).on('click', '#with_media_country_filter', function(e) { 
            if($("#with_media_filter_country_ids").val() != null){
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            }
        });*/
        var url = $(this).attr('href');

        $('#with_media_filter_country_ids').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'SELECT COUNTRY'
        });
        $("button[data-id=with_media_filter_country_ids]").html("SELECT COUNTRY");
        $('#with_media_filter_country_ids').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).data('content')+' '+'<a href="Javascript:void(0);" class="remove-with-media-country" data-country-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .with-media-country-filter-contains").html(results);
            $("button[data-id=with_media_filter_country_ids]").html("SELECT COUNTRY");

            var url = $(this).attr('href');
            getInfluencerWithMedia(url);
        });
        
        $(document).on('click', '.with-media-country-filter-contains .item .remove-with-media-country', function(e) { 
            var wanted_id = $(this).data("country-id"); 
            var wanted_option = $('#with_media_filter_country_ids option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#with_media_filter_country_ids').selectpicker('refresh');
            $(this).parent().remove(); 

            getInfluencerWithMedia(url); 
        });

        //$(document).on('click', '#with_media_nb_of_story_filter', function(e) { 
        $(document).on( "keyup", "#with_media_nb_of_story_from, #with_media_nb_of_story_to", function( e ) { 
            if($("#with_media_nb_of_story_from").val() != '' && $("#with_media_nb_of_story_to").val() != ''){
                $(".activated-filter .with-media-nb-of-story-filter-contains").html('<span class="item">NB OF STORY '+$("#with_media_nb_of_story_from").val()+' TO '+$("#with_media_nb_of_story_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            } else if($("#with_media_nb_of_story_from").val() != ''){
                $(".activated-filter .with-media-nb-of-story-filter-contains").html('<span class="item">NB OF STORY > '+$("#with_media_nb_of_story_from").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            } else if($("#with_media_nb_of_story_to").val() != ''){
                $(".activated-filter .with-media-nb-of-story-filter-contains").html('<span class="item">NB OF STORY < '+$("#with_media_nb_of_story_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            } else {
                $(".activated-filter .with-media-nb-of-story-filter-contains").html('');
            }
        });
        $(document).on('click', '.with-media-nb-of-story-filter-contains .item .remove-with-media-nb-of-story', function(e) { 
            $(this).parent().remove();
            $("#with_media_nb_of_story_from").val('');
            $("#with_media_nb_of_story_to").val('');
            getInfluencerWithMedia(url);  
        });

        //$(document).on('click', '#with_media_promotion_date_filter', function(e) { 
        function with_media_promotion_date_change(){
            if($("#with_media_promotion_date_from").val() != null && $("#with_media_promotion_date_to").val() != null && $("#with_media_promotion_date_from").val() != '' && $("#with_media_promotion_date_to").val() != ''){
                $(".activated-filter .with-media-promotions-date-filter-contains").html('<span class="item">PROMOTION DATE '+$("#with_media_promotion_date_from").val()+' TO '+$("#with_media_promotion_date_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-promotion-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            } else if($("#with_media_promotion_date_from").val() != null && $("#with_media_promotion_date_from").val() != ''){
                $(".activated-filter .with-media-promotions-date-filter-contains").html('<span class="item">PROMOTION DATE > '+$("#with_media_promotion_date_from").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-promotion-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            } else if($("#with_media_promotion_date_to").val() != null && $("#with_media_promotion_date_to").val() != ''){
                $(".activated-filter .with-media-promotions-date-filter-contains").html('<span class="item">PROMOTION DATE < '+$("#with_media_promotion_date_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-promotion-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            } else {
                $(".activated-filter .with-media-promotions-date-filter-contains").html('');
            }
        }
        $(document).on('click', '.with-media-promotions-date-filter-contains .item .remove-with-media-promotion-date', function(e) { 
            $(this).parent().remove();
            $("#with_media_promotion_date_from").val('');
            $("#with_media_promotion_date_to").val('');
            getInfluencerWithMedia(url);  
        });

        //$(document).on('click', '#with_media_engagement_filter', function(e) {
        $(document).on( "keyup", "#with_media_engagement_from, #with_media_engagement_to", function( e ) {  
            if($("#with_media_engagement_from").val() != '' && $("#with_media_engagement_to").val() != ''){
                 $(".activated-filter .with-media-eng-rate-filter-contains").html('<span class="item">ENG. RATE '+$("#with_media_engagement_from").val()+' TO '+$("#with_media_engagement_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-engagement"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            } else if($("#with_media_engagement_from").val() != ''){
                 $(".activated-filter .with-media-eng-rate-filter-contains").html('<span class="item">ENG. RATE > '+$("#with_media_engagement_from").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-engagement"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            } else if($("#with_media_engagement_to").val() != ''){
                 $(".activated-filter .with-media-eng-rate-filter-contains").html('<span class="item">ENG. RATE < '+$("#with_media_engagement_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-engagement"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            } else {
                $(".activated-filter .with-media-eng-rate-filter-contains").html('');
            }
        });
        $(document).on('click', '.with-media-eng-rate-filter-contains .item .remove-with-media-engagement', function(e) { 
            $(this).parent().remove();
            $("#with_media_engagement_from").val('');
            $("#with_media_engagement_to").val('');
            getInfluencerWithMedia(url);  
        });

        //$(document).on('click', '#with_media_follower_filter', function(e) { 
        $(document).on( "keyup", "#with_media_follower_from, #with_media_follower_to", function( e ) { 
            if($("#with_media_follower_from").val() != '' && $("#with_media_follower_to").val() != ''){
                $(".activated-filter .with-media-nb-of-followers-filter-contains").html('<span class="item">FOLLOWERS '+$("#with_media_follower_from").val()+' TO '+$("#with_media_follower_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-nb-of-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            } else if($("#with_media_follower_from").val() != ''){
                $(".activated-filter .with-media-nb-of-followers-filter-contains").html('<span class="item">FOLLOWERS > '+$("#with_media_follower_from").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-nb-of-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            } else if($("#with_media_follower_to").val() != ''){
                $(".activated-filter .with-media-nb-of-followers-filter-contains").html('<span class="item">FOLLOWERS < '+$("#with_media_follower_to").val()+' '+'<a href="Javascript:void(0);" class="remove-with-media-nb-of-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            } else {
                $(".activated-filter .with-media-nb-of-followers-filter-contains").html('');
            }
        });
        $(document).on('click', '.with-media-nb-of-followers-filter-contains .item .remove-with-media-nb-of-followers', function(e) { 
            $(this).parent().remove();
            $("#with_media_follower_from").val('');
            $("#with_media_follower_to").val('');
            getInfluencerWithMedia(url);  
        });

       /* $('#with_media_search_influencer').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                var url = $(this).attr('href');
                getInfluencerWithMedia(url);
            }
        });*/
        $(document).on( "keyup", "#with_media_search_influencer", function( e ) {
            e.stopImmediatePropagation();
            var url = $(this).attr('href');
            getInfluencerWithMedia(url);
        });

        $(document).on('click', '.with-media-verified-filter', function(e) { 
            var url = $(this).attr('href');
            var action = 'is_verified';
            getInfluencerWithMedia(url, action);
        });
        $(document).on('click', '.with-media-verified-filter', function(e) {
            $(".activated-filter .with-media-verified-filter-contains").html('<span class="item"><img src="{{ asset('images/verified.png')}}" width="20"/> '+'<a href="Javascript:void(0);" class="remove-with-media-verified"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            $("#with_media_is_verified").val(1);
            var url = $(this).attr('href');
            getInfluencerWithMedia(url);
        });
        $(document).on('click', '.with-media-verified-filter-contains .item .remove-with-media-verified', function(e) { 
            $(this).parent().remove();
            $("#with_media_is_verified").val(0);
            getInfluencerWithMedia(url);  
        });

        function getInfluencerWithMedia(url, action='') 
        { 
            var sort_by = $("#sort_by_with_media").find(':selected').attr('data-column-name');
            var sort_type =  $("#sort_by_with_media").find(':selected').attr('data-sorting-type');
            var search_influencer = $("#with_media_search_influencer").val();
            var filter_country_ids = $("#with_media_filter_country_ids").val();
            var nb_of_story_from = $("#with_media_nb_of_story_from").val();
            var nb_of_story_to = $("#with_media_nb_of_story_to").val();
            var follower_from = $("#with_media_follower_from").val();
            var follower_to = $("#with_media_follower_to").val();
            var promotion_date_from = $("#with_media_promotion_date_from").val();
            var promotion_date_to = $("#with_media_promotion_date_to").val();
            var engagement_from = $("#with_media_engagement_from").val();
            var engagement_to = $("#with_media_engagement_to").val();
            var is_verified = $("#with_media_is_verified").val();
           
            $.ajax({
                url : url,
                data : { with_media: 1, search_influencer:search_influencer, sort_type:sort_type, sort_by:sort_by, filter_country_ids:filter_country_ids, nb_of_story_from:nb_of_story_from, nb_of_story_to:nb_of_story_to, promotion_date_from:promotion_date_from, promotion_date_to:promotion_date_to, follower_from:follower_from, follower_to:follower_to, engagement_from:engagement_from, engagement_to:engagement_to, is_verified:is_verified},
                dataType: 'json',
            }).done(function (data) { 
                console.log(data.body);
                $('.promotions-with-story').html(data.body);
            });
        }

        $(document).on('click', '.ranking-of-unique-inf .pagination a', function(e) { 
           
            e.preventDefault();
            var url = $(this).attr('href');
            getUniqueInfluencer(url);
            window.history.pushState("", "", url);
        });

        $(document).on('change', '#rou_sort_by', function(e) {
           // e.preventDefault();
            var url = $(this).attr('href');
            getUniqueInfluencer(url);
        });

        /*$(document).on('click', '#rou_market_filter', function(e) { 
            if($("#rou_filter_market_ids").val() != null){
                var url = $(this).attr('href');
                getUniqueInfluencer(url);
            }
        });*/
        var url = $(this).attr('href');

        $('#rou_filter_market_ids').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'SELECT COUNTRY'
        });
        $("button[data-id=rou_filter_market_ids]").html("SELECT COUNTRY");
        $('#rou_filter_market_ids').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).data('content')+' '+'<a href="Javascript:void(0);" class="remove-unique-inf-country" data-country-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .unique-inf-country-filter-contains").html(results);
            $("button[data-id=rou_filter_market_ids]").html("SELECT COUNTRY");

            var url = $(this).attr('href');
            getUniqueInfluencer(url);
        });
        
        $(document).on('click', '.unique-inf-country-filter-contains .item .remove-unique-inf-country', function(e) { 
            var wanted_id = $(this).data("country-id"); 
            var wanted_option = $('#rou_filter_market_ids option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#rou_filter_market_ids').selectpicker('refresh');
            $(this).parent().remove(); 

            getUniqueInfluencer(url); 
        });

        //$(document).on('click', '#rou_nb_of_promo_of_shop_filter', function(e) { 
        $(document).on( "keyup", "#rou_nb_of_promo_of_shop_from, #rou_nb_of_promo_of_shop_to", function( e ) { 
            if($("#rou_nb_of_promo_of_shop_from").val() != '' && $("#rou_nb_of_promo_of_shop_to").val() != ''){
                $(".activated-filter .unique-nb-of-promo-of-the-shop-filter-contains").html('<span class="item">NB OF PROMO OF SHOP '+$("#rou_nb_of_promo_of_shop_from").val()+' TO '+$("#rou_nb_of_promo_of_shop_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-nb-of-promo-of-the-shop"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueInfluencer(url);
            } else if($("#rou_nb_of_promo_of_shop_from").val() != ''){
                $(".activated-filter .unique-nb-of-promo-of-the-shop-filter-contains").html('<span class="item">NB OF PROMO OF SHOP > '+$("#rou_nb_of_promo_of_shop_from").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-nb-of-promo-of-the-shop"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueInfluencer(url);
            } else if($("#rou_nb_of_promo_of_shop_to").val() != ''){
                $(".activated-filter .unique-nb-of-promo-of-the-shop-filter-contains").html('<span class="item">NB OF PROMO OF SHOP < '+$("#rou_nb_of_promo_of_shop_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-nb-of-promo-of-the-shop"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueInfluencer(url);
            } else {
                $(".activated-filter .unique-nb-of-promo-of-the-shop-filter-contains").html('');
            }
        });
        $(document).on('click', '.unique-nb-of-promo-of-the-shop-filter-contains .item .remove-unique-nb-of-promo-of-the-shop', function(e) { 
            $(this).parent().remove();
            $("#rou_nb_of_promo_of_shop_from").val('');
            $("#rou_nb_of_promo_of_shop_to").val('');
            getUniqueInfluencer(url);  
        });

        //$(document).on('click', '#rou_latest_promo_date_filter', function(e) {
        function rou_latest_promo_date_change(){
            if($("#rou_latest_promo_date_from").val() != '' && $("#rou_latest_promo_date_to").val() != ''){
                $(".activated-filter .unique-latest-promo-date-filter-contains").html('<span class="item">LATEST PROMO DATE '+$("#rou_latest_promo_date_from").val()+' TO '+$("#rou_latest_promo_date_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-latest-promo-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueInfluencer(url) 
            } else if($("#rou_latest_promo_date_from").val() != ''){
                $(".activated-filter .unique-latest-promo-date-filter-contains").html('<span class="item">LATEST PROMO DATE > '+$("#rou_latest_promo_date_from").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-latest-promo-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueInfluencer(url) 
            } else if($("#rou_latest_promo_date_to").val() != ''){
                $(".activated-filter .unique-latest-promo-date-filter-contains").html('<span class="item">LATEST PROMO DATE < '+$("#rou_latest_promo_date_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-latest-promo-date"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueInfluencer(url) 
            } else {
                $(".activated-filter .unique-latest-promo-date-filter-contains").html('');
            }
        }
        $(document).on('click', '.unique-latest-promo-date-filter-contains .item .remove-unique-latest-promo-date', function(e) { 
            $(this).parent().remove();
            $("#rou_latest_promo_date_from").val('');
            $("#rou_latest_promo_date_to").val('');
            getUniqueInfluencer(url);  
        });

       // $(document).on('click', '#rou_engagement_rate_filter', function(e) {
        $(document).on( "keyup", "#rou_engagement_rate_from, #rou_engagement_rate_to", function( e ) { 
            if($("#rou_engagement_rate_from").val() != '' && $("#rou_engagement_rate_to").val() != ''){
                $(".activated-filter .unique-eng-rate-filter-contains").html('<span class="item">ENG. RATE '+$("#rou_engagement_rate_from").val()+' TO '+$("#rou_engagement_rate_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-eng-rate"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueInfluencer(url) 
            } else if($("#rou_engagement_rate_from").val() != ''){
                $(".activated-filter .unique-eng-rate-filter-contains").html('<span class="item">ENG. RATE > '+$("#rou_engagement_rate_from").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-eng-rate"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueInfluencer(url) 
            } else if($("#rou_engagement_rate_to").val() != ''){
                $(".activated-filter .unique-eng-rate-filter-contains").html('<span class="item">ENG. RATE < '+$("#rou_engagement_rate_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-eng-rate"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueInfluencer(url) 
            } else {
                $(".activated-filter .unique-eng-rate-filter-contains").html('');
            }
        });
        $(document).on('click', '.unique-eng-rate-filter-contains .item .remove-unique-eng-rate', function(e) { 
            $(this).parent().remove();
            $("#rou_engagement_rate_from").val('');
            $("#rou_engagement_rate_to").val('');
            getUniqueInfluencer(url);  
        });

        //$(document).on('click', '#rou_followers_filter', function(e) {
        $(document).on( "keyup", "#rou_followers_from, #rou_followers_to", function( e ) { 
            if($("#rou_followers_from").val() != '' && $("#rou_followers_to").val() != ''){
                $(".activated-filter .unique-nb-of-followers-filter-contains").html('<span class="item">FOLOWERS '+$("#rou_followers_from").val()+' TO '+$("#rou_followers_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-nb-of-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueInfluencer(url) 
            } else if($("#rou_followers_from").val() != ''){
                $(".activated-filter .unique-nb-of-followers-filter-contains").html('<span class="item">FOLOWERS > '+$("#rou_followers_from").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-nb-of-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueInfluencer(url) 
            } else if($("#rou_followers_to").val() != ''){
                $(".activated-filter .unique-nb-of-followers-filter-contains").html('<span class="item">FOLOWERS < '+$("#rou_followers_to").val()+' '+'<a href="Javascript:void(0);" class="remove-unique-nb-of-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getUniqueInfluencer(url) 
            } 
        });
        $(document).on('click', '.unique-nb-of-followers-filter-contains .item .remove-unique-nb-of-followers', function(e) { 
            $(this).parent().remove();
            $("#rou_followers_from").val('');
            $("#rou_followers_to").val('');
            getUniqueInfluencer(url);  
        });

        /*$('#rou_search_instagram_id').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                var url = $(this).attr('href');
                getUniqueInfluencer(url);
            }
        });*/
        $(document).on( "keyup", "#rou_search_instagram_id", function( e ) {
            e.stopImmediatePropagation();
            var url = $(this).attr('href');
            getUniqueInfluencer(url);
        });

        function getUniqueInfluencer(url) 
        {
            //var sort_by = $("#sort_by").val();
            var sort_by = $("#rou_sort_by").find(':selected').attr('data-column-name');
            var sort_type =  $("#rou_sort_by").find(':selected').attr('data-sorting-type');
            var search_instagram_id = $("#rou_search_instagram_id").val();
            var filter_market_ids = $("#rou_filter_market_ids").val();
            var latest_promo_date_from = $("#rou_latest_promo_date_from").val();
            var latest_promo_date_to = $("#rou_latest_promo_date_to").val();
            var eng_rate_from = $("#rou_engagement_rate_from").val();
            var eng_rate_to = $("#rou_engagement_rate_to").val();
            var followers_from = $("#rou_followers_from").val();
            var followers_to = $("#rou_followers_to").val();
            var promo_this_week_from = $("#rou_promo_this_week_from").val();
            var promo_this_week_to = $("#rou_promo_this_week_to").val();
            var nb_of_promo_of_shop_from = $("#rou_nb_of_promo_of_shop_from").val();
            var nb_of_promo_of_shop_to = $("#rou_nb_of_promo_of_shop_to").val();



            $.ajax({
                url : url,
                data : {unique_influencer: 1, search_instagram_id:search_instagram_id, sort_type:sort_type, sort_by:sort_by, filter_market_ids:filter_market_ids, latest_promo_date_from:latest_promo_date_from, latest_promo_date_to:latest_promo_date_to, eng_rate_from:eng_rate_from, eng_rate_to:eng_rate_to, followers_from:followers_from, followers_to:followers_to, promo_this_week_from: promo_this_week_from, promo_this_week_to:promo_this_week_to, nb_of_promo_of_shop_from:nb_of_promo_of_shop_from,nb_of_promo_of_shop_to:nb_of_promo_of_shop_to},
                dataType: 'json',
            }).done(function (data) { 
                $('.ranking-of-unique-inf-tab-body').html(data.body);
            });
        }
    });
    </script>
    <script>
        function delete_shop(shop_id){

            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this data!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#8BC34A",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                /*iconColor: '#000',*/
            }, function(){   
                swal("Deleted!", "Your data has been deleted.", "success"); 
                $("#laravel_datatable-"+shop_id).submit();
                return true;
            });

            return false;
        }
    </script>
@stop
@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <!--alerts CSS -->
    <link href="{{ asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.css')}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@stop

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="promotedshops">
            
            <div class="topbar">
                @if(isset($name_of_the_list))
                    <h3>{{$name_of_the_list}} <span class="shop-list-favourite" data-shop-list-id="{{$shop_list_id}}" data-toggle="tooltip" data-placement="top" data-html="true" title="If you want to pin a favorite shop list then show this shop list in your dashboard."><i class="fa <?=($is_favourite == 1) ? 'fa-heart' : 'fa-heart-o';?> "></i></span></h3>
                @else
                    <h3>Promoted SHOPS</h3>
                @endif
                
                <div class="sortby">
                    <label>SORT BY:</label>
                    <select name="sort_by" id="sort_by" class="selectpicker">
                        <option data-sorting-type="asc" data-column-name="first_added_date" data-icon="fa fa-arrow-up sort-icon">By first added</option>
                        <option data-sorting-type="desc" data-column-name="first_added_date" data-icon="fa fa-arrow-down sort-icon">By first added</option>
                        <option data-sorting-type="asc" data-column-name="country_name" data-icon="fa fa-arrow-up sort-icon">By market</option>
                        <option data-sorting-type="desc" data-column-name="country_name" data-icon="fa fa-arrow-down sort-icon">By market</option>
                        <option data-sorting-type="asc" data-column-name="last_seen_date" data-icon="fa fa-arrow-up sort-icon">By last seen</option>
                        <option data-sorting-type="desc" data-column-name="last_seen_date" data-icon="fa fa-arrow-down sort-icon">By last seen</option>
                       <!--  <option data-sorting-type="asc" data-column-name="activity" data-icon="fa fa-arrow-up sort-icon">By activity</option>
                       <option data-sorting-type="desc" data-column-name="activity" data-icon="fa fa-arrow-down sort-icon">By activity</option> -->
                        <option data-sorting-type="asc" data-column-name="nb_of_promotions" data-icon="fa fa-arrow-up sort-icon">By nb of promotions</option>
                        <option data-sorting-type="desc" data-column-name="nb_of_promotions" data-icon="fa fa-arrow-down sort-icon">By nb of promotions</option>
                         <option data-sorting-type="asc" data-column-name="total_influencers" data-icon="fa fa-arrow-up sort-icon">By influencers</option>
                        <option data-sorting-type="desc" data-column-name="total_influencers" data-icon="fa fa-arrow-down sort-icon">By influencers</option>
                        <option data-sorting-type="asc" data-column-name="promo_this_week" data-icon="fa fa-arrow-up sort-icon">By nb of promo this week</option>
                        <option data-sorting-type="desc" data-column-name="promo_this_week" data-icon="fa fa-arrow-down sort-icon">By nb of promo this week</option>
                        <option data-sorting-type="asc" data-column-name="categories" data-icon="fa fa-arrow-up sort-icon">By categories/niches</option>
                        <option data-sorting-type="desc" data-column-name="categories" data-icon="fa fa-arrow-down sort-icon">By categories/niches</option>
                    </select>
                </div>
            </div>
            <div class="searchwebsite">
                <label>Search a website:</label>
                <input type="text" name="search_website" id="search_website" placeholder="ENTER A WEBSITE" autocomplete="false" onfocus="this.placeholder = ''" onblur="this.placeholder = 'ENTER A WEBSITE'">
            </div>

            <div class="showonly">
                <label>Show only: </label>
                <div class="showonlylist">
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'MARKET')">WORLD</a>

                    <!-- <select class="selectpicker" multiple id="filter_country_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.png')}}' /> {{ $country->country_name}}"></option>
                        @endforeach
                    </select> -->
                    
                   <!--  <a href="#" class="tablinks first-added-last-week">FIRST ADDED</a>
                   <input type="hidden" id="first_added_last_week" value="0">
                   
                   <a href="#" class="tablinks last-seen-today">LAST SEEN</a>
                   <input type="hidden" id="last_seen_today" value="0"> -->

                    <!-- <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'ALL_ACTIVITY')">ALL ACTIVITY</a>
                     -->
                   <!--  <select class="selectpicker" multiple id="filter_activity">
                       <option value="high">High</option>
                       <option value="medium">Medium</option>
                       <option value="low">Low</option>
                   </select> -->
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'FIRST_ADDED_LAST_WEEK')">FIRST ADDED</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'LAST_SEEN_TODAY')">LAST SEEN</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'NB_OF_PROMOTION')">NB OF PROMOTION</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'PROMO_THIS_WEEK')">PROMO THIS WEEK</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'TOTAL_INFLUENCER')">TOTAL INFLUENCERS</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'NICHES')">NICHES</a>
                    <!-- <a href="javascript:void(0);"><i class="fa fa-heart-o"></i></a> -->
                </div>
            </div>
        
             <div class="filter-tab-content">
                <div id="MARKET" class="tabcontent">
                    <div class="col-md-2">
                        <select class="selectpicker" multiple id="filter_country_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.svg')}}' /> {{ $country->country_name}}"></option>
                        @endforeach
                        </select> 
                    </div>
                   <!--  <div class="col-md-3">
                       <button id="language_filter" class="filter-btn">FILTER</button>
                   </div> -->
                </div>

                <div id="FIRST_ADDED_LAST_WEEK" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="first_added_from" id="first_added_from" placeholder="FROM"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="first_added_to" id="first_added_to" placeholder="TO">
                    </div>
                </div>
                <div id="LAST_SEEN_TODAY" class="tabcontent">
                     <div class="col-md-3">
                        <input type="text" name="last_seen_from" id="last_seen_from" placeholder="FROM"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="last_seen_to" id="last_seen_to" placeholder="TO">
                    </div>
                </div>
                <!-- <div id="ALL_ACTIVITY" class="tabcontent">
                     <div class="col-md-4">
                        <select class="selectpicker" multiple id="filter_activity">
                            <option value="high">High</option>
                            <option value="medium">Medium</option>
                            <option value="low">Low</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button id="activity_filter" class="filter-btn">FILTER</button>
                    </div>
                </div> -->
                <div id="NB_OF_PROMOTION" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="nb_of_promotions_from" id="nb_of_promotions_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="nb_of_promotions_to" id="nb_of_promotions_to" placeholder="TO" class="only-number-allowed">
                    </div>
                   <!--  <div class="col-md-3">
                       <button id="nb_of_promotions_filter" class="filter-btn">FILTER</button>
                   </div> -->
                </div>
                <div id="PROMO_THIS_WEEK" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="promo_this_week_from" id="promo_this_week_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="promo_this_week_to" id="promo_this_week_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="promo_this_week_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="TOTAL_INFLUENCER" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="total_influencers_from" id="total_influencers_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="total_influencers_to" id="total_influencers_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="total_influencers_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="NICHES" class="tabcontent">
                    <div class="col-md-4">
                        <select class="selectpicker" multiple id="filter_category_ids">
                        @foreach($categories as $category)
                          <option value="{{ $category->category_id}}"> {{ $category->category_name}}</option>
                        @endforeach
                        </select> 
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="category_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
            </div>
            <div class="activated-filter showonly">
                <label>Activated filters: </label>
                <span class="country-filter-contains"></span>
                <span class="activity-filter-contains"></span>
                <span class="first-added-filter-contains"></span>
                <span class="last-seen-filter-contains"></span>
                <span class="nb-of-promotions-filter-contains"></span>
                <span class="promo-this-week-filter-contains"></span>
                <span class="total-influencers-filter-contains"></span>
                <span class="category-filter-contains"></span>
            </div>
    </div>
    <div class="promotedshops-listing">
            <div class="shopslistings">

                <div class="error-contains">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success text-success">
                        <i class="fa fa-check"></i> {{ $message }}
                    </div>
                @endif
                </div>

                <div class="shopslistingsinner">
                    <table id="" class="table display nowrap">
                    <thead>
                        <tr>
                            <th width="2%">
                                <a href="Javascript:void(0);" onclick="select_shop_list()"><img class="assign-list-btn" src="{{ asset('images/icons8-add_new.svg')}}" /></a>
                            </th>
                            <th>URL</th>
                           <!--  <th class="text-center">Activity</th> -->
                          <!--   <th class="text-left">Latest promotion</th> -->
                            <th class="text-left">ADDED</th>
                            <th class="text-left">LAST SEEN</th>
                            <th class="text-left">PROMO THIS WEEK</th>
                            <th class="text-left">TOTAL PROMO</th>
                            <th class="text-left">MARKET</th>
                            <th class="text-left">TOTAL INFLU</th>
                            <th class="text-center no-sort">CATEGORIES/NICHES</th>
                        </tr>
                    </thead>
                    
                        @if (count($shops) > 0)
                        <tbody class="shops_tr">
                             @include('user.shop.load_shop')
                         </tbody>   
                        @endif
                    
                    </table>
                </div>
              </div>
            
        </div>
    </div>
</div>
<div id="assign-shop-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">     
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Assign shop into list</h4>
          </div>
          <div class="modal-body">
            
            <input type="hidden" id="assign_shop_id">
            <div class="form-group row">
                <label class="control-label col-sm-2">List Name:</label>
                <div class="col-sm-5">
                    <select class="form-control selectpicker" multiple="" id="assign_shop_list_id"> 
                        <option value="" disabled="">Select Shop List</option>
                        @foreach($shop_list as $list)
                         <option value="{{$list->shop_list_id}}">{{$list->list_name}}</option>
                        @endforeach
                    </select>
                    <input type="hidden" id="assign_shop_ids" name="assign_shop_ids[]">
                    <div class="assign-shop-msg text-success"></div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-blue assign-shop-btn">Submit</button>
          </div>
    </div>
  </div>
</div>
                    
@endsection

@section('pagescript')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
     <!-- Sweet-Alert  -->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap-select.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.js')}}"></script>

    <script type="text/javascript">
        function openTab(evt, tabName) {
          const div = evt.currentTarget;
          if(div.classList.contains('active')){
             evt.currentTarget.className = "tablinks";
             document.getElementById(tabName).style.display = "none";
          } else {
            var i, tabcontent, tablinks;
              tabcontent = document.getElementsByClassName("tabcontent");
              for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
              }
              tablinks = document.getElementsByClassName("tablinks");
              for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
              }
              document.getElementById(tabName).style.display = "block";
              evt.currentTarget.className += " active";
          }
        }
    </script>
    <script type="text/javascript">

    function select_shop_list(){
       if($('input.checked-shop-id:checked').length > 0){
            $("#assign-shop-modal").modal("show");
        } else {
            alert("Please select at least one shop.");
        }
       // $("#assign_shop_ids").val(result);
    }  

    $(function() {

      $(document).on("click", ".shop-list-favourite", function(){
            $(".shop-list-favourite i").toggleClass('fa-heart fa-heart-o');
            var shop_list_id = $(this).data("shop-list-id");
            $.ajax({
                url : "{{route('user.favourite_shop_list')}}",
                data : { shop_list_id: shop_list_id},
                dataType: 'json',
            }).done(function (data) { 
                //$('.shops_tr').html(data.body);
            });
        });


        $(document).on("click", "#select-all-shop", function() { 
            var $img = $(this).find('img') ,
                src = $img.attr('src') ,
                onCheck = /\-deselected\.png$/ ,
                offCheck = /\-selected\.png$/ ,
                normal = /\.png$/
            ;

            if(src.match(onCheck)) {
                $img.attr('src', src.replace(onCheck, '-selected.png'));
                $(".checked-shop-id").prop('checked', true);
            } else if (src.match(offCheck)) {
                $img.attr('src', src.replace(offCheck, '-deselected.png'));
                $(".checked-shop-id").prop('checked', false);
            } else {
                $img.attr('src', src.replace(normal, '-deselected.png'));
                 $(".checked-shop-id").prop('checked', false);
            }
        });

        $("#first_added_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');
                //$('#with_media_promotion_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#first_added_to').datepicker('option', 'minDate', minDate);

                first_added_change();
            }
        });
        $("#first_added_to").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                first_added_change();
            }
        });

        $("#last_seen_from").datepicker({
            dateFormat: "mm/dd/yy",
            changeMonth: true,
            changeYear: true,
            onSelect: function () { 
                var minDate = $(this).datepicker('getDate');
                //$('#with_media_promotion_date_to').datepicker('setDate', minDate);
               // $('#promotion_date_to').datepicker('option', 'maxDate', 0);
                $('#last_seen_to').datepicker('option', 'minDate', minDate);

                last_seen_change();
            }
        });
        $("#last_seen_to").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            onSelect: function () { 
                last_seen_change();
            }
        });

        $(document).on('click', '.assign-shop-btn', function(e) { 
            if($("#assign_shop_list_id").val() != null){
                var shop_ids = $('input.checked-shop-id:checked').map( function () { 
                    return $(this).val();
                }).get();
               // var shop_ids = $("#assign_shop_ids").val();
                var shop_list_ids = $("#assign_shop_list_id").val();
                $.ajax({
                    url : "{{route('user.assign_shop')}}",
                    data : { shop_list_ids:shop_list_ids, shop_ids:shop_ids},
                }).done(function (data) { 
                    $('.assign-shop-msg').html('Shop assigned successfully.');
                    $('input.checked-shop-id').prop('checked', false);
                });
            } else {
                alert("Please select at least one shop list.");
            }
        });

        var url = $(this).attr('href');

        $(document).on('click', '.shop-row-link td:not(.except)', function(){ 
            //window.location.href = $(this).data("shop-details-url");
            window.open($(this).data("shop-details-url"), '_blank');
        });
 
        $('#filter_country_ids').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'SELECT COUNTRY'
        });
        $("button[data-id=filter_country_ids]").html("SELECT COUNTRY");
        $('#filter_country_ids').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).data('content')+' '+'<a href="Javascript:void(0);" class="remove-country" data-country-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .country-filter-contains").html(results);
            $("button[data-id=filter_country_ids]").html("SELECT COUNTRY");

            var url = $(this).attr('href');
            getShop(url);
        });
        
        $(document).on('click', '.country-filter-contains .item .remove-country', function(e) { 
            var wanted_id = $(this).data("country-id"); 
            var wanted_option = $('#filter_country_ids option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#filter_country_ids').selectpicker('refresh');
            $(this).parent().remove(); 

            getShop(url); 
        });

        $('#filter_category_ids').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'SELECT CATEGORY'
        });
        $("button[data-id=filter_category_ids]").html("SELECT CATEGORY");
        $('#filter_category_ids').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).text()+' '+'<a href="Javascript:void(0);" class="remove-category" data-category-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .category-filter-contains").html(results);
            $("button[data-id=filter_category_ids]").html("SELECT CATEGORY");

            var url = $(this).attr('href');
            getShop(url);
        });
        
        $(document).on('click', '.category-filter-contains .item .remove-category', function(e) { 
            var wanted_id = $(this).data("category-id"); 
            var wanted_option = $('#filter_category_ids option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#filter_category_ids').selectpicker('refresh');
            $(this).parent().remove(); 

            getShop(url); 
        });

        /*$('#filter_activity').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'ALL ACTIVITY'
        });
        $("button[data-id=filter_activity]").html("ALL ACTIVITY");
        $('#filter_activity').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).text()+' '+'<a href="Javascript:void(0);" class="remove-activity" data-activity-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .activity-filter-contains").html(results);
            $("button[data-id=filter_activity]").html("ALL ACTIVITY");
            getShop(url); 
        });
        
        $(document).on('click', '.activity-filter-contains .item .remove-activity', function(e) { 
            var wanted_id = $(this).data("activity-id"); 
            var wanted_option = $('#filter_activity option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#filter_activity').selectpicker('refresh');
            $(this).parent().remove(); 
            getShop(url); 
        });*/

        $(document).on('click', '.first-added-filter-contains .item .remove-first-added', function(e) { 
            $(this).parent().remove(); 
            $("#first_added_from").val('');
            $("#first_added_to").val('');
            getShop(url); 
        });
        $(document).on('click', '.last-seen-filter-contains .item .remove-last-seen', function(e) { 
            $(this).parent().remove();
            $("#last_seen_from").val('');
            $("#last_seen_to").val('');
            getShop(url);  
        });
        $(document).on('click', '.nb-of-promotions-filter-contains .item .remove-nb-of-promotions', function(e) { 
            $(this).parent().remove();
            $("#nb_of_promotions_from").val('');
            $("#nb_of_promotions_to").val('');
            getShop(url);  
        });
        $(document).on('click', '.promo-this-week-filter-contains .item .remove-promo-this-week', function(e) { 
            $(this).parent().remove();
            $("#promo_this_week_from").val('');
            $("#promo_this_week_to").val('');
            getShop(url);  
        });
        $(document).on('click', '.total-influencers-filter-contains .item .remove-total-influencers', function(e) { 
            $(this).parent().remove();
            $("#total_influencers_from").val('');
            $("#total_influencers_to").val('');
            getShop(url);  
        });
        

        $('.selectpicker').selectpicker({
            tickIcon: 'fa fa-check'
        });

        $(document).on('click', '.pagination a', function(e) { 
            e.preventDefault();
            var url = $(this).attr('href');
            getShop(url);
            //window.history.pushState("", "", url);
        });

        $(document).on('change', '#sort_by', function(e) {
           // e.preventDefault(); 
            var url = $(this).attr('href');
            getShop(url) 
        });

        /*$(document).on('click', '#language_filter', function(e) { 
            if($("#filter_country_ids").val() != null){
                var url = $(this).attr('href');
                getShop(url);
            }
        });*/

        /*$(document).on('click', '#activity_filter', function(e) { 
            if($("#filter_activity").val() != null){ 
                var url = $(this).attr('href');
                getShop(url);
            }
        });*/

        function first_added_change(){
            if($("#first_added_from").val() != '' && $("#first_added_to").val() != ''){
                $(".activated-filter .first-added-filter-contains").html('<span class="item">FIRST ADDED '+$("#first_added_from").val()+' TO '+$("#first_added_to").val()+' '+'<a href="Javascript:void(0);" class="remove-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url); 
            } else if($("#first_added_from").val() != ''){
                  $(".activated-filter .first-added-filter-contains").html('<span class="item">FIRST ADDED > '+$("#first_added_from").val()+' '+'<a href="Javascript:void(0);" class="remove-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else if($("#first_added_to").val() != ''){
                  $(".activated-filter .first-added-filter-contains").html('<span class="item">FIRST ADDED < '+$("#first_added_to").val()+' '+'<a href="Javascript:void(0);" class="remove-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else {
                $(".activated-filter .first-added-filter-contains").html('');
            }
        }

        /*$(document).on('click', '.first-added-last-week', function(e) { 
            $(".activated-filter .first-added-filter-contains").html('<span class="item">ADDED '+"{{Carbon\Carbon::parse('next sunday')->format('m/d/y')}}"+' '+'<a href="Javascript:void(0);" class="remove-first-added"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            $("#first_added_last_week").val(1);
            var url = $(this).attr('href');
            getShop(url);
        });*/

        function last_seen_change(){
            if($("#last_seen_from").val() != '' && $("#last_seen_to").val() != ''){
                $(".activated-filter .last-seen-filter-contains").html('<span class="item">LAST SEEN '+$("#last_seen_from").val()+' TO '+$("#last_seen_to").val()+' '+'<a href="Javascript:void(0);" class="remove-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url); 
            } else if($("#last_seen_from").val() != ''){
                  $(".activated-filter .last-seen-filter-contains").html('<span class="item">LAST SEEN > '+$("#last_seen_from").val()+' '+'<a href="Javascript:void(0);" class="remove-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else if($("#last_seen_to").val() != ''){
                  $(".activated-filter .last-seen-filter-contains").html('<span class="item">LAST SEEN < '+$("#last_seen_to").val()+' '+'<a href="Javascript:void(0);" class="remove-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else {
                $(".activated-filter .last-seen-filter-contains").html('');
            }
        }

        /*$(document).on('click', '.last-seen-today', function(e) { 
            $(".activated-filter .last-seen-filter-contains").html('<span class="item">LAST SEEN '+"{{Carbon\Carbon::parse('today')->format('m/d/y')}}"+' '+'<a href="Javascript:void(0);" class="remove-last-seen"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
           $("#last_seen_today").val(1);
            var url = $(this).attr('href');
            getShop(url);
        });*/

        //$(document).on('click', '#nb_of_promotions_filter', function(e) {
        $(document).on( "keyup", "#nb_of_promotions_from, #nb_of_promotions_to", function( e ) { 
            if($("#nb_of_promotions_from").val() != '' && $("#nb_of_promotions_to").val() != ''){
                $(".activated-filter .nb-of-promotions-filter-contains").html('<span class="item">NB OF PROMO '+$("#nb_of_promotions_from").val()+' TO '+$("#nb_of_promotions_to").val()+' '+'<a href="Javascript:void(0);" class="remove-nb-of-promotions"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url); 
            } else if($("#nb_of_promotions_from").val() != ''){
                  $(".activated-filter .nb-of-promotions-filter-contains").html('<span class="item">NB OF PROMO > '+$("#nb_of_promotions_from").val()+' '+'<a href="Javascript:void(0);" class="remove-nb-of-promotions"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else if($("#nb_of_promotions_to").val() != ''){
                  $(".activated-filter .nb-of-promotions-filter-contains").html('<span class="item">NB OF PROMO < '+$("#nb_of_promotions_to").val()+' '+'<a href="Javascript:void(0);" class="remove-nb-of-promotions"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url)    
            } else {
                $(".activated-filter .nb-of-promotions-filter-contains").html('');
            }
        });

        //$(document).on('click', '#promo_this_week_filter', function(e) {
        $(document).on( "keyup", "#promo_this_week_from, #promo_this_week_to", function( e ) { 
            if($("#promo_this_week_from").val() != '' && $("#promo_this_week_to").val() != ''){
                $(".activated-filter .promo-this-week-filter-contains").html('<span class="item">PROMO THIS WEEK '+$("#promo_this_week_from").val()+' TO '+$("#promo_this_week_to").val()+' '+'<a href="Javascript:void(0);" class="remove-promo-this-week"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url) 
            } else if($("#promo_this_week_from").val() != ''){
                $(".activated-filter .promo-this-week-filter-contains").html('<span class="item">PROMO THIS WEEK > '+$("#promo_this_week_from").val()+' '+'<a href="Javascript:void(0);" class="remove-promo-this-week"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url) 
            } else if($("#promo_this_week_to").val() != ''){
                $(".activated-filter .promo-this-week-filter-contains").html('<span class="item">PROMO THIS WEEK < '+$("#promo_this_week_to").val()+' '+'<a href="Javascript:void(0);" class="remove-promo-this-week"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url) 
            } else {
                $(".activated-filter .promo-this-week-filter-contains").html('');
            }
        });

        //$(document).on('click', '#total_influencers_filter', function(e) {
        $(document).on( "keyup", "#total_influencers_from, #total_influencers_to", function( e ) { 
            if($("#total_influencers_from").val() != '' && $("#total_influencers_to").val() != ''){
                $(".activated-filter .total-influencers-filter-contains").html('<span class="item">TOTAL INFLU '+$("#total_influencers_from").val()+' TO '+$("#total_influencers_to").val()+' '+'<a href="Javascript:void(0);" class="remove-total-influencers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url) 
            } else if($("#total_influencers_from").val() != ''){
                $(".activated-filter .total-influencers-filter-contains").html('<span class="item">TOTAL INFLU > '+$("#total_influencers_from").val()+' '+'<a href="Javascript:void(0);" class="remove-total-influencers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url) 
            } else if($("#total_influencers_to").val() != ''){
                $(".activated-filter .total-influencers-filter-contains").html('<span class="item">TOTAL INFLU < '+$("#total_influencers_to").val()+' '+'<a href="Javascript:void(0);" class="remove-total-influencers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getShop(url) 
            } else {
                $(".activated-filter .total-influencers-filter-contains").html('');
            }
        });

       /* $('#search_website').keypress(function(event){
            //var keycode = (event.keyCode ? event.keyCode : event.which);
            //if(keycode == '13'){
                var url = $(this).attr('href');
                getShop(url);
            //}
        });*/
        $(document).on( "keyup", "#search_website", function( e ) {
            e.stopImmediatePropagation();
            var url = $(this).attr('href');
            getShop(url);
        });

        function getShop(url) 
        {
                //var sort_by = $("#sort_by").val();

                var sort_by = $("#sort_by").find(':selected').attr('data-column-name');
                var sort_type =  $("#sort_by").find(':selected').attr('data-sorting-type');
                var search_website = $("#search_website").val();
                var filter_country_ids = $("#filter_country_ids").val();
                var filter_activity = $("#filter_activity").val();
                var nb_of_promotions_from = $("#nb_of_promotions_from").val();
                var nb_of_promotions_to = $("#nb_of_promotions_to").val();
                var promo_this_week_from = $("#promo_this_week_from").val();
                var promo_this_week_to = $("#promo_this_week_to").val();
                var total_influencers_from = $("#total_influencers_from").val();
                var total_influencers_to = $("#total_influencers_to").val();
                var first_added_from = $("#first_added_from").val();
                var first_added_to = $("#first_added_to").val();
                var last_seen_from = $("#last_seen_from").val();
                var last_seen_to = $("#last_seen_to").val();
                var filter_category_ids = $("#filter_category_ids").val();
                var flag = '{{Request::get("flag")}}';
                $.ajax({
                    url : url,
                    data : { search_website:search_website, sort_type:sort_type, sort_by:sort_by, filter_country_ids:filter_country_ids, first_added_from:first_added_from, first_added_to:first_added_to, last_seen_from:last_seen_from,last_seen_to:last_seen_to, filter_activity:filter_activity, nb_of_promotions_from:nb_of_promotions_from, nb_of_promotions_to:nb_of_promotions_to, promo_this_week_from:promo_this_week_from, promo_this_week_to:promo_this_week_to,total_influencers_from:total_influencers_from,total_influencers_to:total_influencers_to, filter_category_ids:filter_category_ids, flag:flag},
                    dataType: 'json',
                }).done(function (data) { 
                    $('.shops_tr').html(data.body);
                });
        }
    });
    </script>
    <script>
        $(document).ready(function() {
            $('#shoptable').DataTable( {
                searching: false,
                ordering:  true,
                "language": {
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'><i/>",
                        "next": "<i class='fa fa-angle-right'><i/>",

                    }
                },
            } );
        } );

        $(document).on("change", "#sort_by", function(){
            $("#shop_filter_form").submit();
        });

        function delete_shop(shop_id){

            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this data!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#8BC34A",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                /*iconColor: '#000',*/
            }, function(){   
                swal("Deleted!", "Your data has been deleted.", "success"); 
                $("#laravel_datatable-"+shop_id).submit();
                return true;
            });

            return false;
        }
    </script>
@stop
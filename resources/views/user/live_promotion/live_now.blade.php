@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <!--alerts CSS -->
    <link href="{{ asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.css')}}">
     <link rel="stylesheet" href="{{ asset('css/jquery-ui.css')}}">
@stop

<div class="col-md-9 col-sm-9 col-xs-12 no-padding influencersection">
    <div class="middlepart">
        <div class="promotedshops">
            <div class="topbar">
                <h3>Live promotions of the last 24 hours only:</h3>
                 <div class="sortby-text">
                    SORTED BY LATEST ONLY
                </div>
            </div>

             <div class="searchwebsite">
                <label>Search an Influencer :</label>
                <input type="text" name="search_instagram_id" id="search_instagram_id" placeholder="ENTER AN INSTAGRAM ID" onfocus="this.placeholder = ''" onblur="this.placeholder = 'ENTER AN INSTAGRAM ID'" onfocus="this.placeholder = ''" onblur="this.placeholder = 'ENTER AN INSTAGRAM ID'">
                
            </div>
            <div class="error-contains">
            @if ($message = Session::get('success'))
                <div class="alert alert-success text-success">
                    <i class="fa fa-check"></i> {{ $message }}
                </div>
            @endif
            </div>
            <div class="showonly">
            <label>Show only: </label>
            <div class="showonlylist">
                   <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'COUNTRY')">WORLD</a> 
                    <!-- <select class="selectpicker" multiple id="filter_country_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.png')}}' /> {{ $country->country_name}}"></option>
                        @endforeach
                    </select> -->

                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'ENGAGEMENT_RATE')">ENGAGEMENT RATE</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'FOLLOWERS')">FOLLOWERS</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'NB_OF_STORY')">NB OF STORY</a>
                    <a href="javascript:void(0);" class="tablinks" onclick="openTab(event, 'NICHES')">NICHES</a>
                </div>
            </div>
            <div class="filter-tab-content">
                 <div id="COUNTRY" class="tabcontent">
                    <div class="col-md-2">
                        <select class="selectpicker" multiple id="filter_country_ids">
                        @foreach($countries as $country)
                          <option value="{{ $country->country_id}}" data-content="<img src='{{asset('images/flags/'.Str::lower($country->country_code).'.svg')}}' /> {{ $country->country_name}}"></option>
                        @endforeach
                        </select>
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="country_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div> 
                <div id="ENGAGEMENT_RATE" class="tabcontent">
                     <div class="col-md-3">
                        <input type="text" name="engagement_rate_from" id="engagement_rate_from" placeholder="FROM" class="only-number-allowed" maxlength='2'> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="engagement_rate_to" id="engagement_rate_to" placeholder="TO" class="only-number-allowed" maxlength='3'>
                    </div>
                     <!-- <div class="col-md-3">
                        <button id="engagement_rate_filter" class="filter-btn">FILTER</button>
                                         </div> -->
                </div>
                <div id="FOLLOWERS" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="followers_from" id="followers_from" placeholder="FROM" class="only-number-allowed"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="followers_to" id="followers_to" placeholder="TO" class="only-number-allowed">
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="followers_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
                <div id="NB_OF_STORY" class="tabcontent">
                    <div class="col-md-3">
                        <input type="text" name="nb_of_story_from" id="nb_of_story_from" placeholder="FROM"> 
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="nb_of_story_to" id="nb_of_story_to" placeholder="TO">
                    </div>
                   <!--  <div class="col-md-3">
                       <button id="nb_of_story_filter" class="filter-btn">FILTER</button>
                   </div> -->
                </div>
                <div id="NICHES" class="tabcontent">
                    <div class="col-md-4">
                        <select class="selectpicker" multiple id="filter_category_ids">
                        @foreach($categories as $category)
                          <option value="{{ $category->category_id}}"> {{ $category->category_name}}</option>
                        @endforeach
                        </select> 
                    </div>
                    <!-- <div class="col-md-3">
                        <button id="category_filter" class="filter-btn">FILTER</button>
                    </div> -->
                </div>
            </div>
            <div class="activated-filter">
                <label>Activated filters: </label>
                <span class="country-filter-contains"></span>
                <span class="engagement-rate-filter-contains"></span>
                <span class="followers-filter-contains"></span>
                <span class="nb-of-story-filter-contains"></span>
                <span class="category-filter-contains"></span>
            </div>
        </div>
        <div class="promotedshops-listing">

                <div class="shopslistings">
                    <div class="promotions-of-medias">
                        <div class="promotionsofmediaslistings">
                            <div class="col-md-12 col-sm-12 col-xs-12 live-now col-lprp-0">
                            </div>
                      </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<div id="assign-media-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">     
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Assign media into list</h4>
          </div>
          <div class="modal-body">
            
            <input type="hidden" id="assign_media_id">
            <div class="form-group row">
                <label class="control-label col-sm-2">List Name:</label>
                <div class="col-sm-10">
                    <select multiple="" class="form-control selectpicker" id="assign_media_list_id"> 
                        <option value="" disabled="">Select Media List</option>
                        @foreach($media_list as $list)
                         <option value="{{$list->media_list_id}}">{{$list->list_name}}</option>
                        @endforeach
                    </select>
                    <div class="assign-media-msg text-success"></div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-blue assign-media-btn">Submit</button>
          </div>
    </div>
  </div>
</div>

@endsection

@section('pagescript')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
     <!-- Sweet-Alert  -->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap-select.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.js')}}"></script>

    <script type="text/javascript">
        function openTab(evt, tabName) {
            const div = evt.currentTarget;
            if(div.classList.contains('active')){
                evt.currentTarget.className = "tablinks";
                document.getElementById(tabName).style.display = "none";
            } else {
              var i, tabcontent, tablinks;
              tabcontent = document.getElementsByClassName("tabcontent");
              for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
              }
              tablinks = document.getElementsByClassName("tablinks");
              for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
              }
              document.getElementById(tabName).style.display = "block";
              evt.currentTarget.className += " active";
            }
        }
    </script>
    <script type="text/javascript">

    function select_media_list(shop_id){
        $("#assign_media_id").val(shop_id);
        $("#assign-media-modal").modal("show");
    }   

    $(function() {

        $(document).on('click', '.assign-media-btn', function(e) { 
            if($("#assign_media_list_id").val() != null){
                
                var shop_id = $("#assign_media_id").val();
                var media_list_ids = $("#assign_media_list_id").val();
                $.ajax({
                    url : "{{route('user.assign_media')}}",
                    data : { media_list_ids:media_list_ids, shop_id:shop_id},
                }).done(function (data) { 
                    $('.assign-media-msg').html('Media assigned successfully.');
                    $('input.checked-media-id').prop('checked', false);
                });
            } else {
                alert("Please select at least one media list.");
            }
        });

        /*$(document).on('click', '.assign-media-btn', function(e) { 
            var shop_id = $("#assign_media_id").val();
            var media_list_id = $("#assign_media_list_id").val();
            $.ajax({
                url : "{{route('user.assign_media')}}",
                data : { media_list_id:media_list_id, shop_id:shop_id},
            }).done(function (data) { 
                $('.assign-media-msg').html('Media assigned successfully.');
            });
        }); */ 

        $('.selectpicker').selectpicker({
            tickIcon: 'fa fa-check'
        });

        var url = $(this).attr('href');

        $('#filter_country_ids').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'SELECT COUNTRY'
        });
        $("button[data-id=filter_country_ids]").html("SELECT COUNTRY");
        $('#filter_country_ids').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).data('content')+' '+'<a href="Javascript:void(0);" class="remove-country" data-country-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .country-filter-contains").html(results);
            $("button[data-id=filter_country_ids]").html("SELECT COUNTRY");

            var url = $(this).attr('href');
            getLiveShopMedia(url);
        });
        
        $(document).on('click', '.country-filter-contains .item .remove-country', function(e) { 
            var wanted_id = $(this).data("country-id"); 
            var wanted_option = $('#filter_country_ids option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#filter_country_ids').selectpicker('refresh');
            $(this).parent().remove(); 

            getLiveShopMedia(url); 
        });

        var url = $(this).attr('href');
        getLiveShopMedia(url);
 
        $(document).on('click', '.pagination a', function(e) { 
            e.preventDefault();
            var url = $(this).attr('href');
            getLiveShopMedia(url);
            window.history.pushState("", "", url);
        });

       /* $(document).on('click', '#country_filter', function(e) { 
            if($("#filter_country_ids").val() != null){
                var url = $(this).attr('href');
                getLiveShopMedia(url);
            }
        });*/

        $('.only-number-allowed').bind('keyup paste', function(){
                this.value = this.value.replace(/[^0-9]/g, '');
        });

        //$(document).on('click', '#engagement_rate_filter', function(e) {
        $(document).on( "keyup", "#engagement_rate_from, #engagement_rate_to", function( e ) {
            if($("#engagement_rate_from").val() != '' && $("#engagement_rate_to").val() != ''){
                $(".activated-filter .engagement-rate-filter-contains").html('<span class="item">ENG. RATE '+$("#engagement_rate_from").val()+' TO '+$("#engagement_rate_to").val()+' '+'<a href="Javascript:void(0);" class="remove-engagement-rate"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getLiveShopMedia(url) 
            } else if($("#engagement_rate_from").val() != ''){
                $(".activated-filter .engagement-rate-filter-contains").html('<span class="item">ENG. RATE > '+$("#engagement_rate_from").val()+' '+'<a href="Javascript:void(0);" class="remove-engagement-rate"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getLiveShopMedia(url) 
            } else if($("#engagement_rate_to").val() != ''){
                $(".activated-filter .engagement-rate-filter-contains").html('<span class="item">ENG. RATE < '+$("#engagement_rate_to").val()+' '+'<a href="Javascript:void(0);" class="remove-engagement-rate"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getLiveShopMedia(url) 
            } else {
                $(".activated-filter .engagement-rate-filter-contains").html('');
            }  
        });
        $(document).on('click', '.engagement-rate-filter-contains .item .remove-engagement-rate', function(e) { 
            $(this).parent().remove();
            $("#engagement_rate_from").val('');
            $("#engagement_rate_to").val('');
            getLiveShopMedia(url);  
        });

       // $(document).on('click', '#followers_filter', function(e) {
        $(document).on( "keyup", "#followers_from, #followers_to", function( e ) {
            if($("#followers_from").val() != '' && $("#followers_to").val() != ''){
                $(".activated-filter .followers-filter-contains").html('<span class="item">FOLLOWERS '+$("#followers_from").val()+' TO '+$("#followers_to").val()+' '+'<a href="Javascript:void(0);" class="remove-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getLiveShopMedia(url) 
            } else if($("#followers_from").val() != ''){
                $(".activated-filter .followers-filter-contains").html('<span class="item">FOLLOWERS > '+$("#followers_from").val()+' '+'<a href="Javascript:void(0);" class="remove-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getLiveShopMedia(url) 
            } else if($("#followers_to").val() != ''){
                $(".activated-filter .followers-filter-contains").html('<span class="item">FOLLOWERS < '+$("#followers_to").val()+' '+'<a href="Javascript:void(0);" class="remove-followers"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getLiveShopMedia(url) 
            } else {
                $(".activated-filter .followers-filter-contains").html('');
            }
        });
        
        $(document).on('click', '.followers-filter-contains .item .remove-followers', function(e) { 
            $(this).parent().remove();
            $("#followers_from").val('');
            $("#followers_to").val('');
            getLiveShopMedia(url);  
        });

        //$(document).on('click', '#nb_of_story_filter', function(e) {
        $(document).on( "keyup", "#nb_of_story_from, #nb_of_story_to", function( e ) {
            if($("#nb_of_story_from").val() != '' && $("#nb_of_story_to").val() != ''){
                $(".activated-filter .nb-of-story-filter-contains").html('<span class="item">NB OF STORY '+$("#nb_of_story_from").val()+' TO '+$("#nb_of_story_to").val()+' '+'<a href="Javascript:void(0);" class="remove-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getLiveShopMedia(url) 
            } else if($("#nb_of_story_from").val() != ''){
                $(".activated-filter .nb-of-story-filter-contains").html('<span class="item">NB OF STORY > '+$("#nb_of_story_from").val()+' '+'<a href="Javascript:void(0);" class="remove-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getLiveShopMedia(url) 
            } else if($("#nb_of_story_to").val() != ''){
                $(".activated-filter .nb-of-story-filter-contains").html('<span class="item">NB OF STORY < '+$("#nb_of_story_to").val()+' '+'<a href="Javascript:void(0);" class="remove-nb-of-story"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
                var url = $(this).attr('href');
                getLiveShopMedia(url) 
            } else {
                $(".activated-filter .nb-of-story-filter-contains").html('');
            }
        });
        $(document).on('click', '.nb-of-story-filter-contains .item .remove-nb-of-story', function(e) { 
            $(this).parent().remove();
            $("#nb_of_story_from").val('');
            $("#nb_of_story_to").val('');
            getLiveShopMedia(url);  
        });

        $('#filter_category_ids').selectpicker({
            tickIcon: 'fa fa-check',
            noneSelectedText : 'SELECT CATEGORY'
        });
        $("button[data-id=filter_category_ids]").html("SELECT CATEGORY");
        $('#filter_category_ids').on('change', function(e){
            var selected = $(this).find('option:selected', this);
            var results = [];
            selected.each(function() {
                results.push('<span class="item">'+$(this).text()+' '+'<a href="Javascript:void(0);" class="remove-category" data-category-id="'+$(this).val()+'"><img class="close-filter" src="{{ asset('images/crossing.png')}}" /></a> </span>');
            });
            $(".activated-filter .category-filter-contains").html(results);
            $("button[data-id=filter_category_ids]").html("SELECT CATEGORY");

            var url = $(this).attr('href');
            getLiveShopMedia(url);
        });
         $(document).on('click', '.category-filter-contains .item .remove-category', function(e) { 
            var wanted_id = $(this).data("category-id"); 
            var wanted_option = $('#filter_category_ids option[value="'+ wanted_id +'"]');
            wanted_option.prop('selected', false);
            $('#filter_category_ids').selectpicker('refresh');
            $(this).parent().remove(); 

            getLiveShopMedia(url); 
        });

        /*$('#search_instagram_id').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                var url = $(this).attr('href');
                getLiveShopMedia(url);
            }
        });*/
        $(document).on( "keyup", "#search_instagram_id", function( e ) {
            e.stopImmediatePropagation();
            var url = $(this).attr('href');
            getLiveShopMedia(url);
        });

        function getLiveShopMedia(url, action="") 
        { 
            var search_instagram_id = $("#search_instagram_id").val();
            var filter_country_ids = $("#filter_country_ids").val();
            var eng_rate_from = $("#engagement_rate_from").val();
            var eng_rate_to = $("#engagement_rate_to").val();
            var followers_from = $("#followers_from").val();
            var followers_to = $("#followers_to").val();
            var nb_of_story_from = $("#nb_of_story_from").val(); 
            var nb_of_story_to = $("#nb_of_story_to").val();
            var filter_category_ids = $("#filter_category_ids").val();

            $.ajax({
                url : url,
                data : { search_instagram_id:search_instagram_id, filter_country_ids:filter_country_ids, eng_rate_from:eng_rate_from, eng_rate_to:eng_rate_to, followers_from:followers_from, followers_to:followers_to, nb_of_story_from:nb_of_story_from, nb_of_story_to:nb_of_story_to, filter_category_ids:filter_category_ids},
                dataType: 'json',
            }).done(function (data) { 
                $('.live-now').html(data.body);
            });
        }
    });
    </script>
@stop
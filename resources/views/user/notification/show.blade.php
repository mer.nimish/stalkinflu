@extends('layout.main')
@section('content') 

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="notificationslisting">
            <h3 class="sectitle"><a href="{{route('notification.index')}}"><i class="fa fa-reply" style="color:#508ff4;"></i></a> Notification Details</h3>

            <div class="item">
                <div class="date">
                    <h5>{{date("m/d/y", strtotime($notification->date))}}</h5>
                </div>
                <div class="content" style="width: 84%">
                    <p>
                        {{$notification->notification}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

@section('pagescript')

@stop
@endsection
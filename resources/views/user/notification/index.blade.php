@extends('layout.main')
@section('content') 

<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="notificationslisting">
            <h3 class="sectitle">Notifications</h3>

        @foreach($notifications as $notification)

            <div class="item">
                <div class="date">
                    <h5>{{date("m/d/y", strtotime($notification->date))}}</h5>
                </div>
                <div class="content">
                    <p>
                        {{Str::limit($notification->notification,190)}}
                    </p>
                </div>
                <div class="showmore">
                    <a href="{{route('notification.show', $notification->notification_id)}}">SHOW MORE</a>
                </div>
            </div>

        @endforeach

        <div class="row">
            <div class="text-center pagination-section">
            {!! $notifications->links() !!} 
            </div>
        </div>

        </div>
    </div>
</div>

@section('pagescript')

@stop
@endsection
@if(count($shops) > 0)
    @foreach($shops as $shop)

         <div class="col-md-3 col-sm-4 col-xs-12 {{$shop->shop_id}}">
            <div class="item">
                <div class="mediaheader">
                    <a href="{{$shop->website}}" target="_blank"><span title="{{$shop->website}}" data-toggle="tooltip" data-placement="top">{{ Str::limit($shop->website, 22) }}</span></a>
                    <span class="red-circle"></span>
                </div>
                <div class="more-info-link">
                    <a href="{{route('shop.show', $shop->shop_id)}}" target="_blank">MORE INFO ON SHOP</a>
                     <span class="addlist">
                        <a href="#" onclick="select_media_list('{{$shop->shop_id}}')"><img class="addlist" src="{{ asset('images/addlist.png')}}"></a>
                    </span>
                </div>
                <div class="image">
                    @if(File::exists('public/storage/shop_media/main/'.$shop->shop_id.'.mp4'))
                        <video controls>
                          <source src="{{ asset('storage/shop_media/main/'.$shop->shop_id.'.mp4')}}" type="video/mp4">
                          Your browser does not support the video tag.
                        </video>
                    @else
                        <img src="{{asset('images/no-video.png') }}" width="240" height="368" />
                    @endif
                    
                </div>
                <div class="mediacontent">
                    <div class="time-on-live">
                        <span>{{Helpers::time_elapsed_string($shop->created_at)}}</span>
                    </div>
                    <div class="userinfo">
                    @if(!empty($shop->profile_pic))
                        <img class="usericon" src="{{asset(Config::get('constants.profile_image_thumb_url').$shop->profile_pic)}}">
                    @else
                        <img class="usericon" src="{{asset('images/profile.png')}}">
                    @endif
                    <span>
                        <a href="{{route('influencer.show', $shop->influencer_id)}}" target="_blank">{{$shop->instagram_id}}</a>
                        @if($shop->is_verified)
                            <img class="profile-verified-icon" src="{{ asset('images/verified.png')}}"/>
                        @endif
                    </span>
                </div>
                    <div class="countryinfo">
                        <img class="countryicon" src="{{asset('images/flags/'.Str::lower($shop->country_code).'.svg')}}"  data-toggle="tooltip" data-placement="top" data-html="true" title="{{$shop->country_name}}"/>
                        <span class="followers">{{Helpers::short_number_format($shop->followers)}} followers</span>
                        <span class="stories">{{$shop->nb_of_story}} stories</span>
                    </div>
                    <div class="info">
                        <span class="eng-rate">{{$shop->engagement_rate}}% eng.</span>
                        @if($shop->category_cnt != 0)
               
                          @if(!empty($shop->categories))
                            @php
                            $categories = explode("<br />", $shop->categories);
                            @endphp

                            <span class="category-text">{{ $categories[0] }}</span>
                            @if(count($categories) > 1)
                                @php
                                    unset($categories[0]);
                                @endphp
                                <span class="category-cnt" data-toggle="tooltip" data-placement="top" data-html="true" title="{{implode('<br />', $categories)}}">+{{ ($shop->category_cnt-1) }}</span>
                            @endif
                          @endif
                        @else
                            <span class="category-text">Coming soon</span>
                        @endif 
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <div class="pagination-section">
        @if($shops->total() == $total_rec_cnt)
            <p class="pagination-info">{{ $total_rec_cnt }} medias
            </p>
        @else
            <p class="pagination-info">{{ $shops->total() }} out of {{$total_rec_cnt}} medias
            </p>
        @endif

        {!! $shops->links() !!} 
    </div>
@endif
@extends('layout.main')
@section('content')

@section('pagestylesheet')
<link href="{{ asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ asset('css/bootstrap-select.css')}}">
<link rel="stylesheet" href="{{ asset('css/jquery-ui.css')}}">
@stop

<div class="col-md-9 col-sm-9 col-xs-12">
    <div class="middlepart">
        <div class="media-list-section custom-list-section">
            <div class="topbar">
                <h3>Your list of media</h3>
                 <div class="create-list-link">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#create-new-list-modal">CREATE A NEW LIST</a>
                </div>
            </div>

            @if(!$media_list->isEmpty())

            <div class="col-md-1 col-xs-1 col-lprp-0">
                <a class="list-prev-arrow {{ (($media_list->currentPage() == 1) ? 'disable' : '') }}" href="{{ ($media_list->currentPage() == 1) ? '#' : $media_list->url($media_list->currentPage()-1) }}">‹</a>
            </div>
            <div class="col-md-10 col-xs-10 col-lprp-0">
                @foreach($media_list as $list)
                    <div class="col-md-4 list-item">
                        
                        <div class="list-title">
                            <a href="javascript:void(0);" class="edit" data-media-list-name="{{$list->list_name}}" data-media-list-id="{{$list->media_list_id}}"><i class="fa fa-pencil"></i></a>
                            <a href="{{route('media-list.show', $list->media_list_id)}}" class="text-black title"><h1><strong>{{$list->list_name}}</strong></h1></a>
                             <a href="javascript:void(0);" onclick="delete_list('{{$list->media_list_id}}');" class="delete" style=""><i class="fa fa-close"></i></a>
                        </div>
                        <div class="list-contains">
                            <div class="list-sub-box">
                                <label>Countries:</label>
                                <div class="">
                                    @if(!empty($list->country_code))
                                      @php
                                      $country_codes = explode(", ", $list->country_code);
                                      $country_names = explode(", ", $list->country_name);
                                      @endphp
                                        @foreach($country_codes as $key => $country_code)
                                                <img class="country-flag" src="{{asset('images/flags/'.Str::lower($country_code).'.svg')}}" title="{{$country_names[$key]}}"  data-toggle="tooltip" data-placement="top" data-html="true"/>
                                            @if($key == 3)
                                             @break
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="list-sub-box">
                                <label>Total Medias:</label>
                                <div class="">
                                   <h1><strong>{{($list->tot_media)}}</strong></h1>
                                </div>
                            </div>
                            <div class="list-sub-box">
                                <label>Total Shops:</label>
                                <div class="">
                                   <h1><strong>{{floor($list->tot_shop)}}</strong></h1>
                                </div>
                            </div>
                            <div class="list-sub-box">
                                <label>Total Influencers:</label>
                                <div class="">
                                   <h1><strong>{{($list->tot_inf)}}</strong></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-1 col-xs-1 col-lprp-0">
                <a class="list-next-arrow {{ (($media_list->currentPage() == $media_list->lastPage()) ? 'disable' : '') }}" href="{{ ($media_list->currentPage() == $media_list->lastPage()) ? '#' : $media_list->url($media_list->currentPage()+1) }}" >›</a>
            </div>
             @else
              <div class="col-md-12 text-center">
                <div>No media list found.</div>
              </div>
            @endif
        </div>
    </div>
</div>

<div id="create-new-list-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      {!! Form::open(array('route' => 'media-list.store','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'create-media-list-form')) !!}      
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Create List</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <label class="control-label mb-10 col-sm-3" for="last_name">List Name:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="list_name" name="list_name" maxlength="10" required="">
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-blue">Submit</button>
          </div>
      {!! Form::close() !!} 
    </div>
  </div>
</div>
<div id="edit-list-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      {!! Form::open(array('route' => 'user.media_list.update','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'edit-media-list-form')) !!} 
          <input type="hidden" name="media_list_id" id="media_list_id">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit List</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <label class="control-label mb-10 col-sm-3" for="last_name">List Name:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="edit_list_name" name="list_name" maxlength="10" required="">
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-blue">Submit</button>
          </div>
      {!! Form::close() !!} 
    </div>
  </div>
</div>             
@endsection

@section('pagescript')
    <!-- Sweet-Alert  -->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap-select.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.js')}}"></script>

    <script type="text/javascript">
      function delete_list(media_list){

            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this data!",   
                type: "",   
                showCancelButton: true,   
                confirmButtonColor: "#508FF4",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                /*iconColor: '#000',*/
            }, function(){   
                swal("Deleted!", "Your data has been deleted.", "success"); 
                window.location.href = '{{route('user.media_list.delete')}}?media_list='+media_list;
                return true;
            });

            return false;
        }
    $(function() {
       $(document).on("click", ".list-title .edit", function(){
            var media_list_name = $(this).data('media-list-name');
            $("#edit_list_name").val(media_list_name);
            var media_list_id = $(this).data('media-list-id');
            $("#media_list_id").val(media_list_id);
            $("#edit-list-modal").modal("show");
        });
        
    });
    </script>
@stop
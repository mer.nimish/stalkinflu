@extends('layout.main')
@section('content')
<div class="col-md-9 col-sm-9 col-xs-12 no-padding dskpdng">
    <div class="col-md-6 col-sm-12 col-xs-12 no-padding">
        <div class="middlepart">
            <div class="toptrends">
                <h3 class="sectitle">Top INFLUENCERS of the week (by promo) :</h3>
                @foreach($top_influencer_of_week as $key => $influencer)
                <div class="item influencer-row-link" data-influencer-details-url= "{{route('influencer.show', $influencer->influencer_id)}}">
                    <div class="trendno">
                        <span>{{$key+1}}</span>
                    </div>
                    <div class="trendinfo">
                        @if(!empty($influencer->profile_pic))
                            <img src="{{asset(Config::get('constants.profile_image_thumb_url').$influencer->profile_pic)}}">
                        @else
                            <img src="{{asset('images/profile.png')}}">
                        @endif
                        <p>{{$influencer->instagram_id}}</p>
                    </div>
                    <div class="trendpromos">
                        <p>{{$influencer->promos}} promos</p>
                    </div>
                </div>
                 @endforeach
                <div class="showmore">
                    <a href="{{route('influencer.index')}}?flag=trend">Show more</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 col-xs-12 no-padding">
        <div class="rightside">
            <div class="toptrends">
                <h3 class="sectitle">Top SHOPS of the week (by promo) :</h3>
                @foreach($top_shop_of_week as $key => $top_shop)
                <div class="item shop-row-link" data-shop-details-url= "{{route('shop.show', $top_shop->shop_id)}}">
                    <div class="trendno">
                        <span>{{$key+1}}</span>
                    </div>
                    <div class="trendinfo">
                        {{ Str::limit($top_shop->website, 25) }}
                    </div>
                    <div class="trendpromos">
                        <p>{{$top_shop->promotions}} promotions</p>
                    </div>
                </div>
                @endforeach
                <div class="showmore">
                    <a href="{{route('shop.index')}}?flag=trend">Show more</a>
                </div>
            </div>
        </div>
    </div>
</div>
@section('pagescript')
<script type="text/javascript">
    $(document).on('click', '.influencer-row-link', function(){ 
            window.open($(this).data("influencer-details-url"), '_blank');
        });
     $(document).on('click', '.shop-row-link', function(){ 
            window.open($(this).data("shop-details-url"), '_blank');
        });
</script>
@stop
@endsection
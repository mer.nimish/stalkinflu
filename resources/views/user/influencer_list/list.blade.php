@extends('layout.main')
@section('content')

@section('pagestylesheet')
<link href="{{ asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ asset('css/bootstrap-select.css')}}">
<link rel="stylesheet" href="{{ asset('css/jquery-ui.css')}}">
@stop

<div class="col-md-9 col-sm-9 col-xs-12 ">
    <div class="middlepart">
        <div class="influencer-list-section custom-list-section">
            <div class="topbar">
                <h3>Your list of influencers</h3>
                 <div class="create-list-link">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#create-new-list-modal">CREATE A NEW LIST</a>
                </div>
            </div>
             <div class="error-contains">
              @if ($message = Session::get('success'))
                  <div class="alert alert-success text-success">
                      <i class="fa fa-check"></i> {{ $message }}
                  </div>
              @endif
            </div>

            @if(!$influencer_list->isEmpty())

            <div class="col-md-1 col-xs-1 col-lprp-0">
                <a class="list-prev-arrow {{ (($influencer_list->currentPage() == 1) ? 'disable' : '') }}" href="{{ ($influencer_list->currentPage() == 1) ? '#' : $influencer_list->url($influencer_list->currentPage()-1) }}">‹</a>
            </div>
            <div class="col-md-10 col-xs-10 col-lprp-0">
                @foreach($influencer_list as $list)
                    <div class="col-md-4 list-item">
                        
                        <div class="list-title">
                            <a href="javascript:void(0);" class="edit" data-influencer-list-name="{{$list->list_name}}" data-influencer-list-id="{{$list->influencer_list_id}}"><i class="fa fa-pencil"></i></a>
                            <h1>
                            <a class="title" href="{{route('influencer-list.show', $list->influencer_list_id)}}" class="text-black"><strong>{{$list->list_name}}</strong> </a> <span class="influencer-list-favourite" style="font-size: 30px;" data-influencer-list-id="{{$list->influencer_list_id}}" data-toggle="tooltip" data-placement="top" data-html="true" title="If you want to pin a favorite influencer list then show this influencer list in your dashboard."><i class="il_{{$list->influencer_list_id}} fa <?=($list->is_favourite == 1) ? 'fa-heart' : 'fa-heart-o';?> "></i></span></h1> 
                            
                            <a href="javascript:void(0);" onclick="delete_list('{{$list->influencer_list_id}}');" class="delete" style=""><i class="fa fa-close"></i></a>

                            
                        </div>
                        <div class="list-contains">
                            <div class="list-sub-box">
                                <label>Countries:</label>
                                <div class="">
                                    @if(!empty($list->country_code))
                                      @php
                                      $country_codes = explode(", ", $list->country_code);
                                      $country_names = explode(", ", $list->country_name);
                                      @endphp
                                        @foreach($country_codes as $key => $country_code)
                                                <img class="country-flag" data-toggle="tooltip" data-placement="top" data-html="true" src="{{asset('images/flags/'.Str::lower($country_code).'.svg')}}" title="{{$country_names[$key]}}"/>
                                            @if($key == 3)
                                             @break
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="list-sub-box">
                                <label>Promo This week:</label>
                                <div class="">
                                   <h1><strong>{{($list->promo_this_week)}}</strong></h1>
                                </div>
                            </div>
                            <div class="list-sub-box">
                                <label>Average Eng. rate:</label>
                                <div class="">
                                   <h1><strong>{{floor($list->avg_eng_rate)}}%</strong></h1>
                                </div>
                            </div>
                            <div class="list-sub-box">
                                <label>Total Influencers:</label>
                                <div class="">
                                   <h1><strong>{{($list->tot_influencer)}}</strong></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-1 col-xs-1 col-lprp-0">
                <a class="list-next-arrow {{ (($influencer_list->currentPage() == $influencer_list->lastPage()) ? 'disable' : '') }}" href="{{ ($influencer_list->currentPage() == $influencer_list->lastPage()) ? '#' : $influencer_list->url($influencer_list->currentPage()+1) }}" >›</a>
            </div>
            @else
              <div class="col-md-12 text-center">
                <div>No influencer list found.</div>
              </div>
            @endif
        </div>
    </div>
</div>

<div id="create-new-list-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      {!! Form::open(array('route' => 'influencer-list.store','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'create-influencer-list-form')) !!}      
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Create List</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <label class="control-label mb-10 col-sm-3" for="last_name">List Name:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="list_name" name="list_name" maxlength="10" required="">
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-blue">Submit</button>
          </div>
      {!! Form::close() !!} 
    </div>
  </div>
</div>

<div id="edit-list-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      {!! Form::open(array('route' => 'user.influencer_list.update','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'edit-influencer-list-form')) !!} 
          <input type="hidden" name="influencer_list_id" id="influencer_list_id">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit List</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <label class="control-label mb-10 col-sm-3" for="last_name">List Name:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="edit_list_name" name="list_name" maxlength="10" required="">
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-blue">Submit</button>
          </div>
      {!! Form::close() !!} 
    </div>
  </div>
</div>
           
@endsection

@section('pagescript')
    <!-- Sweet-Alert  -->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap-select.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.js')}}"></script>

    <script type="text/javascript">

      function delete_list(influencer_list){

            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this data!",   
                type: "",   
                showCancelButton: true,   
                confirmButtonColor: "#508FF4",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                /*iconColor: '#000',*/
            }, function(){   
                swal("Deleted!", "Your data has been deleted.", "success"); 
                window.location.href = '{{route('user.influencer_list.delete')}}?influencer_list='+influencer_list;
                return true;
            });

            return false;
        }
    $(function() {

      $(document).on("click", ".influencer-list-favourite", function(){
                
              var influencer_list_id = $(this).data("influencer-list-id");
              $(".influencer-list-favourite i").removeClass('fa-heart');
              $(".influencer-list-favourite i").addClass('fa-heart-o');
              $(".influencer-list-favourite i.il_"+influencer_list_id).toggleClass('fa-heart fa-heart-o');

              $.ajax({
                  url : "{{route('user.favourite_influencer_list')}}",
                  data : { influencer_list_id: influencer_list_id},
                  dataType: 'json',
              }).done(function (data) { 
                  //$('.shops_tr').html(data.body);
              });
        });

      $(document).on("click", ".list-title .edit", function(){
          var influencer_list_name = $(this).data('influencer-list-name');
          $("#edit_list_name").val(influencer_list_name);
          var influencer_list_id = $(this).data('influencer-list-id');
          $("#influencer_list_id").val(influencer_list_id);
          $("#edit-list-modal").modal("show");
      });

     
        
    });
    </script>
@stop
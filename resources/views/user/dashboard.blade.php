@extends('layout.main')
@section('content')
<div class="col-md-6 col-sm-6 col-xs-12 no-padding">
    <div class="middlepart">
        <div class="dashboardwelcom">
            <h3>{{$setting->dashboard_header}}</h3>

             @if ($message = Session::get('success'))
                <div class="alert text-success">
                    <i class="fa fa-check"></i> {{ $message }}
                </div>
            @endif

            <p>{{$setting->dashboard_text}}</p>
            <div class="welcomelist">
                @foreach($notifications as $notification)

                    <div class="item">
                        <div class="date">
                            <h4>{{date("m/d/y", strtotime($notification->date))}}</h4>
                        </div>
                        <div class="content">
                            <p>
                                {{Str::limit($notification->notification,50)}}
                            </p>
                        </div>
                        <div class="readmore">
                            <a href="{{route('notification.show', $notification->notification_id)}}">READ MORE</a>
                        </div>
                    </div>

                @endforeach
                
            </div>
            <div class="showmore">
                <a href="{{route('notification.index')}}">Show more</a>
            </div>
        </div>

        <div class="statisctic">
            <h3>Statistics</h3>
            <div class="statiscticitem">
                <div class="icon">
                    <img src="{{ asset('images/statistic1.png')}}">
                </div>
                <div class="info">
                    <h4>{{$setting->dashboard_total_members}}</h4>
                    <span>Total members</span>
                </div>
            </div>
            <div class="statiscticitem">
                <div class="icon">
                    <img src="{{ asset('images/statistic2.png')}}">
                </div>
                <div class="info">
                    <h4>{{$shops}}</h4>
                    <span>Total shops</span>
                </div>
            </div>
            <div class="statiscticitem">
                <div class="icon">
                    <img src="{{ asset('images/statistic3.png')}}">
                </div>
                <div class="info">
                    <h4>{{$influencers}}</h4>
                    <span>Total influencers </span>
                </div>
            </div>
        </div>
        @if(isset($best_shop_of_the_month->shop_id))
        <div class="bestshopmonth">
            <h3>THE BEST SHOP OF THE MONTH</h3>
            <div class="promotion">
                <div class="leftpart">
                    <a href="{{route('shop.show', $best_shop_of_the_month->shop_id)}}">{{Str::limit($best_shop_of_the_month->website, 25)}}</a>
                    <p>{{$best_shop_of_the_month->promotions}} promotions this month</p>
                </div>
                <div class="rightpart">
                    <a href="{{route('shop.show', $best_shop_of_the_month->shop_id)}}">show more</a>
                </div>
            </div>
            <div class="promoted">
                <div class="leftpart">
                    <h6>Promoted by</h6>
                    @if(!empty($best_shop_of_the_month->profile_pic))
                        <img style="border-radius: 50%;" src="{{asset(Config::get('constants.profile_image_thumb_url').$best_shop_of_the_month->profile_pic)}}">
                    @else
                        <img style="border-radius: 50%;" src="{{asset('images/profile.png')}}">
                    @endif
                    <p>{{$best_shop_of_the_month->instagram_id}}</p>
                </div>
                <div class="rightpart">
                    <a href="{{route('influencer.show', $best_shop_of_the_month->influencer_id)}}">show more</a>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
<div class="col-md-3 col-sm-3 col-xs-12 no-padding">
    <div class="rightside">
        <div class="favoriteinfluencer">
            <h3 class="sectitle">Favorite influencers</h3>
                @if(!$fav_influencers->isEmpty())
                    @foreach($fav_influencers as $influencer)
                    <div class="item">
                        <a href="{{route('influencer.show', $influencer->influencer_id)}}" target="_blank">
                            @if(!empty($influencer->profile_pic))
                                <img src="{{asset(Config::get('constants.profile_image_thumb_url').$influencer->profile_pic)}}">
                            @else
                                <img src="{{asset('images/profile.png')}}">
                            @endif
                        <p>{{$influencer->first_name.' '.$influencer->last_name}}</p>
                        </a>
                        <span class="followers">{{Helpers::short_number_format($influencer->followers)}}</span>
                    </div>
                    @endforeach
                    <!-- <div class="item">
                        <img src="{{ asset('images/favuser2.png')}}">
                        <p>Malcolm Quaday</p>
                        <span>17.6k</span>
                    </div>-->
                    <div class="showmore">
                        <a href="{{route('influencer-list.show', $influencer_list_id)}}">Show all</a>
                    </div>
                @else
                    If you want to pin a favorite influencer list on your dashboard, please go to influencers list page and check one.
                @endif
        </div>

        <div class="favoriteinfluencer favoriteshops">
            <h3 class="sectitle">Favorite shops</h3>
            
            @if(!$fav_shops->isEmpty())
                    @foreach($fav_shops as $shop)
                    <div class="item">
                        <a href="{{route('shop.show', $shop->shop_id)}}" target="_blank">{{Str::limit($shop->website,20)}}</a>
                        @if($shop->activity == 'high')
                            <span><i class="fa fa-circle" style="color: #fbdede;"></i> High </span>
                        @elseif($shop->activity == 'medium')
                            <span><i class="fa fa-circle" style="color: #d2ebd4;"></i> Medium</span>
                        @else
                           <span><i class="fa fa-circle" style="color: #f6e6ba;"></i> Low</span>
                        @endif
                    </div>
                    @endforeach
                    <!-- <div class="item">
                        <img src="{{ asset('images/favuser2.png')}}">
                        <p>Malcolm Quaday</p>
                        <span>17.6k</span>
                    </div>-->
                    <div class="showmore">
                        <a href="{{route('shop-list.show', $shop_list_id)}}">Show all</a>
                    </div>
                @else
                    If you want to pin a favorite shop list on your dashboard, please go to shops list page and check one.
                @endif
            <!-- <div class="item">
                <a href="#">www.exemple.com</a>
                <span><i class="fa fa-circle"></i> High</span>
            </div>
            <div class="item">
                <a href="#">www.exemple.com</a>
                <span><i class="fa fa-circle"></i> High</span>
            </div>
            <div class="item">
                <a href="#">www.exemple.com</a>
                <span><i class="fa fa-circle"></i> High</span>
            </div>
            <div class="item">
                <a href="#">www.exemple.com</a>
                <span><i class="fa fa-circle"></i> High</span>
            </div>
            <div class="item">
                <a href="#">www.exemple.com</a>
                <span><i class="fa fa-circle"></i> High</span>
            </div>
            <div class="item">
                <a href="#">www.exemple.com</a>
                <span><i class="fa fa-circle"></i> High</span>
            </div>
            <div class="showmore">
                <a href="#">Show all</a>
            </div> -->
        </div>
    </div>
</div>
@section('pagescript')

@stop
@endsection
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
        
        @php
        $currentRoute = Request::route()->getName();
        @endphp
        @if($currentRoute)
            <title>{{Config::get('constants.meta_tags.'.$currentRoute.'.title')}}</title>
            <meta name="title" content="{{Config::get('constants.meta_tags.'.$currentRoute.'.title')}}">
            <meta name="keywords" content="{{Config::get('constants.meta_tags.'.$currentRoute.'.keywords')}}">
            <meta name="description" content="{{Config::get('constants.meta_tags.'.$currentRoute.'.description')}}">
            <meta name="author" content="{{Config::get('constants.meta_tags.'.$currentRoute.'.author')}}">
        @endif

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.png')}}">

        <!--Main Stylesheet-->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css')}}">
    </head>
    <body>

        <div class="mainsection">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-4 col-sm-4 col-xs-12">  
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="middlepart">

                        <div class="signup-section">
                            <h1 class="text-center">Sign-up</h1>
                            <div>
                                @if ($errors->any())
                                    <div class="error-contains">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li class="text-danger">{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="text-danger text-left">{{ Session::get('error') }}</div>
                                @elseif(Session::has('success'))
                                    <div class="text-success text-left">{{ Session::get('success') }}</div>
                                @endif
                                <form action="{{ route('user.signup') }}" method="POST" id="signup-form">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="type" value="User" id="type">
                                    
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control" name="first_name" id="first_name">
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control" name="last_name" id="last_name">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email" id="email">
                                    </div>
                                    <div class="form-group">
                                        <label class="confirm-password">Password</label>
                                        <input type="password" class="form-control" name="password" id="password" >
                                    </div>
                                    <div class="form-group">
                                        <label class="confirm-password">Confirm Password</label>
                                        <input type="password" class="form-control" name="confirm_password" id="confirm_password">
                                    </div>
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-blue">SUBMIT</button>
                                    </div>

                                    <div class="form-group text-center">
                                        <a href="{{route('user.login')}}">Already have an account?</a>
                                    </div>
                                </form>
                              
                            </div>
                                    
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document" style="margin-top: 95px;">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel" style="width: 50%;float: left;">Stripe Payment</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="{{route('user.stripe_payment_request')}}" method="post" id="payment-form" data-rewardful>
                  @csrf

                <input type="hidden" name="first_name" id="stripe_first_name">
                <input type="hidden" name="last_name" id="stripe_last_name">
                <input type="hidden" name="email" id="stripe_email">
                <input type="hidden" name="password" id="stripe_password">

                  <div class="row">
                    <div class="col-md-12">
                    <label for="card-element">
                      Credit or debit card
                    </label>
                    <div id="card-element">
                      <!-- A Stripe Element will be inserted here. -->
                    </div>

                    <!-- Used to display form errors. -->
                    <div id="card-errors" role="alert"></div>
                    </div>
                    <div class="col-md-12 text-center">
                      <button>Submit Payment</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div> -->
            </div>
          </div>
        </div>

    <!--jQuery JS-->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <!--Bootstrap JS-->
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>

    <script>(function(w,r){w._rwq=r;w[r]=w[r]||function(){(w[r].q=w[r].q||[]).push(arguments)}})(window,'rewardful');</script>
    <script async src='https://r.wdfl.co/rw.js' data-rewardful='86e701'></script>

    <script type="text/javascript" src="{{ asset('js/jquery.validate.js')}}"></script>
    <script type="text/javascript">

    $(document).ready(function () {
        $('#signup-form').validate({ 
             rules: {
                first_name:{
                    required: true,
                },
                last_name:{
                    required: true,
                },
                email:{
                    required: true,
                    emailfull: true,
                    remote: "{{route('user.is_email_already_exists_user')}}"
                },
                password: {
                    required: true,
                    password_stronger: true,
                },
                confirm_password: {
                    required: true,
                    equalTo : "#password"
                },
            },
            messages: {
                name:{
                    required: 'Please enter name',
                },
                email:{
                    required: 'Please enter email',
                    remote: 'Email already exists.',
                },
                password:{
                    required: 'Please enter password',
                },
                confirm_password:{
                    required: 'Please enter confirm password',
                },
            },
            errorElement : 'div',
            /*errorPlacement: function(error, element){
                $(element).addClass('error');
            },*/
            submitHandler: function (form) {
              $("#stripe_first_name").val($("#first_name").val());
              $("#stripe_last_name").val($("#last_name").val());
              $("#stripe_email").val($("#email").val());
              $("#stripe_password").val($("#password").val());
              $("#paymentModal").modal("show");
              return false;

            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
        });

        jQuery.validator.addMethod("emailfull", function(value, element) {
             return this.optional(element) || /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(value);
        }, "Please enter valid email address");

        jQuery.validator.addMethod("password_stronger", function(value, element) {
              var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/;
              return re.test(value);
        }, "Password must contain at least six characters that are one lowercase and one uppercase letter, numbers or the underscore.");

    });
    </script>
    <script src="https://js.stripe.com/v3/"></script>
  <script type="text/javascript">
    var stripe = Stripe("{{Config::get('constants.stripe.publishable-key')}}");
    var elements = stripe.elements();
    // Custom styling can be passed to options when creating an Element.
    var style = {
      base: {
        // Add your base input styles here. For example:
        fontSize: '16px',
        color: '#32325d',
      },
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Create a token or display an error when the form is submitted.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function(event) {
      event.preventDefault();

      stripe.createToken(card).then(function(result) {
        if (result.error) {
          // Inform the customer that there was an error.
          var errorElement = document.getElementById('card-errors');
          errorElement.textContent = result.error.message;
        } else {
          // Send the token to your server.
          stripeTokenHandler(result.token);
        }
      });
    });

    function stripeTokenHandler(token) {
      // Insert the token ID into the form so it gets submitted to the server
      var form = document.getElementById('payment-form');
      var hiddenInput = document.createElement('input');
      hiddenInput.setAttribute('type', 'hidden');
      hiddenInput.setAttribute('name', 'stripeToken');
      hiddenInput.setAttribute('value', token.id);
      form.appendChild(hiddenInput);

      // Submit the form
      form.submit();
    }
      </script>
    </body>
</html>

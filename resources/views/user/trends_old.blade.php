@extends('layout.main')
@section('content')
<div class="col-md-9 col-sm-9 col-xs-12 no-padding dskpdng">
    <div class="col-md-6 col-sm-12 col-xs-12 no-padding">
        <div class="middlepart">
            <div class="toptrends">
                <h3 class="sectitle">Top INFLUENCERS of the week (by promo) :</h3>
                <div class="item">
                    <div class="trendno">
                        <span>01</span>
                    </div>
                    <div class="trendinfo">
                        <img src="images/favuser1.png">
                        <p>example_id</p>
                    </div>
                    <div class="trendpromos">
                        <p>10 promos</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>02</span>
                    </div>
                    <div class="trendinfo">
                        <img src="images/favuser1.png">
                        <p>example_id</p>
                    </div>
                    <div class="trendpromos">
                        <p>10 promos</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>03</span>
                    </div>
                    <div class="trendinfo">
                        <img src="images/favuser1.png">
                        <p>example_id</p>
                    </div>
                    <div class="trendpromos">
                        <p>10 promos</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>04</span>
                    </div>
                    <div class="trendinfo">
                        <img src="images/favuser1.png">
                        <p>example_id</p>
                    </div>
                    <div class="trendpromos">
                        <p>10 promos</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>05</span>
                    </div>
                    <div class="trendinfo">
                        <img src="images/favuser1.png">
                        <p>example_id</p>
                    </div>
                    <div class="trendpromos">
                        <p>10 promos</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>05</span>
                    </div>
                    <div class="trendinfo">
                        <img src="images/favuser1.png">
                        <p>example_id</p>
                    </div>
                    <div class="trendpromos">
                        <p>10 promos</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>05</span>
                    </div>
                    <div class="trendinfo">
                        <img src="images/favuser1.png">
                        <p>example_id</p>
                    </div>
                    <div class="trendpromos">
                        <p>10 promos</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>05</span>
                    </div>
                    <div class="trendinfo">
                        <img src="images/favuser1.png">
                        <p>example_id</p>
                    </div>
                    <div class="trendpromos">
                        <p>10 promos</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>05</span>
                    </div>
                    <div class="trendinfo">
                        <img src="images/favuser1.png">
                        <p>example_id</p>
                    </div>
                    <div class="trendpromos">
                        <p>10 promos</p>
                    </div>
                </div>
                <div class="showmore">
                    <a href="#">Show more</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 col-xs-12 no-padding">
        <div class="rightside">
            <div class="toptrends">
                <h3 class="sectitle">Top SHOPS of the week (by promo) :</h3>
                @foreach($top_shop_of_week as $key => $top_shop)
                <div class="item">
                    <div class="trendno">
                        <span>{{$key+1}}</span>
                    </div>
                    <div class="trendinfo">
                        <a href="{{$top_shop->website}}">{{$top_shop->website}}</a>
                    </div>
                    <div class="trendpromos">
                        <p>{{$top_shop->promotions}} promotions</p>
                    </div>
                </div>
                @endforeach
                <div class="item">
                    <div class="trendno">
                        <span>02</span>
                    </div>
                    <div class="trendinfo">
                        <a href="#">www.example.com</a>
                    </div>
                    <div class="trendpromos">
                        <p>10 promotions</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>03</span>
                    </div>
                    <div class="trendinfo">
                        <a href="#">www.example.com</a>
                    </div>
                    <div class="trendpromos">
                        <p>10 promotions</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>04</span>
                    </div>
                    <div class="trendinfo">
                        <a href="#">www.example.com</a>
                    </div>
                    <div class="trendpromos">
                        <p>10 promotions</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>04</span>
                    </div>
                    <div class="trendinfo">
                        <a href="#">www.example.com</a>
                    </div>
                    <div class="trendpromos">
                        <p>10 promotions</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>04</span>
                    </div>
                    <div class="trendinfo">
                        <a href="#">www.example.com</a>
                    </div>
                    <div class="trendpromos">
                        <p>10 promotions</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>04</span>
                    </div>
                    <div class="trendinfo">
                        <a href="#">www.example.com</a>
                    </div>
                    <div class="trendpromos">
                        <p>10 promotions</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>04</span>
                    </div>
                    <div class="trendinfo">
                        <a href="#">www.example.com</a>
                    </div>
                    <div class="trendpromos">
                        <p>10 promotions</p>
                    </div>
                </div>
                <div class="item">
                    <div class="trendno">
                        <span>04</span>
                    </div>
                    <div class="trendinfo">
                        <a href="#">www.example.com</a>
                    </div>
                    <div class="trendpromos">
                        <p>10 promotions</p>
                    </div>
                </div>
                <div class="showmore">
                    <a href="#">Show more</a>
                </div>
            </div>
        </div>
    </div>
</div>
@section('pagescript')

@stop
@endsection
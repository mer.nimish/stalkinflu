@extends('layout.main')
@section('content')

@section('pagestylesheet')
<link href="{{ asset('css/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ asset('css/bootstrap-select.css')}}">
<link rel="stylesheet" href="{{ asset('css/jquery-ui.css')}}">
@stop

<div class="col-md-9 col-sm-9 col-xs-12">
    <div class="middlepart">
        <div class="shop-list-section custom-list-section">
            <div class="topbar">
                <h3>Your list of shops</h3>
                 <div class="create-list-link">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#create-new-list-modal">CREATE A NEW LIST</a>
                </div>
            </div>
            @if(!$shop_list->isEmpty())
            
            <div class="col-md-1 col-xs-1 col-lprp-0">
                <a class="list-prev-arrow {{ (($shop_list->currentPage() == 1) ? 'disable' : '') }}" href="{{ ($shop_list->currentPage() == 1) ? '#' : $shop_list->url($shop_list->currentPage()-1) }}">‹</a>
            </div>

            <div class="col-md-10 col-xs-10 col-lprp-0">
            
                @foreach($shop_list as $list)
                    <div class="col-md-4 list-item">
                        <div class="list-title">
                            <a href="javascript:void(0);" class="edit" data-shop-list-name="{{$list->list_name}}" data-shop-list-id="{{$list->shop_list_id}}"><i class="fa fa-pencil"></i></a>

                            <h1>
                              <a href="{{route('shop-list.show', $list->shop_list_id)}}" class="text-black title"><strong>{{$list->list_name}}</strong></a>
                            
                              <span class="shop-list-favourite" style="font-size: 30px;" data-shop-list-id="{{$list->shop_list_id}}" data-toggle="tooltip" data-placement="top" data-html="true" title="If you want to pin a favorite shop list then show this shop list in your dashboard."><i class="il_{{$list->shop_list_id}} fa <?=($list->is_favourite == 1) ? 'fa-heart' : 'fa-heart-o';?> "></i></span>
                             </h1> 

                            <a href="javascript:void(0);" onclick="delete_list('{{$list->shop_list_id}}');" class="delete" style=""><i class="fa fa-close"></i></a>
                        </div>
                        <div class="list-contains">
                            <div class="list-sub-box">
                                <label>Countries:</label>
                                <div class="">
                                    @if(!empty($list->country_code))
                                      @php
                                      $country_codes = explode(", ", $list->country_code);
                                      $country_names = explode(", ", $list->country_name);
                                      @endphp
                                        @foreach($country_codes as $key => $country_code)
                                                <img class="country-flag" src="{{asset('images/flags/'.Str::lower($country_code).'.svg')}}" title="{{$country_names[$key]}}" data-toggle="tooltip" data-placement="top" data-html="true"/>
                                            @if($key == 3)
                                             @break
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="list-sub-box">
                                <label>Promo This week:</label>
                                <div class="">
                                   <h1><strong>{{($list->promo_this_week)}}</strong></h1>
                                </div>
                            </div>
                            <div class="list-sub-box">
                                <label>Total Shops:</label>
                                <div class="">
                                   <h1><strong>{{($list->tot_shop)}}</strong></h1>
                                </div>
                            </div>
                             <div class="list-sub-box">
                                <label>Average Influencers:</label>
                                <div class="">
                                   <h1><strong>{{floor($list->avg_influencer)}}<span>/shop</span></strong></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
              
            </div>
            <div class="col-md-1 col-xs-1 col-lprp-0">
                <a class="list-next-arrow {{ (($shop_list->currentPage() == $shop_list->lastPage()) ? 'disable' : '') }}" href="{{ ($shop_list->currentPage() == $shop_list->lastPage()) ? '#' : $shop_list->url($shop_list->currentPage()+1) }}" >›</a>
            </div>
            @else
              <div class="col-md-12 text-center">
                <div>No shop list found.</div>
              </div>
            @endif
        </div>
    </div>
</div>

<div id="create-new-list-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      {!! Form::open(array('route' => 'shop-list.store','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'create-shop-list-form')) !!}      
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Create List</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <label class="control-label mb-10 col-sm-3" for="last_name">List Name:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="list_name" name="list_name" maxlength="10" required="">
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-blue">Submit</button>
          </div>
      {!! Form::close() !!} 
    </div>
  </div>
</div>

<div id="edit-list-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      {!! Form::open(array('route' => 'user.shop_list.update','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'edit-shop-list-form')) !!} 
          <input type="hidden" name="shop_list_id" id="shop_list_id">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit List</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <label class="control-label mb-10 col-sm-3" for="last_name">List Name:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="edit_list_name" name="list_name" maxlength="10" required="">
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-blue">Submit</button>
          </div>
      {!! Form::close() !!} 
    </div>
  </div>
</div>   
@endsection

@section('pagescript')
    <!-- Sweet-Alert  -->
    <script src="{{ asset('js/sweetalert.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap-select.js')}}"></script>
    <script src="{{ asset('js/jquery-ui.js')}}"></script>

    <script type="text/javascript">
      function delete_list(shop_list){

            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this data!",   
                type: "",   
                showCancelButton: true,   
                confirmButtonColor: "#508FF4",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                /*iconColor: '#000',*/
            }, function(){   
                swal("Deleted!", "Your data has been deleted.", "success"); 
                window.location.href = '{{route('user.shop_list.delete')}}?shop_list='+shop_list;
                return true;
            });

            return false;
        }

    $(function() {
        $(document).on("click", ".shop-list-favourite", function(){
                
              var shop_list_id = $(this).data("shop-list-id");
              $(".shop-list-favourite i").removeClass('fa-heart');
              $(".shop-list-favourite i").addClass('fa-heart-o');
              $(".shop-list-favourite i.il_"+shop_list_id).toggleClass('fa-heart fa-heart-o');

              $.ajax({
                  url : "{{route('user.favourite_shop_list')}}",
                  data : { shop_list_id: shop_list_id},
                  dataType: 'json',
              }).done(function (data) { 
                  //$('.shops_tr').html(data.body);
              });
        });

        $(document).on("click", ".list-title .edit", function(){
            var shop_list_name = $(this).data('shop-list-name');
            $("#edit_list_name").val(shop_list_name);
            var shop_list_id = $(this).data('shop-list-id');
            $("#shop_list_id").val(shop_list_id);
            $("#edit-list-modal").modal("show");
        });
    });
    </script>
@stop
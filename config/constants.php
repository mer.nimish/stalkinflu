<?php
return [
    'app' => [
        'main-title' => 'STALK $ INFLU',
    ],
    'stripe' => [
        'publishable-key' => 'pk_test_51H26fzLp4bo48vYdFazYP18gMmHKvqd29h9uBjYCBs1PUfAAUAFGHdkVwxIkVIxuiE12Gq8Us62f4XWwgxHHxEtx00p9isTIOd',
        'secret-key' => 'sk_test_51H26fzLp4bo48vYdkvClFIlStfQciOuSKL07d7lHaeLf39LTG1RKX1sszo8hMQhGkJRWIKS9jLVLQMhZEjxeKUE400cttbm07b',
        /* 'publishable-key' => 'pk_live_51H26fzLp4bo48vYdHcNwWV9euEF0Jap6jD4Mdda2YUCvz8c0eHH4pYK4bwyXVdqBe5xSTw8LmVVNPzieIJT4PWsV00oMYGhYTn',
        'secret-key' => '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',*/
    ],
    'sendinblue' => [
        'api-key' => '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',
    ],
    'getrewardful' => [
        'secret-key' => '00aac455e688751a0e13516ba56e5d5a',
    ],
    'site-shot' => [
        'access-key' => 'IAAIEYKBJAXYPV6IQBUHHOATCG',
    ],
    'instagram' => [
        'email' => 'compteinsta361@gmail.com',
        'password' => 'bossse12',
    ],
    'uploaded_image_thumb_url' =>  'uploads/thumb/',
    'uploaded_image_url' =>  'uploads/',
    'profile_image_thumb_url' =>  'profile_pic/thumb/', 
    'profile_image_url' =>  'profile_pic/',
    'site_screenshot_url' =>  env('APP_URL', false).'/site_screenshot/',
    'meta_tags' => [
        'title' => 'StalkInflu',
        'description' => '',
        'keywords' => '',
        'author' => '',
        'user' => [
            'login' => [
                'title' => 'Login | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
            'signup' => [
                'title' => 'Signup | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
            'dashboard' => [
                'title' => 'Dashboard | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
            'profile' => [
                'title' => 'Profile | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
        ],
        'trend' => [
            'index' => [
                'title' => 'Trend | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
        ],
        'shop' => [
            'index' => [
                'title' => 'Shop | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
            'show' => [
                'title' => 'Shop Details | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
        ],
        'influencer' => [
            'index' => [
                'title' => 'Influencer | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
            'create' => [
                'title' => 'Create Influencer | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
            'show' => [
                'title' => 'Influencer Details | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
        ],
        'live-now' => [
            'index' => [
                'title' => 'Live-now | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
        ],
        'influencer-list' => [
            'index' => [
                'title' => 'List of Influencers | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
            'show' => [
                'title' => 'List of Influencer Details | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
        ],
        'shop-list' => [
            'index' => [
                'title' => 'List of Shops | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
            'show' => [
                'title' => 'List of Shop Details | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
        ],
        'media-list' => [
            'index' => [
                'title' => 'List of Media | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
            'show' => [
                'title' => 'List of Media Details | StalkInflu',
                'description' => '',
                'keywords' => '',
                'author' => ''
            ],
        ],
    ]
];
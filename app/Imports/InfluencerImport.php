<?php

namespace App\Imports;

use App\Models\Influencer;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use DB;

class InfluencerImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $country = DB::table('countries')->select('country_id')->where('country_name','like', trim($row['country']))->first();

        $influencer_cnt = Influencer::where('instagram_id','like', $row['instagram_id'])->count();
        if(!empty($country) && $influencer_cnt == 0){
            return new Influencer([
                'instagram_id'     => $row['instagram_id'],
                'country_id'    => $country->country_id,
                'is_approved'    => 1,
            ]);
        }
    }
}

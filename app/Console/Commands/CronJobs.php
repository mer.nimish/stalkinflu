<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Config;
use Mail;

class CronJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron_jobs:send_messages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email of new messages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $current_time = date("Y-m-d H:i:s");
        $past_date = date("Y-m-d H:i:s", strtotime('-30 minutes', strtotime($current_time)));
        // -4 hours
        $messages = DB::table('message')->select('message.*', DB::raw('IF(message.is_from_worker = 1, (SELECT email from business where business_id = message.to_id), (SELECT email from work where work_id = message.to_id)) as email'), DB::raw('IF(message.is_from_worker = 1, (SELECT concat(first_name," ",last_name) from work where work_id = message.from_id), (SELECT name from business where business_id = message.from_id)) as name'), DB::raw('IF(message.is_from_worker = 1, (SELECT profile_pic from work where work_id = message.from_id), "") as profile_pic'))
        ->where('message.is_unread', 1)
        ->where('message.is_mail_sent', 0)
        /*->whereRaw('(message.created_at) < (NOW() - INTERVAL 4 HOUR)')*/
        ->whereRaw('(message.created_at) < ("'.$past_date.'")')
        ->get();
        
        foreach ($messages as $key => $message) {

            $profile_pic = empty($message->profile_pic) ? asset('images/default-profile-pic.png') : asset(Config::get('constants.uploaded_image_thumb_url').explode('.', $message->profile_pic)[0].'.jpeg');

            // Send mail to user
            $route = ($message->is_from_worker == 1) ? 'business.messages' : 'worker.messages';
            $data = array('email' => $message->email, 'name' => $message->name, 'message_text' => $message->message, 'is_from_worker' => $message->is_from_worker, 'profile_pic' => $profile_pic, 'route' => $route);
            
            $result = Mail::send('notification_mail', $data, function ($mail) use ($data) {
             //connect@workholler.com
                $mail->from('no_reply@workholler.com', 'Workholler');
                $mail->to($data['email']); // $data['email']
                $mail->subject('You have a new message from '.$data['name']);
                $mail->setBody('', 'text/html');
                   /* if($data['is_from_worker'] == 1){
                        $mail->setBody('<h4>Hello,</h4>
                            <table style="border: 1px solid gray;">
                            <tr>
                                <td width="30%">
                                    <img src="'.$data['profile_pic'].'" width="75" />
                                </td>
                                <td width="60%">
                                <h4>'.$data['name'].'</h4>
                                    <p>'.$data['message'].'</p>
                                </td>
                            </tr>
                            </table>
                            <br />
                            <a href="'.route('business.messages').'">See Message</a>
                            <br />
                            <h4>Thank you.</h4>', 'text/html');
                     
                    } else {
                        $mail->setBody('<h4>Hello,</h4>
                            <table style="border: 1px solid gray;">
                            <tr>
                                <td width="30%">
                                    <img src="'.$data['profile_pic'].'" width="75" />
                                </td>
                                <td width="60%">
                                <h4>'.$data['name'].'</h4>
                                    <p>'.$data['message'].'</p>
                                </td>
                            </tr>
                            </table>
                            <br />
                            <a href="'.route('worker.messages').'">See Message</a>
                            <br />
                            <h4>Thank you.</h4>', 'text/html');
                    }*/
                }); 
                

                DB::table('message')->where('message_id', $message->message_id)
                ->update([
                    'is_mail_sent' => 1,
                ]);
        }



        /* Send mail to invited candidate */
         $invited_workers = DB::table('worker_invite')->select('worker_invite.worker_invite_id', 'business.first_name', 'business.last_name','sub_category.sub_category_name','work.email', 'worker_invite.job_id')
            ->join('business', 'business.business_id', '=', 'worker_invite.business_id')
            ->join('job', 'job.job_id', '=', 'worker_invite.job_id')
             ->leftJoin('sub_category', 'sub_category.sub_category_id','=','job.sub_category_id')
            ->join('work', 'work.work_id', '=', 'worker_invite.work_id')
            ->where('is_mail_sent', 0)
            ->get();
        
        foreach ($invited_workers as $key => $invited_worker) {
            // Send mail to user
            $data = array('email' => $invited_worker->email, 'name' => $invited_worker->first_name.' '.$invited_worker->last_name, 'position' => $invited_worker->sub_category_name, 'job_id' => $invited_worker->job_id);
            
            $result = Mail::send('work.invite_mail', $data, function ($mail) use ($data) {
             //connect@workholler.com
                $mail->from('no_reply@workholler.com', 'Workholler');
                $mail->to($data['email']); // $data['email']
                $mail->subject($data['name'].' has invited you for a job');
                $mail->setBody('', 'text/html');
            }); 
                

            DB::table('worker_invite')->where('worker_invite_id', $invited_worker->worker_invite_id)
                ->update([
                    'is_mail_sent' => 1,
            ]);
        }

    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Requests;
use DB;
use App\Models\Shop;
use App\Models\Influencer;
use Config;

use Instagram\Api;
use Instagram\Exception\InstagramException;

use Psr\Cache\CacheException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

use Image;
use FFMpeg;
use File;

class InstaProfileStories extends Command
{
     /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insta_profile_stories:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Instagram Profile Stories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
		$allStoryData = array();
		try { 

			$cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');

		    $api = new Api($cachePool);
			$api->login(env('INSTAGRAM_LOGIN', false), env('INSTAGRAM_PASSWORD', false));


			$influencerList = influencer::find(3)->get();
		    $i = 1;

		    $shop_media_story = array();
		    $shop_videos = DB::table('shop_media')->get();
		    foreach ($shop_videos as $key => $shop_video) {
		    	$shop_media_story[] = $shop_video->story_id;
		    }

		    foreach ($influencerList as $influencer) {
		    
		        $profile = $api->getProfile($influencer->instagram_id);
		        
		        sleep(1);
		        $feedStories = $api->getStories($profile->getId());

		        //echo "<pre>";
		        //print_r($feedStories);exit;
		       
		        if (count($feedStories->getStories())) {
		            foreach ($feedStories->getStories() as $key => $story) { 

		                    if(!empty($story->getCtaUrl())){ 

		        				$story_arr[] = $story;

		                        $allStoryData[$influencer->instagram_id][$key]['userName'] = $profile->getUserName();
		                        $allStoryData[$influencer->instagram_id][$key]['userID'] = $profile->getId();
		                        //$allStoryData[$influencer][$key]['fullName'] = $profile->getFullName();
		                        $allStoryData[$influencer->instagram_id][$key]['ctaUrl'] = $story->getCtaUrl();
		                        $allStoryData[$influencer->instagram_id][$key]['displayUrl'] = $story->getDisplayUrl();
		                        //$allStoryData[$influencer][$key]['takenAtDate'] = $story->getTakenAtDate()->format('Y-m-d h:i:s');



		                        /* For insert data in shop table */
		                        $cta_url = $story->getCtaUrl();

		                        if (strpos($cta_url, 'instagram.com') === false && strpos($cta_url, 'youtube.com') === false) {
    
			                        $story_id = $story->getId();

			                        if(!in_array($story_id, $shop_media_story)){

				                        $shop = Shop::where(['website' => $cta_url, 'influencer_id' => $influencer->influencer_id])
				                        ->where(DB::raw('DATE(created_at)'), date('Y-m-d'))->first();

				                        if(empty($shop->shop_id)){

				                        	$website_screenshot = $this->store_site_screenshot($cta_url);

					                        $shop = new Shop();	
						                    $shop->website = $cta_url;
						                    $shop->website_screenshot = $website_screenshot;
						                    $shop->influencer_id = $influencer->influencer_id;
						                    $shop->taken_at = $story->getTakenAtDate()->format('Y-m-d h:i:s');
						                    $shop->created_at = date('Y-m-d h:i:s');
							        		$shop->save();
							        	}

							        	if(!empty($story->getVideoResources())){

							        		$video_url = $story->getVideoResources()[0]->src;
							        		if (!is_dir(storage_path('app/public/shop_media/'.$shop->shop_id.'/'))) {
											  mkdir(storage_path('app/public/shop_media/'.$shop->shop_id.'/'), 0777, true);
											}
							        		$video = storage_path('app/public/shop_media/'.$shop->shop_id.'/').$story_id.'.mp4';
											//file_put_contents($video, file_get_contents($video_url));

						                    $last_shop = DB::table('shop_media')->insert([
						                    	'shop_id' => $shop->shop_id,
						                    	'story_id' => $story_id,
						                    	'video' => $story_id.'.mp4',
						                    ]);
							        	}
							        }
						        }
		                    }
		            }

		            /* Update story count in influencer table */

			        Influencer::where('instagram_id', $influencer->instagram_id)->update([
						'story_cnt' => count($feedStories->getStories()),
					]);

		        } else {
		            //$allStoryData[] => 'No stories';
		        }
		        
		    }
			
		    echo "<pre>";print_r($allStoryData);
		    
		    exit;

		} catch (InstagramException $e) {
		    print_r($e->getMessage());
		} catch (CacheException $e) {
		    print_r($e->getMessage());
		}
    }

    public function store_site_screenshot($url){

    	$query_string = http_build_query(array(
	        'url' => $url,
	        'userkey' => Config::get('constants.site-shot.access-key'),
	        'width' => 1280,
	        'full_size' => 1,
	        'delay_time' => 5000,
	        'timeout' => 90000,
	        /*'response_type' => 'json',*/
	    ));
	    $image = file_get_contents("https://api.site-shot.com/?$query_string");
	   
	    $img_name = md5($url).'.png';
    	$file_name = public_path('/site_screenshot/') . $img_name;

	    file_put_contents($file_name, $image);

	    return $img_name;
    }
}

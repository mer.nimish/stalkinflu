<?php
 
namespace App\Libraries;
use DateTime;

class Helpers{

	public function __construct()
    {
        //do some stuff
    }
    
    public static function gen_code($prefix, $id) 
    {
        return $prefix.'-'.sprintf("%05d", $id); 
    }

    /**
	 * @param $n
	 * @return string
	 * Use to convert large positive numbers in to short form like 1k, 1.5k, 1m, 1b+ etc
	 */
	public static function short_number_format( $input ) {
		$input = number_format($input);
	    $input_count = substr_count($input, ',');
	    $input = str_replace(',', '.', $input);
	    if($input_count != '0'){
	        if($input_count == '1'){
	            return substr($input, 0, -4).'k';
	        } else if($input_count == '2'){
	            return substr($input, 0, -6).'M';
	        } else if($input_count == '3'){
	            return substr($input, 0,  -12).'b';
	        } else {
	            return;
	        }
	    } else {
	        return $input;
	    }
	}

	public static function time_elapsed_string($datetime, $full = false) {
	      $time_ago = '';
	      $date = new DateTime($datetime);
		  $diff = $date->diff(new Datetime('now'));

		  if (($t = $diff->format("%m")) > 0)
		    //$time_ago = $t . ' month'.($t > 1 ? 's' : '');
			$time_ago = $diff->days . ' day'.($diff->days > 1 ? 's' : '');
		  else if (($t = $diff->format("%d")) > 0)
		    $time_ago = $t . ' day'.($t > 1 ? 's' : '');
		  else if (($t = $diff->format("%H")) > 0)
		    $time_ago = $t . ' hour'.($t > 1 ? 's' : '');
		  else
		  	$time_ago = '0 hour';
		    //$time_ago = 'minute'.($t > 1 ? 's' : '');

		  return $time_ago . ' ago';
	}
}
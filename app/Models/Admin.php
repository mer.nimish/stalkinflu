<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
	use Notifiable;
    use SoftDeletes;

    protected $guard = 'Admin';

    protected $table = 'admin';

    protected $primaryKey = 'admin_id';
	
    protected $fillable = [
        'name', 'email', 'password', 'is_admin', 'emp_access'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    public function getAuthEmail()
    {
        return $this->email;
    }

    public function getAuthPassword()
    {
        return $this->password;
    }
}

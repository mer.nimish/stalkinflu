<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Influencer extends Model
{
    use SoftDeletes;

    protected $table = 'influencer';

    protected $primaryKey = 'influencer_id';
	
    protected $fillable = [
        'admin_id', 'profile_pic','instagram_id', 'country_id', 'followers', 'engagement_rate', 'uploads', 'avg_likes', 'avg_comments', 'videos', 'photos', 'last_post_date', 'likes_consistency', 'comments_consistency', 'frequency', 'is_verified', 'is_approved'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;

    protected $table = 'notification';

    protected $primaryKey = 'notification_id';
	
    protected $fillable = [
        'notification', 'date', 'created_at', 'updated_at'
    ];
}

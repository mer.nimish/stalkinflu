<?php

namespace App\Imports;

use App\Models\Influencer;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class InfluencerImport extends ToModel
{
     /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
        return new Influencer([
           'instagram_id'     => $row[0],
           'country_id'    => $row[1],
        ]);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;
    use SoftDeletes;

    protected $guard = 'User';
    
    protected $table = 'user';

    protected $primaryKey = 'user_id';
	
    protected $fillable = [
        'profile_pic', 'first_name', 'last_name', 'instagram_id','country_id', 'email', 'password', 'admin_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    public function getAuthEmail()
    {
        return $this->email;
    }

    public function getAuthPassword()
    {
        return $this->password;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;

    protected $table = 'shop';

    protected $primaryKey = 'shop_id';
	
    protected $fillable = [
        'website', 'language_id', 'influencer_id','website_screenshot', 'admin_id', 'taken_at', 'activity'
    ];
}

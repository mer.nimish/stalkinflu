<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        /*if (Auth::guard($guard)->check()) {
            return redirect('/dashboard');
        }*/

        if ($guard == "User" && Auth::guard($guard)->check()) {
            return redirect('/user/dashboard');
        } else if ($guard == "Admin" && Auth::guard($guard)->check()) {
            return redirect('/admin/dashboard');
        } else {
            return redirect('/admin/login');
        }

        return $next($request);
    }
}

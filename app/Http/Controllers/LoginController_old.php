<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Admin;
use Auth;
use Hash;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use Mail;
use DB;
use Config;

class LoginController extends Controller
{

	/*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware(function ($request, $next) {

            if($request->type == 'Admin')
	           $this->user = Auth::guard('Admin')->user();
            else if($request->type == 'User')
               $this->user = Auth::guard('User')->user();
	
	        return $next($request);
	    });
    }

    public function index(Request $request)
    { 
        if (Auth::guard('Admin')->check()) {
            return redirect()->route('admin.dashboard');
        } else if (Auth::guard('User')->check()) {
            return redirect()->route('user.dashboard');
        }

        if(request()->is('admin*'))
          return view('admin.login');
        else
          return view('user.login');
    }

    public function authCheck(Request $request){

        if($request->type == 'User'){
            if (Auth::guard($request->type)->attempt(['email' => $request->email, 'password' => $request->password])) {

              $user_id = Auth::guard('User')->user()->user_id;

              // Check use subscription
              $subscription = DB::table('subscription')->where('user_id', $user_id)->first();
              $status = $period_end_date = '';
              if(!empty($subscription)){
                $curl = curl_init();
                curl_setopt_array($curl, [
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => "https://api.stripe.com/v1/subscriptions/".$subscription->stripe_subscription_id,
                    CURLOPT_POST => 0,
                    CURLOPT_HTTPHEADER => [
                        "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
                    ],
                ]);
                $resp = curl_exec($curl);
                $resp = json_decode($resp);
                $status = $resp->status;
                $period_end_date = date("Y-m-d", $resp->current_period_end);
                curl_close($curl);

                if($status != 'active' || $period_end_date <= date("Y-m-d"))
                  User::where('user_id', $user_id)->update(['status'=>'cancel']);
              }

              // $status = Auth::guard('User')->user()->status;
              if(1)
              {
                  Session::put('login_user_id', Auth::guard('User')->user()->user_id);
                  Session::put('login_user_type', 'User');
                  return redirect()->intended('/user/dashboard');
              } 
              else 
              {
                  Session::flush();
                  Auth::logout();
                  Auth::guard('User')->logout();
                  return redirect()->route('user.login')->with('error','Your subscription plan has been expired.');
              }
            } else {
                return redirect()->route('user.login')->with('error','Invalid email or password.');
            }
       } else if($request->type == 'Admin') {

            if (Auth::guard($request->type)->attempt(['email' => $request->email, 'password' => $request->password])) {

                Session::put('login_admin_id', Auth::guard('Admin')->user()->admin_id);
                Session::put('login_user_type', 'Admin');
                
                return redirect()->intended('/admin/dashboard');
            } else { 
                return redirect()->route('admin.login')->with('error','Invalid email or password.');
            }
       }
    }

    public function admin_forgot_password(Request $request){

        if(isset($_REQUEST) && !empty($_REQUEST)){
           $admin = Admin::where('email', $request->email)->first(); 

            $data = array('email' => $request->email, 'admin_id' => $admin->admin_id);
            $result = Mail::send([], $data, function ($message) use ($data) {  
              $message->from('no_reply@beforesubmit.com', '');
              $message->to($data['email'])
                ->subject('Forgot Password')
                ->setBody('<p>Hi,</p>
                  <p><a href="'.route('admin.new_password', md5($data['admin_id'])).'">Click on to reset password.</a></p>
                  <p>Thank You.</p>
                  ', 'text/html');
            });

           return redirect()->route('admin.forgot_password')->with('success','Please check your email. We have sent reset-password link in your mail.');

        } else {
            return view('admin.forgot_password');
        }
    }

    public function admin_new_password($admin_id){

        return view('admin.new_password')->with('admin_id', $admin_id);
    }

    public function admin_new_password_update($admin_id, Request $request){

        $admin = Admin::where(DB::raw('md5(admin_id)'), $admin_id)->first();
        $admin->password = Hash::make($request->password);
        $admin->save();

        return redirect()->route('admin.login')->with('success','Your new password updated successfully.');
    }

    public function is_email_register_admin(Request $Request){
        $admin_count = Admin::where('email', $Request->email)->count();
    
        if($admin_count > 0)
            echo 'true';
        else
            echo 'false';
    }

    public function is_email_register_user(Request $Request){
        $admin_count = User::where('email', $Request->email)->count();
    
        if($admin_count > 0)
            echo 'true';
        else
            echo 'false';
    }

    public function user_forgot_password(Request $request){
        
        if(isset($_REQUEST) && !empty($_REQUEST)){
           $user = User::where('email', $request->email)->first(); 
           //return redirect()->route('user.new_password',[$user->user_id]);

            $data = array('email' => $request->email, 'user_id' => $admin->user_id);
            $result = Mail::send([], $data, function ($message) use ($data) {  
              $message->from('no_reply@beforesubmit.com', '');
              $message->to($data['email'])
                ->subject('Forgot Password')
                ->setBody('<p>Hi,</p>
                  <p><a href="'.route('user.new_password', md5($data['user_id'])).'">Click on to reset password.</a></p>
                  <p>Thank You.</p>
                  ', 'text/html');
            });

           return redirect()->route('user.forgot_password')->with('success','Please check your email. We have sent reset-password link in your mail.');

        } else {
            return view('user.forgot_password');
        }
    }

    public function user_new_password($user_id){
        return view('user.new_password')->with('user_id', $user_id);
    }

    public function user_new_password_update($user_id, Request $request){

        $user = User::where(DB::raw('md5(user_id)'), $user_id)->first();
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->route('user.login')->with('success','Your new password updated successfully.');
    }

    public function logout(Request $request) {

      Session::flush();
      Auth::logout();

      Auth::guard('Admin')->logout();
      Auth::guard('User')->logout();

      if(request()->is('admin*'))
          return redirect('admin/login');
      else
          return redirect('user/login');
  	  
  	}
}

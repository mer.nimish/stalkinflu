<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Hash;
use DB; 
use Session;
use Auth;

class CategoryController extends Controller
{
    
    public function index()
    { 
        $categories = Category::select("category.*")
            ->latest()
            ->get();
        return view('admin.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 
        $validator = request()->validate([
            'category_name' => 'required|unique:category,category_name,NULL,category_id,deleted_at,NULL',
        ]);

        Category::create($request->all());
        
        return redirect()->route('admin-category.index')->with('success','Category created successfully.');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($category_id)
    {
        $category = Category::find($category_id);
        return view('admin.category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category_id)
    { 
        $category = Category::find($category_id);
        request()->validate([
            'category_name' => 'required|unique:category,category_name,'.$category['category_id'].',category_id,deleted_at,NULL',
        ]);  

        $category->update($request->all());

        return redirect()->route('admin-category.index')->with('success','Category updated successfully.');
    }
    
    public function destroy($id)
    { 
        Category::find($id)->delete();

        return redirect()->route('admin-category.index')->with('success','Category deleted successfully.');
    }
}

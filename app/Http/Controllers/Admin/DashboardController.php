<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Shop;
use App\Models\Influencer;
use App\Models\Experience;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Mail;
use Auth;
use Hash;
use File;

class DashboardController extends Controller
{
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $employees = Admin::select("admin.*")->where('admin_id','!=','1')->count();
        $influencers = Influencer::select("influencer.*")->count();
        $shops = Shop::select("shop.*")->count();

        return view('admin.dashboard', compact('employees', 'shops', 'influencers'));
        
    }

    public function change_password(){
        return view('admin.change_password');
    }

    public function update_password(Request $Request){

        $admin_id = Auth::guard('Admin')->user()->admin_id;
        $admin = Admin::where('admin_id', $admin_id)->first();
        if (!Hash::check($Request->old_password, $admin->password)) {
            return redirect()->route('admin.edit_profile')->with('error','Your old password dose not match.');
         } else {

            Admin::where('admin_id', $admin_id)->update(['password' => Hash::make($Request->new_password)]);
            return redirect()->route('admin.change_password')->with('success','Password changed successfully.');
         }
     }

    public function setting(){

        $setting = DB::table('setting')->first();

        return view('admin.setting', compact('setting'));
    }

     public function setting_update(Request $request){
       
        DB::table('setting')->where('setting_id', 1)->update(
            [
                'bug_or_an_idea_link' => $request->bug_or_an_idea_link,
                'commission_text' => $request->commission_text,
                'dashboard_header' => $request->dashboard_header,
                'dashboard_text' => $request->dashboard_text,
                'dashboard_total_members' => $request->dashboard_total_members
            ]);

        return redirect()->route('admin.setting')->with('success','Setting changed successfully.');
     }

     public function check_old_password(Request $Request){
        $admin_id = Auth::guard('Admin')->user()->admin_id;
        $admin = Admin::where('admin_id', $admin_id)->first();
        
        if (Hash::check($Request->old_password, $admin->password)) 
            return 'true';
        else 
            return 'false';
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $Request)
    { 
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //return view('admin.edit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $Request)
    {
        
    }
}

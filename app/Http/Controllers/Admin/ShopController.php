<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Shop;
use App\Models\Influencer;
use App\Models\Category;
use Hash;
use DB; 
use Session;
use Auth;
use Str;

class ShopController extends Controller
{
    
    public function index()
    { 
        $categories = Category::select("category.*")
            ->latest()
            ->get();
        /*$shops = Shop::select("shop.*","country_name", DB::raw('CONCAT(first_name, " ", last_name) AS name'),DB::raw('GROUP_CONCAT(DISTINCT category.category_name  SEPARATOR "<br />") as categories'), DB::raw('count(DISTINCT shop_category.category_id) as category_cnt'))
            ->leftJoin('influencer','shop.influencer_id', '=' ,'influencer.influencer_id')
            ->leftJoin('countries','countries.country_id', '=' ,'influencer.country_id')
            ->leftJoin('shop_category','shop_category.shop_id', '=' ,'shop.shop_id')
            ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id')
            ->groupBy('shop.shop_id')
            ->latest()
            ->get();*/
        return view('admin.shop.index', compact('shops','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $users = Influencer::get();
        $categories = Category::get();
        return view('admin.shop.create', compact('users','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 
        $validator = request()->validate([
            'website' => 'required', // |unique:shop,website,NULL,shop_id,deleted_at,NULL
            'influencer_id' => 'required',
            'category_id' => 'required',
        ]);

        if($request->hasFile('screenshot'))
        { 
            request()->validate([
                'screenshot' => 'image|mimes:jpeg,png,jpg,gif',
            ]);

            $file = $request->file('screenshot');

            $imageName = date("dmYHis").substr(uniqid('', true), -3).'.'.$file->getClientOriginalExtension();  

            $file->move(public_path('site_screenshot'), $imageName);
            $request['website_screenshot'] = $imageName;
        }

        $request['admin_id'] = Auth::guard('Admin')->user()->admin_id;
        $shop = Shop::create($request->all());
        foreach ($request->category_id as $key => $category_id) {
            DB::table('shop_category')->insert(['category_id' => $category_id, 'shop_id' => $shop->shop_id]);
        }
        
        return redirect()->route('admin-shop.index')->with('success','Shop created successfully.');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($shop_id)
    {
        $shop = Shop::find($shop_id);
        $users = Influencer::get();
        $shop_categories = DB::table('shop_category')->where(['shop_id' => $shop_id])->get();
        $shop_category_ids = array();
        foreach ($shop_categories as $key => $shop_category) {
            $shop_category_ids[] = $shop_category->category_id;
        }
        $categories = Category::get();
        return view('admin.shop.edit',compact('shop', 'users', 'categories', 'shop_category_ids'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $shop_id)
    { 
        $shop = Shop::find($shop_id);
        request()->validate([
            'website' => 'required', //|unique:shop,website,'.$shop['shop_id'].',shop_id,deleted_at,NULL
            'influencer_id' => 'required',
            'category_id' => 'required',
        ]);

        if($request->hasFile('screenshot'))
        { 
            request()->validate([
                'screenshot' => 'image|mimes:jpeg,png,jpg,gif',
            ]);

            $file = $request->file('screenshot');

            $imageName = date("dmYHis").substr(uniqid('', true), -3).'.'.$file->getClientOriginalExtension();  

            $file->move(public_path('site_screenshot'), $imageName);
            $request['website_screenshot'] = $imageName;
        }  
        
        $shop->update($request->all());

        DB::table('shop_category')->where(['shop_id' => $shop->shop_id])->delete();
        foreach ($request->category_id as $key => $category_id) {
            DB::table('shop_category')->insert(['category_id' => $category_id, 'shop_id' => $shop->shop_id]);
        }

        return redirect()->route('admin-shop.edit', $shop->shop_id)->with('success','Shop updated successfully.');
    }
    
    public function destroy($id)
    { 
        Shop::find($id)->delete();

        return redirect()->route('admin-shop.index')->with('success','Shop deleted successfully.');
    }

    public function assign_shop_into_category(Request $request){
        if(!empty($request->shop_ids) && !empty($request->category_ids)){
            foreach ($request->category_ids as $key => $category_id) {
                foreach ($request->shop_ids as $key => $shop_id) {
                     $result = DB::table('shop_category')->where([
                        'category_id' => $category_id,
                        'shop_id' => $shop_id
                    ])->first();
                  
                    if(empty($result->shop_category_id)){
                        DB::table('shop_category')->insert([
                            'category_id' => $category_id,
                            'shop_id' => $shop_id
                        ]);
                    }
                }
            }
        }
    }

    public function shop_list(Request $request)
    {
        $columns = array("shop_id","website", "country_name","instagram_id","created_at","categories","shop_id2");
        $columns2 = array("shop.shop_id","website", "country_name","instagram_id","shop.created_at", DB::raw('GROUP_CONCAT(DISTINCT category.category_name  SEPARATOR "<br />") as categories',"shop.shop_id as shop_id2"));

        $totalData = Shop::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $dir   = $request->input('order.0.dir');
            
        $query = Shop::select($columns2);
        $vouchers = $query->leftJoin('influencer','shop.influencer_id', '=' ,'influencer.influencer_id')
            ->leftJoin('countries','countries.country_id', '=' ,'influencer.country_id')
            ->leftJoin('shop_category','shop_category.shop_id', '=' ,'shop.shop_id')
            ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id');

        if (!empty($request->input('search.value'))) {            
            $search = $request->input('search.value');
            $vouchers = $query->where(function ($query2) use ($columns2,$search) {
                foreach ($columns2 as $key => $value) {
                    if ($key == 0) {
                       $query2->where($value,'LIKE',"%{$search}%");
                    } else {
                        if ($key != 5) 
                            $query2->orWhere($value,'LIKE',"%{$search}%");
                    }
                }
                return $query2;
            });
        }

        $vouchers2 = $query->groupBy('shop.shop_id')->get();
        $totalFiltered = $vouchers2->count();

        $vouchers = $query->offset($start);
        $vouchers = $query->limit($limit);
        if(isset($columns2[$request->input('order.0.column')])){
            $order = $columns[$request->input('order.0.column')];
            $vouchers = $query->orderBy($order,$dir);
        }
        $vouchers = $query->latest('shop.created_at', 'desc');
        $vouchers = $query->groupBy('shop.shop_id')->get();

        $data = array();
        if(!empty($vouchers)) {
            foreach ($vouchers as $key => $voucher) {
                $edit_url = route('admin-shop.edit', $voucher->shop_id);
                $delete_url = route('admin-shop.destroy', $voucher->shop_id);
                foreach ($columns as $key2 => $column) {

                    if($column == 'created_at') {
                         $nestedData[$column] = date('M j Y H:i A',strtotime($voucher->$column));
                    } else if($column == 'shop_id') {
                         $nestedData[$column] = '<input type="checkbox" name="shop_ids[]" class="checked-shop-ids" value="'.$voucher->$column.'">';
                    } else if($column == 'website') {
                         $nestedData[$column] = Str::limit($voucher->$column,25);
                    } else if($column == 'categories' && !empty($voucher->categories)) {

                           if(isset($voucher->categories) && !empty($voucher->categories)){
                                $categories2 = explode("<br />", $voucher->categories);
                                         
                                $nestedData[$column] =  ' <span class="category-text">'.$categories2[0].'</span>';
                                    if(count($categories2) > 1)
                                    {   
                                        $nestedData[$column] .= '<span class="category-cnt" data-toggle="tooltip" data-placement="top" data-html="true" title="'.$voucher->categories.'">+'.count($categories2).'</span>';
                                    }
                            }
                    } else if($column == 'shop_id2' && $key2 == 6) {
                        $nestedData[$column] =  "<a href='{$edit_url}'  class='btn btn-primary btn-xs action' target='_blank'><i class='fa fa-pencil'></i></a>
                        <form action='{$delete_url}' method='POST' id='laravel_datatable-".$voucher->$column."' style='display: -webkit-inline-box;''>
                                        <input name='_method' type='hidden' value='DELETE'>
                                        ".csrf_field()."
                                        <button type='button' title='DELETE' class='btn btn-danger btn-xs action' onclick='delete_shop('".$voucher->$column."');'>
                                            <i class='fa fa-trash-o text-danger'></i>
                                        </button>
                                    </form>";
                           
                    } else {
                        $nestedData[$column] = $voucher->$column;
                    }
                }
                $data[] = $nestedData;
            }
        }

        $json_data = array(
                        "draw"            => intval($request->input('draw')),  
                        "recordsTotal"    => intval($totalData),  
                        "recordsFiltered" => intval($totalFiltered), 
                        "data"            => $data   
                    );   
        echo json_encode($json_data);
        
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use Hash;
use DB; 
use Session;
use Auth;

class CountryController extends Controller
{
    
    public function index()
    { 
        $countries = Country::select("countries.*")
            ->latest()
            ->get();
        return view('admin.country.index', compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        return view('admin.country.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 
        $validator = request()->validate([
            'country_code' => 'required|unique:countries,country_code,NULL,country_id,deleted_at,NULL',
        ]);

        if($request->hasFile('flag_image'))
        { 
            request()->validate([
                'flag_image' => 'image|mimes:svg',
            ]);

            $file = $request->file('flag_image');

            $imageName = strtolower($request->country_code).'.svg';  

            $file->move(public_path('images/flags'), $imageName);
        }
        $request['country_code'] = strtoupper($request->country_code);
        Country::create($request->all());
        
        return redirect()->route('admin-country.index')->with('success','Country created successfully.');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($country_id)
    {
        $country = Country::find($country_id);
        return view('admin.country.edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $country_id)
    { 
        $country = Country::find($country_id);
        request()->validate([
            'country_code' => 'required|unique:countries,country_code,'.$country['country_id'].',country_id,deleted_at,NULL',
        ]);

        if($request->hasFile('flag_image'))
        { 
            request()->validate([
                'flag_image' => 'image|mimes:svg',
            ]);

            $file = $request->file('flag_image');

            $imageName = strtolower($request->country_code).'.svg';  

            $file->move(public_path('images/flags'), $imageName);
        }
        $request['country_code'] = strtoupper($request->country_code);

        $country->update($request->all());

        return redirect()->route('admin-country.index')->with('success','Country updated successfully.');
    }
    
    public function destroy($id)
    { 
        Country::find($id)->delete();

        return redirect()->route('admin-country.index')->with('success','Country deleted successfully.');
    }
}

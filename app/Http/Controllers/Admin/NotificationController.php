<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use Hash;
use DB; 
use Session;
use Auth;

class NotificationController extends Controller
{
    
    public function index()
    { 
        $notifications = Notification::select("notification.*")
            ->latest('date')
            ->get();
        return view('admin.notification.index', compact('notifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        return view('admin.notification.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 
        $request['date'] = date("Y-m-d", strtotime($request->date));
        Notification::create($request->all());

        DB::table('user')->update(['is_unread_notification' => 0]);
        
        return redirect()->route('admin-notification.index')->with('success','Notification created successfully.');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($notification_id)
    {
        $notification = Notification::find($notification_id);
        return view('admin.notification.edit',compact('notification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $notification_id)
    { 
        $notification = Notification::find($notification_id);
        $request['date'] = date("Y-m-d", strtotime($request->date));
        $notification->update($request->all());

        return redirect()->route('admin-notification.index')->with('success','Notification updated successfully.');
    }
    
    public function destroy($id)
    { 
        Notification::find($id)->delete();

        return redirect()->route('admin-notification.index')->with('success','Notification deleted successfully.');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use Hash;
use DB;
use Session;
use Auth;

class EmployeeController extends Controller
{
    
    public function index()
    { 
        $admin_id = Auth::guard('Admin')->user()->admin_id;
        if(Auth::guard('Admin')->user()->is_admin == 1)
            $employees = Admin::select("admin.*")->latest()->get();
        else
            $employees = Admin::select("admin.*")->where('admin_id', $admin_id)->latest()->get();

        return view('admin.employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        return view('admin.employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 
        $validator = request()->validate([
            'name' => 'required',
            'email' => 'required|unique:admin,email,NULL,admin_id,deleted_at,NULL',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
            'emp_access' => 'required'
        ]);

        $request['password'] = Hash::make($request['password']);
        Admin::create($request->all());
        
        return redirect()->route('employee.index')->with('success','Employee created successfully.');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $employee)
    {
        return view('admin.employee.edit',compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $employee)
    { 
        request()->validate([
            'name' => 'required',
            'email' => 'required|unique:admin,email,'.$employee['admin_id'].',admin_id,deleted_at,NULL',
            'password' => 'nullable',
            'confirm_password' => 'nullable|same:password',
            'emp_access' => 'required'
        ]);  

        if(empty($request['password']) || empty($request['confirm_password'])){
            unset($request['password']);
            unset($request['confirm_password']);
        } else {
            $request['password'] = Hash::make($request['password']);
        }
       
        $employee->update($request->all());

        return redirect()->route('employee.index')->with('success','Employee updated successfully.');
    }
    
    public function destroy($id)
    { 
        Admin::find($id)->delete();

        return redirect()->route('employee.index')->with('success','Employee deleted successfully.');
    }
}

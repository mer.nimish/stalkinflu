<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Influencer;
use App\Imports\InfluencerImport;
use Hash;
use DB;
use Session;
use Auth;
use Excel;

class InfluencerController extends Controller
{
    public function index()
    {  

        /*$influencers = Influencer::select("influencer.*","countries.country_name")
            ->leftJoin('countries','influencer.country_id', '=' ,'countries.country_id')
            ->latest()
            ->get();*/

        return view('admin.influencer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $countries = DB::table('countries')->get();
        return view('admin.influencer.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 
        $validator = request()->validate([
            'instagram_id' => 'required|unique:influencer,instagram_id,NULL,influencer_id,deleted_at,NULL',
            'country_id' => 'required',
        ]);
        
        $request['is_approved'] = 1;
        $request['admin_id'] = Auth::guard('Admin')->user()->admin_id;
        Influencer::create($request->all());
        
        return redirect()->route('admin-influencer.index')->with('success','Influencer created successfully.');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($influencer_id)
    {
        $influencer = Influencer::find($influencer_id);
        $countries = DB::table('countries')->get();
        return view('admin.influencer.edit',compact('influencer', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $influencer_id)
    { 
        $influencer = Influencer::find($influencer_id);
        request()->validate([
            'instagram_id' => 'required|unique:influencer,instagram_id,'.$influencer_id.',influencer_id,deleted_at,NULL',
            'country_id' => 'required',
        ]);  

        $influencer->update($request->all());

        return redirect()->route('admin-influencer.index')->with('success','Influencer updated successfully.');
    }
    
    public function destroy($id)
    { 
        Influencer::find($id)->delete();

        return redirect()->route('admin-influencer.index')->with('success','Influencer deleted successfully.');
    }

    public function approve_reject($influencer_id, $is_approved){

        $is_approved = ($is_approved == 1) ? 0 : 1;

        Influencer::where('influencer_id', $influencer_id)->update(['is_approved'=>$is_approved]);

        return redirect()->route('admin-influencer.index')->with('success','Influencer updated successfully.');
    }

    public function import_influencer(){
        return view('admin.influencer.import');
    }

    public function import_influencer_store(Request $request){

        request()->validate([
            'influencer_file' => 'required|mimes:csv,xlsx,xls',
        ]); 

        if($request->hasFile('influencer_file')){
            Excel::import(new InfluencerImport,request()->file('influencer_file'));
        }

        return redirect()->route('admin.import_influencer')->with('success','Influencers import successfully.');
    }

    public function influencer_list(Request $request){

        $columns = array("instagram_id", "country_name","created_at","categories","influencer_id","is_approved");
        $columns2 = array("influencer.instagram_id", "country_name","influencer.created_at","influencer.influencer_id","is_approved");

        $totalData = Influencer::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $dir   = $request->input('order.0.dir');
            
        $query = Influencer::select($columns2);
        $vouchers = $query->leftJoin('countries','countries.country_id', '=' ,'influencer.country_id');

        if (!empty($request->input('search.value'))) {            
            $search = $request->input('search.value');
            $vouchers = $query->where(function ($query2) use ($columns2,$search) {
                foreach ($columns2 as $key => $value) {
                    if ($key == 0) {
                       $query2->where($value,'LIKE',"%{$search}%");
                    } else {
                        $query2->orWhere($value,'LIKE',"%{$search}%");
                    }
                }
                return $query2;
            });
        }

        $vouchers2 = $query->get();
        $totalFiltered = $vouchers2->count();

        $vouchers = $query->offset($start);
        $vouchers = $query->limit($limit);
        if(isset($columns2[$request->input('order.0.column')])){
            $order = $columns[$request->input('order.0.column')];
            $vouchers = $query->orderBy($order,$dir);
        }
        $vouchers = $query->latest('influencer.created_at', 'desc');
        $vouchers = $query->get();

        $data = array();
        if(!empty($vouchers)) {
            foreach ($vouchers as $key => $voucher) {
                $edit_url = route('admin-influencer.edit', $voucher->influencer_id);
                $delete_url = route('admin-influencer.destroy', $voucher->influencer_id);
                $approve_reject_url = route('admin.approve_reject', [$voucher->influencer_id, $voucher->is_approved]);
                foreach ($columns as $key2 => $column) {

                    if($column == 'created_at') {
                         $nestedData[$column] = date('M j Y H:i A',strtotime($voucher->$column));
                    } else if($column == 'influencer_id') {
                        $nestedData[$column] =  '<a href="'.$edit_url.'"  class="btn btn-primary btn-xs action"><i class="fa fa-pencil"></i></a>
                            <form action="'.$delete_url.'" method="POST" id="laravel_datatable-'.$voucher->influencer_id.'" style="display: -webkit-inline-box;">
                                <input name="_method" type="hidden" value="DELETE">
                                '.csrf_field().'
                                <button type="button" title="DELETE" class="btn btn-danger btn-xs action" onclick="delete_influencer('.$voucher->influencer_id.');">
                                    <i class="fa fa-trash-o text-danger"></i>
                                </button>
                            </form>';
                            if($voucher->is_approved == 1)
                            {
                                $nestedData[$column] .= '&nbsp;<a href="'.$approve_reject_url.'"  class="btn btn-success btn-xs action"><i class="fa fa-check"></i></a>';
                            } else {
                               $nestedData[$column] .= '&nbsp;<a href="'.$approve_reject_url.'" class="btn btn-danger btn-xs action"><i class="fa fa-close"></i></a>';
                            }
                    } else {
                        $nestedData[$column] = $voucher->$column;
                    }
                }
                $data[] = $nestedData;
            }
        }

        $json_data = array(
                        "draw"            => intval($request->input('draw')),  
                        "recordsTotal"    => intval($totalData),  
                        "recordsFiltered" => intval($totalFiltered), 
                        "data"            => $data   
                    );   
        echo json_encode($json_data);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\User;
use Hash;
use DB;
use Session;
use Auth;

class UserController extends Controller
{
    
    public function index()
    { 
        $admin_id = Auth::guard('Admin')->user()->admin_id;
        $users = User::select("user.*")->whereIn('status', ['pending','cancel'])->latest()->get();

        return view('admin.user.paying_user', compact('users'));
    }

    public function paid_users()
    { 
        $admin_id = Auth::guard('Admin')->user()->admin_id;
        $users = User::select("user.*")->where('status', 'subscribed')->latest()->get();

        return view('admin.user.paid_user', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $employee)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $employee)
    { 
    }
    
    public function destroy($id)
    { 
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Models\Shop;
use App\Models\User;

use Instagram\Api;
use Instagram\Exception\InstagramException;

use Psr\Cache\CacheException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;


/*print_r($profile->getMedias); // 12 first medias
do {
    $profile = $api->getMoreMedias($profile);
    //print_r($profile->getMedias()); // 12 more medias
    $medias[] = $profile->getMedias();
    // avoid 429 Rate limit from Instagram
    sleep(1);
} while ($profile->hasMoreMedias());*/

class InstagramAPITestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

        	$cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');

		    $api = new Api($cachePool);
		    $api->login(env('INSTAGRAM_LOGIN', false), env('INSTAGRAM_PASSWORD', false));

		    $profile = $api->getProfile('kendalljenner');

		    echo "<pre>";
		    print_r($profile);exit;

		    echo '============================' . "\n";
		    echo 'User Information : ' . "\n";
		    echo '============================' . "\n";
		    echo 'ID               : ' . $profile->getId() . "\n";
		    echo 'Full Name        : ' . $profile->getFullName() . "\n";
		    echo 'UserName         : ' . $profile->getUserName() . "\n";
		    echo 'Following        : ' . $profile->getFollowing() . "\n";
		    echo 'Followers        : ' . $profile->getFollowers() . "\n";
		    echo 'Biography        : ' . $profile->getBiography() . "\n";
		    echo 'External Url     : ' . $profile->getExternalUrl() . "\n";
		    echo 'Profile Picture  : ' . $profile->getProfilePicture() . "\n";
		    echo 'Verified Account : ' . ($profile->isVerified() ? 'Yes' : 'No') . "\n";
		    echo 'Private Account  : ' . ($profile->isPrivate() ? 'Yes' : 'No') . "\n";
		    echo 'Medias Count     : ' . $profile->getMediaCount() . "\n";
		    echo '============================' . "\n";

		} catch (InstagramException $e) {
		    print_r($e->getMessage());
		} catch (CacheException $e) {
		    print_r($e->getMessage());
		}
    }
    public function profile_stories(Request $request)
    {

    	$allShopsAry = $existsWebsiteAry = array();
    	$shopList = Shop::get();
    	foreach ($shopList as $key => $shop) {
    		$allShopsAry[$shop->user_id][$shop->website] = $shop->website;
    	}

		$allStoryData = array();
		$existsWebsiteArr2 = array();
		$existsWebsiteDB = array();
		try { 

			$cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');

		    $api = new Api($cachePool);
			$api->login(env('INSTAGRAM_LOGIN', false), env('INSTAGRAM_PASSWORD', false));

			$influencerList = User::where('is_active', 1)->get();
			$data = $finalData = array();
		    $i = 1;
		    foreach ($influencerList as $influencer) {
		    
		        $profile = $api->getProfile($influencer->instagram_id);
		        
		        sleep(1);
		        $feedStories = $api->getStories($profile->getId());

		        if (count($feedStories->getStories())) {
		            foreach ($feedStories->getStories() as $key => $story) {
		                    if(!empty($story->getCtaUrl())){
		                        $allStoryData[$influencer->instagram_id][$key]['userName'] = $profile->getUserName();
		                        $allStoryData[$influencer->instagram_id][$key]['userID'] = $profile->getId();
		                        //$allStoryData[$influencer][$key]['fullName'] = $profile->getFullName();
		                        $allStoryData[$influencer->instagram_id][$key]['ctaUrl'] = $story->getCtaUrl();
		                        $allStoryData[$influencer->instagram_id][$key]['displayUrl'] = $story->getDisplayUrl();
		                        //$allStoryData[$influencer][$key]['takenAtDate'] = $story->getTakenAtDate()->format('Y-m-d h:i:s');

		                        /* For insert data in shop table */
		                        /*$ctaUrl = $story->getCtaUrl();
		                       
		                        $existsWebsiteDB = isset($allShopsAry[$influencer->user_id]) ? $allShopsAry[$influencer->user_id] : array();
		                         $existsWebsiteArr2 = isset($existsWebsiteAry[$influencer->user_id]) ? $existsWebsiteAry[$influencer->user_id] : array();

		                        if(!in_array($ctaUrl, $existsWebsiteDB)){
		                        	if(!in_array($ctaUrl, $existsWebsiteArr2)){

				                        $data['website'] =  $ctaUrl;
				                        $data['user_id'] =  $influencer->user_id;
				                        $data['created_at'] =  $story->getTakenAtDate()->format('Y-m-d h:i:s');
				                        $finalData[] = $data;

				                        $existsWebsiteAry[$influencer->user_id][$ctaUrl] = $ctaUrl;
				                    }
			                    }*/
		                    }

		            }
		        } else {
		            //$allStoryData[] => 'No stories';
		        }
		        
		    }

		    /* For insert data in shop table */
		    /*if(!empty($finalData)){
		    	Shop::insert($finalData);
		    }*/
			
		    echo "<pre>";print_r($allStoryData);
		    
		    exit;

		} catch (InstagramException $e) {
		    print_r($e->getMessage());
		} catch (CacheException $e) {
		    print_r($e->getMessage());
		}
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Mail;
use Hash;
use App\Models\User;
use Auth;
use Config;

class SignupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       /* $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.getrewardful.com/v1/affiliates/503ed49f-772d-4cf6-a9e9-0d1e80d804ae",
            CURLOPT_POST => 0,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer 00aac455e688751a0e13516ba56e5d5a"
            ],
        ]);
        $resp = curl_exec($curl);
        print_r($resp);exit;
        $resp = json_decode($resp);
        curl_close($curl);
*/

    	$countries = DB::table('countries')->get();
        return view('user.signup', compact('countries'));
    }

    public function signup(Request $request)
    {
    	/*$validator = request()->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:user,email,NULL,user_id,deleted_at,NULL',
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ]);

        $request['password'] = Hash::make($request['password']);
        User::create($request->all());
        
        return redirect()->route('user.signup')->with('success','Sign-up successfully.');*/
    }

    public function stripe_payment_request(Request $request)
    {
        $validator = request()->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:user,email,NULL,user_id,deleted_at,NULL',
            'password' => 'required',
            /*'confirm_password' => 'required|same:password'*/
        ]);


        /* Stripe API Start */
        $apiKey = Config::get('constants.stripe.secret-key');

        $cust_data = [
                'name' => $request->first_name.' '.$request->last_name,
                'description' => 'Stripe Payment Request',
                'email' => $request->email,
                'source' => $_POST['stripeToken']
            ];
        if( isset($request->referral)) {
            $cust_data['metadata'] = array('referral' => $request->referral);
        }

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/customers",
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . $apiKey
            ],
            CURLOPT_POSTFIELDS => http_build_query($cust_data)
        ]);
        $res = curl_exec($curl);
        curl_close($curl);
        $res = json_decode($res);

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/subscriptions",
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . $apiKey
            ],
            CURLOPT_POSTFIELDS => http_build_query([
              'customer' => $res->id,
              'items[0][price]' => 'price_1HPNQaLp4bo48vYdlOwYjBCy'
            ])
        ]);
        $resp = curl_exec($curl);
        $resp = json_decode($resp);
        curl_close($curl);
        /* Stripe API End*/


        if(isset($resp->status) && $resp->status == 'active'){

            $request['password'] = Hash::make($request['password']);
            $user = User::create($request->all());

            /* Start - Send to sendinblue data */
            $curl = curl_init();
            $post =  $email_campaigns['sender'] = array("attributes" => array('FirstName'=> $request->first_name, 'LastName'=>$request->last_name), "email" => $request->email, "listIds" => [6]);
            curl_setopt_array($curl, [
              CURLOPT_URL => "https://api.sendinblue.com/v3/contacts",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => json_encode($post),
              CURLOPT_HTTPHEADER => [
                "accept: application/json",
                "content-type: application/json",
                "api-key: ". Config::get('constants.sendinblue.api-key')
              ],
            ]);
            $response = curl_exec($curl);
            $response = json_decode($response);
            //$err = curl_error($curl);
            curl_close($curl);
            /*if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              echo $response;
            }*/
            if(isset($response->id)){
                User::where('user_id', $user->user_id)->update(['sendinblue_id' => $response->id]);  
            } 
            /* End - Send to sendinblue data */

            DB::table('subscription')->insert([
                'stripe_subscription_id' => $resp->id,
                'user_id' => $user->user_id,
                'current_period_start' => date("Y-m-d H:i:s", ($resp->current_period_start)),
                'current_period_end' => date("Y-m-d H:i:s", ($resp->current_period_end)),
                'customer' => $resp->customer,
                'plan_id' => $resp->plan->id,
                'start_date' => date("Y-m-d H:i:s", ($resp->start_date)),
                'status' => $resp->status,
                'created_at' => date("Y-m-d H:i:s"),
            ]);

            User::where('user_id', $user->user_id)->update(['status' => 'subscribed']);

            $user = User::find($user->user_id); 
            Auth::login($user);


            /* Rewardful API Start*/
           // $rand = substr(uniqid('', true), -5);
           // $token = trim(strtolower($request->first_name)).'-'.$rand;
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "https://api.getrewardful.com/v1/affiliates",
                CURLOPT_POST => 1,
                CURLOPT_HTTPHEADER => [
                    "Authorization: Bearer ".Config::get('constants.getrewardful.secret-key')
                ],
                CURLOPT_POSTFIELDS => http_build_query([
                  'first_name' => $request->first_name,
                  'last_name' => $request->last_name,
                  'email' => $request->email,
                 /* 'token' => $token,*/
                 /* 'stripe_customer_id' => 'cus_I5iE2xEEaiww04'*///$resp->customer
                ])
            ]);
            $resp2 = curl_exec($curl);
            
            $resp2 = json_decode($resp2);
            
            curl_close($curl);
            if(isset($resp2->id)){
                User::where('user_id', $user->user_id)->update(['rewardful_token' => $resp2->links[0]->token, 'rewardful_id' => $resp2->id]);  
            } 
            

            /*$curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "https://api.getrewardful.com/v1/affiliates/8e8cde1a-d8c9-4e00-8507-2e2b7ae5b90c",
                CURLOPT_POST => 0,
                CURLOPT_HTTPHEADER => [
                    "Authorization: Bearer 00aac455e688751a0e13516ba56e5d5a"
                ],
            ]);
            $resp = curl_exec($curl);
            print_r($resp);exit;
            $resp = json_decode($resp);
            curl_close($curl);*/
            /* Rewardful API End*/

            return redirect('user/dashboard');
           // return redirect()->route('user.signup')->with('success','Sign-up successfully.');
        } else{
            return redirect()->route('user.signup')->with('error','Payment failed.');
        }
    }

     public function is_email_already_exists_user(Request $Request){
        $admin_count = User::where('email', $Request->email)->count();
    
        if($admin_count > 0)
            echo 'false';
        else
            echo 'true';
    }
}

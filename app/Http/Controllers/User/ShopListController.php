<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Influencer;
use App\Models\Shop;
use App\Models\Category;
use Hash;
use DB;
use Session;
use Auth;
use Carbon;

class ShopListController extends Controller
{
    
    public function index(Request $request)
    { 
        $this_week_start = Carbon\Carbon::parse('last monday')->startOfDay();
        $this_week_end = Carbon\Carbon::parse('next sunday')->endOfDay();

        $shop_list = DB::table('shop_list')->select('is_favourite','list_name', 'shop_list.shop_list_id',
            DB::raw('(SELECT COUNT(s.influencer_id)/count(assigned_shop.shop_id) from shop s where s.website IN (select website from assigned_shop as assp JOIN shop sp ON assp.shop_id=sp.shop_id where assp.shop_list_id=shop_list.shop_list_id)) as avg_influencer'),
            DB::raw('count(assigned_shop.shop_id) as tot_shop'),
            DB::raw('GROUP_CONCAT(DISTINCT country_name  SEPARATOR ", ") as country_name'),
            DB::raw('GROUP_CONCAT(DISTINCT country_code  SEPARATOR ", ") as country_code'), 
            DB::raw('count(assigned_shop.shop_id) as tot_shop'),
            DB::raw('(SELECT COUNT(DISTINCT influencer_id) from shop s where s.shop_id IN (select shop_id from assigned_shop where shop_list_id=shop_list.shop_list_id and (s.created_at BETWEEN "'.$this_week_start.'" AND "'.$this_week_end.'"))) as promo_this_week'))
         ->leftJoin('assigned_shop','assigned_shop.shop_list_id', '=' ,'shop_list.shop_list_id')
         ->leftJoin('shop','assigned_shop.shop_id', '=' ,'shop.shop_id')
         ->leftJoin('influencer','influencer.influencer_id', '=' ,'shop.influencer_id')
         ->leftJoin('countries','influencer.country_id', '=' ,'countries.country_id')
        ->where('user_id', Auth::guard('User')->user()->user_id)
        ->groupBy('shop_list.shop_list_id')
        ->latest('shop_list.created_at')
        ->paginate(6)
        ->onEachSide(1);
       
        return view('user.shop_list.list', compact('shop_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 
        /*$validator = request()->validate([
            'list_name' => 'required|unique:shop_list,list_name,NULL'
        ]);*/
        DB::table('shop_list')->insert(
        [
            'list_name' => $request->list_name, 
            'user_id' => Auth::guard('User')->user()->user_id,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        
        return redirect()->route('shop-list.index')->with('success','List created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function show(Request $request, $shop_list_id)
    { 
       $user_id = Auth::guard('User')->user()->user_id; 
       $countries = DB::table('countries')->get();
       $categories = Category::all();
       $shop_list = DB::table('shop_list')->where('user_id', $user_id)->get();
       $sp_list = DB::table('shop_list')->where('shop_list_id', $shop_list_id)->first();
       
       $name_of_the_list = $sp_list->list_name;
       $is_favourite = $sp_list->is_favourite;

        $sort_by = $request->get('sort_by');
        $sort_type = $request->get('sort_type');

        $this_week_start = Carbon\Carbon::parse('last monday')->startOfDay();
        $this_week_end = Carbon\Carbon::parse('next sunday')->endOfDay();

        $query = Shop::select("shop.*","countries.country_code","countries.country_name","category_name",  
            DB::raw('CONCAT(first_name, " ", last_name) AS name'), 
            DB::raw('(Select CONCAT(instagram_id, "--", profile_pic,"--",is_verified) FROM shop s join influencer u on s.influencer_id=u.influencer_id where s.website=shop.website order by s.created_at DESC LIMIT 1) as influencer_data'), 
            DB::raw('(SELECT COUNT(s.influencer_id) from shop s where s.website=shop.website) as nb_of_promotions'),
            DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website) as total_influencers'),
            DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1) as first_added_date'),
            DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1) as last_seen_date'),
            DB::raw('(SELECT COUNT(DISTINCT influencer_id) from shop s2 where s2.website=shop.website AND (s2.created_at BETWEEN "'.$this_week_start.'" AND "'.$this_week_end.'")) as promo_this_week'),
            DB::raw('count(DISTINCT shop_category.category_id) as category_cnt'),
            DB::raw('GROUP_CONCAT(DISTINCT category.category_name  SEPARATOR "<br />") as categories')
        )
        ->leftJoin('shop_category','shop_category.shop_id', '=' ,'shop.shop_id')
        ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id')
        ->leftJoin('influencer','influencer.influencer_id', '=' ,'shop.influencer_id');
        $shops = $query->leftJoin('countries','countries.country_id', '=' ,'influencer.country_id');

        if(isset($request->search_website) && !empty($request->search_website))
            $shops = $query->where('website', 'like', '%' .$request->search_website.'%');

        if(isset($request->filter_country_ids) && !empty($request->filter_country_ids)){
            $shops = $query->whereIn('influencer.country_id', $request->filter_country_ids);
        }

        if(isset($request->filter_category_ids) && !empty($request->filter_category_ids)){
            $shops = $query->whereIn('shop_category.category_id', $request->filter_category_ids);
        }

        if(isset($request->filter_activity) && !empty($request->filter_activity)){
            $shops = $query->whereIn('activity', $request->filter_activity);
        }

        if(isset($request->first_added_last_week) && !empty($request->first_added_last_week)){
            $last_week_from = date("Y-m-d", strtotime("last week monday"));
            $last_week_to = date("Y-m-d", strtotime("last week sunday"));
            $shops = $query->whereBetween(DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), [$last_week_from, $last_week_to]);
        }

        if(isset($request->last_seen_today) && !empty($request->last_seen_today)){
            $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'), date("Y-m-d"));
        }

        if(isset($request->promo_this_week_from) && !empty($request->promo_this_week_from) && isset($request->promo_this_week_to) && !empty($request->promo_this_week_to)){ 
            $shops = $query->whereBetween(DB::raw('(SELECT COUNT(DISTINCT influencer_id) from shop s2 where s2.website=shop.website AND (s2.created_at BETWEEN "'.$this_week_start.'" AND "'.$this_week_end.'"))'), [$request->promo_this_week_from, $request->promo_this_week_to]);
        } else if(isset($request->promo_this_week_from) && !empty($request->promo_this_week_from)){ 
            $shops = $query->where(DB::raw('(SELECT COUNT(DISTINCT influencer_id) from shop s2 where s2.website=shop.website AND (s2.created_at BETWEEN "'.$this_week_start.'" AND "'.$this_week_end.'"))'), '>=' , $request->promo_this_week_from);
        } else if(isset($request->promo_this_week_to) && !empty($request->promo_this_week_to)){ 
            $shops = $query->where(DB::raw('(SELECT COUNT(DISTINCT influencer_id) from shop s2 where s2.website=shop.website AND (s2.created_at BETWEEN "'.$this_week_start.'" AND "'.$this_week_end.'"))'), '<=' , $request->promo_this_week_to);
        } 

        if(isset($request->nb_of_promotions_from) && !empty($request->nb_of_promotions_from) && isset($request->nb_of_promotions_to) && !empty($request->nb_of_promotions_to)){
            $shops = $query->whereBetween(DB::raw('(SELECT COUNT(s.influencer_id) from shop s where s.website=shop.website)'), [$request->nb_of_promotions_from, $request->nb_of_promotions_to]);
        } else if(isset($request->nb_of_promotions_from) && !empty($request->nb_of_promotions_from)){
            $shops = $query->where(DB::raw('(SELECT COUNT(s.influencer_id) from shop s where s.website=shop.website)'), '>=', $request->nb_of_promotions_from);
        } else if(isset($request->nb_of_promotions_to) && !empty($request->nb_of_promotions_to)){
            $shops = $query->where(DB::raw('(SELECT COUNT(s.influencer_id) from shop s where s.website=shop.website)'), '<=', $request->nb_of_promotions_to);
        }

        if(isset($request->total_influencers_from) && !empty($request->total_influencers_from) && isset($request->total_influencers_to) && !empty($request->total_influencers_to)){
            $shops = $query->whereBetween(DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website)'), [$request->total_influencers_from, $request->total_influencers_to]);
        } else if(isset($request->total_influencers_from) && !empty($request->total_influencers_from)){
            $shops = $query->where(DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website)'),'>=' , $request->total_influencers_from);
        } else if(isset($request->total_influencers_to) && !empty($request->total_influencers_to)){
            $shops = $query->where(DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website)'),'<=' , $request->total_influencers_to);
        }

        /*if(isset($request->sort_by) && !empty($request->sort_by))
            $shops = $query->orderBy('created_at', $request->sort_by);*/
        if(isset($sort_by) && !empty($sort_by) && isset($sort_type) && !empty($sort_type)){
            $shops = $query->orderBy($sort_by, $sort_type);
        }

        $shops = $query->whereIn('shop.shop_id', function($query) use ($shop_list_id){
               $query->select('shop_id')->from('assigned_shop')->where('shop_list_id', $shop_list_id)->get();
            })
        ->groupBy('shop.shop_id')
        ->paginate(10)->onEachSide(1);

        $total_rec_cnt = Shop::whereIn('shop.shop_id', function($query) use ($shop_list_id){
               $query->select('shop_id')->from('assigned_shop')->where('shop_list_id', $shop_list_id)->get();
            })->count();

        if ($request->ajax()) {
            return response()->json([
                'body' => view('user.shop.load_shop', ['shops' => $shops, 'name_of_the_list' => $name_of_the_list, 'total_rec_cnt' => $total_rec_cnt])->render()
            ]);  
        } 
       
        return view('user.shop.shop', compact('shops','countries','shop_list', 'name_of_the_list','categories', 'total_rec_cnt', 'is_favourite', 'shop_list_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($shop_id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       /* $validator = request()->validate([
            'list_name' => 'required|unique:shop_list,list_name,'.$request->shop_list_id.',shop_list_id'
        ]);*/
        DB::table('shop_list')->where('shop_list_id', $request->shop_list_id)->update([
                'list_name' => $request->list_name
            ]);
        
        return redirect()->route('shop-list.index')->with('success','List updated successfully.');
    }
    
    public function delete(Request $request)
    { 
        DB::table('shop_list')->where('shop_list_id', $request->shop_list)->delete();

        return redirect()->route('shop-list.index')->with('success','List deleted successfully.');
    }

    public function favourite_shop_list(Request $request){

        $cnt = DB::table('shop_list')->where(['shop_list_id' => $request->shop_list_id, 'user_id' => Auth::guard('User')->user()->user_id, 'is_favourite' => 1])->count();

        DB::table('shop_list')->where(['user_id' => Auth::guard('User')->user()->user_id, 'is_favourite' => 1])->update(['is_favourite' => 0]);

        if($cnt > 0){
           DB::table('shop_list')->where(['shop_list_id' => $request->shop_list_id, 'user_id' => Auth::guard('User')->user()->user_id])->update(['is_favourite' => 0]); 
        } else {
           DB::table('shop_list')->where(['shop_list_id' => $request->shop_list_id, 'user_id' => Auth::guard('User')->user()->user_id])->update(['is_favourite' => 1]);  
        }
    }

    public function assign_shop(Request $request){

        foreach ($request->shop_list_ids as $key => $shop_list_id) {
            foreach ($request->shop_ids as $key => $shop_id) {
                 $result = DB::table('assigned_shop')->where([
                    'shop_list_id' => $shop_list_id,
                    'shop_id' => $shop_id
                ])->first();
              
                if(empty($result->assigned_shop_id)){
                    DB::table('assigned_shop')->insert([
                        'shop_list_id' => $shop_list_id,
                        'shop_id' => $shop_id
                    ]);
                } else {
                    DB::table('assigned_shop')->where('shop_list_id', $result->assigned_shop_id)->update([
                        'shop_list_id' => $shop_list_id,
                        'shop_id' => $shop_id
                    ]);
                }
            }
        }
       
    }
}

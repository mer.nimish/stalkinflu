<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use Hash;
use DB; 
use Session;
use Auth;

class NotificationController extends Controller
{
    public function index()
    { 
        $notifications = Notification::select("notification.*")
            ->latest('date')
            ->paginate(10);

        DB::table('user')->where('user_id', Auth::guard('User')->user()->user_id)->update(['is_unread_notification' => 1]);

        /*$notification = Notification::first();
        $user_id = Auth::guard('User')->user()->user_id; 

        $cnt = Notification::whereRaw('FIND_IN_SET('.Auth::guard('User')->user()->user_id.', view_user_ids) IS NULL')->count();
        if($cnt != 0)
        {
             $view_user_ids = $notification->view_user_ids;
            $view_user_ids_arr = !empty($view_user_ids) ? explode(",", $view_user_ids) : array();
            array_push($view_user_ids_arr, $user_id);
            DB::table('notification')->update(['view_user_ids' => implode(",", $view_user_ids_arr)]); 
        }*/
        return view('user.notification.index', compact('notifications'));
    }

    public function show($notification_id)
    { 
        $notification = Notification::select("notification.*")
            ->where('notification_id', $notification_id)
            ->first();

        return view('user.notification.show', compact('notification'));
    }
}

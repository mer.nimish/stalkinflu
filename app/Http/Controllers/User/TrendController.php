<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Shop;
use App\Models\Influencer;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Mail;
use File;
use Carbon;

class TrendController extends Controller
{
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $top_influencer_of_week = Influencer::select('influencer.influencer_id','instagram_id', 'profile_pic', DB::raw('COUNT(DISTINCT shop.website) as promos'))
            ->leftJoin('shop','shop.influencer_id', '=' ,'influencer.influencer_id')
            ->whereBetween('shop.created_at', [
                Carbon\Carbon::parse('last monday')->startOfDay(),
                Carbon\Carbon::parse('next sunday')->endOfDay(),
            ])
            ->where('is_approved', 1)
            ->groupBy('influencer.influencer_id')
            ->orderBy('promos', 'desc')
            ->limit(10)->get();

        $top_shop_of_week = Shop::select('shop.shop_id','website', DB::raw('COUNT(DISTINCT influencer_id) as promotions'))
            ->whereBetween('created_at', [
                Carbon\Carbon::parse('last monday')->startOfDay(),
                Carbon\Carbon::parse('next sunday')->endOfDay(),
            ])
            ->groupBy('website')
            ->orderBy('promotions', 'desc')
            ->limit(10)->get();

        return view('user.trend', compact('top_shop_of_week', 'top_influencer_of_week'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $Request)
    { 
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //return view('user.edit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $Request)
    {
        
    }
}

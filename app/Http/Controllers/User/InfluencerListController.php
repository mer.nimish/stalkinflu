<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Influencer;
use App\Models\Category;
use App\Models\Shop;
use Hash;
use DB;
use Session;
use Auth;
use Carbon;

class InfluencerListController extends Controller
{
    
    public function index(Request $request)
    { 
        $this_week_start = Carbon\Carbon::parse('last monday')->startOfDay();
        $this_week_end = Carbon\Carbon::parse('next sunday')->endOfDay();

        $influencer_list = DB::table('influencer_list')->select('is_favourite','list_name', 'influencer_list.influencer_list_id',
            DB::raw('GROUP_CONCAT(DISTINCT influencer.influencer_id  SEPARATOR ",") as inf'),  
            DB::raw('AVG(engagement_rate) as avg_eng_rate'), 
            DB::raw('count(assigned_influencer.influencer_id) as tot_influencer'),
            DB::raw('GROUP_CONCAT(DISTINCT country_name  SEPARATOR ", ") as country_name'),
            DB::raw('GROUP_CONCAT(DISTINCT country_code  SEPARATOR ", ") as country_code'), 
            DB::raw('count(assigned_influencer.influencer_id) as tot_influencer'),
             DB::raw('(select count(*) from shop where influencer_id IN (select influencer_id from assigned_influencer where influencer_list_id=influencer_list.influencer_list_id and (shop.created_at BETWEEN "'.$this_week_start.'" AND "'.$this_week_end.'"))) as promo_this_week'))
         ->leftJoin('assigned_influencer','assigned_influencer.influencer_list_id', '=' ,'influencer_list.influencer_list_id')
         ->leftJoin('influencer','assigned_influencer.influencer_id', '=' ,'influencer.influencer_id')
         ->leftJoin('countries','influencer.country_id', '=' ,'countries.country_id')
        ->where('user_id', Auth::guard('User')->user()->user_id)
        ->groupBy('influencer_list.influencer_list_id')
        ->latest('influencer_list.created_at')
        ->paginate(6)
        ->onEachSide(1);
       
        return view('user.influencer_list.list', compact('influencer_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 
        /*$validator = request()->validate([
            'list_name' => 'required|unique:influencer_list,list_name,NULL'
        ]);*/
        DB::table('influencer_list')->insert(
        [
            'list_name' => $request->list_name, 
            'user_id' => Auth::guard('User')->user()->user_id,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        
        return redirect()->route('influencer-list.index')->with('success','List created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function show(Request $request, $influencer_list_id)
    { 
        $user_id = Auth::guard('User')->user()->user_id;
        $countries = DB::table('countries')->get();
        $influencer_list = DB::table('influencer_list')->where('user_id', $user_id)->get();
        
        $categories = Category::all();

        $inf_list = DB::table('influencer_list')->where('influencer_list_id', $influencer_list_id)->first();
        $name_of_the_list = $inf_list->list_name;
        $is_favourite = $inf_list->is_favourite;

        $sort_by = $request->get('sort_by');
        $sort_type = $request->get('sort_type');

        $week_start_date = Carbon\Carbon::parse('last sunday')->startOfDay();
        $week_end_date = Carbon\Carbon::parse('next sunday')->endOfDay();

        $query = Influencer::select("influencer.*","countries.country_code","countries.country_name","category_name",
            DB::raw('(SELECT website FROM shop where influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1) as lastest_website_promoted'), 
            DB::raw('(SELECT created_at FROM shop where influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1) as lastest_promo'),
            DB::raw('ROUND((SELECT COUNT(s.website) FROM shop s where s.influencer_id=influencer.influencer_id)/(SELECT COUNT(DISTINCT s.website) FROM shop s where s.influencer_id=influencer.influencer_id), 2) as avg_promo_shops'),
            DB::raw('(SELECT COUNT(DISTINCT s.website) FROM shop s where s.influencer_id=influencer.influencer_id AND (s.created_at BETWEEN "'.$week_start_date.'" AND "'.$week_end_date.'")) as promo_this_week'),
            DB::raw('(SELECT s2.website FROM shop s2 where s2.influencer_id=influencer.influencer_id AND (s2.created_at BETWEEN "'.$week_start_date.'" AND "'.$week_end_date.'") group by s2.website order by count(*) desc limit 1) as most_promoted_shop_week'),
            DB::raw('count(DISTINCT shop_category.category_id) as category_cnt'),
            DB::raw('GROUP_CONCAT(DISTINCT category.category_name  SEPARATOR "<br />") as categories')
        )
        ->leftJoin('shop','shop.influencer_id', '=' ,'influencer.influencer_id')
        ->leftJoin('shop_category','shop_category.shop_id', '=' ,'shop.shop_id')
        ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id');
        $influencers = $query->leftJoin('countries','influencer.country_id', '=' ,'countries.country_id');

        if(isset($request->search_instagram_id) && !empty($request->search_instagram_id)){
            $influencers = $query->where('instagram_id', 'like', '%' .$request->search_instagram_id.'%');
        }

        if(isset($request->filter_country_ids) && !empty($request->filter_country_ids)){
            $influencers = $query->whereIn('influencer.country_id', $request->filter_country_ids);
        }

        if(isset($request->filter_category_ids) && !empty($request->filter_category_ids)){
            $shops = $query->whereIn('shop_category.category_id', $request->filter_category_ids);
        }

        if(isset($request->eng_rate_from) && !empty($request->eng_rate_from) && isset($request->eng_rate_to) && !empty($request->eng_rate_to)){
            $influencers = $query->whereBetween('engagement_rate', [$request->eng_rate_from, $request->eng_rate_to]);
        } else if(isset($request->eng_rate_from) && !empty($request->eng_rate_from)){
            $influencers = $query->where('engagement_rate', '>=', $request->eng_rate_from);
        } else if(isset($request->eng_rate_to) && !empty($request->eng_rate_to)){
            $influencers = $query->where('engagement_rate', '<=', $request->eng_rate_to);
        }

        if(isset($request->followers_from) && !empty($request->followers_from) && isset($request->followers_to) && !empty($request->followers_to)){
            $influencers = $query->whereBetween('followers', [$request->followers_from, $request->followers_to]);
        } else if(isset($request->followers_from) && !empty($request->followers_from)){
            $influencers = $query->where('followers', '>=',$request->followers_from);
        } else if(isset($request->followers_to) && !empty($request->followers_to)){
            $influencers = $query->where('followers', '<=',$request->followers_to);
        } 

        if(isset($request->latest_promo_date_from) && !empty($request->latest_promo_date_from) && isset($request->latest_promo_date_to) && !empty($request->latest_promo_date_to)){
            $influencers = $query->whereBetween(DB::raw('(SELECT DATE(created_at) FROM shop where influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1)'), [date("Y-m-d", strtotime($request->latest_promo_date_from)), date("Y-m-d", strtotime($request->latest_promo_date_to))]);
        } else if(isset($request->latest_promo_date_from) && !empty($request->latest_promo_date_from)){
            $influencers = $query->where(DB::raw('(SELECT DATE(created_at) FROM shop where influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1)'), '>=', date("Y-m-d", strtotime($request->latest_promo_date_from)));
        } else if(isset($request->latest_promo_date_to) && !empty($request->latest_promo_date_to)){
            $influencers = $query->where(DB::raw('(SELECT DATE(created_at) FROM shop where influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1)'), '<=', date("Y-m-d", strtotime($request->latest_promo_date_to)));
        }

        if(isset($request->promo_this_week_from) && !empty($request->promo_this_week_from) && isset($request->promo_this_week_to) && !empty($request->promo_this_week_to)){ 
            $influencers = $query->whereBetween(DB::raw('(SELECT COUNT(DISTINCT s.website) FROM shop s where s.influencer_id=influencer.influencer_id AND (s.created_at BETWEEN "'.$week_start_date.'" AND "'.$week_end_date.'"))'), [$request->promo_this_week_from, $request->promo_this_week_to]);
        } else if(isset($request->promo_this_week_from) && !empty($request->promo_this_week_from)){ 
            $influencers = $query->where(DB::raw('(SELECT COUNT(DISTINCT s.website) FROM shop s where s.influencer_id=influencer.influencer_id AND (s.created_at BETWEEN "'.$week_start_date.'" AND "'.$week_end_date.'"))'),'>=' ,$request->promo_this_week_from);
        } else if(isset($request->promo_this_week_to) && !empty($request->promo_this_week_to)){ 
            $influencers = $query->where(DB::raw('(SELECT COUNT(DISTINCT s.website) FROM shop s where s.influencer_id=influencer.influencer_id AND (s.created_at BETWEEN "'.$week_start_date.'" AND "'.$week_end_date.'"))'),'<=' ,$request->promo_this_week_to);
        }

        if(isset($sort_by) && !empty($sort_by) && isset($sort_type) && !empty($sort_type)){
            $influencers = $query->orderBy($sort_by, $sort_type);
        }

        $influencers = $query->whereIn('influencer.influencer_id', function($query) use ($influencer_list_id){
               $query->select('influencer_id')->from('assigned_influencer')->where('influencer_list_id', $influencer_list_id)->get();
            })
            ->where('is_approved', 1)
            ->groupBy('influencer.influencer_id')
            ->paginate(10)->onEachSide(1);

        $total_rec_cnt = Influencer::where('is_approved', 1)->whereIn('influencer.influencer_id', function($query) use ($influencer_list_id){
               $query->select('influencer_id')->from('assigned_influencer')->where('influencer_list_id', $influencer_list_id)->get();
            })->count();

        if ($request->ajax()) {
            return response()->json([
                'body' => view('user.influencer.load_influencer', ['influencers' => $influencers, 'name_of_the_list' => $name_of_the_list, 'total_rec_cnt' => $total_rec_cnt])->render()
            ]);  
        } 
       
        return view('user.influencer.influencer', compact('influencers', 'countries', 'influencer_list', 'name_of_the_list','categories', 'total_rec_cnt','influencer_list_id','is_favourite'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($influencer_id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        /*$validator = request()->validate([
            'list_name' => 'required|unique:influencer_list,list_name,'.$request->influencer_list_id.',influencer_list_id'
        ]);*/
        DB::table('influencer_list')->where('influencer_list_id', $request->influencer_list_id)->update([
                'list_name' => $request->list_name
            ]);
        
        return redirect()->route('influencer-list.index')->with('success','List updated successfully.');
    }
    
    public function delete(Request $request)
    { 
        DB::table('influencer_list')->where('influencer_list_id', $request->influencer_list)->delete();

        return redirect()->route('influencer-list.index')->with('success','List deleted successfully.');
    }   

    public function favourite_influencer_list(Request $request){

        $cnt = DB::table('influencer_list')->where(['influencer_list_id' => $request->influencer_list_id, 'user_id' => Auth::guard('User')->user()->user_id, 'is_favourite' => 1])->count();

        DB::table('influencer_list')->where(['user_id' => Auth::guard('User')->user()->user_id, 'is_favourite' => 1])->update(['is_favourite' => 0]);
        if($cnt > 0){
            DB::table('influencer_list')->where(['influencer_list_id' => $request->influencer_list_id, 'user_id' => Auth::guard('User')->user()->user_id])->update(['is_favourite' => 0]); 
        } else {
           DB::table('influencer_list')->where(['influencer_list_id' => $request->influencer_list_id, 'user_id' => Auth::guard('User')->user()->user_id])->update(['is_favourite' => 1]); 
        }
    }

    public function assign_influencer(Request $request){

        foreach ($request->influencer_list_ids as $key => $influencer_list_id) {
            foreach ($request->influencer_ids as $key => $influencer_id) {
                 $result = DB::table('assigned_influencer')->where([
                    'influencer_list_id' => $influencer_list_id,
                    'influencer_id' => $influencer_id
                ])->first();
              
                if(empty($result->assigned_influencer_id)){
                    DB::table('assigned_influencer')->insert([
                        'influencer_list_id' => $influencer_list_id,
                        'influencer_id' => $influencer_id
                    ]);
                } else {
                    DB::table('assigned_influencer')->where('influencer_list_id', $result->assigned_influencer_id)->update([
                        'influencer_list_id' => $influencer_list_id,
                        'influencer_id' => $influencer_id
                    ]);
                }
            }
        }
       
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Mail;
use Auth;
use Hash;
use File;
use Image;
use Config;

class ProfileController extends Controller
{
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {   
        $rewardful_id = Auth::guard('User')->user()->rewardful_id;
        if(!empty($rewardful_id)){
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "https://api.getrewardful.com/v1/affiliates/".$rewardful_id."/sso",
                CURLOPT_POST => 0,
                CURLOPT_HTTPHEADER => [
                    "Authorization: Bearer ".Config::get('constants.getrewardful.secret-key')
                ],
            ]);
            $resp = curl_exec($curl);
            $resp = json_decode($resp);
            curl_close($curl);
        }
        $rewardful_app_login = isset($resp->sso->url) ? $resp->sso->url : '';

        $commission_text = DB::table('setting')->first()->commission_text;
        return view('user.profile', compact('rewardful_app_login','commission_text'));
    }

    public function update_email(Request $request){

        $user_id = Auth::guard('User')->user()->user_id;

        $validator = request()->validate([
            'email' => 'required|unique:user,email,'.$user_id.',user_id,deleted_at,NULL',
        ]);
        
        User::where('user_id', $user_id)->update(['email' => $request->email]);
        
        return redirect()->route('user.profile')->with('success','Email updated successfully.');
    }

    public function update_password(Request $Request){

        $user_id = Auth::guard('User')->user()->user_id;
        $user = User::where('user_id', $user_id)->first();

        if (!Hash::check($Request->old_password, $user->password)) {
            return redirect()->route('user.profile')->with('error','Your old password dose not match.');
         } else {

            User::where('user_id', $user_id)->update(['password' => Hash::make($Request->new_password)]);
            return redirect()->route('user.profile')->with('success','Password changed successfully.');
         }
     }

     public function check_old_password(Request $Request){
        $user_id = Auth::guard('User')->user()->user_id;
        $user = User::where('user_id', $user_id)->first();
        
        if (Hash::check($Request->old_password, $user->password)) 
            return 'true';
        else 
            return 'false';
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $Request)
    { 
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $Request)
    {
    }

    public function cancel_subscription(){

        $user_id = Auth::guard('User')->user()->user_id;
        $email = Auth::guard('User')->user()->email;
        $subscription = DB::table('subscription')->where('user_id', $user_id)->first();

        $apiKey = Config::get('constants.stripe.secret-key');

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/subscriptions/".$subscription->stripe_subscription_id,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . $apiKey
            ],
            CURLOPT_POSTFIELDS => http_build_query([
              'cancel_at_period_end' => 'true',
            ])
        ]);
        $res = curl_exec($curl);
        curl_close($curl);

        /* Start - Send to sendinblue data */
            /*$curl = curl_init();
            curl_setopt_array($curl, [
              CURLOPT_URL => "https://api.sendinblue.com/v3/contacts/".$email,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "DELETE",
              CURLOPT_HTTPHEADER => [
                "accept: application/json",
                "api-key: ". Config::get('constants.sendinblue.api-key')
              ],
            ]);
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);*/
            /*if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              echo $response;
            }*/
            /* End - Send to sendinblue data */

        return redirect()->route('user.profile')->with('success','Your subscription has been canceled successfully.');
    }
}

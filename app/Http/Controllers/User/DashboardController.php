<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Influencer;
use App\Models\Shop;
use App\Models\User;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Mail;
use Auth;
use Hash;
use File;
use Config;
use Carbon;

class DashboardController extends Controller
{
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {   
        $influencers = Influencer::select("influencer.*")->count();
        $shops = Shop::select("shop.*")->count();
        $users = User::select("user.*")->count();

        $notifications = Notification::latest('date')->limit(4)->get();

        $setting = DB::table('setting')->first();

        $user_id = Auth::guard('User')->user()->user_id;

        $influencer_list = DB::table('influencer_list')->where('is_favourite', 1)->where('user_id', $user_id)->first();
        $influencer_list_id = isset($influencer_list->influencer_list_id) ? $influencer_list->influencer_list_id : 0;
        $fav_influencers = Influencer::whereIn('influencer.influencer_id', function($query) use ($influencer_list_id){
           $query->select('influencer_id')->from('assigned_influencer')->where('influencer_list_id', $influencer_list_id)->get();
        })
        ->where('is_approved', 1)
        ->groupBy('influencer.influencer_id')
        ->limit(7)
        ->get();

        $shop_list = DB::table('shop_list')->where('is_favourite', 1)->where('user_id', $user_id)->first();
        $shop_list_id = isset($shop_list->shop_list_id) ? $shop_list->shop_list_id : 0;
        $fav_shops = Shop::whereIn('shop.shop_id', function($query) use ($shop_list_id){
           $query->select('shop_id')->from('assigned_shop')->where('shop_list_id', $shop_list_id)->get();
        })
        ->groupBy('shop.shop_id')
        ->limit(7)
        ->get();

        $best_shop_of_the_month = Shop::select('shop.shop_id','website', DB::raw('COUNT(DISTINCT shop.influencer_id) as promotions'),'influencer.influencer_id','instagram_id', 'profile_pic')
        ->leftJoin('influencer','shop.influencer_id', '=' ,'influencer.influencer_id')
            ->whereBetween('shop.created_at', [
                Carbon\Carbon::parse('first day of last month')->startOfDay(),
                Carbon\Carbon::parse('last day of last month')->endOfDay(),
            ])
            ->where('is_approved', 1)
            ->groupBy('website')
            ->orderBy('promotions', 'desc')
            ->first();

        return view('user.dashboard', compact('shops', 'influencers', 'setting', 'notifications','fav_influencers', 'influencer_list_id', 'fav_shops','shop_list_id','best_shop_of_the_month','users'));
        
    }

    public function change_password(){
        return view('user.change_password');
    }

    public function update_password(Request $Request){

        $user_id = Auth::guard('User')->user()->user_id;
        $user = Admin::where('user_id', $user_id)->first();
        if (!Hash::check($Request->old_password, $user->password)) {
            return redirect()->route('user.edit_profile')->with('error','Your old password dose not match.');
         } else {

            Admin::where('user_id', $user_id)->update(['password' => Hash::make($Request->new_password)]);
            return redirect()->route('user.change_password')->with('success','Password changed successfully.');
         }
     }

     public function check_old_password(Request $Request){
        $user_id = Auth::guard('Admin')->user()->user_id;
        $user = Admin::where('user_id', $user_id)->first();
        
        if (Hash::check($Request->old_password, $user->password)) 
            return 'true';
        else 
            return 'false';
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $Request)
    { 
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //return view('user.edit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $Request)
    {
        
    }
}

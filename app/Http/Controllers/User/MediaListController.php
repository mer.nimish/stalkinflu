<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Influencer;
use App\Models\Shop;
use Hash;
use DB;
use Session;
use Auth;
use Carbon;

class MediaListController extends Controller
{
    
    public function index(Request $request)
    { 
        $media_list = DB::table('media_list')->select('list_name', 'media_list.media_list_id',
            DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website IN (select website from assigned_media as assp JOIN shop sp ON assp.shop_id=sp.shop_id where assp.media_list_id=media_list.media_list_id)) as tot_inf'),
            DB::raw('(Select count(DISTINCT website) from assigned_media am JOIN shop shp ON am.shop_id=shp.shop_id where media_list_id=media_list.media_list_id) as tot_shop'),
            DB::raw('count(assigned_media.assigned_media_id) as tot_media'),
            DB::raw('GROUP_CONCAT(DISTINCT country_name  SEPARATOR ", ") as country_name'),
            DB::raw('GROUP_CONCAT(DISTINCT country_code  SEPARATOR ", ") as country_code'))
         ->leftJoin('assigned_media','assigned_media.media_list_id', '=' ,'media_list.media_list_id')
         ->leftJoin('shop','assigned_media.shop_id', '=' ,'shop.shop_id')
         ->leftJoin('influencer','influencer.influencer_id', '=' ,'shop.influencer_id')
         ->leftJoin('countries','influencer.country_id', '=' ,'countries.country_id')
        ->where('user_id', Auth::guard('User')->user()->user_id)
        ->groupBy('media_list.media_list_id')
        ->latest('media_list.created_at')
        ->paginate(6)
        ->onEachSide(1);
       
        return view('user.media_list.list', compact('media_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 

       /* $validator = request()->validate([
            'list_name' => 'required|unique:media_list,list_name,NULL'
        ]);*/
        DB::table('media_list')->insert(
        [
            'list_name' => $request->list_name, 
            'user_id' => Auth::guard('User')->user()->user_id,
            'created_at' => date("Y-m-d H:i:s")
        ]);
        
        return redirect()->route('media-list.index')->with('success','List created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function show(Request $request, $media_list_id)
    { 
        $user_id = Auth::guard('User')->user()->user_id;
        $countries = DB::table('countries')->get();
        $media_list = DB::table('media_list')->where('user_id', $user_id)->get();
        $name_of_the_list = DB::table('media_list')->select('list_name')->where('media_list_id', $media_list_id)->first()->list_name;

        $sort_by = $request->get('sort_by');
        $sort_type = $request->get('sort_type');

        $this_week_start = Carbon\Carbon::parse('last monday')->startOfDay();
        $this_week_end = Carbon\Carbon::parse('next sunday')->endOfDay();

        $query = Shop::select("shop.*","countries.country_code","countries.country_name", "influencer.*", "shop.created_at","category_name", 
            DB::raw('count(DISTINCT shop_category.category_id) as category_cnt'),  
            DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id and shop.influencer_id=influencer.influencer_id) as nb_of_story'),
            DB::raw('GROUP_CONCAT(DISTINCT category.category_name  SEPARATOR "<br />") as categories') 
            )
        ->leftJoin('shop_category','shop_category.shop_id', '=' ,'shop.shop_id')
        ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id')
        ->leftJoin('influencer','influencer.influencer_id', '=' ,'shop.influencer_id');
        $shops = $query->leftJoin('countries','countries.country_id', '=' ,'influencer.country_id');

        if(isset($request->search_instagram_id) && !empty($request->search_instagram_id))
                $shops = $query->where('instagram_id', 'like', '%' .$request->search_instagram_id.'%');

        if(isset($request->filter_country_ids) && !empty($request->filter_country_ids)){
            $shops = $query->whereIn('influencer.country_id', $request->filter_country_ids);
        }

        if(isset($request->eng_rate_from) && !empty($request->eng_rate_from) && isset($request->eng_rate_to) && !empty($request->eng_rate_to)){
            $influencers = $query->whereBetween('engagement_rate', [$request->eng_rate_from, $request->eng_rate_to]);
        } else if(isset($request->eng_rate_from) && !empty($request->eng_rate_from)){
            $influencers = $query->where('engagement_rate', '>=', $request->eng_rate_from);
        } else if(isset($request->eng_rate_to) && !empty($request->eng_rate_to)){
            $influencers = $query->where('engagement_rate', '<=', $request->eng_rate_to);
        }

        if(isset($request->followers_from) && !empty($request->followers_from) && isset($request->followers_to) && !empty($request->followers_to)){
            $influencers = $query->whereBetween('followers', [$request->followers_from, $request->followers_to]);
        } else if(isset($request->followers_from) && !empty($request->followers_from)){
            $influencers = $query->where('followers', '>=',$request->followers_from);
        } else if(isset($request->followers_to) && !empty($request->followers_to)){
            $influencers = $query->where('followers', '<=',$request->followers_to);
        }

        if(isset($request->nb_of_story_from) && !empty($request->nb_of_story_from) && isset($request->nb_of_story_to) && !empty($request->nb_of_story_to)){
            $influencers = $query->whereBetween(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id and shop.influencer_id=influencer.influencer_id)'), [$request->nb_of_story_from, $request->nb_of_story_to]);
        } else if(isset($request->nb_of_story_from) && !empty($request->nb_of_story_from)){
            $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id and shop.influencer_id=influencer.influencer_id)'),'>=', $request->nb_of_story_from);
        } else if(isset($request->nb_of_story_to) && !empty($request->nb_of_story_to)){
            $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id and shop.influencer_id=influencer.influencer_id)'),'<=', $request->nb_of_story_to);
        }

        if(isset($sort_by) && !empty($sort_by) && isset($sort_type) && !empty($sort_type)){
            $shops = $query->orderBy($sort_by, $sort_type);
        }
       
        $shops = $query->whereIn('shop.shop_id', function($query) use ($media_list_id){
               $query->select('shop_id')->from('assigned_media')->where('media_list_id', $media_list_id)->get();
            })
            ->latest('shop.created_at')->groupBy('shop.shop_id')->paginate(8)->onEachSide(1);

        $total_rec_cnt = Shop::whereIn('shop.shop_id', function($query) use ($media_list_id){
               $query->select('shop_id')->from('assigned_media')->where('media_list_id', $media_list_id)->get();
            })->count();

        if ($request->ajax()) {
            return response()->json([
                'body' => view('user.media_list.load_media', ['shops' => $shops, 'name_of_the_list' => $name_of_the_list, 'total_rec_cnt' => $total_rec_cnt])->render()
            ]);  
        } 
       
        return view('user.media_list.media', compact('shops','countries', 'media_list', 'name_of_the_list'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request)
    {
        /*$validator = request()->validate([
            'list_name' => 'required|unique:media_list,list_name,'.$request->media_list_id.',media_list_id'
        ]);*/
        DB::table('media_list')->where('media_list_id', $request->media_list_id)->update([
                'list_name' => $request->list_name
            ]);
        
        return redirect()->route('media-list.index')->with('success','List updated successfully.');
    }
    
    public function delete(Request $request)
    { 
        DB::table('media_list')->where('media_list_id', $request->media_list)->delete();

        return redirect()->route('media-list.index')->with('success','List deleted successfully.');
    }

    public function assign_media(Request $request){
        foreach ($request->media_list_ids as $key => $media_list_id) {
            $result = DB::table('assigned_media')->where([
                'media_list_id' => $media_list_id,
                'shop_id' => $request->shop_id
            ])->first();
          
            if(empty($result->assigned_media_id)){
                DB::table('assigned_media')->insert([
                    'media_list_id' => $media_list_id,
                    'shop_id' => $request->shop_id
                ]);
            } else {
                DB::table('assigned_media')->where('media_list_id', $result->assigned_media_id)->update([
                    'media_list_id' => $media_list_id,
                    'shop_id' => $request->shop_id
                ]);
            }
        }
    }
}

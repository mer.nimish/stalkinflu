<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Shop;
use App\Models\Influencer;
use App\Models\Category;
use Hash;
use DB; 
use Session;
use Auth;
use Carbon;
use Helpers;

class ShopController extends Controller
{
    
    public function index(Request $request)
    { 
        $user_id = Auth::guard('User')->user()->user_id;
        $countries = DB::table('countries')->get();
        $categories = Category::all();
        $shop_list = DB::table('shop_list')->where('user_id', $user_id)->get();

        $sort_by = $request->get('sort_by');
        $sort_type = $request->get('sort_type');

        $this_week_start = Carbon\Carbon::parse('last monday')->startOfDay();
        $this_week_end = Carbon\Carbon::parse('next sunday')->endOfDay();

        $query = Shop::select("shop.*","countries.country_code","countries.country_name","category_name", 
            DB::raw('CONCAT(first_name, " ", last_name) AS name'), 
            DB::raw('(Select CONCAT(instagram_id, "--", profile_pic,"--",is_verified) FROM shop s join influencer u on s.influencer_id=u.influencer_id where s.website=shop.website order by s.created_at DESC LIMIT 1) as influencer_data'), 
            DB::raw('(SELECT COUNT(s.influencer_id) from shop s where s.website=shop.website) as nb_of_promotions'),
            DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website) as total_influencers'),
            DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1) as first_added_date'),
            DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1) as last_seen_date'),
            DB::raw('(SELECT COUNT(DISTINCT influencer_id) from shop s2 where s2.website=shop.website AND (s2.created_at BETWEEN "'.$this_week_start.'" AND "'.$this_week_end.'")) as promo_this_week'),
            DB::raw('GROUP_CONCAT(DISTINCT countries.country_code  SEPARATOR ",") as country_codes'),
            DB::raw('GROUP_CONCAT(DISTINCT countries.country_name  SEPARATOR ",") as country_names'),
            DB::raw('count(DISTINCT shop_category.category_id) as category_cnt'),
            DB::raw('GROUP_CONCAT(DISTINCT category.category_name  SEPARATOR "<br />") as categories')
        )
        ->leftJoin('shop_category','shop_category.shop_id', '=' ,'shop.shop_id')
        ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id')
        ->leftJoin('influencer','influencer.influencer_id', '=' ,'shop.influencer_id');
        $shops = $query->leftJoin('countries','countries.country_id', '=' ,'influencer.country_id');

        if(isset($request->search_website) && !empty($request->search_website))
            $shops = $query->where('website', 'like', '%' .$request->search_website.'%');

        if(isset($request->filter_country_ids) && !empty($request->filter_country_ids)){
            $shops = $query->whereIn('influencer.country_id', $request->filter_country_ids);
        }

        if(isset($request->filter_category_ids) && !empty($request->filter_category_ids)){
            $shops = $query->whereIn('shop_category.category_id', $request->filter_category_ids);
        }

        if(isset($request->filter_activity) && !empty($request->filter_activity)){
            $shops = $query->whereIn('activity', $request->filter_activity);
        }

        if(isset($request->first_added_from) && !empty($request->first_added_from) && isset($request->first_added_to) && !empty($request->first_added_to)){ 
            
            $shops = $query->whereBetween(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), [$request->first_added_from, date('Y-m-d', strtotime($request->first_added_to))]);
        } else if(isset($request->first_added_from) && !empty($request->first_added_from)){ 
            $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), '>=' , date('Y-m-d', strtotime($request->first_added_from)));
        } else if(isset($request->first_added_to) && !empty($request->first_added_to)){
            $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), '<=' , date('Y-m-d', strtotime($request->first_added_to))); 
        } 

        if(isset($request->last_seen_from) && !empty($request->last_seen_from) && isset($request->last_seen_to) && !empty($request->last_seen_to)){ 
            
           $shops = $query->whereBetween(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'), [date('Y-m-d', strtotime($request->last_seen_from)), date('Y-m-d', strtotime($request->last_seen_to))]);
        } else if(isset($request->last_seen_from) && !empty($request->last_seen_from)){ 
            $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'),'>=',date('Y-m-d', strtotime($request->last_seen_from)));
        } else if(isset($request->last_seen_to) && !empty($request->last_seen_to)){
             $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'),'<=',date('Y-m-d', strtotime($request->last_seen_to)));
        } 

        /*if(isset($request->first_added_last_week) && !empty($request->first_added_last_week)){
            $last_week_from = date("Y-m-d", strtotime("last week monday"));
            $last_week_to = date("Y-m-d", strtotime("last week sunday"));
            $shops = $query->whereBetween(DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), [$last_week_from, $last_week_to]);
        }

        if(isset($request->last_seen_today) && !empty($request->last_seen_today)){
            $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'), date("Y-m-d"));
        }*/

        if(isset($request->promo_this_week_from) && !empty($request->promo_this_week_from) && isset($request->promo_this_week_to) && !empty($request->promo_this_week_to)){ 
            $shops = $query->whereBetween(DB::raw('(SELECT COUNT(DISTINCT influencer_id) from shop s2 where s2.website=shop.website AND (s2.created_at BETWEEN "'.$this_week_start.'" AND "'.$this_week_end.'"))'), [$request->promo_this_week_from, $request->promo_this_week_to]);
        } else if(isset($request->promo_this_week_from) && !empty($request->promo_this_week_from)){ 
            $shops = $query->where(DB::raw('(SELECT COUNT(DISTINCT influencer_id) from shop s2 where s2.website=shop.website AND (s2.created_at BETWEEN "'.$this_week_start.'" AND "'.$this_week_end.'"))'), '>=' , $request->promo_this_week_from);
        } else if(isset($request->promo_this_week_to) && !empty($request->promo_this_week_to)){ 
            $shops = $query->where(DB::raw('(SELECT COUNT(DISTINCT influencer_id) from shop s2 where s2.website=shop.website AND (s2.created_at BETWEEN "'.$this_week_start.'" AND "'.$this_week_end.'"))'), '<=' , $request->promo_this_week_to);
        } 

        if(isset($request->nb_of_promotions_from) && !empty($request->nb_of_promotions_from) && isset($request->nb_of_promotions_to) && !empty($request->nb_of_promotions_to)){
            $shops = $query->whereBetween(DB::raw('(SELECT COUNT(s.influencer_id) from shop s where s.website=shop.website)'), [$request->nb_of_promotions_from, $request->nb_of_promotions_to]);
        } else if(isset($request->nb_of_promotions_from) && !empty($request->nb_of_promotions_from)){
            $shops = $query->where(DB::raw('(SELECT COUNT(s.influencer_id) from shop s where s.website=shop.website)'), '>=', $request->nb_of_promotions_from);
        } else if(isset($request->nb_of_promotions_to) && !empty($request->nb_of_promotions_to)){
            $shops = $query->where(DB::raw('(SELECT COUNT(s.influencer_id) from shop s where s.website=shop.website)'), '<=', $request->nb_of_promotions_to);
        }

        if(isset($request->total_influencers_from) && !empty($request->total_influencers_from) && isset($request->total_influencers_to) && !empty($request->total_influencers_to)){
            $shops = $query->whereBetween(DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website)'), [$request->total_influencers_from, $request->total_influencers_to]);
        } else if(isset($request->total_influencers_from) && !empty($request->total_influencers_from)){
            $shops = $query->where(DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website)'),'>=' , $request->total_influencers_from);
        } else if(isset($request->total_influencers_to) && !empty($request->total_influencers_to)){
            $shops = $query->where(DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website)'),'<=' , $request->total_influencers_to);
        }

        /*if(isset($request->sort_by) && !empty($request->sort_by))
            $shops = $query->orderBy('created_at', $request->sort_by);*/
        if(isset($sort_by) && !empty($sort_by) && isset($sort_type) && !empty($sort_type)){
            $shops = $query->orderBy($sort_by, $sort_type);
        }

        if ($request->flag == 'trend') {
            $query->whereBetween('shop.created_at', [
                Carbon\Carbon::parse('last monday')->startOfDay(),
                Carbon\Carbon::parse('next sunday')->endOfDay(),
            ]);
            $query->orderBy('nb_of_promotions', 'desc');
            
        }

        $shops = $query->groupBy('shop.website')->orderBy('last_seen_date', 'desc')->paginate(10)->onEachSide(1);

        $total_rec_cnt = Shop::distinct('shop.website')->count();

        if ($request->ajax()) {
            return response()->json([
                'body' => view('user.shop.load_shop', ['shops' => $shops, 'total_rec_cnt' => $total_rec_cnt])->render()
            ]);  
        } 
       
        return view('user.shop.shop', compact('shops','countries','shop_list','categories', 'total_rec_cnt'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Shop $shop)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Shop $shop)
    {
        $countries = DB::table('countries')->get();
        $user_id = Auth::guard('User')->user()->user_id;
        $influencer_list = DB::table('influencer_list')->where('user_id', $user_id)->get();
        $shop_list = DB::table('shop_list')->where('user_id', $user_id)->get();

        $category_names = DB::table('shop_category')->select(DB::raw('GROUP_CONCAT(DISTINCT category_name  SEPARATOR ", ") as category_name'))
        ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id')
        ->where('shop_category.shop_id', $shop->shop_id)
        ->first();
        /*$website = Shop::select(DB::raw('count(*) as count'))->where('website', $shop->website)->groupBy('website')->first();
        $total_promotions = $website->count;*/

        $promotions = Shop::select(DB::raw('COUNT(influencer_id) as count'))->where('website', $shop->website)->first();
        $total_promotions = $promotions->count;

        $influencers = Shop::select(DB::raw('COUNT(DISTINCT influencer_id) as count'))->where('website', $shop->website)->first();
        $total_influencers = $influencers->count;

        $stories = DB::table('shop_media')->select(DB::raw('COUNT(*) as count'))
        ->leftJoin('shop','shop.shop_id', '=' ,'shop_media.shop_id')
        ->where('website', $shop->website)->first();
        $total_stories = number_format($stories->count,1);

        $impressions = Shop::select('followers')
        ->leftJoin('influencer','influencer.influencer_id', '=' ,'shop.influencer_id')
        ->where('website', $shop->website)->distinct('shop.influencer_id')->get();
        $total_followers = 0;
        foreach ($impressions as $key => $impression) {
            $total_followers += $impression->followers;
        }

        $total_unique_impressions = $total_followers;
        $average_impressions = !empty($total_influencers) ? ($total_followers / $total_influencers) : number_format(0,1);
        $average_story_nb = !empty($total_influencers) ? number_format(floor($total_stories / $total_influencers),1) : number_format(0,1);

        $most_used_influence = Shop::select(DB::raw('COUNT(DISTINCT instagram_id) as count'), 'instagram_id')
        ->leftJoin('influencer','influencer.influencer_id', '=' ,'shop.influencer_id')
        ->where('website', $shop->website)->orderBy('count', 'desc')->first();
        $most_used_influence = $most_used_influence->instagram_id;

        $nb_of_unique_markets = Shop::select(DB::raw('COUNT(DISTINCT country_id) as count'))
        ->leftJoin('influencer','influencer.influencer_id', '=' ,'shop.influencer_id')
        ->where('website', $shop->website)->orderBy('count', 'desc')->first();
        $nb_of_unique_markets = number_format($nb_of_unique_markets->count,1);

        /*$promotions = Shop::select(DB::raw('COUNT(influencer_id) as count'))->where('website', $shop->website)
            ->whereBetween('created_at', [
                Carbon\Carbon::parse('last monday')->startOfDay(),
                Carbon\Carbon::parse('next sunday')->endOfDay(),
            ])
        ->first();
        $total_promotions = $promotions->count;*/

        $last_week_from = date("Y-m-d", strtotime("last week monday"));
        $last_week_to = date("Y-m-d", strtotime("last week sunday"));
        $average_promo_by_day = Shop::select(DB::raw('COUNT(DISTINCT influencer_id) as promos'))
            ->whereBetween('created_at', [$last_week_from, $last_week_to])
            ->where('website', $shop->website)->first();
        $average_promo_by_day = !empty($average_promo_by_day->promos) ? number_format(($average_promo_by_day->promos / 7),1) : number_format(0,1);

        $last_month_from = date("Y-m-d", strtotime("first day of last month"));
        $last_month_to = date("Y-m-d", strtotime("last day of last month"));
        $average_promo_by_week = Shop::select(DB::raw('COUNT(DISTINCT influencer_id) as promos'))
            ->whereBetween('created_at', [$last_month_from, $last_month_to])
            ->where('website', $shop->website)->first();
        $average_promo_by_week = !empty($average_promo_by_week->promos) ? number_format(($average_promo_by_week->promos / 4),1) : number_format(0,1);

        $country = Shop::select('countries.country_code', 'countries.country_name', DB::raw('COUNT(website) as promo'))
        ->leftJoin('influencer','influencer.influencer_id', '=' ,'shop.influencer_id')
        ->leftJoin('countries','influencer.country_id', '=' ,'countries.country_id')
        ->where('website', $shop->website)
        ->groupBy('countries.country_id')
        ->orderBy('promo', 'desc')
        ->first();
        $country_name = $country->country_name;

        $promo_week = Shop::select(DB::raw('COUNT(DISTINCT influencer_id) as promos'))
            ->whereBetween('created_at', [
                Carbon\Carbon::parse('last monday')->startOfDay(),
                Carbon\Carbon::parse('next sunday')->endOfDay(),
            ])
            ->where('website', $shop->website)->first();
        $promo_of_week = $promo_week->promos;
        /* Start - For update activity */
        if($promo_of_week > 15) 
            $activity = 'high';
        else if($promo_of_week > 7)
            $activity = 'medium';
        else
            $activity = 'low';
        Shop::where('website', $shop->website)->update(['activity' => $activity]);
        /* End - For update activity */
        
        /* Start - For all activity chart data */
        $startDate = date ("Y-m-d", strtotime("-60 days", strtotime(date ("Y-m-d"))));
        $endDate = date ("Y-m-d");
        $promotions = Shop::select(DB::raw('COUNT(influencer_id) as promos'), 'created_at')
            ->whereBetween('created_at', [$startDate, $endDate])
            ->where('website', $shop->website)
            ->groupBy(DB::raw('DATE(created_at)'))
            ->get();

        $promos_db = array();
        foreach ($promotions as $key => $promotion) {
            $promos_db[date("M d", strtotime($promotion->created_at))] = $promotion->promos;
        }

        while (strtotime($startDate) <= strtotime($endDate)) {      
            $startDate = date ("M d", strtotime("+1 day", strtotime($startDate)));
            $promotions_data[$startDate] = isset($promos_db[$startDate]) ? $promos_db[$startDate] : 0;
            $avg_promotions_data[$startDate] = number_format(array_sum($promos_db)/60, 1);
        }
        /* End - For all activity chart data */

        $promotions = Shop::select(DB::raw('DISTINCT instagram_id'), DB::raw('MIN(shop.created_at) as first_added'), DB::raw('MAX(shop.created_at) as last_seen'))
            ->leftJoin('influencer','influencer.influencer_id', '=' ,'shop.influencer_id')
            ->where('shop.website', $shop->website)
            ->orderBy('shop.created_at', 'desc')
            ->first();

        $latest_promotion = $promotions->instagram_id;
        $first_added = $promotions->first_added;
        $last_seen = $promotions->last_seen;

        /* Start - Explored Market */
        $explored_market = Shop::select('countries.country_code', 'countries.country_name', DB::raw('COUNT(website) as promo'))
        ->leftJoin('influencer','influencer.influencer_id', '=' ,'shop.influencer_id')
        ->leftJoin('countries','influencer.country_id', '=' ,'countries.country_id')
        ->where('website', $shop->website)
        ->groupBy('countries.country_id')
        ->orderBy('promo', 'desc')
        ->get();
        /* End - Explored Market */

        // For All promotions of this shop
        if ($request->ajax() && isset($request->with_media) && $request->with_media == 0) {

            $sort_by = $request->get('sort_by');
            $sort_type = $request->get('sort_type');

            $query = Influencer::select("influencer.*","countries.country_code","countries.country_name","category_name","shop.created_at",
                DB::raw('(Select website FROM shop where influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1) as lastest_website_promoted'), 
                DB::raw('(Select created_at FROM shop where influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1) as lastest_promo'),
                DB::raw('(SELECT COUNT(*) from shop_media sm join shop sp on sp.shop_id=sm.shop_id where sp.influencer_id = influencer.influencer_id) as nb_of_story'),
                DB::raw('count(DISTINCT shop_category.category_id) as category_cnt'),
               /* DB::raw('GROUP_CONCAT(DISTINCT category.category_name  SEPARATOR "<br />") as categories'),*/
                DB::raw('(SELECT GROUP_CONCAT(category_name," (",cnt,")" SEPARATOR "<br />")
               from (SELECT count(s.shop_id) as cnt, c.category_name, inf.influencer_id as inf_id  FROM influencer inf 
             join shop s ON inf.influencer_id=s.influencer_id 
             join shop_category sc ON sc.shop_id=s.shop_id 
             join category c ON c.category_id=sc.category_id 
            WHERE 1 GROUP BY inf.influencer_id, c.category_id) q where q.inf_id=influencer.influencer_id) as categories')
            )
                ->leftJoin('shop','shop.influencer_id', '=' ,'influencer.influencer_id')
                ->leftJoin('shop_category','shop_category.shop_id', '=' ,'shop.shop_id')
                ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id');
            $influencers = $query->leftJoin('countries','influencer.country_id', '=' ,'countries.country_id');
            
            if(isset($request->search_instagram_id) && !empty($request->search_instagram_id))
                $influencers = $query->where('instagram_id', 'like', '%' .$request->search_instagram_id.'%');

            if(isset($request->filter_country_ids) && !empty($request->filter_country_ids)){
                $influencers = $query->whereIn('influencer.country_id', $request->filter_country_ids);
            }

            if(isset($request->eng_rate_from) && !empty($request->eng_rate_from) && isset($request->eng_rate_to) && !empty($request->eng_rate_to)){
                $influencers = $query->whereBetween('engagement_rate', [$request->eng_rate_from, $request->eng_rate_to]);
            } else if(isset($request->eng_rate_from) && !empty($request->eng_rate_from)){
                $influencers = $query->where('engagement_rate','>=' ,$request->eng_rate_from);
            } else if(isset($request->eng_rate_to) && !empty($request->eng_rate_to)){
                $influencers = $query->where('engagement_rate','<=' ,$request->eng_rate_to);
            }

            if(isset($request->followers_from) && !empty($request->followers_from) && isset($request->followers_to) && !empty($request->followers_to)){
                $influencers = $query->whereBetween('followers', [$request->followers_from, $request->followers_to]);
            } else if(isset($request->followers_from) && !empty($request->followers_from)){
                $influencers = $query->where('followers','>=' ,$request->followers_from);
            } else if(isset($request->followers_to) && !empty($request->followers_to)){
                $influencers = $query->where('followers','<=' ,$request->followers_to);
            } 

            if(isset($request->promotion_date_from) && !empty($request->promotion_date_from) && isset($request->promotion_date_to) && !empty($request->promotion_date_to)){
                $influencers = $query->whereBetween(DB::raw('(Select DATE(s4.created_at) FROM shop s4 where s4.influencer_id=influencer.influencer_id and s4.website="'.$shop->website.'" LIMIT 1)'), [date("Y-m-d", strtotime($request->promotion_date_from)), date("Y-m-d", strtotime($request->promotion_date_to))]);
            } else if(isset($request->promotion_date_from) && !empty($request->promotion_date_from)){
                $influencers = $query->where(DB::raw('(Select DATE(s4.created_at) FROM shop s4 where s4.influencer_id=influencer.influencer_id and s4.website="'.$shop->website.'" LIMIT 1)'), '>=', date("Y-m-d", strtotime($request->promotion_date_from)));
            } else if(isset($request->promotion_date_to) && !empty($request->promotion_date_to)){
                $influencers = $query->where(DB::raw('(Select DATE(s4.created_at) FROM shop s4 where s4.influencer_id=influencer.influencer_id and s4.website="'.$shop->website.'" LIMIT 1)'), '<=', date("Y-m-d", strtotime($request->promotion_date_to)));
            }

            if(isset($request->nb_of_story_from) && !empty($request->nb_of_story_from) && isset($request->nb_of_story_to) && !empty($request->nb_of_story_to)){
                $influencers = $query->whereBetween(DB::raw('(SELECT COUNT(*) from shop_media sm join shop sp on sp.shop_id=sm.shop_id where sp.influencer_id = influencer.influencer_id)'), [$request->nb_of_story_from, $request->nb_of_story_to]);
            } else if(isset($request->nb_of_story_from) && !empty($request->nb_of_story_from)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm join shop sp on sp.shop_id=sm.shop_id where sp.influencer_id = influencer.influencer_id)'),'>=' , $request->nb_of_story_from);
            } else if(isset($request->nb_of_story_to) && !empty($request->nb_of_story_to)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm join shop sp on sp.shop_id=sm.shop_id where sp.influencer_id = influencer.influencer_id)'),'<=' , $request->nb_of_story_to);
            }

            if(isset($request->is_verified) && !empty($request->is_verified)){
                $influencers = $query->where('is_verified', $request->is_verified);
            }

            if(isset($sort_by) && !empty($sort_by) && isset($sort_type) && !empty($sort_type)){
                $influencers = $query->orderBy($sort_by, $sort_type);
            }

            $influencers = $query->where('shop.website', $shop->website)
            ->where('is_approved', 1)
            ->groupBy('influencer.influencer_id')
            ->paginate(10)->onEachSide(1);

            $total_rec_cnt = Influencer::where('is_approved', 1)->whereIn('influencer.influencer_id', function($query) use ($shop){
               $query->select('influencer_id')->from('shop')->where('website', $shop->website)->get();
            })->count();

            return response()->json([
                'body' => view('user.shop.load_influencer', ['influencers' => $influencers, 'total_rec_cnt' => $total_rec_cnt])->render()
            ]);  
        } 

        // Influencer with media
        if ($request->ajax() && isset($request->with_media) && $request->with_media == 1) {

            $sort_by = $request->get('sort_by');
            $sort_type = $request->get('sort_type');

            $query = Influencer::select("influencer.*","countries.country_code","countries.country_name", "shop.created_at", "category_name", 
                DB::raw('(Select s3.shop_id FROM shop s3 where s3.influencer_id=influencer.influencer_id and s3.website="'.$shop->website.'" LIMIT 1) as shop_id'),
                DB::raw('(SELECT COUNT(*) from shop_media sm where shop.shop_id = sm.shop_id) as stories'),
                DB::raw('count(DISTINCT shop_category.category_id) as category_cnt'),
               /* DB::raw('GROUP_CONCAT(DISTINCT category.category_name  SEPARATOR "<br />") as categories'),*/
                DB::raw('(SELECT GROUP_CONCAT(category_name," (",cnt,")" SEPARATOR "<br />")
                   from (SELECT count(s.shop_id) as cnt, c.category_name, inf.influencer_id as inf_id  FROM influencer inf 
                 join shop s ON inf.influencer_id=s.influencer_id 
                 join shop_category sc ON sc.shop_id=s.shop_id 
                 join category c ON c.category_id=sc.category_id 
                WHERE 1 GROUP BY inf.influencer_id, c.category_id) q where q.inf_id=influencer.influencer_id) as categories')
                )
                ->leftJoin('shop','shop.influencer_id', '=' ,'influencer.influencer_id')
                ->leftJoin('shop_category','shop_category.shop_id', '=' ,'shop.shop_id')
                ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id');
            $influencers = $query->leftJoin('countries','influencer.country_id', '=' ,'countries.country_id');
            
            if(isset($request->search_influencer) && !empty($request->search_influencer))
                $influencers = $query->where('instagram_id', 'like', '%' .$request->search_influencer.'%');


            if(isset($request->is_verified) && !empty($request->is_verified)){
                $influencers = $query->where('is_verified', $request->is_verified);
            }

            if(isset($request->filter_country_ids) && !empty($request->filter_country_ids)){
                $influencers = $query->whereIn('influencer.country_id', $request->filter_country_ids);
            }

            if(isset($request->engagement_from) && !empty($request->engagement_from) && isset($request->engagement_to) && !empty($request->engagement_to)){
                $influencers = $query->whereBetween('engagement_rate', [$request->engagement_from, $request->engagement_to]);
            } else if(isset($request->engagement_from) && !empty($request->engagement_from)){
                $influencers = $query->where('engagement_rate','>=',$request->engagement_from);
            } else if(isset($request->engagement_to) && !empty($request->engagement_to)){
                $influencers = $query->where('engagement_rate','<=',$request->engagement_to);
            }

            if(isset($request->promotion_date_from) && !empty($request->promotion_date_from) && isset($request->promotion_date_to) && !empty($request->promotion_date_to)){
                $influencers = $query->whereBetween(DB::raw('(Select DATE(s4.created_at) FROM shop s4 where s4.influencer_id=influencer.influencer_id and s4.website="'.$shop->website.'" LIMIT 1)'), [date("Y-m-d", strtotime($request->promotion_date_from)), date("Y-m-d", strtotime($request->promotion_date_to))]);
            } else if(isset($request->promotion_date_from) && !empty($request->promotion_date_from)){
                $influencers = $query->where(DB::raw('(Select DATE(s4.created_at) FROM shop s4 where s4.influencer_id=influencer.influencer_id and s4.website="'.$shop->website.'" LIMIT 1)'),'>=' ,date("Y-m-d", strtotime($request->promotion_date_from)));
            } else if(isset($request->promotion_date_to) && !empty($request->promotion_date_to)){
                $influencers = $query->where(DB::raw('(Select DATE(s4.created_at) FROM shop s4 where s4.influencer_id=influencer.influencer_id and s4.website="'.$shop->website.'" LIMIT 1)'),'<=' ,date("Y-m-d", strtotime($request->promotion_date_to)));
            }

            if(isset($request->nb_of_story_from) && !empty($request->nb_of_story_from) && isset($request->nb_of_story_to) && !empty($request->nb_of_story_to)){
                $influencers = $query->whereBetween(DB::raw('(SELECT COUNT(*) from shop_media sm join shop sp on sp.shop_id=sm.shop_id where sp.influencer_id = influencer.influencer_id)'), [$request->nb_of_story_from, $request->nb_of_story_to]);
            } else if(isset($request->nb_of_story_from) && !empty($request->nb_of_story_from)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm join shop sp on sp.shop_id=sm.shop_id where sp.influencer_id = influencer.influencer_id)'),'>=',$request->nb_of_story_from);
            } else if(isset($request->nb_of_story_to) && !empty($request->nb_of_story_to)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm join shop sp on sp.shop_id=sm.shop_id where sp.influencer_id = influencer.influencer_id)'),'<=',$request->nb_of_story_to);
            }

            if(isset($request->follower_from) && !empty($request->follower_from) && isset($request->follower_to) && !empty($request->follower_to)){
                $influencers = $query->whereBetween('followers', [$request->follower_from, $request->follower_to]);
            } else if(isset($request->follower_from) && !empty($request->follower_from)){
                $influencers = $query->where('followers','>=' ,$request->follower_from);
            } else if(isset($request->follower_to) && !empty($request->follower_to)){
                $influencers = $query->where('followers','<=' ,$request->follower_to);
            }

            $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id)'), '>', 0);

            if(isset($sort_by) && !empty($sort_by) && isset($sort_type) && !empty($sort_type)){
                $influencers = $query->orderBy($sort_by, $sort_type);
            }

            $influencers = $query->where('shop.website', $shop->website)->where('is_approved', 1)
            ->groupBy('influencer.influencer_id')
            ->paginate(10)->onEachSide(1);

            $total_rec_cnt = Influencer::where('is_approved', 1)->whereIn('influencer.influencer_id', function($query) use ($shop){
               $query->select('influencer_id')->from('shop')->where('website', $shop->website)->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id)'), '>', 0)->get();
            })->count();

            return response()->json([
                'body' => view('user.shop.load_influencer_with_media', ['influencers' => $influencers, 'total_rec_cnt' => $total_rec_cnt])->render()
            ]);  
        } 

        
        /*DB::statement('SET @rank_new := 0;');
        DB::statement('Update influencer as C inner join (SELECT count(*) as promo, s.influencer_id FROM shop s WHERE s.website="'.$shop->website.'" GROUP BY s.influencer_id ORDER BY promo desc) as A on C.influencer_id = A.influencer_id
            set C.rank = @rank_new := @rank_new + 1');*/

        $result = DB::select('SELECT count(*) as promo, s.influencer_id FROM shop s WHERE s.website="'.$shop->website.'" GROUP BY s.influencer_id ORDER BY promo desc');
        $prev_promo = 0;
        $rank = 0;
        foreach ($result as $key => $raw) {
            if($prev_promo != $raw->promo)
                $rank++;
            DB::select('UPDATE influencer SET rank="'.$rank.'" WHERE influencer_id="'.$raw->influencer_id.'"'); 
            $prev_promo = $raw->promo;
        }

        // For All promotions of this shop
        if ($request->ajax() && isset($request->unique_influencer) && $request->unique_influencer == 1) {

            $sort_by = $request->get('sort_by');
            $sort_type = $request->get('sort_type');

            //DB::statement('SET @rank := 0;');
            //DB::statement('SET @prev_promo = NULL');
            $query = Influencer::select("influencer.*","countries.country_code","countries.country_name",  
                DB::raw('(Select website FROM shop where shop.influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1) as lastest_website_promoted'), 
                DB::raw('(Select created_at FROM shop where shop.influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1) as lastest_promo'),
                DB::raw('(Select count(*) FROM shop where shop.influencer_id=influencer.influencer_id and website="'.$shop->website.'" group by website) as nb_of_promotion_of_the_shop'),
               /* DB::raw('CASE WHEN @prev_promo = @condition THEN @rank WHEN @prev_promo := @condition THEN @rank := @rank + 1 END AS inf_rank'),*/
                DB::raw('count(DISTINCT shop_category.category_id) as category_cnt'),
                /*DB::raw('GROUP_CONCAT(DISTINCT category.category_name  SEPARATOR "<br />") as categories'),*/
                DB::raw('(SELECT GROUP_CONCAT(category_name," (",cnt,")" SEPARATOR "<br />")
                from (SELECT count(s.shop_id) as cnt, c.category_name, inf.influencer_id as inf_id  FROM influencer inf 
                 join shop s ON inf.influencer_id=s.influencer_id 
                 join shop_category sc ON sc.shop_id=s.shop_id 
                 join category c ON c.category_id=sc.category_id 
                WHERE 1 GROUP BY inf.influencer_id, c.category_id) q where q.inf_id=influencer.influencer_id) as categories')
                )
                ->leftJoin('shop','shop.influencer_id', '=' ,'influencer.influencer_id')
                ->leftJoin('shop_category','shop_category.shop_id', '=' ,'shop.shop_id')
                ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id');
            $influencers = $query->leftJoin('countries','influencer.country_id', '=' ,'countries.country_id');
            
            if(isset($request->search_instagram_id) && !empty($request->search_instagram_id))
                $influencers = $query->where('instagram_id', 'like', '%' .$request->search_instagram_id.'%');

            if(isset($request->filter_market_ids) && !empty($request->filter_market_ids)){
                $influencers = $query->whereIn('influencer.country_id', $request->filter_market_ids);
            }

            if(isset($request->eng_rate_from) && !empty($request->eng_rate_from) && isset($request->eng_rate_to) && !empty($request->eng_rate_to)){
                $influencers = $query->whereBetween('engagement_rate', [$request->eng_rate_from, $request->eng_rate_to]);
            } else if(isset($request->eng_rate_from) && !empty($request->eng_rate_from)){
                $influencers = $query->where('engagement_rate', '>=',$request->eng_rate_from);
            } else if(isset($request->eng_rate_to) && !empty($request->eng_rate_to)){
                $influencers = $query->where('engagement_rate', '<=',$request->eng_rate_to);
            }

            if(isset($request->followers_from) && !empty($request->followers_from) && isset($request->followers_to) && !empty($request->followers_to)){
                $influencers = $query->whereBetween('followers', [$request->followers_from, $request->followers_to]);
            } else if(isset($request->followers_from) && !empty($request->followers_from)){
                $influencers = $query->where('followers', '>=',$request->followers_from);
            } else if(isset($request->followers_to) && !empty($request->followers_to)){
                $influencers = $query->where('followers', '<=',$request->followers_to);
            }

            if(isset($request->nb_of_promo_of_shop_from) && !empty($request->nb_of_promo_of_shop_from) && isset($request->nb_of_promo_of_shop_to) && !empty($request->nb_of_promo_of_shop_to)){
                $influencers = $query->whereBetween(DB::raw('(Select count(*) FROM shop where shop.influencer_id=influencer.influencer_id and website="'.$shop->website.'" group by website)'), [$request->nb_of_promo_of_shop_from, $request->nb_of_promo_of_shop_to]);
            } else if(isset($request->nb_of_promo_of_shop_from) && !empty($request->nb_of_promo_of_shop_from)){
                $influencers = $query->where(DB::raw('(Select count(*) FROM shop where shop.influencer_id=influencer.influencer_id and website="'.$shop->website.'" group by website)'),'>=' ,$request->nb_of_promo_of_shop_from);
            } else if(isset($request->nb_of_promo_of_shop_to) && !empty($request->nb_of_promo_of_shop_to)){
                $influencers = $query->where(DB::raw('(Select count(*) FROM shop where shop.influencer_id=influencer.influencer_id and website="'.$shop->website.'" group by website)'),'<=' ,$request->nb_of_promo_of_shop_to);
            }

            if(isset($request->latest_promo_date_from) && !empty($request->latest_promo_date_from) && isset($request->latest_promo_date_to) && !empty($request->latest_promo_date_to)){
                $influencers = $query->whereBetween(DB::raw('DATE(created_at)'), [date("Y-m-d", strtotime($request->latest_promo_date_from)), date("Y-m-d", strtotime($request->latest_promo_date_to))]);
            } else if(isset($request->latest_promo_date_from) && !empty($request->latest_promo_date_from)){
                $influencers = $query->where(DB::raw('DATE(created_at)'),'>=',date("Y-m-d", strtotime($request->latest_promo_date_from)));
            } else if(isset($request->latest_promo_date_to) && !empty($request->latest_promo_date_to)){
                $influencers = $query->where(DB::raw('DATE(created_at)'),'<=',date("Y-m-d", strtotime($request->latest_promo_date_to)));
            }

            if(isset($sort_by) && !empty($sort_by) && isset($sort_type) && !empty($sort_type)){
                $influencers = $query->orderBy($sort_by, $sort_type);
            } else {
                $query->orderBy('rank', 'asc');
            }

           /* $influencers = $query->whereIn('influencer.influencer_id', function($query) use ($shop){
               $query->select('influencer_id')->from('shop')->where('website', $shop->website)->get();
            })*/
            $influencers = $query->where('is_approved', 1)
            ->where('shop.website', $shop->website)
            ->groupBy('influencer.influencer_id')
           /* ->orderBy('nb_of_promotion_of_the_shop', 'desc')*/
            ->paginate(10)->onEachSide(1);

            $total_rec_cnt = Influencer::where('is_approved', 1)->whereIn('influencer.influencer_id', function($query) use ($shop){
               $query->select('influencer_id')->from('shop')->where('website', $shop->website)->get();
            })->count();

            return response()->json([
                'body' => view('user.shop.load_unique_influencer', ['influencers' => $influencers, 'total_rec_cnt' => $total_rec_cnt])->render()
            ]);  
        } 

        return view('user.shop.details', compact('shop', 'countries', 'total_promotions', 'total_influencers', 'latest_promotion', 'first_added', 'last_seen', 'promo_of_week', 'promotions_data','country_name', 'avg_promotions_data', 'total_stories','total_followers','total_unique_impressions','average_impressions','average_story_nb','most_used_influence','nb_of_unique_markets','average_promo_by_day','explored_market','influencer_list','shop_list','category_names','average_promo_by_week')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shop $shop)
    { 
    }
    
    public function destroy($id)
    { 
    }
}

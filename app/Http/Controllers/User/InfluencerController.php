<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Influencer;
use App\Models\Shop;
use App\Models\Category;
use Hash;
use DB;
use Session;
use Auth;
use Carbon;
use Helpers;

class InfluencerController extends Controller
{
    
    public function index(Request $request)
    { 
        $user_id = Auth::guard('User')->user()->user_id;
        $countries = DB::table('countries')->get();
        $categories = Category::all();
        $influencer_list = DB::table('influencer_list')->where('user_id', $user_id)->get();
        
        $sort_by = $request->get('sort_by');
        $sort_type = $request->get('sort_type');

        $week_start_date = Carbon\Carbon::parse('last monday')->startOfDay();
        $week_end_date = Carbon\Carbon::parse('next sunday')->endOfDay();

        $query = Influencer::select("influencer.*","countries.country_code","countries.country_name",
            DB::raw('(SELECT website FROM shop where influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1) as lastest_website_promoted'), 
            DB::raw('(SELECT created_at FROM shop where influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1) as lastest_promo'),
            DB::raw('(SELECT COUNT(DISTINCT s.website) FROM shop s where s.influencer_id=influencer.influencer_id AND (s.created_at BETWEEN "'.$week_start_date.'" AND "'.$week_end_date.'")) as promo_this_week'),
            DB::raw('(SELECT s2.website FROM shop s2 where s2.influencer_id=influencer.influencer_id AND (s2.created_at BETWEEN "'.$week_start_date.'" AND "'.$week_end_date.'") group by s2.website order by count(*) desc limit 1) as most_promoted_shop_week'),
           /* DB::raw('(SELECT COUNT(DISTINCT s.website) FROM shop s where s.influencer_id=influencer.influencer_id) as total_shops'),
            DB::raw('(SELECT COUNT(s.website) FROM shop s where s.influencer_id=influencer.influencer_id) as total_promo'),*/
            DB::raw('ROUND((SELECT COUNT(s.website) FROM shop s where s.influencer_id=influencer.influencer_id)/(SELECT COUNT(DISTINCT s.website) FROM shop s where s.influencer_id=influencer.influencer_id), 2) as avg_promo_shops'),
            DB::raw('count(DISTINCT shop_category.category_id) as category_cnt'),
            DB::raw('GROUP_CONCAT(DISTINCT category.category_name  SEPARATOR "<br />") as categories'),
            DB::raw('COUNT(DISTINCT shop.website) as promos'),
            /*DB::raw('(SELECT GROUP_CONCAT(category_name," (",cnt,")" SEPARATOR "<br />")
               from (SELECT count(s.shop_id) as cnt, c.category_name, inf.influencer_id as inf_id  FROM influencer inf 
             join shop s ON inf.influencer_id=s.influencer_id 
             join shop_category sc ON sc.shop_id=s.shop_id 
             join category c ON c.category_id=sc.category_id 
            WHERE inf.influencer_id=inf_id GROUP BY c.category_id) q where inf_id=influencer.influencer_id) as categories')*/
            DB::raw('(SELECT GROUP_CONCAT(category_name," (",cnt,")" SEPARATOR "<br />")
               from (SELECT count(s.shop_id) as cnt, c.category_name, inf.influencer_id as inf_id  FROM influencer inf 
             join shop s ON inf.influencer_id=s.influencer_id 
             join shop_category sc ON sc.shop_id=s.shop_id 
             join category c ON c.category_id=sc.category_id 
            WHERE 1 GROUP BY inf.influencer_id, c.category_id) q where q.inf_id=influencer.influencer_id) as categories')
        )
        ->leftJoin('shop','shop.influencer_id', '=' ,'influencer.influencer_id')
        ->leftJoin('shop_category','shop_category.shop_id', '=' ,'shop.shop_id')
        ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id')
        ;
        $influencer = $query->leftJoin('countries','influencer.country_id', '=' ,'countries.country_id');

        if(isset($request->is_verified) && !empty($request->is_verified)){
            $influencers = $query->where('is_verified', $request->is_verified);
        }

        if(isset($request->search_instagram_id) && !empty($request->search_instagram_id)){
            $influencers = $query->where('instagram_id', 'like', '%' .$request->search_instagram_id.'%');
        }

        if(isset($request->filter_country_ids) && !empty($request->filter_country_ids)){
            $influencers = $query->whereIn('influencer.country_id', $request->filter_country_ids);
        }

        if(isset($request->filter_category_ids) && !empty($request->filter_category_ids)){
            $shops = $query->whereIn('shop_category.category_id', $request->filter_category_ids);
        }

        if(isset($request->eng_rate_from) && !empty($request->eng_rate_from) && isset($request->eng_rate_to) && !empty($request->eng_rate_to)){
            $influencers = $query->whereBetween('engagement_rate', [$request->eng_rate_from, $request->eng_rate_to]);
        } else if(isset($request->eng_rate_from) && !empty($request->eng_rate_from)){
            $influencers = $query->where('engagement_rate', '>=', $request->eng_rate_from);
        } else if(isset($request->eng_rate_to) && !empty($request->eng_rate_to)){
            $influencers = $query->where('engagement_rate', '<=', $request->eng_rate_to);
        }

        if(isset($request->followers_from) && !empty($request->followers_from) && isset($request->followers_to) && !empty($request->followers_to)){
            $influencers = $query->whereBetween('followers', [$request->followers_from, $request->followers_to]);
        } else if(isset($request->followers_from) && !empty($request->followers_from)){
            $influencers = $query->where('followers', '>=',$request->followers_from);
        } else if(isset($request->followers_to) && !empty($request->followers_to)){
            $influencers = $query->where('followers', '<=',$request->followers_to);
        } 

        if(isset($request->latest_promo_date_from) && !empty($request->latest_promo_date_from) && isset($request->latest_promo_date_to) && !empty($request->latest_promo_date_to)){
            $influencers = $query->whereBetween(DB::raw('(SELECT DATE(created_at) FROM shop where influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1)'), [date("Y-m-d", strtotime($request->latest_promo_date_from)), date("Y-m-d", strtotime($request->latest_promo_date_to))]);
        } else if(isset($request->latest_promo_date_from) && !empty($request->latest_promo_date_from)){
            $influencers = $query->where(DB::raw('(SELECT DATE(created_at) FROM shop where influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1)'), '>=', date("Y-m-d", strtotime($request->latest_promo_date_from)));
        } else if(isset($request->latest_promo_date_to) && !empty($request->latest_promo_date_to)){
            $influencers = $query->where(DB::raw('(SELECT DATE(created_at) FROM shop where influencer_id=influencer.influencer_id order by created_at DESC LIMIT 1)'), '<=', date("Y-m-d", strtotime($request->latest_promo_date_to)));
        }

        /*if(isset($request->is_favourite) && !empty($request->is_favourite)){
            $influencers = $query->where(DB::raw('(SELECT COUNT(*) FROM  influencer_favourite WHERE user_id="'.$user_id.'" AND influencer_id=influencer.influencer_id)'), $request->is_favourite);
        }*/

        if(isset($request->promo_this_week_from) && !empty($request->promo_this_week_from) && isset($request->promo_this_week_to) && !empty($request->promo_this_week_to)){ 
            $influencers = $query->whereBetween(DB::raw('(SELECT COUNT(DISTINCT s.website) FROM shop s where s.influencer_id=influencer.influencer_id AND (s.created_at BETWEEN "'.$week_start_date.'" AND "'.$week_end_date.'"))'), [$request->promo_this_week_from, $request->promo_this_week_to]);
        } else if(isset($request->promo_this_week_from) && !empty($request->promo_this_week_from)){ 
            $influencers = $query->where(DB::raw('(SELECT COUNT(DISTINCT s.website) FROM shop s where s.influencer_id=influencer.influencer_id AND (s.created_at BETWEEN "'.$week_start_date.'" AND "'.$week_end_date.'"))'),'>=' ,$request->promo_this_week_from);
        } else if(isset($request->promo_this_week_to) && !empty($request->promo_this_week_to)){ 
            $influencers = $query->where(DB::raw('(SELECT COUNT(DISTINCT s.website) FROM shop s where s.influencer_id=influencer.influencer_id AND (s.created_at BETWEEN "'.$week_start_date.'" AND "'.$week_end_date.'"))'),'<=' ,$request->promo_this_week_to);
        }

        if(isset($sort_by) && !empty($sort_by) && isset($sort_type) && !empty($sort_type)){
            $influencers = $query->orderBy($sort_by, $sort_type);
        }

        if ($request->flag == 'trend') {
            $query->whereBetween('shop.created_at', [
                Carbon\Carbon::parse('last monday')->startOfDay(),
                Carbon\Carbon::parse('next sunday')->endOfDay(),
            ]);
            $query->orderBy('promos', 'desc');
        }

        $influencers = $query->where('is_approved', 1)->groupBy('influencer.influencer_id')->paginate(10)->onEachSide(1);

        $total_rec_cnt = Influencer::where('is_approved', 1)->count();

        if ($request->ajax()) {
            return response()->json([
                'body' => view('user.influencer.load_influencer', ['influencers' => $influencers, 'total_rec_cnt' => $total_rec_cnt])->render()
            ]);  
        } 
       
        return view('user.influencer.influencer', compact('influencers', 'countries', 'influencer_list', 'categories', 'total_rec_cnt'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $countries = DB::table('countries')->get();
        return view('user.influencer.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 
        $validator = request()->validate([
            'instagram_id' => 'required|unique:influencer,instagram_id,NULL,influencer_id,deleted_at,NULL',
            'country_id' => 'required',
        ]);
        Influencer::create($request->all());
        
        return redirect()->route('influencer.index')->with('success','Influencer created successfully.');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function show(Request $request, Influencer $influencer)
    { 
        $user_id = Auth::guard('User')->user()->user_id;
        $countries = DB::table('countries')->get();
        $shop_list = DB::table('shop_list')->where('user_id', $user_id)->get();
        $influencer_list = DB::table('influencer_list')->where('user_id', $user_id)->get();

        $is_favourite = DB::table('influencer_favourite')
        ->where(['influencer_id' => $influencer->influencer_id, 'user_id' => Auth::guard('User')->user()->user_id])
        ->count();

        $influencer = Influencer::select('influencer.*')
            ->where('influencer.influencer_id', $influencer->influencer_id)
            ->first();

        $top_promo_of_week = Shop::select('website', DB::raw('COUNT(DISTINCT influencer_id) as promotions'))
            ->whereBetween('created_at', [
                Carbon\Carbon::parse('last monday')->startOfDay(),
                Carbon\Carbon::parse('next sunday')->endOfDay(),
            ])
            ->where('influencer_id', $influencer->influencer_id)
            ->groupBy('website')
            ->orderBy('promotions', 'desc')
            ->limit(7)->get();

        $top_promo_of_week_cnt = Shop::select(DB::raw('COUNT(DISTINCT website) as cnt'))
            ->whereBetween('created_at', [
                Carbon\Carbon::parse('last monday')->startOfDay(),
                Carbon\Carbon::parse('next sunday')->endOfDay(),
            ])
            ->where('influencer_id', $influencer->influencer_id)
            ->first()->cnt;

        $month_start_date = new Carbon\Carbon('first day of last month');
        $month_end_date = new Carbon\Carbon('last day of last month');
        $frequency_of_promotions_week_qr = Shop::select(DB::raw('COUNT(DISTINCT website) as promotions'))
            ->whereBetween('created_at', [$month_start_date,$month_end_date,
            ])
            ->where('influencer_id', $influencer->influencer_id)
            ->first();
        $frequency_of_promotions_week = !empty($frequency_of_promotions_week_qr->promotions) ? floor($frequency_of_promotions_week_qr->promotions / 4) : 0;

        $start_day_of_last_3_mon = Carbon\Carbon::now()->startOfMonth()->modify('-3 months');
        $last_month_end_date = new Carbon\Carbon('last day of last month');
        $frequency_of_promotions_mon_qr = Shop::select(DB::raw('COUNT(DISTINCT website) as promotions'))
            ->whereBetween('created_at', [$start_day_of_last_3_mon,$last_month_end_date,
            ])
            ->where('influencer_id', $influencer->influencer_id)
            ->first();
        $frequency_of_promotions_mon = !empty($frequency_of_promotions_mon_qr->promotions) ? floor($frequency_of_promotions_mon_qr->promotions / 3) : 0;

        $total_promotion_in_database_qr = Shop::select(DB::raw('COUNT(*) as promotion'))
            ->where('influencer_id', $influencer->influencer_id)
            ->first();
        $total_promotion_in_database = $total_promotion_in_database_qr->promotion;

        $date_of_latest_promotion_qr = Shop::select('created_at', 'website','shop_id')
            ->where('influencer_id', $influencer->influencer_id)
            ->orderBy('created_at', 'desc')
            ->first();
        $date_of_latest_promotion = !empty($date_of_latest_promotion_qr) ? Helpers::time_elapsed_string($date_of_latest_promotion_qr->created_at) : '';

        $latest_promoted_shop = !empty($date_of_latest_promotion_qr) ? $date_of_latest_promotion_qr->website : ''; 
        $latest_promoted_shop_shop_id = !empty($date_of_latest_promotion_qr) ? $date_of_latest_promotion_qr->shop_id : '#'; 

        $most_promoted_shop_ever_qr = Shop::select(DB::raw('count(*) as cnt'), 'website', 'shop_id')
            ->where('influencer_id', $influencer->influencer_id)
            ->groupBy('website')
            ->orderBy('cnt', 'desc')
            ->first();
        $most_promoted_shop_ever = !empty($most_promoted_shop_ever_qr) ? $most_promoted_shop_ever_qr->website : '';
        $most_promoted_shop_ever_shop_id = !empty($most_promoted_shop_ever_qr) ? $most_promoted_shop_ever_qr->shop_id : '#';

        $most_promoted_shop_week_qr = Shop::select(DB::raw('count(*) as cnt'), 'website', 'shop_id')
            ->where('influencer_id', $influencer->influencer_id)
            ->whereBetween('created_at', [
                Carbon\Carbon::parse('last monday')->startOfDay(),
                Carbon\Carbon::parse('next sunday')->endOfDay(),
            ])
            ->groupBy('website')
            ->orderBy('cnt', 'desc')
            ->first();
        $most_promoted_shop_week = !empty($most_promoted_shop_week_qr) ? $most_promoted_shop_week_qr->website : '-';
        $most_promoted_shop_week_shop_id = !empty($most_promoted_shop_week_qr) ? $most_promoted_shop_week_qr->shop_id : '#';

        $most_promoted_shop_month_qr = Shop::select(DB::raw('count(*) as cnt'), 'website', 'shop_id')
            ->where('influencer_id', $influencer->influencer_id)
            ->whereBetween('created_at', [$month_start_date, $month_end_date])
            ->groupBy('website')
            ->orderBy('cnt', 'desc')
            ->first();
        $most_promoted_shop_month = !empty($most_promoted_shop_month_qr) ? $most_promoted_shop_month_qr->website : '';
        $most_promoted_shop_month_shop_id = !empty($most_promoted_shop_month_qr) ? $most_promoted_shop_month_qr->shop_id : '#';

        /* Start - Calculate RENTABILITY ANALYSER */
        $nb_of_shops_2_times_more = Shop::select(DB::raw('COUNT(*)'))
            ->where('influencer_id', $influencer->influencer_id)
            ->groupBy('website')
            ->havingRaw('COUNT(*) >= 2')
            ->count();

        $multiple_promotions_indicator = 0;
        if($nb_of_shops_2_times_more != 0 && $total_promotion_in_database != 0){
            $multiple_promotions_indicator = ($nb_of_shops_2_times_more / $total_promotion_in_database) * 100;
        }

        $total_shops_qr = Shop::select(DB::raw('COUNT(DISTINCT website) as total_shops'))
            ->where('influencer_id', $influencer->influencer_id)
            ->first();
        $total_shops = $total_shops_qr->total_shops;

        $average_promo_shop = 0;
        if($total_shops != 0 && $total_promotion_in_database != 0){
            $average_promo_shop = round($total_promotion_in_database / $total_shops, 2);
        }
      
        $nb_of_promo_qr = Shop::select(DB::raw('COUNT(website) as nb_of_promo'))
            ->where('influencer_id', $influencer->influencer_id)
            ->where('created_at', '>', Carbon\Carbon::now()->subDays(15))
            ->first();
        $average_promo_day = !empty($nb_of_promo_qr->nb_of_promo) ? round($nb_of_promo_qr->nb_of_promo/15, 2) : 0;
        /* End - Calculate RENTABILITY ANALYSER */


        // For All promotions of this shop
        if ($request->ajax() && (isset($request->with_media) && $request->with_media == 0)) {

            $sort_by = $request->get('sort_by');
            $sort_type = $request->get('sort_type');

            $this_week_start = Carbon\Carbon::parse('last monday')->startOfDay();
            $this_week_end = Carbon\Carbon::parse('next sunday')->endOfDay();

            $query = Shop::select("shop.*","countries.country_code","countries.country_name", "category_name", 
                DB::raw('CONCAT(first_name, " ", last_name) AS name'), 
                /*DB::raw('(Select DISTINCT instagram_id FROM shop s join influencer u on s.influencer_id=u.influencer_id where s.website=shop.website order by s.created_at DESC LIMIT 1) as latest_promotion'),*/
                /*DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website) as nb_of_promotions'),*/
                DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1) as first_added_date'),
                DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1) as last_seen_date'),
                DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id) as nb_of_story'),
                DB::raw('(SELECT COUNT(DISTINCT influencer_id) from shop s2 where s2.website=shop.website AND (s2.created_at BETWEEN "'.$this_week_start.'" AND "'.$this_week_end.'")) as promo_this_week'),
               /* DB::raw('(SELECT COUNT(s.website) from shop s where s.website=shop.website and s.influencer_id='.$influencer->influencer_id.') as total_promo_of_shop_by_this_influencer')*/
                DB::raw('count(DISTINCT shop_category.category_id) as category_cnt'),
                DB::raw('GROUP_CONCAT(DISTINCT category.category_name  SEPARATOR "<br />") as categories')
            )
            ->leftJoin('shop_category','shop_category.shop_id', '=' ,'shop.shop_id')
            ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id')
            ->leftJoin('influencer','shop.influencer_id', '=' ,'influencer.influencer_id')
            ->leftJoin('countries','countries.country_id', '=' ,'influencer.country_id');
             $shops = $query->where('shop.influencer_id', $influencer->influencer_id);

            if(isset($request->search_website) && !empty($request->search_website))
                $shops = $query->where('website', 'like', '%' .$request->search_website.'%');

            if(isset($request->filter_country_ids) && !empty($request->filter_country_ids)){
                $influencers = $query->whereIn('influencer.country_id', $request->filter_country_ids);
            }

            if(isset($request->filter_activity) && !empty($request->filter_activity)){
                $influencers = $query->whereIn('activity', $request->filter_activity);
            }

            if(isset($request->first_added_from) && !empty($request->first_added_from) && isset($request->first_added_to) && !empty($request->first_added_to)){ 
                
                $shops = $query->whereBetween(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), [$request->first_added_from, date('Y-m-d', strtotime($request->first_added_to))]);
            } else if(isset($request->first_added_from) && !empty($request->first_added_from)){ 
                $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), '>=' , date('Y-m-d', strtotime($request->first_added_from)));
            } else if(isset($request->first_added_to) && !empty($request->first_added_to)){
                $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), '<=' , date('Y-m-d', strtotime($request->first_added_to))); 
            } 

            if(isset($request->last_seen_from) && !empty($request->last_seen_from) && isset($request->last_seen_to) && !empty($request->last_seen_to)){ 
                
               $shops = $query->whereBetween(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'), [date('Y-m-d', strtotime($request->last_seen_from)), date('Y-m-d', strtotime($request->last_seen_to))]);
            } else if(isset($request->last_seen_from) && !empty($request->last_seen_from)){ 
                $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'),'>=',date('Y-m-d', strtotime($request->last_seen_from)));
            } else if(isset($request->last_seen_to) && !empty($request->last_seen_to)){
                 $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'),'<=',date('Y-m-d', strtotime($request->last_seen_to)));
            } 

            /*if(isset($request->first_added_last_week) && !empty($request->first_added_last_week)){
                $last_week_from = date("Y-m-d", strtotime("last week monday"));
                $last_week_to = date("Y-m-d", strtotime("last week sunday"));
                $influencers = $query->whereBetween(DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), [$last_week_from, $last_week_to]);
            }

            if(isset($request->last_seen_today) && !empty($request->last_seen_today)){
                $influencers = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'), date("Y-m-d"));
            }*/

            if(isset($request->promotion_date_from) && !empty($request->promotion_date_from) && isset($request->promotion_date_to) && !empty($request->promotion_date_to)){
                $influencers = $query->whereBetween(DB::raw('DATE(shop.created_at)'), [date("Y-m-d", strtotime($request->promotion_date_from)), date("Y-m-d", strtotime($request->promotion_date_to))]);
            } else if(isset($request->promotion_date_from) && !empty($request->promotion_date_from)){
                $influencers = $query->where(DB::raw('DATE(shop.created_at)'),'>=' ,date("Y-m-d", strtotime($request->promotion_date_from)));
            } else if(isset($request->promotion_date_to) && !empty($request->promotion_date_to)){
                $influencers = $query->where(DB::raw('DATE(shop.created_at)'),'<=' ,date("Y-m-d", strtotime($request->promotion_date_to)));
            }

            /*if(isset($request->nb_of_promotions_from) && !empty($request->nb_of_promotions_from) && isset($request->nb_of_promotions_to) && !empty($request->nb_of_promotions_to)){
                $influencers = $query->whereBetween(DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website)'), [$request->nb_of_promotions_from, $request->nb_of_promotions_to]);
            }

            if(isset($request->total_promo_of_shop_by_this_inf_from) && !empty($request->total_promo_of_shop_by_this_inf_from) && isset($request->total_promo_of_shop_by_this_inf_to) && !empty($request->total_promo_of_shop_by_this_inf_to)){
                $influencers = $query->whereBetween(DB::raw('(SELECT COUNT(s.website) from shop s where s.website=shop.website and s.influencer_id='.$influencer->influencer_id.')'), [$request->total_promo_of_shop_by_this_inf_from, $request->total_promo_of_shop_by_this_inf_to]);
            }*/

            if(isset($request->nb_of_story_from) && !empty($request->nb_of_story_from) && isset($request->nb_of_story_to) && !empty($request->nb_of_story_to)){
                $influencers = $query->whereBetween(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id)'), [$request->nb_of_story_from, $request->nb_of_story_to]);
            } else if(isset($request->nb_of_story_from) && !empty($request->nb_of_story_from)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id)'), '>=',$request->nb_of_story_from);
            } else if(isset($request->nb_of_story_to) && !empty($request->nb_of_story_to)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id)'), '<=',$request->nb_of_story_to);
            }

            if(isset($sort_by) && !empty($sort_by) && isset($sort_type) && !empty($sort_type)){
                $influencers = $query->orderBy($sort_by, $sort_type);
            }

            $shops = $query->groupBy('shop.shop_id')->paginate(10)->onEachSide(1);

            $total_rec_cnt = Shop::where('influencer_id', $influencer->influencer_id)->count();

            return response()->json([
                'body' => view('user.influencer.load_shop', ['shops' => $shops, 'total_rec_cnt' => $total_rec_cnt])->render()
            ]);  
            
        } 

        // For All promotions with medias of this shop
        if ($request->ajax() && (isset($request->with_media) && $request->with_media == 1)) {

            $sort_by = $request->get('sort_by');
            $sort_type = $request->get('sort_type');

            $query = Shop::select("shop.*","countries.country_code","countries.country_name", 
                DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id) as nb_of_story'),
                DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1) as first_added_date'),
                DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1) as last_seen_date')
                )
                ->leftJoin('influencer','shop.influencer_id', '=' ,'influencer.influencer_id')
                ->leftJoin('countries','countries.country_id', '=' ,'influencer.country_id');
             $shops = $query->where('shop.influencer_id', $influencer->influencer_id);

            if(isset($request->search_influencer) && !empty($request->search_influencer))
                $shops = $query->where('influencer.instagram_id', 'like', '%' .$request->search_influencer.'%');

            if(isset($request->filter_country_ids) && !empty($request->filter_country_ids)){
                $influencers = $query->whereIn('influencer.country_id', $request->filter_country_ids);
            }

            if(isset($request->promotion_date_from) && !empty($request->promotion_date_from) && isset($request->promotion_date_to) && !empty($request->promotion_date_to)){
                $influencers = $query->whereBetween(DB::raw('DATE(shop.created_at)'), [date("Y-m-d", strtotime($request->promotion_date_from)), date("Y-m-d", strtotime($request->promotion_date_to))]);
            } else if(isset($request->promotion_date_from) && !empty($request->promotion_date_from)){
                $influencers = $query->where(DB::raw('DATE(shop.created_at)'),'>=' ,date("Y-m-d", strtotime($request->promotion_date_from)));
            } else if(isset($request->promotion_date_to) && !empty($request->promotion_date_to)){
                $influencers = $query->where(DB::raw('DATE(shop.created_at)'),'<=' ,date("Y-m-d", strtotime($request->promotion_date_to)));
            }

            if(isset($request->first_added_from) && !empty($request->first_added_from) && isset($request->first_added_to) && !empty($request->first_added_to)){ 
                
                $shops = $query->whereBetween(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), [$request->first_added_from, date('Y-m-d', strtotime($request->first_added_to))]);
            } else if(isset($request->first_added_from) && !empty($request->first_added_from)){ 
                $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), '>=' , date('Y-m-d', strtotime($request->first_added_from)));
            } else if(isset($request->first_added_to) && !empty($request->first_added_to)){
                $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), '<=' , date('Y-m-d', strtotime($request->first_added_to))); 
            } 

            if(isset($request->last_seen_from) && !empty($request->last_seen_from) && isset($request->last_seen_to) && !empty($request->last_seen_to)){ 
                
               $shops = $query->whereBetween(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'), [date('Y-m-d', strtotime($request->last_seen_from)), date('Y-m-d', strtotime($request->last_seen_to))]);
            } else if(isset($request->last_seen_from) && !empty($request->last_seen_from)){ 
                $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'),'>=',date('Y-m-d', strtotime($request->last_seen_from)));
            } else if(isset($request->last_seen_to) && !empty($request->last_seen_to)){
                 $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'),'<=',date('Y-m-d', strtotime($request->last_seen_to)));
            } 

            if(isset($request->nb_of_story_from) && !empty($request->nb_of_story_from) && isset($request->nb_of_story_to) && !empty($request->nb_of_story_to)){
                $influencers = $query->whereBetween(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id)'), [$request->nb_of_story_from, $request->nb_of_story_to]);
            } else if(isset($request->nb_of_story_from) && !empty($request->nb_of_story_from)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id)'),'>=',$request->nb_of_story_from);
            } else if(isset($request->nb_of_story_to) && !empty($request->nb_of_story_to)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id)'),'<=',$request->nb_of_story_to);
            }

            $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id)'), '>', 0);

            if(isset($sort_by) && !empty($sort_by) && isset($sort_type) && !empty($sort_type)){
                $influencers = $query->orderBy($sort_by, $sort_type);
            }

            $shops = $query->paginate(8)->onEachSide(1);

            $total_rec_cnt = Shop::where('influencer_id', $influencer->influencer_id)->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id)'), '>', 0)->count();

            return response()->json([
                'body' => view('user.influencer.load_shop_with_media', ['shops' => $shops, 'total_rec_cnt' => $total_rec_cnt])->render()
            ]);
        } 

        // For unique shop
        if ($request->ajax() && (isset($request->with_unique_shop) &&  $request->with_unique_shop == 1)) {

            $sort_by = $request->get('sort_by');
            $sort_type = $request->get('sort_type');


            $this_week_start = Carbon\Carbon::parse('last monday')->startOfDay();
            $this_week_end = Carbon\Carbon::parse('next sunday')->endOfDay();
            
            $query = Shop::select("shop.*","countries.country_code","countries.country_name", 'category_name',
                DB::raw('(Select CONCAT(instagram_id, "--", profile_pic,"--",is_verified) FROM shop s join influencer u on s.influencer_id=u.influencer_id where s.website=shop.website order by s.created_at DESC LIMIT 1) as influencer_data'), 
                DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website) as nb_of_promotions'),
                DB::raw('(SELECT COUNT(*) from shop s where s.website=shop.website and influencer_id='.$influencer->influencer_id.') as total_promotions_by_this_inf'),
                DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1) as first_added_date'),
                DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1) as last_seen_date'),
                DB::raw('(SELECT COUNT(s.website) from shop s where s.website=shop.website and s.influencer_id='.$influencer->influencer_id.' and (DATE(s.created_at) between "'.$this_week_start.'" and "'.$this_week_end.'")) as promo_this_week_by_this_inf'),
                DB::raw('(SELECT COUNT(DISTINCT influencer_id) from shop s2 where s2.website=shop.website AND (s2.created_at BETWEEN "'.$this_week_start.'" AND "'.$this_week_end.'")) as promo_this_week'),
                DB::raw('count(shop_category.category_id) as category_cnt')
            )
            ->leftJoin('shop_category','shop_category.shop_id', '=' ,'shop.shop_id')
            ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id')
            ->leftJoin('influencer','shop.influencer_id', '=' ,'influencer.influencer_id')
            ->leftJoin('countries','countries.country_id', '=' ,'influencer.country_id');
             $shops = $query->where('shop.influencer_id', $influencer->influencer_id);

            if(isset($request->search_website) && !empty($request->search_website))
                $shops = $query->where('website', 'like', '%' .$request->search_website.'%');

            if(isset($request->filter_country_ids) && !empty($request->filter_country_ids)){
                $influencers = $query->whereIn('influencer.country_id', $request->filter_country_ids);
            }

           /* if(isset($request->filter_activity) && !empty($request->filter_activity)){
                $influencers = $query->whereIn('activity', $request->filter_activity)->whereBetween(DB::raw('DATE(shop.created_at)'), [$this_week_start, $this_week_end]);
            }*/

            /*if(isset($request->first_added_last_week) && !empty($request->first_added_last_week)){
                $last_week_from = date("Y-m-d", strtotime("last week monday"));
                $last_week_to = date("Y-m-d", strtotime("last week sunday"));
                $influencers = $query->whereBetween(DB::raw('(SELECT s.created_at from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), [$last_week_from, $last_week_to]);
            }

            if(isset($request->last_seen_today) && !empty($request->last_seen_today)){
                $influencers = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'), date("Y-m-d"));
            }*/

             if(isset($request->first_added_from) && !empty($request->first_added_from) && isset($request->first_added_to) && !empty($request->first_added_to)){ 
                
                $shops = $query->whereBetween(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), [$request->first_added_from, date('Y-m-d', strtotime($request->first_added_to))]);
            } else if(isset($request->first_added_from) && !empty($request->first_added_from)){ 
                $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), '>=' , date('Y-m-d', strtotime($request->first_added_from)));
            } else if(isset($request->first_added_to) && !empty($request->first_added_to)){
                $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at ASC LIMIT 1)'), '<=' , date('Y-m-d', strtotime($request->first_added_to))); 
            } 

            if(isset($request->last_seen_from) && !empty($request->last_seen_from) && isset($request->last_seen_to) && !empty($request->last_seen_to)){ 
                
               $shops = $query->whereBetween(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'), [date('Y-m-d', strtotime($request->last_seen_from)), date('Y-m-d', strtotime($request->last_seen_to))]);
            } else if(isset($request->last_seen_from) && !empty($request->last_seen_from)){ 
                $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'),'>=',date('Y-m-d', strtotime($request->last_seen_from)));
            } else if(isset($request->last_seen_to) && !empty($request->last_seen_to)){
                 $shops = $query->where(DB::raw('(SELECT DATE(s.created_at) from shop s where s.website=shop.website order by s.created_at DESC LIMIT 1)'),'<=',date('Y-m-d', strtotime($request->last_seen_to)));
            } 

            if(isset($request->nb_of_promotions_from) && !empty($request->nb_of_promotions_from) && isset($request->nb_of_promotions_to) && !empty($request->nb_of_promotions_to)){
                $influencers = $query->whereBetween(DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website)'), [$request->nb_of_promotions_from, $request->nb_of_promotions_to]);
            } else if(isset($request->nb_of_promotions_from) && !empty($request->nb_of_promotions_from)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website)'), '>=', $request->nb_of_promotions_from);
            } else if(isset($request->nb_of_promotions_to) && !empty($request->nb_of_promotions_to)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(DISTINCT s.influencer_id) from shop s where s.website=shop.website)'), '<=', $request->nb_of_promotions_to);
            }

            if(isset($request->nb_of_promo_by_inf_from) && !empty($request->nb_of_promo_by_inf_from) && isset($request->nb_of_promo_by_inf_to) && !empty($request->nb_of_promo_by_inf_to)){
                $influencers = $query->whereBetween(DB::raw('(SELECT COUNT(*) from shop s where s.website=shop.website and influencer_id='.$influencer->influencer_id.')'), [$request->nb_of_promo_by_inf_from, $request->nb_of_promo_by_inf_to]);
            } else if(isset($request->nb_of_promo_by_inf_from) && !empty($request->nb_of_promo_by_inf_from)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop s where s.website=shop.website and influencer_id='.$influencer->influencer_id.')'), '>=', $request->nb_of_promo_by_inf_from);
            } else if(isset($request->nb_of_promo_by_inf_to) && !empty($request->nb_of_promo_by_inf_to)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop s where s.website=shop.website and influencer_id='.$influencer->influencer_id.')'), '<=', $request->nb_of_promo_by_inf_to);
            }

            if(isset($request->promo_this_week_by_inf_from) && !empty($request->promo_this_week_by_inf_from) && isset($request->promo_this_week_by_inf_to) && !empty($request->promo_this_week_by_inf_to)){
                $influencers = $query->whereBetween(DB::raw('(SELECT COUNT(s.website) from shop s where s.website=shop.website and s.influencer_id='.$influencer->influencer_id.' and (DATE(s.created_at) between "'.$this_week_start.'" and "'.$this_week_end.'"))'), [$request->promo_this_week_by_inf_from, $request->promo_this_week_by_inf_to]);
            } else if(isset($request->promo_this_week_by_inf_from) && !empty($request->promo_this_week_by_inf_from)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(s.website) from shop s where s.website=shop.website and s.influencer_id='.$influencer->influencer_id.' and (DATE(s.created_at) between "'.$this_week_start.'" and "'.$this_week_end.'"))'), '>=',$request->promo_this_week_by_inf_from);
            } else if(isset($request->promo_this_week_by_inf_to) && !empty($request->promo_this_week_by_inf_to)){
                $influencers = $query->where(DB::raw('(SELECT COUNT(s.website) from shop s where s.website=shop.website and s.influencer_id='.$influencer->influencer_id.' and (DATE(s.created_at) between "'.$this_week_start.'" and "'.$this_week_end.'"))'), '<=',$request->promo_this_week_by_inf_to);
            }
 
            if(isset($sort_by) && !empty($sort_by) && isset($sort_type) && !empty($sort_type)){
                $influencers = $query->orderBy($sort_by, $sort_type);
            }

            $shops = $query->groupBy('shop.website')->paginate(10);

            $total_rec_cnt = Shop::where('influencer_id', $influencer->influencer_id)->distinct('shop.website')->count();

            return response()->json([
                'body' => view('user.influencer.load_unique_shop', ['shops' => $shops, 'total_rec_cnt' => $total_rec_cnt])->render()
            ]);  
            
        } 

        return view('user.influencer.details', compact('influencer', 'countries','top_promo_of_week', 'frequency_of_promotions_week', 'frequency_of_promotions_mon', 'total_promotion_in_database','date_of_latest_promotion','latest_promoted_shop','latest_promoted_shop_shop_id','most_promoted_shop_ever','most_promoted_shop_ever_shop_id','most_promoted_shop_week','most_promoted_shop_week_shop_id','most_promoted_shop_month','most_promoted_shop_month_shop_id', 'is_favourite', 'multiple_promotions_indicator','average_promo_shop','average_promo_day','shop_list', 'influencer_list','top_promo_of_week_cnt'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($influencer_id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $influencer_id)
    { 
    }
    
    public function destroy($id)
    { 
    }

    public function influencer_favourite(Request $request){
        $inf_cnt = DB::table('influencer_favourite')
        ->where(['influencer_id' => $request->influencer_id, 'user_id' => Auth::guard('User')->user()->user_id])
        ->count();

        if($inf_cnt == 0){
            DB::table('influencer_favourite')->insert(
            [
                'influencer_id' => $request->influencer_id, 
                'user_id' => Auth::guard('User')->user()->user_id
            ]);
        } else {
            DB::table('influencer_favourite')->where(['influencer_id' => $request->influencer_id, 'user_id' => Auth::guard('User')->user()->user_id])->delete();
        }
    }
}

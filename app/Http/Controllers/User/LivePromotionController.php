<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Influencer;
use App\Models\Shop;
use App\Models\Category;
use Hash;
use DB;
use Session;
use Auth;
use Carbon;

class LivePromotionController extends Controller
{
    
    public function index(Request $request)
    { 
        $user_id = Auth::guard('User')->user()->user_id;
        $countries = DB::table('countries')->get();
        $media_list = DB::table('media_list')->where('user_id', $user_id)->get();
        $categories = Category::all();

        $sort_by = $request->get('sort_by');
        $sort_type = $request->get('sort_type');

        $this_week_start = Carbon\Carbon::parse('last monday')->startOfDay();
        $this_week_end = Carbon\Carbon::parse('next sunday')->endOfDay();

        $query = Shop::select("shop.*","countries.country_code","countries.country_name", "influencer.*", "shop.created_at","category_name", 
            DB::raw('count(DISTINCT shop_category.category_id) as category_cnt'),   
            DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id and shop.influencer_id=influencer.influencer_id) as nb_of_story'),
            DB::raw('GROUP_CONCAT(DISTINCT category.category_name  SEPARATOR "<br />") as categories') 
            )
            ->leftJoin('shop_category','shop_category.shop_id', '=' ,'shop.shop_id')
            ->leftJoin('category','category.category_id', '=' ,'shop_category.category_id')
            ->leftJoin('influencer','influencer.influencer_id', '=' ,'shop.influencer_id');
        $shops = $query->leftJoin('countries','countries.country_id', '=' ,'influencer.country_id');

        if(isset($request->search_instagram_id) && !empty($request->search_instagram_id))
                $influencers = $query->where('instagram_id', 'like', '%' .$request->search_instagram_id.'%');

        if(isset($request->filter_country_ids) && !empty($request->filter_country_ids)){
            $influencers = $query->whereIn('influencer.country_id', $request->filter_country_ids);
        }

        if(isset($request->eng_rate_from) && !empty($request->eng_rate_from) && isset($request->eng_rate_to) && !empty($request->eng_rate_to)){
            $influencers = $query->whereBetween('engagement_rate', [$request->eng_rate_from, $request->eng_rate_to]);
        } else if(isset($request->eng_rate_from) && !empty($request->eng_rate_from)){
            $influencers = $query->where('engagement_rate', '>=', $request->eng_rate_from);
        } else if(isset($request->eng_rate_to) && !empty($request->eng_rate_to)){
            $influencers = $query->where('engagement_rate', '<=', $request->eng_rate_to);
        }

        if(isset($request->followers_from) && !empty($request->followers_from) && isset($request->followers_to) && !empty($request->followers_to)){
            $influencers = $query->whereBetween('followers', [$request->followers_from, $request->followers_to]);
        } else if(isset($request->followers_from) && !empty($request->followers_from)){
            $influencers = $query->where('followers', '>=',$request->followers_from);
        } else if(isset($request->followers_to) && !empty($request->followers_to)){
            $influencers = $query->where('followers', '<=',$request->followers_to);
        }

        if(isset($request->nb_of_story_from) && !empty($request->nb_of_story_from) && isset($request->nb_of_story_to) && !empty($request->nb_of_story_to)){
            $influencers = $query->whereBetween(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id and shop.influencer_id=influencer.influencer_id)'), [$request->nb_of_story_from, $request->nb_of_story_to]);
        } else if(isset($request->nb_of_story_from) && !empty($request->nb_of_story_from)){
            $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id and shop.influencer_id=influencer.influencer_id)'),'>=', $request->nb_of_story_from);
        } else if(isset($request->nb_of_story_to) && !empty($request->nb_of_story_to)){
            $influencers = $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id and shop.influencer_id=influencer.influencer_id)'),'<=', $request->nb_of_story_to);
        }

        if(isset($request->filter_category_ids) && !empty($request->filter_category_ids)){
            $shops = $query->whereIn('shop_category.category_id', $request->filter_category_ids);
        }

        $query->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id)'), '>', 0);
       
        $shops = $query->where('shop.created_at', '>=', Carbon\Carbon::now()->subDay())->groupBy('shop.shop_id')->latest('shop.created_at')->paginate(8)->onEachSide(2);

        //$shops = $query->groupBy('shop.shop_id')->latest('shop.created_at')->paginate(8)->onEachSide(1);

        $total_rec_cnt = Shop::where('created_at', '>=', Carbon\Carbon::now()->subDay())->where(DB::raw('(SELECT COUNT(*) from shop_media sm where sm.shop_id=shop.shop_id)'), '>', 0)->count();

        if ($request->ajax()) {
            return response()->json([
                'body' => view('user.live_promotion.load_live_now', ['shops' => $shops, 'total_rec_cnt' => $total_rec_cnt])->render()
            ]);  
        } 
       
        return view('user.live_promotion.live_now', compact('shops','countries','media_list','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    { 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function show(Request $request, Influencer $influencer)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($influencer_id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $influencer_id)
    {

    }
    
    public function destroy($id)
    { 

    }
}

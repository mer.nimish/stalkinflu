<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Models\Shop;
use App\Models\Influencer;
use Config;

use Instagram\Api;
use Instagram\Exception\InstagramException;

use Psr\Cache\CacheException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

use Image;
use FFMpeg;
use File;

class InstagramAPITestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

        	$cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');

		    $api = new Api($cachePool);
		    $api->login(Config::get('constants.instagram.email'), Config::get('constants.instagram.password'));

		    $influencerList = Influencer::orderBy('created_at', 'desc')->get();

		    foreach ($influencerList as $influencer) {

		    	$profile = $api->getProfile($influencer->instagram_id);
		    	
		    	sleep(1);
			  // echo "<pre>";
			   //print_r($profile);exit;

			   	/* Start - Engagement  Rate Calculation */
			    $followers = empty($profile->getFollowers()) ? 1 : $profile->getFollowers();
			    $postEngRate  = 0;
				$cnt = 0;
				$engRate = 0;
				$totalLikes = 0;
				$totalComments = 0;
				$totalVideos = 0;
				$totalImages = 0;
				$minLikes = 0;
				$maxLikes = 0;
				$last3MonthsTotalPosts = 0;
				$last_post_date = !empty($profile->getMedias()) ? $profile->getMedias()[0]->getDate()->format('Y-m-d H:i:s') : NULL;

			    if(!empty($profile->getMedias())){

					//do {
						//if($cnt > 0)
							 //$profile = $api->getMoreMedias($profile);

						foreach ($profile->getMedias() as $key => $post) {
							$last3MonthsTotalPosts += (strtotime($post->getDate()->format('Y-m-d')) > strtotime('-3 months')) ? 1 : 0;

					    	$minLikes = ($minLikes > $post->getLikes()) ? $post->getLikes() : $minLikes;
					    	$maxLikes = ($maxLikes > $post->getLikes()) ? $maxLikes : $post->getLikes() ;

					    	$totalLikes += $post->getLikes();
					    	$totalComments += $post->getComments();
					    	$totalVideos += $post->getTypeName() == 'GraphVideo' ? 1 : 0;
					    	//$totalImages += $post->getTypeName() == 'GraphImage' ? 1 : 0;

					    	$postEngRate  += (($post->getLikes() + $post->getComments()) / $followers) * 100;
					    	$cnt++;
						}
					   
					    // avoid 429 Rate limit from Instagram
					    //sleep(1);
					//} while ($profile->hasMoreMedias());
					
					$engRate =  $postEngRate  / $cnt;
				}
				
				$uploads = $profile->getMediaCount();
				$avg_likes= round($totalLikes / $cnt);
				$avg_comments= round($totalComments / $cnt);
				$videos = round(($totalVideos / $cnt) * 100);
				$photos = 100 - $videos;
				$likes_consistency = $minLikes;
				$comments_consistency = $maxLikes;
				$frequency = !empty($last3MonthsTotalPosts) ? round(($last3MonthsTotalPosts / 12)) : 0;
				
			    /* End - Engagement  Rate Calculation */

			   /* echo '============================' . "\n";
			    echo 'User Information : ' . "\n";
			    echo '============================' . "\n";
			    echo 'ID               : ' . $profile->getId() . "\n";
			    echo 'Full Name        : ' . $profile->getFullName() . "\n";
			    echo 'UserName         : ' . $profile->getUserName() . "\n";
			    echo 'Following        : ' . $profile->getFollowing() . "\n";
			    echo 'Followers        : ' . $profile->getFollowers() . "\n";
			    echo 'Biography        : ' . $profile->getBiography() . "\n";
			    echo 'External Url     : ' . $profile->getExternalUrl() . "\n";
			    echo 'Profile Picture  : ' . $profile->getProfilePicture() . "\n";
			    echo 'Verified Account : ' . ($profile->isVerified() ? 'Yes' : 'No') . "\n";
			    echo 'Private Account  : ' . ($profile->isPrivate() ? 'Yes' : 'No') . "\n";
			    echo 'Medias Count     : ' . $profile->getMediaCount() . "\n";
			    echo '============================' . "\n";*/

			    // For save profile pic
			    $img = public_path('/profile_pic/').$influencer->instagram_id.'.jpg';
				file_put_contents($img, file_get_contents($profile->getProfilePicture()));

				$img = Image::make($img);
                $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path('/profile_pic/thumb/').$influencer->instagram_id.'.jpg');

			    Influencer::where('instagram_id', $influencer->instagram_id)->update([
					'profile_pic' => $influencer->instagram_id.'.jpg',
					'followers' => $profile->getFollowers(),
					'engagement_rate' => $engRate,
					'uploads' => $uploads,
					'avg_likes' => $avg_likes,
					'avg_comments' => $avg_comments,
					'videos' => $videos,
					'photos' => $photos,
					'last_post_date' => $last_post_date,
					'likes_consistency' => $likes_consistency,
					'comments_consistency' => $comments_consistency,
					'frequency' => $frequency,
					'is_verified' => ($profile->isVerified() ? 1 : 0)
				]);
			}

		} catch (InstagramException $e) {
		    print_r($e->getMessage());
		} catch (CacheException $e) {
		    print_r($e->getMessage());
		}
    }
    public function profile_stories(Request $request)
    {
		$allStoryData = array();
		try { 

			$cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');

		    $api = new Api($cachePool);
			$api->login(Config::get('constants.instagram.email'), Config::get('constants.instagram.password'));

			$influencerList = Influencer::get();
		    $i = 1;

		    $shop_media_story = array();
		    $shop_videos = DB::table('shop_media')->get();
		    foreach ($shop_videos as $key => $shop_video) {
		    	$shop_media_story[] = $shop_video->story_id;
		    }

		    foreach ($influencerList as $influencer) {
		    
		        $profile = $api->getProfile($influencer->instagram_id);
		        
		        sleep(1);
		        $feedStories = $api->getStories($profile->getId());

		        //echo "<pre>";
		        //print_r($feedStories);exit;
		       
		        if (count($feedStories->getStories())) {
		            foreach ($feedStories->getStories() as $key => $story) { 

		                    if(!empty($story->getCtaUrl())){ 

		        				$story_arr[] = $story;

		                        $allStoryData[$influencer->instagram_id][$key]['userName'] = $profile->getUserName();
		                        $allStoryData[$influencer->instagram_id][$key]['userID'] = $profile->getId();
		                        //$allStoryData[$influencer][$key]['fullName'] = $profile->getFullName();
		                        $allStoryData[$influencer->instagram_id][$key]['ctaUrl'] = $story->getCtaUrl();
		                        $allStoryData[$influencer->instagram_id][$key]['displayUrl'] = $story->getDisplayUrl();
		                        //$allStoryData[$influencer][$key]['takenAtDate'] = $story->getTakenAtDate()->format('Y-m-d h:i:s');



		                        /* For insert data in shop table */
		                        $cta_url = $story->getCtaUrl();

		                        if (strpos($cta_url, 'instagram.com') === false && strpos($cta_url, 'youtube.com') === false) {
    
			                        $story_id = $story->getId();
			                       
			                        if(!in_array($story_id, $shop_media_story)){

				                        /*$shop = Shop::where(['website' => $cta_url, 'influencer_id' => $influencer->influencer_id])
				                        ->where(DB::raw('DATE(created_at)'), date('Y-m-d'))->first();*/
				                         
								        $cta_url_domain = preg_replace("/^www\./", "", strtolower(parse_url($cta_url, PHP_URL_HOST)));

				                        $shop = Shop::where('influencer_id', $influencer->influencer_id)
				                        ->where(DB::raw('DATE(created_at)'), date('Y-m-d'))
				                        ->where(DB::raw("(REPLACE(LOWER(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(website, 
										    '?', 1), # split on url params to remove weirdest stuff first 
										    '://', -1), # remove protocal http:// https:// ftp:// ...
										    '/', 1), # split on path 
										    ':', 2), # split on user:pass
										    '@', 1), # split on user:port@
										    ':', 1), # split on port
										    'www.', -1), # remove www.
										    '.', 4), # keep TLD + domain name
										    '/', 1) ), 'www.',''))"), $cta_url_domain)
				                        ->first();

				                        if(empty($shop->shop_id)){

				                        	$website_screenshot = $this->store_site_screenshot($cta_url);

					                        $shop = new Shop();	
						                    $shop->website = $cta_url;
						                    $shop->website_screenshot = $website_screenshot;
						                    $shop->influencer_id = $influencer->influencer_id;
						                    $shop->taken_at = $story->getTakenAtDate()->format('Y-m-d h:i:s');
						                    $shop->created_at = date('Y-m-d h:i:s');
							        		$shop->save();
							        	}

							        	if(!empty($story->getVideoResources())){

							        		$video_url = $story->getVideoResources()[0]->src;
							        		if (!is_dir(storage_path('app/public/shop_media/'.$shop->shop_id.'/'))) {
											  mkdir(storage_path('app/public/shop_media/'.$shop->shop_id.'/'), 0777, true);
											}
							        		$video = storage_path('app/public/shop_media/'.$shop->shop_id.'/').$story_id.'.mp4';
											file_put_contents($video, file_get_contents($video_url));

						                    $last_shop = DB::table('shop_media')->insert([
						                    	'shop_id' => $shop->shop_id,
						                    	'story_id' => $story_id,
						                    	'video' => $story_id.'.mp4',
						                    ]);
							        	}
							        }
						        }
		                    }
		            }

		            /* Update story count in influencer table */

			        Influencer::where('instagram_id', $influencer->instagram_id)->update([
						'story_cnt' => count($feedStories->getStories()),
					]);

		        } else {
		            //$allStoryData[] => 'No stories';
		        }
		        
		    }
			
		    //echo "<pre>";print_r($allStoryData);
		    
		    //exit;

		} catch (InstagramException $e) {
		    print_r($e->getMessage());
		} catch (CacheException $e) {
		    print_r($e->getMessage());
		}
    }

    public function set_influencer_rank(){

    	$result = DB::statement('SET @r=0');
    	$result = DB::statement('UPDATE influencer SET rank=@r:= (@r+1) where 1 ORDER BY story_cnt DESC');
    }

    public function shop_video_concat(){

    	$shop_videos = DB::table('shop_media')
    	->where(DB::raw('DATE(created_at)'), date('Y-m-d'))
    	->get();

    	$video_urls = array();
    	foreach ($shop_videos as $key => $video) {
    		if(File::exists(base_path('storage/app/public/shop_media/'.$video->shop_id.'/'.$video->video))){
	    		$video_urls[$video->shop_id][] = 'public/shop_media/'.$video->shop_id.'/'.$video->video;
	    	}
    	}
    	//echo "<pre>";
    	//print_r($video_urls);exit;
    	
    	foreach ($video_urls as $key => $video_url) {
    		if(!empty($video_url) && count($video_url) >= 2){
	    		if(!File::exists(base_path('storage/app/public/shop_media/main/'.$key.'.mp4'))){
		    		FFMpeg::open($video_url)
			        ->export()
			        ->concatWithoutTranscoding()
			        ->save(('/public/shop_media/main/'.$key.'.mp4'));
			     }
		    } else if(!empty($video_url) && count($video_url) == 1){
		    	
	    		$success = copy(
	    			base_path('storage/app/'.$video_url[0]), 
	    			base_path('storage/app/public/shop_media/main/'.$key.'.mp4')
	    		);
		    }
		}
    	
    	

    	//echo asset('storage/shop_media/.mp4');

    }

    public function store_site_screenshot($url){

    	$query_string = http_build_query(array(
	        'url' => $url,
	        'userkey' => Config::get('constants.site-shot.access-key'),
	        'width' => 1280,
	        'full_size' => 1,
	        'delay_time' => 5000,
	        'timeout' => 90000,
	        /*'response_type' => 'json',*/
	    ));
	    $image = file_get_contents("https://api.site-shot.com/?$query_string");
	   
	    $img_name = md5($url).'.png';
    	$file_name = public_path('/site_screenshot/') . $img_name;

	    file_put_contents($file_name, $image);

	    return $img_name;
    }

    public function sendinblue_contact_status_update(){

    	$curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/subscriptions?status=ended&limit=100",
            CURLOPT_POST => 0,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
            ],
        ]); 
        $resp = curl_exec($curl);
        $resp = json_decode($resp);
        //echo "<pre>";print_r($resp);exit;
        curl_close($curl);

        foreach ($resp->data as $key => $res) {
        	$data['sub_id'][] = $res->id;
        }

        $sendinblue_ids = DB::table('subscription')->select('sendinblue_id')
        	->join('user','user.user_id', '=' ,'subscription.user_id')
        	->whereIn('stripe_subscription_id', $data['sub_id'])
        	->get();

        $sendinblue_id_arr = array();
        if(!$sendinblue_ids->isEmpty()){
	        foreach ($sendinblue_ids as $key => $sendinblue_id) {
	        	if(!empty($sendinblue_id->sendinblue_id))
	        		$sendinblue_id_arr[] =  $sendinblue_id->sendinblue_id;
	        }
	    }

        if(!empty($sendinblue_id_arr))
        {
	        $curl = curl_init();
			curl_setopt_array($curl, [
			  CURLOPT_URL => "https://api.sendinblue.com/v3/contacts/lists/7/contacts/add",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "{\"ids\":[".implode(",", $sendinblue_id_arr)."]}",
			  CURLOPT_HTTPHEADER => [
			    "accept: application/json",
			    "api-key: " . Config::get('constants.sendinblue.api-key'),
			    "content-type: application/json"
			  ],
			]);
			$response = curl_exec($curl);
			//echo "<pre>";print_r($response);exit;
			$err = curl_error($curl);
			curl_close($curl);

			$curl = curl_init();
			curl_setopt_array($curl, [
			  CURLOPT_URL => "https://api.sendinblue.com/v3/contacts/lists/6/contacts/remove",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "{\"ids\":[".implode(",", $sendinblue_id_arr)."]}",
			  CURLOPT_HTTPHEADER => [
			    "accept: application/json",
			    "api-key: ". Config::get('constants.sendinblue.api-key'),
			    "content-type: application/json"
			  ],
			]);

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
		}
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Auth;
use DB;
use App\Models\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         view()->composer(
            'layout.main',
            function ($view) {
                $view->with('bug_or_an_idea_link', DB::table('setting')->first()->bug_or_an_idea_link);
                /*$view->with('notification_unrad_cnt', Notification::whereRaw('FIND_IN_SET('.Auth::guard('User')->user()->user_id.', view_user_ids) IS NULL')->count());*/
                if (Auth::guard('User')->check())
                {
                    $view->with('is_unread_notification', User::select('is_unread_notification')->where('user_id', Auth::guard('User')->user()->user_id)->first()->is_unread_notification);
                }
            }
        );
    }
}
